sudo chown -R www-data:www-data /var/www
sudo usermod -a -G www-data ubuntu
sudo find /var/www -type f -exec chmod 644 {} \;    
sudo find /var/www -type d -exec chmod 755 {} \;
sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache
