<?php

namespace Tests\Integration;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class PasswordRecoveryTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_a_register_form()
    {
        $response = $this->get('/password/recover');

        $response->assertSuccessful();
        $response->assertViewIs('auth.passwords.recover');
    }

    public function test_user_cannot_recover_password_with_incorrect_passphrase()
    {
        $user = $this->createUser();

        $response = $this->from('/password/recover')->post('/password/recover', [
            'username' => $user->username,
            'passphrase' => 'test',
        ]);

        $response->assertRedirect('/password/recover');
        $response->assertSessionHasErrors('passphrase');
        $this->assertFalse(session()->hasOldInput('username'));
        $this->assertFalse(session()->hasOldInput('passphrase'));
        $this->assertGuest();
    }

    public function test_user_can_recover_password_with_passphrase()
    {
        $user = $this->createUser([
            'passphrase' => Hash::make('passphrase'),
        ]);

        $response = $this->from('/password/recover')->post('/password/recover', [
            'username' => $user->username,
            'passphrase' => 'passphrase',
        ]);

        $response->assertRedirect();
        $this->assertGuest();
    }
}
