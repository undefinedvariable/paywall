<?php

namespace Tests\Integration;

use App\Enums\AccountType;
use App\Enums\Priority;
use App\Models\Ticket;
use App\Models\TicketCategory;
use App\Models\User;
use App\Models\States\Ticket\Open;
use App\Models\States\Ticket\Closed;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class TicketTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function view_all_tickets()
    {
        $user = $this->login();

        $response = $this->from('/home')->get('/tickets');
        $response->assertSuccessful();
        $response->assertSessionHasNoErrors();
        $response->assertViewIs('tickets.index');
    }

    /** @test */
    public function create_ticket()
    {
        $user = $this->login();

        $response = $this->from('/tickets')->get('/tickets/create');
        $response->assertSuccessful();
        $response->assertSessionHasNoErrors();
        $response->assertViewIs('tickets.create');
    }

    /** @test */
    public function store_a_ticket()
    {
        $user = $this->login();

        $ticket_category = TicketCategory::factory()->create();

        $title = Str::random(10);
        $message = Str::random(20);

        $response = $this->from('/tickets/create')->post('/tickets', [
            'title' => $title,
            'ticket_category_id' => $ticket_category->id,
            'message' => $message,
        ]);
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('tickets', [
            'title' => $title,
            'ticket_category_id' => $ticket_category->id,
        ]);
    }

    /** @test */
    public function view_a_stored_ticket()
    {
        $user = $this->login();
        $ticket = Ticket::factory()->create([
            'user_id' => $user->id
        ]);

        $response = $this->from('/tickets/create')->get('/tickets/'.$ticket->id);
        $response->assertSuccessful();
        $response->assertSessionHasNoErrors();
        $response->assertViewIs('tickets.show');
    }

    /** @test */
    public function add_user_comment_to_a_ticket()
    {
        $user = $this->login();
        $ticket = Ticket::factory()->create([
            'user_id' => $user->id
        ]);

        $response = $this->from('/tickets')->put(route('tickets.update', $ticket), [
            'ticket_id' => $ticket->id,
            'comment' => Str::random(20),
        ]);
        $response->assertRedirect('/tickets');
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('ticket_comments', [
            'user_id' => $user->id,
            'ticket_id' => $ticket->id,
        ]);
    }

    /** @test */
    public function add_staff_comment_to_a_ticket()
    {
        $user = $this->loginAsStaff();
        $ticket = Ticket::factory()->create();

        $response = $this->from('/admin/tickets/show/'.$ticket->id)->put(route('tickets.update', $ticket), [
            'ticket_id' => $ticket->id,
            'comment' => Str::random(20),
        ]);
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('ticket_comments', [
            'user_id' => $user->id,
            'ticket_id' => $ticket->id,
        ]);
    }

    /** @test */
    public function change_ticket_priority()
    {
        $user = User::factory()->create();
        $staff = $this->loginAsStaff();

        $ticket = Ticket::factory()->create([
            'user_id' => $user->id,
            'priority' => Priority::HIGH,
        ]);

        $response = $this->from('/admin/tickets/show/'.$ticket->id)->get('/admin/tickets/change/priority/'.$ticket->id.'/low');
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('tickets', [
            'id' => $ticket->id,
            'user_id' => $user->id,
            'priority' => Priority::LOW,
        ]);
    }

    /** @test */
    public function close_ticket()
    {
        $staff = $this->loginAsStaff();

        $ticket = Ticket::factory()->create([
            'priority' => Priority::HIGH,
            'status' => Open::class,
        ]);

        $response = $this->from('/admin/tickets/show/'.$ticket->id)->get('/admin/tickets/change/status/'.$ticket->id.'/closed');
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('tickets', [
            'id' => $ticket->id,
            'status' => 'closed',
        ]);
    }
}
