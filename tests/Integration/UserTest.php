<?php

namespace Tests\Integration;

use App\Enums\Language;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_find_by_username()
    {
        $this->createUser(['username' => 'johndoe']);

        $this->assertInstanceOf(User::class, User::findByUsername('johndoe'));
    }

    /** @test */
    public function user_can_set_language()
    {
        $user = $this->login([
            'language' => Language::ES,
        ]);

        $response = $this->from('/users/'.$user->id)->post('/setlanguage', [
            'language' => 40,
        ]);
        $response->assertRedirect('/users/'.$user->id);
        $this->assertTrue($user->language->value == Language::EN);
    }

    /** @test */
    public function user_can_change_personal_wallet_validation_error()
    {
        $user = $this->login();

        $response = $this->from('/users/'.$user->id)->post('/wallet', [
            'refund_address' => Str::random(10),
        ]);

        $response->assertSessionHasErrors(['refund_address' => 'The refund address must be at least 90 characters.']);
        $response->assertRedirect('/users/'.$user->id);
    }

    /** @test */
    public function user_can_change_personal_wallet()
    {
        $user = $this->login();

        $refund_address = Str::random(95);
        $response = $this->from('/users/'.$user->id)->post('/wallet', [
            'refund_address' => $refund_address,
        ]);

        $this->assertTrue($user->refund_address == $refund_address);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect('/users/'.$user->id);
    }

    /** @test */
    public function user_can_add_pgp_key()
    {
        $user = $this->login();
        $this->assertTrue($user->pgp == null);

        $pgp =
'
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: BCPG C# v1.6.1.0

mQENBF/s6LYBCACV4yQ805dV4JhpGad2ipUCKlV63h9CURNso/2am1YZEFjlU/26
c+ZdIhxnI2WYpZyPsvDUdlSvtMcyky3819HwpUZXTclUfm6fuhqGt5pVeefGtErK
RHHGfQL7j7FEgkSguucnbQM+I70nAboz2Z3PtqL0AtmBXEdvj3K4/+mwyhsuF+mK
Uco4SKPhqpJizkP6RBzSoRy2vP1dl6jYYV/PkgLKbAXO4FJddpTOfIcNg5+FQ3HG
CrGSAUKcyQo8GPy7T7hpqHYT1+aXlcQ+PR2MoN7W/ki2l6yH0WyY/ZCCZYsiDTI3
D4QRqrfYdHn5yARpsTdglebwa+QQhHHKbZPtABEBAAG0DnRlc3RAdGVzdC50ZXN0
iQEcBBABAgAGBQJf7Oi2AAoJEL2S4srBVYe7844H/11vAiTVM5PMuYdcLnAAibhH
3dwZKpl3OM6Z7+H5Mj+BKQ9ZB8akcTI15xpDpkyQrhrHw0bkLmeLgMKtRW9rOOeO
I5JSYhrprGoYTnyFyBMo3FLRBdtgjcgIb//EmGcopnggXi9h9MfLXektRrjTOaMn
yQ85+4V71ZGAs32QC4BNELqxr1d8iX6w+QYOgHMwI1dP+Vc/u/gdPgwDGZOaetsT
d9bu38bDvjeLJUc68+JBRwCQxzIUdprMvVnu6kU1KLtqF8DY+QyhUBHgpU1ZRU78
8O0BipXx1jKqoidve+rRGGXhdhxRZq2y3jh8ecjd5Jiby3LWn+siMSWVUui/dUQ=
=rXRW
-----END PGP PUBLIC KEY BLOCK-----
';
        $response = $this->from('/users/'.$user->id)->post('/addpgp', [
            'pgp' => $pgp,
        ]);

        $this->assertTrue($user->pgp != null);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect('/users/'.$user->id);
    }

    /** @test */
    public function user_can_enable_pgp_login()
    {
        $user = $this->login();
        $this->assertTrue($user->pgp == null);
        $this->assertFalse($user->pgp_auth);

        $pgp =
'
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: BCPG C# v1.6.1.0

mQENBF/s6LYBCACV4yQ805dV4JhpGad2ipUCKlV63h9CURNso/2am1YZEFjlU/26
c+ZdIhxnI2WYpZyPsvDUdlSvtMcyky3819HwpUZXTclUfm6fuhqGt5pVeefGtErK
RHHGfQL7j7FEgkSguucnbQM+I70nAboz2Z3PtqL0AtmBXEdvj3K4/+mwyhsuF+mK
Uco4SKPhqpJizkP6RBzSoRy2vP1dl6jYYV/PkgLKbAXO4FJddpTOfIcNg5+FQ3HG
CrGSAUKcyQo8GPy7T7hpqHYT1+aXlcQ+PR2MoN7W/ki2l6yH0WyY/ZCCZYsiDTI3
D4QRqrfYdHn5yARpsTdglebwa+QQhHHKbZPtABEBAAG0DnRlc3RAdGVzdC50ZXN0
iQEcBBABAgAGBQJf7Oi2AAoJEL2S4srBVYe7844H/11vAiTVM5PMuYdcLnAAibhH
3dwZKpl3OM6Z7+H5Mj+BKQ9ZB8akcTI15xpDpkyQrhrHw0bkLmeLgMKtRW9rOOeO
I5JSYhrprGoYTnyFyBMo3FLRBdtgjcgIb//EmGcopnggXi9h9MfLXektRrjTOaMn
yQ85+4V71ZGAs32QC4BNELqxr1d8iX6w+QYOgHMwI1dP+Vc/u/gdPgwDGZOaetsT
d9bu38bDvjeLJUc68+JBRwCQxzIUdprMvVnu6kU1KLtqF8DY+QyhUBHgpU1ZRU78
8O0BipXx1jKqoidve+rRGGXhdhxRZq2y3jh8ecjd5Jiby3LWn+siMSWVUui/dUQ=
=rXRW
-----END PGP PUBLIC KEY BLOCK-----
';
        $response = $this->from('/users/'.$user->id)->post('/addpgp', [
            'pgp' => $pgp,
        ]);

        $response = $this->from('/users/'.$user->id)->get('/enable2fa');
        $this->assertTrue($user->pgp_auth);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect('/users/'.$user->id);

        $response = $this->from('/users/'.$user->id)->get('/disable2fa');
        $this->assertFalse($user->pgp_auth);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect('/users/'.$user->id);
    }

    /** @test */
    public function user_can_be_deleted()
    {
        $user = $this->login();
        $this->assertFalse($user->trashed());

        $response = $this->from('/users/'.$user->id)->delete('/users/'.$user->id);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect('/login');

        $this->assertDatabaseHas('users', ['username' => $user->username]);
        $this->assertTrue($user->trashed());
    }
}
