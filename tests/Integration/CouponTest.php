<?php

namespace Tests\Integration;

use App\Models\Coupon;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class CouponTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function delete_coupon()
    {
        $user = $this->loginAsMerchant();

        $coupon = Coupon::factory()->create([
            'merchant_id' => $user->merchant->id
        ]);

        $response = $this->from('/home')->delete(route('coupons.destroy', $coupon));
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
    }

    /** @test */
    public function user_create_coupon()
    {
        $user = $this->loginAsMerchant();

        $code = Str::random(5);
        $response = $this->from(route('coupons.index'))->post(route('coupons.store'), [
            'code' => $code,
            'discount' => 10,
            'user_limit' => 10,
            'usage_limit' => 1,
            'expires_at' => now(),
            'merchant_id' => $user->merchant->id,
        ]);
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('coupons', [
            'code' => $code,
        ]);
    }
}
