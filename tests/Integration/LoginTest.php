<?php

namespace Tests\Integration;

use App\Enums\AccountType;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\PGP\PGP;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Carbon\Carbon;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_user_can_view_a_login_form()
    {
        $response = $this->get('/login');
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    /** @test */
    public function test_banned_user()
    {
        $user = $this->createUser([
            'status' => 'banned',
            'banned_until' => Carbon::now()->addDays(15),
        ]);

        $response = $this->actingAs($user)->get('/home');
        $response->assertRedirect('/login');
    }

    /** @test */
    public function test_user_cannot_view_a_login_form_when_authenticated()
    {
        $user = User::factory()->make();

        $response = $this->actingAs($user)->get('/login');
        $response->assertRedirect(RouteServiceProvider::HOME);
    }

    /** @test */
    public function test_user_can_login_with_correct_credentials()
    {
        $user = $this->createUser([
            'password' => bcrypt($password = 'randompass'),
        ]);

        $response = $this->from('/login')->post('/login', [
            'username' => $user->username,
            'password' => $password,
            'captcha' => true,
        ]);

        $response->assertRedirect(RouteServiceProvider::HOME);
    }

    /** @test */
    public function test_staff_login_redirect()
    {
        $user = $this->createUser([
            'password' => bcrypt($password = 'randompass'),
            'account' => AccountType::STAFF,
        ]);

        $response = $this->from('/login')->post('/login', [
            'username' => $user->username,
            'password' => $password,
            'captcha' => true,
        ]);

        $response->assertRedirect(RouteServiceProvider::STAFF_HOME);
        $this->assertTrue($user->isStaff());
    }

    /** @test */
    public function user_cannot_login_with_incorrect_password()
    {
        $user = $this->createUser([
            'password' => bcrypt('randompass'),
        ]);

        $response = $this->from('/login')->post('/login', [
            'username' => $user->username,
            'password' => 'invalid-password',
            'captcha' => true,
        ]);

        $response->assertRedirect('/login');
        $response->assertSessionHasErrors('username');
        $this->assertTrue(session()->hasOldInput('username'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    /** @test */
    public function user_cannot_login_2fa_if_disabled()
    {
        $user = User::factory()->create([
            'password' => bcrypt($password = 'randompass'),
            'pgp_auth' => false,
        ]);

        $response = $this->from('/2fa/form')->post('/2fa/challenge', [
            'username' => $user->username,
            'captcha' => true,
        ]);

        $response->assertSessionHasErrors(['err' => '2FA login disabled for this user.']);
        $response->assertRedirect('/2fa/form');
    }

    /** @test */
    public function user_cannot_login_2fa_if_pgp_missing()
    {
        $user = User::factory()->create([
            'password' => bcrypt($password = 'randompass'),
            'pgp_auth' => true,
            'pgp' => '',
        ]);

        $response = $this->from('/2fa/form')->post('/2fa/challenge', [
            'username' => $user->username,
            'captcha' => true,
        ]);

        $response->assertSessionHasErrors(['err' => 'PGP key not found.']);
        $response->assertRedirect('/2fa/form');
    }

    /** @test */
    public function user_can_login_2fa()
    {
        $username = 'johndoe';
        $pgp_service = new PGP();
        $pgp =
'
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: BCPG C# v1.6.1.0

mQENBF/s6LYBCACV4yQ805dV4JhpGad2ipUCKlV63h9CURNso/2am1YZEFjlU/26
c+ZdIhxnI2WYpZyPsvDUdlSvtMcyky3819HwpUZXTclUfm6fuhqGt5pVeefGtErK
RHHGfQL7j7FEgkSguucnbQM+I70nAboz2Z3PtqL0AtmBXEdvj3K4/+mwyhsuF+mK
Uco4SKPhqpJizkP6RBzSoRy2vP1dl6jYYV/PkgLKbAXO4FJddpTOfIcNg5+FQ3HG
CrGSAUKcyQo8GPy7T7hpqHYT1+aXlcQ+PR2MoN7W/ki2l6yH0WyY/ZCCZYsiDTI3
D4QRqrfYdHn5yARpsTdglebwa+QQhHHKbZPtABEBAAG0DnRlc3RAdGVzdC50ZXN0
iQEcBBABAgAGBQJf7Oi2AAoJEL2S4srBVYe7844H/11vAiTVM5PMuYdcLnAAibhH
3dwZKpl3OM6Z7+H5Mj+BKQ9ZB8akcTI15xpDpkyQrhrHw0bkLmeLgMKtRW9rOOeO
I5JSYhrprGoYTnyFyBMo3FLRBdtgjcgIb//EmGcopnggXi9h9MfLXektRrjTOaMn
yQ85+4V71ZGAs32QC4BNELqxr1d8iX6w+QYOgHMwI1dP+Vc/u/gdPgwDGZOaetsT
d9bu38bDvjeLJUc68+JBRwCQxzIUdprMvVnu6kU1KLtqF8DY+QyhUBHgpU1ZRU78
8O0BipXx1jKqoidve+rRGGXhdhxRZq2y3jh8ecjd5Jiby3LWn+siMSWVUui/dUQ=
=rXRW
-----END PGP PUBLIC KEY BLOCK-----
';
        $key = $pgp_service->loadKey($pgp);
        $this->assertTrue($pgp_service->saveKey($pgp, $username));

        $user = User::factory()->create([
            'username' => $username,
            'password' => bcrypt($password = 'randompass'),
            'pgp_auth' => true,
            'pgp' => $key->packets[0]->fingerprint,
        ]);

        $response = $this->from('/2fa/form')->post('/2fa/challenge', [
            'username' => $user->username,
            'captcha' => true,
        ]);

        $response->assertOk();
        $response->assertSee('Message');
        $response->assertSee('-----BEGIN PGP MESSAGE-----');
        $response->assertSee('Code');

        $response = $this->post('/2fa/login', [
            'user_id' => $user->id,
            'code' => $user->code,
        ]);

        $response->assertRedirect('/home');
    }
}
