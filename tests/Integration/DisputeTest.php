<?php

namespace Tests\Integration;

use App\Models\Dispute;
use App\Models\PaymentRequest;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class DisputeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function view_all_disputes()
    {
        $this->login();

        $response = $this->from('/home')->get(route('disputes.index'));
        $response->assertSuccessful();
        $response->assertSessionHasNoErrors();
        $response->assertViewIs('disputes.index');
    }

    /** @test */
    public function view_single_dispute()
    {
        $user = $this->login();

        $payment = PaymentRequest::factory()->create([
            'user_id' => $user->id
        ]);
        $dispute = Dispute::factory()->create([
            'payment_request_id' => $payment->id
        ]);

        $response = $this->from('/home')->get(route('disputes.show', $dispute));
        $response->assertSuccessful();
        $response->assertSessionHasNoErrors();
        $response->assertViewIs('disputes.show');
    }

    /** @test */
    public function user_create_dispute()
    {
        $user = $this->login();

        $payment = PaymentRequest::factory()->create([
            'user_id' => $user->id
        ]);

        $response = $this->from(route('disputes.index'))->post(route('disputes.store'), [
            'payment_request_id' => $payment->id->toString(),
            'user_notes' => Str::random(20),
        ]);
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('disputes', [
            'payment_request_id' => $payment->id->toString(),
        ]);
    }

    /** @test */
    public function merchant_reply_to_dispute()
    {
        $user = $this->loginAsMerchant();

        $payment = PaymentRequest::factory()->create([
            'merchant_id' => $user->merchant->id
        ]);
        $dispute = Dispute::factory()->create([
            'payment_request_id' => $payment->id,
            'merchant_notes' => '',
        ]);

        $response = $this->from(route('disputes.index'))->put(route('disputes.show', $dispute), [
            'merchant_notes' => Str::random(20),
        ]);
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('disputes', [
            'id' => $dispute->id,
        ]);
    }

    /** @test */
    public function merchant_cannot_see_dispute()
    {
        $user = $this->loginAsMerchant();

        $payment = PaymentRequest::factory()->create();
        $dispute = Dispute::factory()->create([
            'payment_request_id' => $payment->id
        ]);

        $response = $this->from(route('disputes.index'))->get(route('disputes.show', $dispute));
        $response->assertForbidden();
    }

    /** @test */
    public function merchant_cannot_reply_to_third_dispute()
    {
        $user = $this->loginAsMerchant();

        $payment = PaymentRequest::factory()->create();
        $dispute = Dispute::factory()->create([
            'payment_request_id' => $payment->id,
            'merchant_notes' => '',
        ]);

        $response = $this->from(route('disputes.index'))->put(route('disputes.show', $dispute), [
            'merchant_notes' => Str::random(20),
        ]);
        $response->assertForbidden();
    }

    /** @test */
    public function staff_accept_dispute()
    {
        $user = $this->loginAsStaff();

        $dispute = Dispute::factory()->create([
            'status' => 'in_review',
        ]);

        $response = $this->from(route('admin.disputes.show', $dispute))->get(route('admin.disputes.accept', $dispute));
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();

        $dispute->refresh();
        $this->assertTrue($dispute->isAccepted());
    }

    /** @test */
    public function staff_reject_dispute()
    {
        $user = $this->loginAsStaff();

        $dispute = Dispute::factory()->create([
            'status' => 'in_review',
        ]);

        $response = $this->from(route('admin.disputes.show', $dispute))->get(route('admin.disputes.reject', $dispute));
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();

        $dispute->refresh();
        $this->assertTrue($dispute->isRejected());
    }
}
