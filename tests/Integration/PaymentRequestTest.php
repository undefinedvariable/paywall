<?php

namespace Tests\Integration;

use App\Enums\AccountType;
use App\Enums\PaymentType;
use App\Models\PaymentRequest;
use App\Models\User;
use App\Jobs\General\PaymentRequestsExpirer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;
use Carbon\Carbon;

class PaymentRequestTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function see_all_payments()
    {
        $user = $this->login();

        $response = $this->get('/payments');
        $response->assertSuccessful();
        $response->assertSessionHasNoErrors();
        $response->assertViewIs('payments.index');
    }

    /** @test */
    public function see_specific_payment()
    {
        $user = $this->login();
        $payment = PaymentRequest::factory()->create([
            'status' => 'pending',
            'user_id' => $user->id,
        ]);

        $response = $this->get(route('payments.show', $payment));
        $response->assertSuccessful();
        $response->assertSessionHasNoErrors();
        $response->assertViewIs('payments.show');
    }

    /** @test */
    public function user_cannot_create_payment()
    {
        $user = $this->login();

        $response = $this->from(route('payments.create'))->post(route('payments.store'), [
            'description' => Str::random(10),
            'amount_usd' => 10,
            'shipping_usd' => 10,
            'goods_type' => 30,
            'payment_type' => 61,
        ]);
        $response->assertForbidden();
    }

    /** @test */
    public function merchant_can_create_fe_payment()
    {
        $user = $this->loginAsMerchant();

        $description = Str::random(10);
        $response = $this->from(route('payments.create'))->post(route('payments.store'), [
            'description' => $description,
            'amount_usd' => 10,
            'shipping_usd' => 10,
            'goods_type' => 30,
            'payment_type' => 61,
        ]);
        $response->assertRedirect();
        $this->assertDatabaseHas('payment_requests', [
            'merchant_id' => $user->merchant->id,
            'description' => $description,
        ]);
    }

    /** @test */
    public function merchant_can_cancel_a_payment()
    {
        $user = $this->loginAsMerchant();

        $payment = PaymentRequest::factory()->create([
            'status' => 'pending',
            'merchant_id' => $user->merchant->id,
        ]);
        $response = $this->from(route('payments.show', $payment))->post(route('payments.cancel', $payment));
        $response->assertRedirect();
        $payment->refresh();
        $this->assertTrue($payment->isCancelled());
    }

    /** @test */
    public function merchant_can_delete_a_payment()
    {
        $user = $this->loginAsMerchant();

        $payment = PaymentRequest::factory()->create([
            'status' => 'cancelled',
            'merchant_id' => $user->merchant->id,
        ]);
        $response = $this->from(route('payments.show', $payment))->delete(route('payments.destroy', $payment));
        $response->assertRedirect();
        $this->assertTrue(PaymentRequest::where('merchant_id', $user->merchant->id)->count() === 0);
    }

    /** @test */
    public function merchant_cannot_create_escrow_payment()
    {
        $user = $this->loginAsMerchant([], ['personal_wallet_address' => '']);

        $description = Str::random(10);
        $response = $this->from(route('payments.create'))->post(route('payments.store'), [
            'description' => $description,
            'amount_usd' => 10,
            'shipping_usd' => 10,
            'goods_type' => 30,
            'payment_type' => 60,
        ]);
        $response->assertSessionHas('error', 'Add your personal Monero wallet first.');
    }

    /** @test */
    public function user_can_see_checkout_page()
    {
        $user = $this->login();

        $payment = PaymentRequest::factory()->create([
            'status' => 'pending',
            'user_id' => '',
        ]);

        $response = $this->from(route('home'))->get($payment->url);
        $response->assertSuccessful();
        $response->assertViewIs('checkout.review');
    }

    // /** @test */
    // public function another_user_cannot_see_checkout_page()
    // {
    //     $user = $this->login();

    //     $payment = PaymentRequest::factory()->create([
    //         'status' => 'pending',
    //         'user_id' => '',
    //     ]);

    //     $response = $this->from(route('home'))->get($payment->url);
    //     $response->assertForbidden();
    // }

    /** @test */
    public function user_can_accept_fe_payment()
    {
        $user = $this->login();

        $payment = PaymentRequest::factory()->create([
            'status' => 'pending',
            'payment_type' => PaymentType::FE,
        ]);

        $notes = Str::random(10);
        $response = $this->from($payment->url)->put(route('payments.update', $payment), [
            'notes' => $notes,
        ]);
        $response->assertRedirect(route('checkout.currency', $payment));
        $response->assertSessionHasNoErrors();
    }

    /** @test */
    public function user_can_accept_escrow_payment()
    {
        $user = $this->login();

        $payment = PaymentRequest::factory()->create([
            'status' => 'pending',
            'payment_type' => PaymentType::ESCROW,
        ]);

        $notes = Str::random(10);
        $response = $this->from($payment->url)->put(route('payments.update', $payment), [
            'notes' => $notes,
        ]);
        $response->assertRedirect(route('checkout.wallet', $payment));
        $response->assertSessionHasNoErrors();
    }

    /** @test */
    public function payment_expiration_job()
    {
        $payment1 = PaymentRequest::factory()->create(['status' => 'pending']);
        $payment1->updated_at = Carbon::now()->subDays(config('expiration.payment.pending')+1);
        $payment1->timestamps = false;
        $payment1->save();

        $payment2 = PaymentRequest::factory()->create(['status' => 'confirming']);
        $payment2->updated_at = Carbon::now()->subDays(config('expiration.payment.confirming')+1);
        $payment2->timestamps = false;
        $payment2->save();

        PaymentRequestsExpirer::dispatchNow();

        $payment1->refresh();
        $payment2->refresh();

        $this->assertTrue($payment1->isExpired());
        $this->assertTrue($payment2->isExpired());
    }
}
