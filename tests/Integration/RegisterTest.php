<?php

namespace Tests\Integration;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_a_register_form()
    {
        $response = $this->get('/register');

        $response->assertSuccessful();
        $response->assertSessionHasNoErrors();
        $response->assertViewIs('auth.register');
    }

    public function test_user_cannot_view_a_register_form_when_authenticated()
    {
        $user = $this->createUser();

        $response = $this->actingAs($user)->get('/register');
        $response->assertRedirect(RouteServiceProvider::HOME);
    }

    public function test_user_incorrect_password_confirmation()
    {
        $response = $this->from(route('register'))->post(route('register'), [
            'username'              => 'username',
            'password'              => 'password',
            'password_confirmation' => 'nopassword',
        ]);

        $this->assertGuest();
        $response->assertRedirect(route('register'));
        $response->assertSessionHasErrors('password');
        $this->assertFalse(session()->hasOldInput('password'));
    }

    public function test_register_creates_and_authenticates_a_user()
    {
        $username = 'fakeuser';
        $response = $this->from(route('register'))->post(route('register'), [
            'username'              => $username,
            'password'              => 'password',
            'password_confirmation' => 'password',
            'captcha'               => true,
        ]);

        $response->assertRedirect(route('register.optional'));
        $response->assertSessionHasNoErrors();
        $user = User::where('username', $username)->first();
        $this->assertDatabaseHas('users', [
            'username' => $username,
        ]);
    }
}
