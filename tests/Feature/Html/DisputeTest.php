<?php

namespace Tests\Feature\Html;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class DisputeTest extends BrowserKitTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function users_can_see_disputes_tab()
    {
        $this->login();
        $this->visit('/disputes')
            ->see('Latest opened')
            ->see('Refunds')
            ->see('Disputes List');
    }
}
