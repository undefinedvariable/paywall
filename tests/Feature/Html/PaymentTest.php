<?php

namespace Tests\Feature\Html;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class PaymentTest extends BrowserKitTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function users_can_see_payment_tab()
    {
        $this->login();

        $this->visit('/payments')
            ->see('Payments')
            ->dontSee('Coupons');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function merchants_can_see_coupon_tab()
    {
        $this->loginAsMerchant();

        $this->visit('/payments')
            ->see('Payments')
            ->see('Coupons')
            ->dontSeeLink('Add New Coupon');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function clients_cannot_see_create_payment_button_coupon_tab()
    {
        $this->login();

        $this->visit('/payments')
            ->dontSeeLink('Create New Payment Request')
            ->dontSeeLink('Add New Coupon');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function marketplaces_cannot_see_create_payment_button_coupon_tab()
    {
        $this->loginAsMarketplace();

        $this->visit('/payments')
            ->dontSeeLink('Create New Payment Request')
            ->dontSeeLink('Add New Coupon');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function merchants_can_see_create_payment_button()
    {
        $this->loginAsMerchant();

        $this->visit('/payments')
            ->seeLink('Create New Payment Request');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function merchants_can_view_payment()
    {
        $this->loginAsMerchant();

        $this->visit('/payments')
            ->see('Payments')
            ->see('Coupons')
            ->dontSeeLink('Add New Coupon');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function merchants_can_view_create_payment_form()
    {
        $this->loginAsMerchant();

        $this->visit('/payments/create')
            ->see('Create Payment')
            ->see('Coupons')
            ->dontSeeLink('Add New Coupon');
        $this->assertResponseStatus(200);
    }
}
