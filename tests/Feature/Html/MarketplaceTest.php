<?php

namespace Tests\Feature\Html;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class MarketplaceTest extends BrowserKitTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function marketplace_can_see_page()
    {
        $user = $this->loginAsMarketplace();

        $this->visit('/marketplaces/'.$user->marketplace->id)
            ->see('Change Tier')
            ->see($user->user_tier->tier->key)
            ->see('Set')
            ->see('Remaining '.$user->user_tier->credits.' credits, expiring on '.$user->user_tier->tier_expiration->format('m/d/Y'))
            ->see('OAuth Data')
            ->see('Client ID')
            ->see($user->clients->first()->id)
            ->see('Client Secret')
            ->see($user->clients->first()->secres)
            ->see('Manage')
            ->see('Marketplace Settings')
            ->see('IPN URL')
            ->see($user->marketplace->ipn_url)
            ->see('IPN Callback')
            ->see($user->marketplace->ipn_callback)
            ->see('Commission Type')
            ->see($user->marketplace->commission_type->key)
            ->see('Commission Value')
            ->see($user->marketplace->commission)
            ->see('Redirect URL')
            ->see($user->marketplace->redirect_url)
            ->see('Update')
            ->see('Monero Wallet')
            ->see('Wallet Address')
            ->see($user->marketplace->wallet_address);
        $this->assertResponseStatus(200);
    }
}
