<?php

namespace Tests\Feature\Html;

use App\Models\Ticket;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TicketTest extends BrowserKitTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function user_can_see_support_page()
    {
        $this->login();

        $this->visit('/tickets')
            ->see('Open')
            ->see('Closed')
            ->see('Tickets List')
            ->see('Open a Ticket');
        $this->assertResponseStatus(200);
    }

    /* @test */
    public function create_ticket_and_view()
    {
        $this->login();

        $ticket = Ticket::factory()->make();
        $this->visit('/tickets/'.$ticket->id)
            ->see('View a Ticket')
            ->see('Ticket ID');
        $this->assertResponseStatus(200);
    }
}
