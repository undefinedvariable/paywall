<?php

namespace Tests\Feature\Html;

class HomeTest extends BrowserKitTestCase
{
    /** @test */
    public function users_can_see_the_homepage()
    {
        $this->visit('/')
            ->see(config('app.name'))
            ->dontSee('Laravel');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_a_login_and_registration_link_when_logged_out()
    {
        $this->visit('/login')
            ->see('Login')
            ->see('Register')
            ->dontSeeLink('Sign out');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_a_logout_button_when_logged_in()
    {
        $this->login();

        $this->visit('/home')
            ->see('Logout')
            ->dontSee('Login')
            ->dontSee('Register');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_notification_tab()
    {
        $this->login();

        $this->visit('/home')
            ->see('Notifications List');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_news_tab()
    {
        $this->login();

        $this->visit('/home')
            ->see('News');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_menu()
    {
        $this->login();

        $this->visit('/home')
            ->seeLink('Dashboard')
            ->seeLink('Payments')
            ->seeLink('Disputes')
            ->seeLink('My Account');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function clients_cannot_see_special_menu_tab()
    {
        $this->login();

        $this->visit('/home')
            ->dontSeeLink('Marketplace')
            ->dontSeeLink('Merchant')
            ->dontSeeLink('Manage');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function merchants_can_see_special_menu_tab()
    {
        $this->loginAsMerchant();

        $this->visit('/home')
            ->seeLink('Merchant')
            ->dontSeeLink('Marketplace')
            ->dontSeeLink('Manage');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function marketplaces_can_see_special_menu_tab()
    {
        $this->loginAsMarketplace();

        $this->visit('/home')
            ->seeLink('Marketplace')
            ->dontSeeLink('Merchant')
            ->dontSeeLink('Manage');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function staff_can_see_special_menu_tab()
    {
        $this->loginAsStaff();

        $this->visit('/home')
            ->seeLink('Manage')
            ->dontSeeLink('Merchant')
            ->dontSeeLink('Marketplace');
        $this->assertResponseStatus(200);
    }
}
