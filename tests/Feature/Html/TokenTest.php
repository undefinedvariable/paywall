<?php

namespace Tests\Feature\Html;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class TokenTest extends BrowserKitTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function users_can_see_tokens_tab()
    {
        $this->login();

        $this->visit('/tokens')
            ->see('Clients')
            ->see('Recently Generated Tokens')
            ->see('All Tokens List');
        $this->assertResponseStatus(200);
    }
}
