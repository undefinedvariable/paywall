<?php

namespace Tests\Feature\Html;

class AuthTest extends BrowserKitTestCase
{
    /** @test */
    public function users_can_see_login_fields()
    {
        $this->visit('/login')
            ->see('Username')
            ->see('Password')
            ->see('Captcha')
            ->see('Forgot your password?')
            ->see('Login')
            ->see('Register')
            ->see('Switch To 2FA')
            ->dontSee('Sign out');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_2fa_login_fields()
    {
        $this->visit('/2fa/form')
            ->see('Username')
            ->see('Captcha')
            ->see('Continue')
            ->see('Back To Login')
            ->dontSee('Switch To 2FA')
            ->dontSee('Sign out');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_register_fields()
    {
        $this->visit('/register')
            ->see('Username')
            ->see('Password')
            ->see('Confirm Password')
            ->see('Captcha')
            ->see('Login')
            ->see('Register')
            ->dontSee('Forgot your password?')
            ->dontSee('Sign out');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_password_recovery_fields()
    {
        $this->visit('/password/recover')
            ->see('Username')
            ->see('Passphrase')
            ->see('Send')
            ->see('Back To Login')
            ->dontSee('Forgot your password?');
        $this->assertResponseStatus(200);
    }
}
