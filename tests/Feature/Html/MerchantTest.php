<?php

namespace Tests\Feature\Html;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class MerchantTest extends BrowserKitTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function merchant_can_see_page()
    {
        $user = $this->loginAsMerchant();

        $this->visit('/merchants/'.$user->merchant->id)
            ->see('Change Tier')
            ->see($user->user_tier->tier->key)
            ->see('Set')
            ->see('Remaining '.$user->user_tier->credits.' credits, expiring on '.$user->user_tier->tier_expiration->format('m/d/Y'))
            ->see('Marketplaces')
            ->see('Manage')
            ->see('Merchant Settings')
            ->see('IPN URL')
            ->see($user->merchant->ipn_url)
            ->see('IPN Callback')
            ->see($user->merchant->ipn_callback)
            ->see('Website')
            ->see($user->merchant->website)
            ->see('Update')
            ->see('Monero Personal Wallet')
            ->see('Wallet Address')
            ->see('Secret View Key')
            ->see($user->merchant->personal_wallet_address);
        $this->assertResponseStatus(200);
    }
}
