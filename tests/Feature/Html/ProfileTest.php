<?php

namespace Tests\Feature\Html;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProfileTest extends BrowserKitTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function users_can_see_profile_name()
    {
        $user = $this->login();

        $this->visitRoute('users.show', ['user' => $user])
            ->see($user->username);
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_language_tab()
    {
        $user = $this->login();

        $this->visitRoute('users.show', ['user' => $user])
            ->see('Change Language')
            ->see('Set Language');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_security_tab()
    {
        $user = $this->login();

        $this->visitRoute('users.show', ['user' => $user])
            ->see('Security')
            ->see('Enable 2FA');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function users_can_see_account_tab()
    {
        $user = $this->login();

        $this->visitRoute('users.show', ['user' => $user])
            ->see('Account Settings')
            ->see('Username')
            ->see('Account Type')
            ->see('New Password')
            ->see('Re-enter New Password')
            ->see('Old Password')
            ->see('Update Password')
            ->see('Delete My Account')
            ->see('USER');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function merchants_can_see_account_tab()
    {
        $user = $this->loginAsMerchant();

        $this->visitRoute('users.show', ['user' => $user])
            ->see('Account Settings')
            ->see('Username')
            ->see('Account Type')
            ->see('New Password')
            ->see('Re-enter New Password')
            ->see('Old Password')
            ->see('Update Password')
            ->see('Delete My Account')
            ->see('MERCHANT');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function marketplaces_can_see_account_tab()
    {
        $user = $this->loginAsMarketplace();

        $this->visitRoute('users.show', ['user' => $user])
            ->see('Account Settings')
            ->see('Username')
            ->see('Account Type')
            ->see('New Password')
            ->see('Re-enter New Password')
            ->see('Old Password')
            ->see('Update Password')
            ->see('Delete My Account')
            ->see('MARKETPLACE');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function staff_can_see_account_tab()
    {
        $user = $this->loginAsStaff();

        $this->visitRoute('users.show', ['user' => $user])
            ->see('Account Settings')
            ->see('Username')
            ->see('Account Type')
            ->see('New Password')
            ->see('Re-enter New Password')
            ->see('Old Password')
            ->see('Update Password')
            ->see('Delete My Account')
            ->see('STAFF');
        $this->assertResponseStatus(200);
    }

    /** @test */
    public function admins_can_see_account_tab()
    {
        $user = $this->loginAsAdmin();

        $this->visitRoute('users.show', ['user' => $user])
            ->see('Account Settings')
            ->see('Username')
            ->see('Account Type')
            ->see('New Password')
            ->see('Re-enter New Password')
            ->see('Old Password')
            ->see('Update Password')
            ->see('Delete My Account')
            ->see('ADMIN');
        $this->assertResponseStatus(200);
    }
}
