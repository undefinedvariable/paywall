<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TokenTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_token_index()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(route('tokens.index'));
        $response->assertSuccessful();
        $response->assertViewIs('private.tokens');
    }

    public function test_user_can_revoke_token()
    {
        $user = User::factory()->make();

        $response = $this->actingAs($user)->get(route('tokens.revoke', ['token' => 1]))->assertRedirect(route('tokens.index'));
    }

    public function test_user_can_delete_token()
    {
        $user = User::factory()->make();

        $response = $this->actingAs($user)->delete(route('tokens.delete', ['token' => 1]))->assertRedirect(route('tokens.index'));
    }
}
