<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        $app['validator']->extend('captcha', function () {
            return true;
        });
        $app['validator']->extend('allowed_username', function () {
            return true;
        });

        return $app;
    }
}
