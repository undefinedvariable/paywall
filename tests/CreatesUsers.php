<?php

namespace Tests;

use App\Enums\AccountType;
use App\Enums\Tier;
use App\Models\Marketplace;
use App\Models\Merchant;
use App\Models\User;
use App\Models\UserTier;
use Carbon\Carbon;
use Laravel\Passport\ClientRepository;

trait CreatesUsers
{
    protected function login($attributes = []): User
    {
        $user = $this->createUser($attributes);

        $this->be($user);

        UserTier::create([
            'user_id' => $user->id,
            'credits' => config('api.free_credits'),
            'tier' => Tier::FREE,
            'tier_expiration' => Carbon::now()->addMonths(config('api.tier_expiration')),
        ]);

        return $user;
    }

    protected function loginAsMerchant(array $user_attributes = [], array $merchant_attributes = []): User
    {
        $user = $this->login(array_merge($user_attributes, ['account' => AccountType::MERCHANT]));
        Merchant::factory()->create(array_merge($merchant_attributes, ['user_id' => $user->id]));

        return $user;
    }

    protected function loginAsMarketplace(array $attributes = []): User
    {
        $user = $this->login(array_merge($attributes, ['account' => AccountType::MARKETPLACE]));

        $clients = new ClientRepository();
        $clients->create($user->id, $user->username, 'http://test.com');

        Marketplace::factory()->create(['user_id' => $user->id]);

        return $user;
    }

    protected function loginAsStaff(array $attributes = []): User
    {
        return $this->login(array_merge($attributes, ['account' => AccountType::STAFF]));
    }

    protected function loginAsAdmin(array $attributes = []): User
    {
        return $this->login(array_merge($attributes, ['account' => AccountType::ADMIN]));
    }

    protected function createUser(array $attributes = []): User
    {
        return User::factory()->create(array_merge([
            'username' => 'johndoe',
            'password' => bcrypt('password'),
            'account' => AccountType::USER,
        ], $attributes));
    }
}
