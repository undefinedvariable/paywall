<?php

namespace Tests\Unit\Models;

use App\Enums\GoodsType;
use App\Enums\PaymentType;
use App\Models\MultisigWallet;
use App\Models\PaymentRequest;
use App\Models\States\PaymentRequest\Closed;
use App\Models\States\PaymentRequest\Confirming;
use App\Models\States\PaymentRequest\Pending;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Tests\TestCase;

class PaymentRequestTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function test_model_configuration()
    {
        $m = new PaymentRequest();

        $this->assertEquals([
            'merchant_id',
            'marketplace_id',
            'user_id',
            'orderid',
            'amount_usd',
            'shipping_usd',
            'amount_xmr',
            'cryptocurrency_id',
            'goods_type',
            'payment_type',
            'coupon_id',
            'payment_id',
            'description',
            'item_list',
            'notes',
            'form_step',
            'url',
            'success_url',
            'cancel_url',
            'cryptoexchange_id',
            'status',
        ], $m->getFillable());

        $this->assertEquals([
            'merchant_id'           => 'integer',
            'user_id'               => 'integer',
            'cryptocurrency_id'     => 'integer',
            'coupon_id'             => 'integer',
            'notes'                 => Encrypted::class,
            'amount_xmr'            => 'decimal:18',
            'created_at'            => 'datetime',
            'updated_at'            => 'datetime',
            'goods_type'            => GoodsType::class,
            'payment_type'          => PaymentType::class,
        ], $m->getCasts());

        $this->assertEquals([
            'created_at',
            'updated_at',
        ], $m->getDates());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new PaymentRequest();

        $relation = $m->dispute();
        $this->assertInstanceOf(HasOne::class, $relation);
        $this->assertEquals('payment_request_id', $relation->getForeignKeyName());

        $relation = $m->multisigWallet();
        $this->assertInstanceOf(HasOne::class, $relation);
        $this->assertEquals('payment_request_id', $relation->getForeignKeyName());

        $relation = $m->merchant();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('merchant_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());

        $relation = $m->marketplace();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('marketplace_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());

        $relation = $m->user();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());

        $relation = $m->coupon();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('coupon_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());

        $relation = $m->cryptocurrency();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('cryptocurrency_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());
    }

    /** @test */
    public function payment_is_valid()
    {
        $m = PaymentRequest::factory()->create([
            'status' => Pending::class,
            'created_at' => Carbon::now(),
        ]);

        $this->assertTrue($m->isValid());

        $m = PaymentRequest::factory()->create([
            'status' => Pending::class,
            'created_at' => Carbon::now()->subDays(config('expiration.payment.pending')),
        ]);

        $this->assertFalse($m->isValid());

        $m = PaymentRequest::factory()->create([
            'status' => Confirming::class,
            'created_at' => Carbon::now(),
        ]);

        $this->assertTrue($m->isValid());

        $m = PaymentRequest::factory()->create([
            'status' => Confirming::class,
            'created_at' => Carbon::now()->subDays(config('expiration.payment.confirming')),
        ]);

        $this->assertFalse($m->isValid());

        $m = PaymentRequest::factory()->create([
            'status' => Closed::class,
        ]);

        $this->assertTrue($m->isValid());
    }

    /** @test */
    public function payment_final_price()
    {
        $amount_usd = rand();
        $shipping_usd = rand();
        $total = $amount_usd + $shipping_usd;

        $m = PaymentRequest::factory()->create([
            'coupon_id' => null,
            'amount_usd' => $amount_usd,
            'shipping_usd' => $shipping_usd,
        ]);

        $this->assertTrue($m->getFinalPrice() == $total);

        $m = PaymentRequest::factory()->create([
            'amount_usd' => $amount_usd,
            'shipping_usd' => $shipping_usd,
        ]);

        $total = $amount_usd + $shipping_usd - ($amount_usd * $m->coupon->discount * 0.01);

        $this->assertTrue($m->getFinalPrice() == $total);
    }

    /** @test */
    public function payment_can_be_cancelled()
    {
        $m = PaymentRequest::factory()->create([
            'status' => Pending::class,
        ]);

        $this->assertTrue($m->canBeCancelled());
    }

    /** @test */
    public function payment_can_be_deleted()
    {
        $m = PaymentRequest::factory()->create([
            'status' => Pending::class,
        ]);

        $this->assertFalse($m->canBeDeleted());

        $m = PaymentRequest::factory()->create([
            'status' => Closed::class,
            'created_at' => Carbon::now()->subDays(5),
        ]);

        $this->assertTrue($m->canBeDeleted());
    }

    /** @test */
    public function payment_get_commission()
    {
        $m = PaymentRequest::factory()->create();

        if ($m->marketplace->hasPercentageCommission()) {
            $commission = $m->marketplace->commission * $m->amount_xmr * 0.01;
        } elseif ($m->marketplace->hasFixedCommission()) {
            $commission = $m->marketplace->commission < $m->amount_xmr ? $m->marketplace->commission : 0;
        } else {
            $commission = 0;
        }

        $this->assertTrue($m->getCommissionInXMR() - $commission < 0.1);
    }

    /** @test */
    public function payment_broadcastable()
    {
        PaymentRequest::factory()->create([
            'payment_type' => PaymentType::ESCROW,
            'status' => Pending::class,
        ]);

        PaymentRequest::factory()->create([
            'payment_type' => PaymentType::FE,
            'status' => Pending::class,
        ]);

        PaymentRequest::factory()->create([
            'payment_type' => PaymentType::ESCROW,
            'status' => Closed::class,
        ]);

        $p = PaymentRequest::factory()->create([
            'payment_type' => PaymentType::ESCROW,
            'status' => Pending::class,
        ]);

        MultisigWallet::factory()->create([
            'payment_request_id' => $p->id,
        ]);

        $m = PaymentRequest::broadcastable()->count();
        $this->assertTrue($m == 1);
    }
}
