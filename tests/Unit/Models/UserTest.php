<?php

namespace Tests\Unit\Models;

use App\Enums\AccountType;
use App\Enums\Flower;
use App\Enums\Language;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new User();

        $this->assertEquals([
            'username',
            'pgp',
            'password',
            'passphrase',
            'refund_address',
            'language',
            'flower',
            'account',
            'status',
            'reputation',
            'pgp_auth',
            'code',
            'banned_until',
            'last_login',
        ], $m->getFillable());

        $this->assertEquals([
            'password',
            'passphrase',
            'code',
            'banned_until',
            'remember_token',
        ], $m->getHidden());

        $this->assertEquals([
            'language'      => Language::class,
            'flower'        => Flower::class,
            'account'       => AccountType::class,
            'reputation'    => 'integer',
            'last_login'    => 'datetime',
            'banned_until'  => 'datetime',
            'created_at'    => 'datetime',
            'updated_at'    => 'datetime',
            'deleted_at' => 'datetime',
        ], $m->getCasts());

        $this->assertEquals([
            'created_at',
            'updated_at',
        ], $m->getDates());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new User();

        $relation = $m->merchant();
        $this->assertInstanceOf(HasOne::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());

        $relation = $m->user_tier();
        $this->assertInstanceOf(HasOne::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());

        $relation = $m->marketplace();
        $this->assertInstanceOf(HasOne::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());

        $relation = $m->paymentRequests();
        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());

        $relation = $m->user_notification_channels();
        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());
    }
}
