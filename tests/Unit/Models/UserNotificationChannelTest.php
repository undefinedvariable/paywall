<?php

namespace Tests\Unit\Models;

use App\Enums\NotificationType;
use App\Models\UserNotificationChannel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Tests\TestCase;

class UserNotificationChannelTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new UserNotificationChannel();

        $this->assertEquals([
            'user_id',
            'type',
            'account',
        ], $m->getFillable());

        $this->assertEquals([
            'id'                    => 'int',
            'user_id'               => 'integer',
            'type'                  => NotificationType::class,
            'account'               => Encrypted::class,
            'created_at'            => 'datetime',
            'updated_at'            => 'datetime',
        ], $m->getCasts());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new UserNotificationChannel();

        $relation = $m->user();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());
    }
}
