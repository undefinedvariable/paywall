<?php

namespace Tests\Unit\Models;

use App\Enums\CommissionType;
use App\Models\Marketplace;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Tests\TestCase;

class MarketplaceTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new Marketplace();

        $this->assertEquals([
            'user_id',
            'wallet_address',
            'commission',
            'commission_type',
            'ipn_enable',
            'ipn_url',
        ], $m->getFillable());

        $this->assertEquals([
            'user_id'           => 'integer',
            'ipn_enable'        => 'boolean',
            'tier_expiration'   => 'datetime',
            'created_at'        => 'datetime',
            'updated_at'        => 'datetime',
            'commission_type'   => CommissionType::class,
            'wallet_address'    => Encrypted::class,
        ], $m->getCasts());

        $this->assertEquals([
            'created_at',
            'updated_at',
        ], $m->getDates());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new Marketplace();

        $relation = $m->paymentRequests();
        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertEquals('marketplace_id', $relation->getForeignKeyName());

        $relation = $m->user();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());

        $relation = $m->merchants();
        $this->assertInstanceOf(BelongsToMany::class, $relation);
        $this->assertEquals('marketplace_id', $relation->getForeignPivotKeyName());
        $this->assertEquals('merchant_id', $relation->getRelatedPivotKeyName());
    }
}
