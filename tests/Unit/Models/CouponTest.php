<?php

namespace Tests\Unit\Models;

use App\Models\Coupon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tests\TestCase;

class CouponTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new Coupon();

        $this->assertEquals([
            'merchant_id',
            'code',
            'discount',
            'usage_limit',
            'expires_at',
        ], $m->getFillable());

        $this->assertEquals([
            'id' => 'int',
            'discount' => 'integer',
            'usage_limit' => 'integer',
            'expires_at' => 'datetime',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
        ], $m->getCasts());

        $this->assertEquals([
            'created_at',
            'updated_at',
        ], $m->getDates());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new Coupon();

        $relation = $m->paymentRequests();
        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertEquals('coupon_id', $relation->getForeignKeyName());

        $relation = $m->users();
        $this->assertInstanceOf(BelongsToMany::class, $relation);
        $this->assertEquals('coupon_id', $relation->getForeignPivotKeyName());
        $this->assertEquals('user_id', $relation->getRelatedPivotKeyName());

        $relation = $m->merchant();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('merchant_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());
    }

    /** @test */
    public function test_status_getter()
    {
        $m = new Coupon();
        $m->setRawAttributes([
            'discount' => 1,
        ]);

        // Test if getter is working.
        $this->assertEquals(1, $m->getAttributeValue('discount'));
    }
}
