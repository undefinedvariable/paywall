<?php

namespace Tests\Unit\Models;

use App\Models\MultisigWallet;
use App\Models\States\MultisigWallet\Created;
use App\Models\States\MultisigWallet\Finalized;
use App\Models\States\MultisigWallet\Made;
use App\Models\States\MultisigWallet\Prepared;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use Tests\TestCase;

class MultisigWalletTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function test_model_configuration()
    {
        $m = new MultisigWallet();

        $this->assertEquals([
            'filename',
            'address',
            'payment_request_id',
            'prepare_multisig_data_customer',
            'prepare_multisig_data_vendor',
            'prepare_multisig_data_system',
            'make_multisig_data_customer',
            'make_multisig_data_vendor',
            'make_multisig_data_system',
            'finalize_multisig_data_customer',
            'finalize_multisig_data_vendor',
            'status',
        ], $m->getFillable());

        $this->assertEquals([
            'id' => 'int',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
        ], $m->getCasts());

        $this->assertEquals([
            'created_at',
            'updated_at',
        ], $m->getDates());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new MultisigWallet();

        $relation = $m->paymentRequest();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('payment_request_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());
    }

    /** @test */
    public function wallet_has_been_prepared()
    {
        $m = MultisigWallet::factory()->create([
            'status' => Created::class,
            'prepare_multisig_data_customer' => Str::random(10),
            'prepare_multisig_data_vendor' => Str::random(10),
            'prepare_multisig_data_system' => Str::random(10),
        ]);

        $this->assertTrue($m->hasBeenPrepared());
    }

    /** @test */
    public function wallet_has_been_prepared_fails_lacks_prepare_multisig_data()
    {
        $m = MultisigWallet::factory()->create([
            'status' => Created::class,
            'prepare_multisig_data_customer' => Str::random(10),
            'prepare_multisig_data_vendor' => Str::random(10),
            'prepare_multisig_data_system' => null,
        ]);

        $this->assertFalse($m->hasBeenPrepared());
    }

    /** @test */
    public function wallet_has_been_prepared_fails_lacks_status()
    {
        $m = MultisigWallet::factory()->create([
            'status' => Prepared::class,
            'prepare_multisig_data_customer' => Str::random(10),
            'prepare_multisig_data_vendor' => Str::random(10),
            'prepare_multisig_data_system' => Str::random(10),
        ]);

        $this->assertFalse($m->hasBeenPrepared());
    }

    /** @test */
    public function wallet_has_been_made()
    {
        $m = MultisigWallet::factory()->create([
            'status' => Prepared::class,
            'make_multisig_data_customer' => Str::random(10),
            'make_multisig_data_vendor' => Str::random(10),
            'make_multisig_data_system' => Str::random(10),
        ]);

        $this->assertTrue($m->hasBeenMade());
    }

    /** @test */
    public function wallet_has_been_made_fails_lacks_make_multisig_data()
    {
        $m = MultisigWallet::factory()->create([
            'status' => Prepared::class,
            'make_multisig_data_customer' => Str::random(10),
            'make_multisig_data_vendor' => Str::random(10),
            'make_multisig_data_system' => null,
        ]);

        $this->assertFalse($m->hasBeenMade());
    }

    /** @test */
    public function wallet_has_been_made_fails_lacks_status()
    {
        $m = MultisigWallet::factory()->create([
            'status' => Created::class,
            'make_multisig_data_customer' => Str::random(10),
            'make_multisig_data_vendor' => Str::random(10),
            'make_multisig_data_system' => Str::random(10),
        ]);

        $this->assertFalse($m->hasBeenMade());

        $m = MultisigWallet::factory()->create([
            'status' => Made::class,
            'make_multisig_data_customer' => Str::random(10),
            'make_multisig_data_vendor' => Str::random(10),
            'make_multisig_data_system' => Str::random(10),
        ]);

        $this->assertFalse($m->hasBeenMade());

        $m = MultisigWallet::factory()->create([
            'status' => Finalized::class,
            'make_multisig_data_customer' => Str::random(10),
            'make_multisig_data_vendor' => Str::random(10),
            'make_multisig_data_system' => Str::random(10),
        ]);

        $this->assertFalse($m->hasBeenMade());
    }

    /** @test */
    public function wallet_has_been_finalized()
    {
        $m = MultisigWallet::factory()->create([
            'status' => Made::class,
            'finalize_multisig_data_customer' => Str::random(10),
            'finalize_multisig_data_vendor' => Str::random(10),
            'address' => Str::random(10),
        ]);

        $this->assertTrue($m->hasBeenFinalized());
    }

    /** @test */
    public function wallet_has_been_finalized_fails_lacks_finalize_multisig_data()
    {
        $m = MultisigWallet::factory()->create([
            'status' => Made::class,
            'finalize_multisig_data_customer' => Str::random(10),
            'finalize_multisig_data_vendor' => Str::random(10),
            'address' => null,
        ]);

        $this->assertFalse($m->hasBeenFinalized());
    }

    /** @test */
    public function wallet_has_been_finalized_fails_lacks_status()
    {
        $m = MultisigWallet::factory()->create([
            'status' => Created::class,
            'finalize_multisig_data_customer' => Str::random(10),
            'finalize_multisig_data_vendor' => Str::random(10),
            'address' => Str::random(10),
        ]);

        $this->assertFalse($m->hasBeenFinalized());

        $m = MultisigWallet::factory()->create([
            'status' => Prepared::class,
            'finalize_multisig_data_customer' => Str::random(10),
            'finalize_multisig_data_vendor' => Str::random(10),
            'address' => Str::random(10),
        ]);

        $this->assertFalse($m->hasBeenFinalized());

        $m = MultisigWallet::factory()->create([
            'status' => Finalized::class,
            'finalize_multisig_data_customer' => Str::random(10),
            'finalize_multisig_data_vendor' => Str::random(10),
            'address' => Str::random(10),
        ]);

        $this->assertFalse($m->hasBeenFinalized());
    }
}
