<?php

namespace Tests\Unit\Models;

use App\Models\TicketCategory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tests\TestCase;

class TicketCategoryTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new TicketCategory();

        $this->assertEquals([
            'name',
        ], $m->getFillable());

        $this->assertEquals([
            'id' => 'int',
            'created_at' => 'datetime',
        ], $m->getCasts());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new TicketCategory();

        $relation = $m->tickets();
        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertEquals('ticket_category_id', $relation->getForeignKeyName());
    }
}
