<?php

namespace Tests\Unit\Models;

use App\Models\Cryptocurrency;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tests\TestCase;

class CryptocurrencyTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new Cryptocurrency();

        $this->assertEquals([
            'name',
            'abbreviation',
            'fee',
        ], $m->getFillable());

        $this->assertEquals([
            'id'          => 'int',
            'fee'         => 'decimal:8',
            'created_at'  => 'datetime',
            'updated_at'  => 'datetime',
        ], $m->getCasts());

        $this->assertEquals([
            'created_at',
            'updated_at',
        ], $m->getDates());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new Cryptocurrency();

        $relation = $m->paymentRequests();
        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertEquals('cryptocurrency_id', $relation->getForeignKeyName());
    }
}
