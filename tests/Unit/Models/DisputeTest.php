<?php

namespace Tests\Unit\Models;

use App\Models\Dispute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Tests\TestCase;

class DisputeTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new Dispute();

        $this->assertEquals([
            'payment_request_id',
            'user_notes',
            'merchant_notes',
            'status',
        ], $m->getFillable());

        $this->assertEquals([
            'user_notes' => Encrypted::class,
            'merchant_notes' => Encrypted::class,
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
        ], $m->getCasts());

        $this->assertEquals([
            'created_at',
            'updated_at',
        ], $m->getDates());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new Dispute();

        $relation = $m->paymentRequest();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('payment_request_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());
    }
}
