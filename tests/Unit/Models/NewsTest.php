<?php

namespace Tests\Unit\Models;

use App\Models\News;
use Tests\TestCase;

class NewsTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new News();

        $this->assertEquals([
            'body',
        ], $m->getFillable());

        $this->assertEquals([
            'id' => 'int',
            'created_at' => 'datetime',
        ], $m->getCasts());
    }
}
