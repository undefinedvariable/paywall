<?php

namespace Tests\Unit\Models;

use App\Models\Merchant;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Tests\TestCase;

class MerchantTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new Merchant();

        $this->assertEquals([
            'user_id',
            'personal_wallet_address',
            'website',
            'ipn_enable',
            'ipn_url',
        ], $m->getFillable());

        $this->assertEquals([
            'user_id'                   => 'integer',
            'ipn_enable'                => 'boolean',
            'created_at'                => 'datetime',
            'updated_at'                => 'datetime',
            'personal_wallet_address'   => Encrypted::class,
        ], $m->getCasts());

        $this->assertEquals([
            'created_at',
            'updated_at',
        ], $m->getDates());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new Merchant();

        $relation = $m->coupons();
        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertEquals('merchant_id', $relation->getForeignKeyName());

        $relation = $m->paymentRequests();
        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertEquals('merchant_id', $relation->getForeignKeyName());

        $relation = $m->multisigWallets();
        $this->assertInstanceOf(HasManyThrough::class, $relation);
        $this->assertEquals('payment_request_id', $relation->getForeignKeyName());

        $relation = $m->disputes();
        $this->assertInstanceOf(HasManyThrough::class, $relation);
        $this->assertEquals('payment_request_id', $relation->getForeignKeyName());

        $relation = $m->user();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());

        $relation = $m->marketplaces();
        $this->assertInstanceOf(BelongsToMany::class, $relation);
        $this->assertEquals('merchant_id', $relation->getForeignPivotKeyName());
        $this->assertEquals('marketplace_id', $relation->getRelatedPivotKeyName());
    }
}
