<?php

namespace Tests\Unit\Models;

use App\Models\Message;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Tests\TestCase;

class MessageTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new Message();

        $this->assertEquals([
            'contact',
            'contact_type',
            'body',
        ], $m->getFillable());

        $this->assertEquals([
            'id'            => 'int',
            'body'          => Encrypted::class,
            'created_at'    => 'datetime',
        ], $m->getCasts());
    }
}
