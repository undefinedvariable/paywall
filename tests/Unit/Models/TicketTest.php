<?php

namespace Tests\Unit\Models;

use App\Enums\Priority;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Tests\TestCase;

class TicketTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new Ticket();

        $this->assertEquals([
            'user_id',
            'ticket_category_id',
            'title',
            'priority',
            'message',
            'status',
        ], $m->getFillable());

        $this->assertEquals([
            'id'                    => 'int',
            'user_id'               => 'integer',
            'ticket_category_id'    => 'integer',
            'created_at'            => 'datetime',
            'updated_at'            => 'datetime',
            'priority'              => Priority::class,
            'message'               => Encrypted::class,
        ], $m->getCasts());

        $this->assertEquals([
            'created_at',
            'updated_at',
        ], $m->getDates());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new Ticket();

        $relation = $m->ticketComments();
        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertEquals('ticket_id', $relation->getForeignKeyName());

        $relation = $m->ticketCategory();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('ticket_category_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());

        $relation = $m->user();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());
    }
}
