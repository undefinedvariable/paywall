<?php

namespace Tests\Unit\Models;

use App\Models\TicketComment;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Tests\TestCase;

class TicketCommentTest extends TestCase
{
    /** @test */
    public function test_model_configuration()
    {
        $m = new TicketComment();

        $this->assertEquals([
            'user_id',
            'ticket_id',
            'comment',
        ], $m->getFillable());

        $this->assertEquals([
            'id'            => 'int',
            'user_id'       => 'integer',
            'ticket_id'     => 'integer',
            'comment'       => Encrypted::class,
            'created_at'    => 'datetime',
        ], $m->getCasts());
    }

    /** @test */
    public function test_model_relations()
    {
        $m = new TicketComment();

        $relation = $m->ticket();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('ticket_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());

        $relation = $m->user();
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals('user_id', $relation->getForeignKeyName());
        $this->assertEquals('id', $relation->getOwnerKeyName());
    }
}
