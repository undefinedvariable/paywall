<?php

namespace Tests\Unit\Rules;

use App\Rules\ValidMoneroAddressRule;
use Illuminate\Support\Str;
use Tests\TestCase;

class ValidMoneroAddressRuleTest extends TestCase
{
    // /** @test */
    // public function it_passes()
    // {
    //     $address = '888tNkZrPN6JsEgekjMnABU4TBzc2Dt29EPAvkRxbANsAnjyPbb3iQ1YBRk1UXcdRsiKc9dhwMVgN5S9cQUiyoogDavup3H';
    //     if(app()->environment('testing'))
    //         $this->assertFalse(
    //             (new ValidMoneroAddressRule())->passes('address', $address),
    //         );
    //     else
    //         $this->assertTrue(
    //             (new ValidMoneroAddressRule())->passes('address', $address),
    //         );
    // }

    // /** @test */
    // public function it_fails_with_random_string()
    // {
    //     $address = Str::random(10);
    //     $this->assertFalse(
    //         (new ValidMoneroAddressRule())->passes('address', $address),
    //     );
    // }

    // /** @test */
    // public function it_fails_with_testnet_address()
    // {
    //     $address = '9vmn8Vyxh6JEVmPr4qTcj3ND3FywDpMXH2fVLLEARyKCJTc3jWjxeWcbRNcaa57Bj36cARBSfWnfS89oFVKBBvGTAegdRxG';
    //     $this->assertFalse(
    //         (new ValidMoneroAddressRule())->passes('address', $address),
    //     );
    // }

    /** @test */
    public function it_fails_with_stagenet_address()
    {
        $address = '59McWTPGc745SRWrSMoh8oTjoXoQq6sPUgKZ66dQWXuKFQ2q19h9gvhJNZcFTizcnT12r63NFgHiGd6gBCjabzmzHAMoyD6';
        if (app()->environment('testing')) {
            $this->assertTrue(
                (new ValidMoneroAddressRule())->passes('address', $address),
            );
        } else {
            $this->assertFalse(
                (new ValidMoneroAddressRule())->passes('address', $address),
            );
        }
    }
}
