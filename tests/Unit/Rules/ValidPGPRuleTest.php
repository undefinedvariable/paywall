<?php

namespace Tests\Unit\Rules;

use App\Rules\ValidPGPRule;
use Illuminate\Support\Str;
use Tests\TestCase;

class ValidPGPRuleTest extends TestCase
{
    /** @test */
    public function it_passes()
    {
        $pgp =
'-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2

mQINBFRPlbwBEAClPH/0j1UOpW9bUqQiWx6M8H1Lk9XX038eTehUVks5khpLuJPV
mycBgf+wlCAk+D87NCzoCROCeCzWGrz1zf/lbxkceJOU9lkpUNLZ2Y2qKmsc/ntH
/WIdBoL957bpHjgjySl06hFMKN038lji+yrUFZ+yT8+E3+iaxvDFSYTF4MxoTLAm
OjYnPg6HfvKN7UpkzMAumokdZLHUxDAt88S2zigbJPx7GhGZ1ZV1is9RUHovd4ef
VNrt8NVBSXAtd3eAk4oGpMlY3JM5DVsE4K7pd7ixNSo/8egEhi1HncLjWhL3NOlZ
Iq2SGVdDYFnNb1OdAv/k6QGS1h/M66Ttf5uQX3GA4YxiJ7Ucl0ziFcQGpDHskZZ+
iOGSIxrf2SzpaQVA7Auo4W3Kn2BFxJ26aIwbaibRAPB76VBtYjheuGhIweCF/Kpf
TdR457WWzWTrLfcELOLs8WZXIcSWwDm/UNVdkcdfkZN8Q/V+pYcEAVFlEAqQntBx
v9gYW/9vQgH3c+iVtPqdioI/ZBgg5vGlbpXRFnjUmGNa8CpaAjSCmRqUEYMjpsi8
RiZAokLm7vTxAqe0wtqVGwHTFRiqM9FkIZ/cCn9agG9uG/vCsGVNOEYkUpCnaANN
xvOaYM++dDpD+GOye3QAe8Ryhh+ze+inaBIRoStDuD8LpSqJP8NQDt749QARAQAB
tC9JRkFDIFNlY3JldGFyaWF0IDxzZWNyZXRhcmlhdEBpZmFjLWNvbnRyb2wub3Jn
PokCPwQTAQIAKQUCVE+VvAIbIwUJCWYBgAcLCQgHAwIBBhUIAgkKCwQWAgMBAh4B
AheAAAoJEJDzOqcO2rjPxzgP/1xPARf7ioopHD5VcieojbAzktcSFU8TlJYoXnDo
MKODuabnTdWzO7LNr9ierpMhbpm8XomJwPDMWXaF06YQS+Jkx1Zwk/c0Ww4m052X
NyDdPb+CteNyTIgg0eYxEChjUP/q09094v6/r+1Dhs4AZOVz2huLwBPNSZgzivCj
YlFQpZ8gfiJ7nSQEFEjxk0hNUGGWiX0wKqO+/0QEbiX1V7ZwXe2OX03NxRMdWGyU
Jz40jd0Dv+28RqLtzFzIiXU1oBCu7YSJRpoVRQvD3hQVFOEuCGkqXYJmQO+QaFJf
awECT/00fy6yxkUBEjGyMvcOASVSZQvFu1I3rADHWk7IUnY50i2X/wqLefSZBb2Z
HRMG6JcTFm3DcvNwec7lKhR/X13qAA+sEInCmV5qHiyxxjPkyeOayGWvoqMhX3jI
kdOMWfZmTDrAMZPHHkJikjuxGMy7mRcBzDQSfoLFy2VnpuuTtZSu1Nbbl2PAWiVm
jPxgGQLctg9/f0nJDBvJW2rEH2QIm2IWZkEAq5g7V5/ATuV9oHb7d0TiB3djzL6G
1hYG/YqGZkVHZpfBL1Uu3CJCRvGG7G/ATDEWUpgfCamlbhIAAFmE7iYzM9oKOErx
vOF/TxoCpujn5Ezd+J6OWyujzcr43QnDxdAl0Unadki8XOv5fqTvCM7BDtC/y72q
NM+PuQINBFRPlbwBEACyWdKSzjKnjjGUZUd5dnHOoheSL6QuroTwdFlSKKNVv1Y6
KlTaPdDwtXkZF1BtLm6y3UesY3N31NjkaGsDztOg3uZ/GBOAdayF8KXSjO/Q8YF0
SSxC6XiePuXt6B0j2wqdxJkiAXzbDJ0J5FwBD+8dMg0XwhB74sgFV8xcco1FwG4w
bKtJC7Om2UZJrotU3KWVeYoLuBFr2+wKFNQVUpHrM/Esdwm/cI94gUO6t+bBWdvX
h1W6PgX6FxtGmwKkNAIZEmAIPkWUo4PAclpXh4uR9efDsz+TWJZeWlNSKFP6OxBs
P2EnVF8kqG1nC3bROvWFtNKyWa/f+EaJvXtByosijNlJviMoRQJHF1XQNwDl51N2
l6lOM9+cIz7LIFOz5HMcPU0nqlcJ1JnA8e/nnyh3LAK5KNeZLW+66MdRR+cJw7XU
FSWdz9Nmz9N3clzSrKmhEOLbtf/9Evso03MuPlp3Xun7osoLFK4smAhaLThL+Oco
Jpb73KX3DLrVu3gDsBVTQ0hONz3RV8nMMsJI7X8afbSOGqPTrQAA9GKFJhYA0KcL
OYSpbcwA5nUSl7jT/mTxzmkOcVC9vFi4wkMZWBWgHYkMbhV7VUqvjQOqnM3f9f0y
UVf7A0wTJvOqP2uou0Rvsebm0hk0Go+BFqY5FOrQbp1DcyHMuGxVmmRt8AugywAR
AQABiQIlBBgBAgAPBQJUT5W8AhsMBQkJZgGAAAoJEJDzOqcO2rjPeJIP/Rnvwfp4
WBnF1MxmvLD+u1nuaROTX44cNIk4YC5pKcPx7YJwHQFHVPXviFyEN9DAf6iMItwF
nqKgEm49SDdRIluDLaG91zPd8TIy7AnLlgenlTQMzAGsRtM4DW3AVj3/S4iSNCGa
2MISgmq125u0k3U0jwi4bEo+IBemSckbuAjsDByP0bDAjyRJ5DlEwbywbeobkimG
eVokQ4L3sLb4sQMA3aKgt+U6X5j+T6NWnOcwuEmOJHFVTGTb8OYvXb+q2xThjik4
8wI9WYC4YExdgNp8VH2HpXihO1lUvPlFLggqy1WdVZ08ihl7u2F1SuYdgNuEz614
eOSBu+NyGssBmTyeUMuI3aXFrAFhRqLDxn1pzr6Fb3XrJuoXGPViNiFcDvg08CjX
83rE/m7+AQMy9VE0rbffk/9BAL6DqrtgoDAExMNSjxsNWPW+mz355bjGgdSnjhuN
aKRMa2txoodyWB6SUsaxqyWlcm6QghGTu6J/5f6BA4ega3a0F9rl3kttWET3rScX
3dRBbEySAAzePmTKXhQoCwEApQM7dbWCLqUuhVqabxYNsotzpO3elTkP9FvJAZjJ
1oU+mRynyPtWUPh9htwIduAhiUgSNAQsfhFvAOFv0IWx6KuEgPOMh9Ng2IYxUxWO
ymkdogaEzX81GesQ2zMTU4gQghPgGU6V320J
=JdAB
-----END PGP PUBLIC KEY BLOCK-----';
        $this->assertTrue(
            (new ValidPGPRule())->passes('pgp', $pgp),
        );
    }

    /** @test */
    public function it_fails_if_empty()
    {
        $pgp = '';
        $this->assertFalse(
            (new ValidPGPRule())->passes('pgp', $pgp),
        );
    }

    /** @test */
    public function it_fails_if_invalid_key_is_passed()
    {
        $pgp = '-----BEGIN PGP PUBLIC KEY BLOCK-----'.Str::random(10).'-----END PGP PUBLIC KEY BLOCK-----';
        $this->assertFalse(
            (new ValidPGPRule())->passes('pgp', $pgp),
        );
    }
}
