<?php

namespace App\Listeners;

use App\Enums\Tier;
use App\Models\UserTier;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        UserTier::create([
            'user_id' => $event->user->id,
            'credits' => config('api.free_credits'),
            'tier' => Tier::FREE,
            'tier_expiration' => Carbon::now()->addMonths(config('api.tier_expiration')),
        ]);
    }
}
