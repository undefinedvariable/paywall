<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Cookie;

class LoginListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = $event->user;
        $user->last_login = date('Y-m-d H:i:s');
        $user->save(['timestamps' => false]);

        activity()
        ->causedBy($user)
        ->log('login');

        if(Cookie::has('sameuser'))
        {
            $old_username = Crypt::decryptString(Cookie::get('sameuser'));
            if($old_username != $user->username)
            {
                activity()
                ->causedBy($user)
                ->log('previously logged in as '.$old_username);
            }
        } else {
            Cookie::queue('sameuser', Crypt::encryptString($user->username));
        }
    }
}
