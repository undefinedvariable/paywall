<?php

namespace App\Console;

use App\Jobs\General\PaymentRequestsExpirer;
use App\Jobs\General\TierExpirer;
use App\Jobs\General\UsersHousekeeper;
use App\Jobs\General\WalletsHousekeeper;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('quicksand:run')->dailyAt('00:00')->withoutOverlapping();
        $schedule->command('schedule-monitor:sync')->dailyAt('02:00')->withoutOverlapping();
        $schedule->command('schedule-monitor:clean')->dailyAt('04:00')->withoutOverlapping();
        $schedule->command('disk-monitor:record-metrics')->dailyAt('07:00')->withoutOverlapping();
        $schedule->command('clean:models')->dailyAt('08:00')->withoutOverlapping();
        $schedule->command('passport:purge')->dailyAt('09:00')->withoutOverlapping();

        // General site-wide jobs
        $schedule->job(new UsersHousekeeper)->dailyAt('10:00')->withoutOverlapping();
        $schedule->job(new TierExpirer)->dailyAt('12:00')->withoutOverlapping();
        $schedule->job(new WalletsHousekeeper)->dailyAt('20:00')->withoutOverlapping();
        $schedule->job(new PaymentRequestsExpirer)->hourly()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
