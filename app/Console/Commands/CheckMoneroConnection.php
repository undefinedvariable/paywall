<?php

namespace App\Console\Commands;

use App\Services\MoneroRPC\Monero;
use Illuminate\Console\Command;

class CheckMoneroConnection extends Command
{
    protected $signature = 'check:monero';

    protected $description = 'Check if can connect to Monero RPC daemon';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        /*
        $res = Monero::validate_address('59McWTPGc745SRWrSMoh8oTjoXoQq6sPUgKZ66dQWXuKFQ2q19h9gvhJNZcFTizcnT12r63NFgHiGd6gBCjabzmzHAMoyD6');
        if ($res) {
            $this->info('ok');
        } else {
            $this->error('connection error');
        }
        */

        $res = Monero::get_address('test_wallet');
        dd($res);
        if ($res) {
            $this->info('ok');
        } else {
            $this->error('connection error');
        }

        return 0;
    }
}
