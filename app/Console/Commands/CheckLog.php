<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if logging works';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::error('error');
        Log::warning('warning');
        Log::info('info');
        Log::debug('debug');

        return 0;
    }
}
