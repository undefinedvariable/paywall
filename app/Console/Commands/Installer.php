<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Installer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CLI Installer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->welcome();

        return 0;
    }

    protected function welcome()
    {
        $this->info('Welcome to the Installer!');
        $this->line(' ');
        $this->info('Scanning system...');
        $this->preflightCheck();
    }

    protected function preflightCheck()
    {
        $this->line(' ');
        $this->info('Checking for installed dependencies...');
        $this->checkPhpDependencies();
        $this->checkPermissions();
        $this->envCheck();
    }

    protected function checkPhpDependencies()
    {
        $extensions = [
            'bcmath',
            'ctype',
            'curl',
            'json',
            'mbstring',
            'openssl',
        ];

        $this->line('');
        $this->info('Checking for required php extensions...');
        foreach ($extensions as $ext) {
            if (extension_loaded($ext) == false) {
                $this->error("- {$ext} extension not found, aborting installation");
                exit;
            } else {
            }
        }
        $this->info('Required PHP extensions found!');
    }

    protected function checkPermissions()
    {
        $this->line('');
        $this->info('Checking for proper filesystem permissions...');

        $paths = [
            base_path('bootstrap'),
            base_path('storage'),
        ];

        foreach ($paths as $path) {
            if (is_writeable($path) == false) {
                $this->error('- Invalid permission found! Aborting installation.');
                $this->error('  Please make the following path writeable by the web server:');
                $this->error("  $path");
                exit;
            } else {
                $this->info("- Found valid permissions for {$path}");
            }
        }
    }

    protected function envCheck()
    {
        if (! file_exists(base_path('.env')) || filesize(base_path('.env')) == 0) {
            $this->line('');
            $this->info('No .env configuration file found. We will create one now!');
            $this->createEnv();
        } else {
            $confirm = $this->confirm('Found .env file, do you want to overwrite it?');
            if (! $confirm) {
                $this->info('Cancelling installation.');
                exit;
            }
            $confirm = $this->confirm('The application may be installed already, are you really sure you want to overwrite it?');
            if (! $confirm) {
                $this->info('Cancelling installation.');
                exit;
            }
            $this->createEnv();
        }
        $this->postInstall();
    }

    protected function createEnv()
    {
        $this->line('');
        // copy env
        if (! file_exists(app()->environmentFilePath())) {
            exec('cp .env.example .env');
            $this->call('key:generate');
        }

        $name = $this->ask('Site name');
        $this->updateEnvFile('APP_NAME', $name ?? 'test');

        $domain = $this->ask('Site url [ex: test.com]');
        $this->updateEnvFile('APP_URL', 'http://'.$domain ?? 'http://localhost:8000');

        $environment = $this->choice('Select environment', ['local', 'production']);
        $this->updateEnvFile('APP_ENV', $environment ?? 'local');

        $logs = $this->choice('Select log channel', ['stack', 'single', 'daily']);
        $this->updateEnvFile('LOG_CHANNEL', $logs ?? 'daily');

        $debug = $this->choice('Enable debug', ['true', 'false']);
        $this->updateEnvFile('APP_DEBUG', $debug ?? 'true');
        if ($debug && $environment == 'local') {
            $this->updateEnvFile('TELESCOPE_ENABLED', 'true');
        } else {
            $this->updateEnvFile('TELESCOPE_ENABLED', 'false');
        }

        $termination = $this->secret('Site termination password');
        $this->updateEnvFile('APP_TERMINATION_PASS', $termination);

        $model_cache = $this->choice('Enable model cache', ['true', 'false']);
        $this->updateEnvFile('MODEL_CACHE_ENABLED', $model_cache ?? 'true');

        if ($model_cache) {
            $model_cache_driver = $this->choice('Select model cache driver', ['redis', 'file', 'database', 'memcached', 'array']);
            $this->updateEnvFile('MODEL_CACHE_STORE', $model_cache_driver ?? 'array');
        }

        $database = $this->choice('Select database driver', ['mysql', 'pgsql']);
        $this->updateEnvFile('DB_CONNECTION', $database ?? 'mysql');
        switch ($database) {
            case 'mysql':
                $database_host = $this->ask('Select database host', '127.0.0.1');
                $this->updateEnvFile('DB_HOST', $database_host ?? 'mysql');

                $database_port = $this->ask('Select database port');
                $this->updateEnvFile('DB_PORT', $database_port ?? 3306);

                $database_db = $this->ask('Select database', 'db');
                $this->updateEnvFile('DB_DATABASE', $database_db ?? 'db');

                $database_username = $this->ask('Select database username');
                $this->updateEnvFile('DB_USERNAME', $database_username ?? 'root');

                $database_password = $this->secret('Select database password');
                $this->updateEnvFile('DB_PASSWORD', $database_password ?? 'password');
            break;

        }

        $queue = $this->choice('Select queue connection', ['redis', 'apc', 'array', 'database', 'file', 'memcached']);
        $this->updateEnvFile('QUEUE_CONNECTION', $queue ?? 'database');

        $cache = $this->choice('Select cache driver', ['redis', 'apc', 'array', 'database', 'file', 'memcached']);
        $this->updateEnvFile('CACHE_DRIVER', $cache ?? 'database');

        $session = $this->choice('Select session driver', ['redis', 'file', 'cookie', 'database', 'apc', 'memcached', 'array']);
        $this->updateEnvFile('SESSION_DRIVER', $cache ?? 'database');

        if ($cache == 'redis' || $session == 'redis' || $queue == 'redis') {
            $redis_host = $this->ask('Set redis host', 'localhost');
            $this->updateEnvFile('REDIS_HOST', $redis_host);

            $redis_password = $this->ask('Set redis password');
            $this->updateEnvFile('REDIS_PASSWORD', $redis_password ?? '');

            $redis_port = $this->ask('Set redis port');
            $this->updateEnvFile('REDIS_PORT', $redis_port ?? 6379);
        }

        $session_lifetime = $this->ask('Session lifetime [minutes]');
        $this->updateEnvFile('SESSION_LIFETIME', $session_lifetime ?? 60);

        $telegram = $this->ask('Set Telegram bot token');
        $this->updateEnvFile('TELEGRAM_BOT_TOKEN', $telegram ?? '');
    }

    protected function updateEnvFile($key, $value)
    {
        $envPath = app()->environmentFilePath();
        $payload = file_get_contents($envPath);

        if ($existing = $this->existingEnv($key, $payload)) {
            $payload = str_replace("{$key}={$existing}", "{$key}=\"{$value}\"", $payload);
            $this->storeEnv($payload);
        } else {
            $payload = $payload."\n{$key}=\"{$value}\"\n";
            $this->storeEnv($payload);
        }
    }

    protected function existingEnv($needle, $haystack)
    {
        preg_match("/^{$needle}=[^\r\n]*/m", $haystack, $matches);
        if ($matches && count($matches)) {
            return substr($matches[0], strlen($needle) + 1);
        }

        return false;
    }

    protected function storeEnv($payload)
    {
        $file = fopen(app()->environmentFilePath(), 'w');
        fwrite($file, $payload);
        fclose($file);
    }

    protected function postInstall()
    {
        $this->callSilent('config:cache');
        $this->callSilent('route:cache');
        $this->info('The website has been successfully installed!');
    }
}
