<?php

namespace App\Console\Commands;

use App\Enums\AccountType;
use App\Models\User;
use App\Notifications\PromotedToStaff;
use Illuminate\Console\Command;

class ToggleStaff extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:staff {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Toggle staff privileges';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        $user = User::where('username', $id)->orWhere('id', $id)->first();
        if (! $user) {
            $this->error('Could not find any user with that username or id!');
            exit;
        }
        $this->info('Found username: '.$user->username);

        if ($user->isMerchant()) {
            $this->error('Merchants cannot be staff members!');
            exit;
        }
        if ($user->isStaff()) {
            $confirmed = $this->confirm('Remove staff privileges from this user?');
            if (! $confirmed) {
                exit;
            }
            $user->update(['account' => AccountType::USER]);
        } else {
            $confirmed = $this->confirm('Add staff privileges to this user?');
            if (! $confirmed) {
                exit;
            }
            $user->update(['account' => AccountType::STAFF]);

            $user->notify(new PromotedToStaff($user));
        }

        $this->info('Successfully changed permissions!');
    }
}
