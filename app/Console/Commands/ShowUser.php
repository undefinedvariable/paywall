<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class ShowUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:show {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show user info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        $user = User::where('username', $id)->orWhere('id', $id)->first();
        if (! $user) {
            $this->error('Could not find any user with that username or id.');
            exit;
        }

        $this->info('User ID: '.$user->id);
        $this->info('Username: '.$user->username);
        $this->info('Joined: '.$user->created_at->diffForHumans());
        $this->info('Status: '.$user->status);
    }
}
