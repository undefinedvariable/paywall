<?php

namespace App\Console\Commands;

use App\Services\PGP\PGP;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CheckPGP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:pgp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check PGP working';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PGP $pgp)
    {
        $publickey = Storage::disk('local')->get('keys/test_pgp_public.key');
        $key = $pgp->loadKey($publickey);
        $code = random_int(100000, 999999);
        $message = 'Hello World #'.$code;
        $msg = $pgp->encryptMsg($key, $message);
        $this->line($msg);
        if ($this->ask('Insert the code') == $code) {
            $this->info('ok');
        } else {
            $this->error('err');
        }
    }
}
