<?php

namespace App\Console\Commands;

use App\Services\MoneroRPC\Monero;
use Illuminate\Console\Command;

class CheckMultisig extends Command
{
    protected $signature = 'check:multisig {filename}';

    protected $description = 'Check if a Monero wallet is multisig';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $filename = $this->argument('filename');

        if (Monero::is_multisig($filename)) {
            $this->info('wallet is multisig');
        } else {
            $this->error('wallet is NOT multisig');
        }

        return 0;
    }
}
