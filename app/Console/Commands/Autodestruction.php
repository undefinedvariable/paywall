<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class Autodestruction extends Command
{
    /**
     * Hide from command list
     * 
     * @var bool
     */
    protected $hidden = true;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autodestruction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Do you wish to continue?')) {
            // change app key
            \Artisan::call('key:generate', ['--force' => true]);

            // drop all tables
            \Artisan::call('migrate:reset', ['--force' => true]);

            // clear all cache
            \Artisan::call('cache:clear', ['--force' => true]);

            // remove config file
            $removeEnv = new Process(['rm', '.env']);
            $removeEnv->mustRun();

            // remove token keys
            $removePassportKeys = new Process(['rm', 'storage/oauth*']);
            $removePassportKeys->mustRun();
        }
    }
}
