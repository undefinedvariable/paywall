<?php

namespace App\Console\Commands;

use App\Services\CryptoRates\CryptoRatesContract;
use App\Services\MoneroRPC\Monero;
use Illuminate\Console\Command;

class GenericTester extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generic tests';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(CryptoRatesContract $service)
    {
        $this->line($service->usd_to_xmr());

        return 0;
    }
}
