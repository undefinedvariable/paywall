<?php

namespace App\Console\Commands;

use App\Enums\AccountType;
use App\Models\User;
use Illuminate\Console\Command;

class MakeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a user a global admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('id');
        $user = User::where('username', $id)->orWhere('id', $id)->first();
        if (! $user) {
            $this->error('Could not find any user with that username or id.');
            exit;
        }
        $this->info('Found user with username: '.$user->username);
        if ($this->confirm('Are you sure you want to make this user a global admin?')) {
            $user->update(['account' => AccountType::ADMIN]);
            $this->info('User account has been promoted to Admin.');
        }

        return 0;
    }
}
