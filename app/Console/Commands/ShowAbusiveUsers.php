<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class ShowAbusiveUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:abuses {limit=10}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List users by reputation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = (int) $this->argument('limit');
        $headers = ['ID', 'Username', 'Reputation', 'Joined'];
        $users = User::where('reputation', '<', 0)->orderByDesc('reputation')->take($limit)->get(['id', 'username', 'reputation', 'created_at'])->toArray();
        $this->table($headers, $users);
    }
}
