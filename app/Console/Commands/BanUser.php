<?php

namespace App\Console\Commands;

use App\Models\States\User\Banned;
use App\Models\User;
use Illuminate\Console\Command;

class BanUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:ban {id}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ban a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        $user = User::where('username', $id)->orWhere('id', $id)->first();
        if (! $user) {
            $this->error('Could not find any user with that username or id.');
            exit;
        }
        $this->info('Found user with username: '.$user->username);
        if ($this->confirm('Are you sure you want to ban this user?')) {
            $user->transitionTo(Banned::class);
            $this->info('User account has been banned.');
        }
    }
}
