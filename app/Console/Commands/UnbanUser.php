<?php

namespace App\Console\Commands;

use App\Models\States\User\Active;
use App\Models\User;
use Illuminate\Console\Command;

class UnbanUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:unban {id}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unban a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        $user = User::where('username', $id)->orWhere('id', $id)->first();
        if (! $user) {
            $this->error('Could not find any user with that username or id.');
            exit;
        }
        $this->info('Found user with username: '.$user->username);
        if ($this->confirm('Are you sure you want to unban this user?')) {
            $user->transitionTo(Active::class);
            $this->info('User account has been unbanned.');
        }
    }
}
