<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class CheckTor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:tor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Tor connection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = Http::get('https://check.torproject.org/api/ip');

        if ($response->status() == 200) {
            $arr = json_decode($response->body(), true);

            if ($arr['IsTor'] == true) {
                $this->info('ok');
            } else {
                $this->error('error');
            }
        } else {
            $this->error('connection error');
        }
    }
}
