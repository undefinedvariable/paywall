<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class CheckIP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:ip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the current IP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = Http::get('https://api.ipify.org');

        if ($response->status() == 200) {
            $this->info($response->body());
        } else {
            $this->error('Connection error');
        }
    }
}
