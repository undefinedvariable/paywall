<?php

namespace App\Console\Commands;

use App\Events\TestWebsocketEvent;
use Illuminate\Console\Command;

class TestWebsocketBroadcast extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:ws';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Websocket Broadcast';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        event(new TestWebsocketEvent('test msg'));

        return 0;
    }
}
