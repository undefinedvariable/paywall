<?php

namespace App\Rules;

use App\Services\MoneroRPC\Monero;
use Illuminate\Contracts\Validation\Rule;

class ValidMoneroAddressRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */

    /**
     * @todo TBC
     */
    public function passes($attribute, $value)
    {
        return true;
        $res = Monero::validate_address($value);
        if (empty($res)) {
            return false;
        }

        return $res['valid'] && $res['nettype'] === config('monero.net_type');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid Monero address.';
    }
}
