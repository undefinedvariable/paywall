<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidPGPRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $key = \OpenPGP_Message::parse(
            \OpenPGP::unarmor($value, 'PGP PUBLIC KEY BLOCK')
        );

        return $key != null;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid PGP key.';
    }
}
