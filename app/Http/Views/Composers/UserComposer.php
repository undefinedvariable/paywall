<?php

namespace App\Http\Views\Composers;

use Illuminate\View\View;

class UserComposer
{
    protected $user;

    public function __construct()
    {
        $this->user = auth()->user();
    }

    /**
     * @brief Passes the current logged user to all views
     */
    public function compose(View $view)
    {
        return $view->with('currentUser', $this->user);
    }
}
