<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Dispute extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'payment_request_id'    => $this->payment_request_id,
            'user_notes'            => $this->user_notes,
            'merchant_notes'        => $this->merchant_notes,
            'status'                => $this->status,
            'created'               => $this->created_at,
            'updated'               => $this->updated_at,
        ];
    }
}
