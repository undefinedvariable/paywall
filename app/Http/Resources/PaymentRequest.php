<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PaymentRequest extends JsonResource
{
    public function toArray($request)
    {
        $whoami = 'UNKNOWN';
        if (Auth::user()->id == $this->merchant->user->id) {
            $whoami = 'MERCHANT';
        } elseif ($this->user != null && Auth::user()->id == $this->user->id) {
            $whoami = 'CUSTOMER';
        }

        $arr = [
            'id' => $this->id,
            'orderid' => $this->orderid,
            'whoami' => $whoami,
            'merchant' => $this->merchant->user->username,
            'amount_usd' => $this->amount_usd,
            'shipping_usd' => $this->shipping_usd,
            'goods_type' => strtoupper($this->goods_type->key),
            'payment_type' => strtoupper($this->payment_type->key),
            'url' => $this->url,
            'status' => strtoupper($this->status),
            'expires_at' => $this->created_at->addDays(config('expiration.payment.pending')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if ($this->item_list != null) {
            $arr['item_list'] = $this->item_list;
        } else {
            $arr['description'] = $this->description;
        }

        if ($this->coupon != null) {
            $arr['applied_coupon'] = $this->coupon->code;
            $arr['applied_discount'] = $this->coupon->discount;
        }

        if ($this->marketplace_id != null) {
            $arr['marketplace'] = $this->marketplace->user->username;
        }

        if ($this->user_id != null) {
            $arr['customer'] = $this->user->username;
            $arr['notes'] = $this->notes;
        }

        return $arr;
    }
}
