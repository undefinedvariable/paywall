<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class Transfer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->isMarketplaceInvolved()) {
            return [
                'payment_request_id'    => $this->id,
                'total_xmr'             => $this->getMerchantPayoutInXMR(),
                'commission_xmr'        => $this->getCommissionInXMR(),
                'closing_address'       => $this->merchant->personal_wallet_address,
                'refunding_address'     => $this->user->refund_address,
                'commission_address'    => $this->getCommissionAddress(),
            ];
        } else {
            return [
                'payment_request_id'    => $this->id,
                'total_xmr'             => $this->getMerchantPayoutInXMR(),
                'closing_address'       => $this->merchant->personal_wallet_address,
                'refunding_address'     => $this->user->refund_address,
            ];
        }
    }
}
