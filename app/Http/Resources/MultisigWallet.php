<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class MultisigWallet extends JsonResource
{
    public function toArray($request)
    {
        if ($this->isCreated()) {
            $multisig_info = Auth::user()->isMerchantInPayment($this->paymentRequest) ? $this->prepare_multisig_data_vendor : $this->prepare_multisig_data_customer;

            return [
                'payment_request_id'            => $this->paymentRequest->id,
                'multisig_info'                 => $multisig_info,
                'status'                        => $this->status,
            ];
        } elseif ($this->isPrepared()) {
            $multisig_info = Auth::user()->isMerchantInPayment($this->paymentRequest) ? $this->make_multisig_data_vendor : $this->make_multisig_data_customer;

            $multisig_info_peers = [$this->prepare_multisig_data_system];
            if (Auth::user()->isMerchantInPayment($this->paymentRequest)) {
                array_push($multisig_info_peers, $this->prepare_multisig_data_customer);
            } else {
                array_push($multisig_info_peers, $this->prepare_multisig_data_vendor);
            }

            return [
                'payment_request_id'            => $this->paymentRequest->id,
                'multisig_info'                 => $multisig_info,
                'multisig_info_peers'           => Arr::remove_null($multisig_info_peers),
                'status'                        => $this->status,
            ];
        } elseif ($this->isMade()) {
            $multisig_info = Auth::user()->isMerchantInPayment($this->paymentRequest) ? $this->finalize_multisig_data_vendor : $this->finalize_multisig_data_customer;

            $multisig_info_peers = [$this->make_multisig_data_system];
            if (Auth::user()->isMerchantInPayment($this->paymentRequest)) {
                array_push($multisig_info_peers, $this->make_multisig_data_customer);
            } else {
                array_push($multisig_info_peers, $this->make_multisig_data_vendor);
            }

            return [
                'payment_request_id'            => $this->paymentRequest->id,
                'multisig_address'              => $multisig_info,
                'multisig_info_peers'           => Arr::remove_null($multisig_info_peers),
                'status'                        => $this->status,
            ];
        } else {
            return [
                'payment_request_id'            => $this->paymentRequest->id,
                'address'   => $this->address,
                'status'    => $this->status,
            ];
        }
    }
}
