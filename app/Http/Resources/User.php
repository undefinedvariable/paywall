<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'language' => strtoupper($this->language->key),
            'account' => strtoupper($this->account->key),
            'flower' => strtoupper($this->flower->key),
            'reputation' => $this->reputation,
            'created' => $this->created_at,
            'updated' => $this->created_at,
        ];
    }
}
