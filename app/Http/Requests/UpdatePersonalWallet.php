<?php

namespace App\Http\Requests;

use App\Rules\ValidMoneroAddressRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePersonalWallet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isMerchant();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(ValidMoneroAddressRule $validateXMR)
    {
        return [
            'personal_view_key' => ['required', 'alphanum', 'min:60', 'max:100'],
            'personal_wallet_address' => ['required', 'alphanum', 'min:90', 'max:110', $validateXMR],
        ];
    }
}
