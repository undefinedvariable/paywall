<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePswd extends FormRequest
{
    /**
     * @brief Everyone can perform this action
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password'  => 'required|min:8|max:50',
            'password'      => 'required|min:8|max:50|confirmed',
        ];
    }
}
