<?php

namespace App\Http\Requests;

use App\Enums\Language;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class ChangeLanguage extends FormRequest
{
    /**
     * @brief Only logged-in users can perform this action
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'language' => ['required', new EnumValue(Language::class, false)],
        ];
    }
}
