<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCryptocurrency extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => ['required', 'alpha', 'max:20'],
            'abbreviation'      => ['required', 'alpha', 'max:10'],
            'fee'               => ['required', 'numeric'],
            'min_val'           => ['required', 'numeric'],
        ];
    }
}
