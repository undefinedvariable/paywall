<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMerchant extends FormRequest
{
    /**
     * @brief Only merchants can perform this action
     */
    public function authorize()
    {
        return auth()->user()->isMerchant();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'website'           => ['nullable', 'url', 'max:190'],
            'ipn_enable'        => ['nullable', 'boolean'],
            'ipn_url'           => ['nullable', 'required_if:ipn_enable,1', 'url', 'max:190'],
        ];
    }
}
