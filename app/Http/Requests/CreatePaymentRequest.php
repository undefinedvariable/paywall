<?php

namespace App\Http\Requests;

use App\Enums\GoodsType;
use App\Enums\PaymentType;
use App\Rules\DoesNotContainUrlRule;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class CreatePaymentRequest extends FormRequest
{
    /**
     * @brief Only merchants can perform this action
     */
    public function authorize()
    {
        return auth()->user()->isMerchant();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orderid'                       => ['nullable', 'string', 'max:50'],
            'amount_usd'                    => ['required', 'numeric', 'between:1,99999999'],
            'shipping_usd'                  => ['required', 'numeric', 'between:0,99999999'],
            'goods_type'                    => ['required', new EnumValue(GoodsType::class, false)],
            'payment_type'                  => ['required', new EnumValue(PaymentType::class, false)],
            'description'                   => ['required', 'string', 'max:500', new DoesNotContainUrlRule()],
            'success_url'                   => ['nullable', 'url', 'max:190'],
            'cancel_url'                    => ['nullable', 'url', 'max:190', 'different:success_url'],
        ];
    }
}
