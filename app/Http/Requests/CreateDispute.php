<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDispute extends FormRequest
{
    /**
     * @brief Only logged-in users can perform this action
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_request_id'    => ['required', 'uuid', 'exists:payment_requests,id'],
            'user_notes'            => ['required', 'string', 'min:10', 'max:5000'],
        ];
    }
}
