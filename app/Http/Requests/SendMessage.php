<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendMessage extends FormRequest
{
    /**
     * @brief Everyone can perform this action
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact'           => ['required', 'string', 'max:100'],
            'contact_type'      => ['required', 'string', 'max:200'],
            'body'              => ['required', 'string', 'min:10', 'max:1000'],
            'robot'             => ['required', 'numeric', 'size:1'],
        ];
    }
}
