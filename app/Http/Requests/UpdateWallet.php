<?php

namespace App\Http\Requests;

use App\Rules\ValidMoneroAddressRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateWallet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isMarketplace();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(ValidMoneroAddressRule $validateXMR)
    {
        return [
            'wallet_address' => ['required', 'alphanum', 'min:90', 'max:110', $validateXMR],
        ];
    }
}
