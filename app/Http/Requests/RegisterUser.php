<?php

namespace App\Http\Requests;

use App\Enums\Flower;
use App\Rules\ValidMoneroAddressRule;
use App\Rules\ValidPGPRule;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class RegisterUser extends FormRequest
{
    /**
     * @brief Everyone can perform this action
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(ValidMoneroAddressRule $validateXMR)
    {
        return [
            'pgp'                       => ['nullable', 'min:256', 'max:262144', 'string', new ValidPGPRule()],
            'flower'                    => ['required', new EnumValue(Flower::class, false)],
            'refund_address'            => ['required', 'alphanum', 'min:90', 'max:110', $validateXMR],
        ];
    }
}
