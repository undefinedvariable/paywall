<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCoupon extends FormRequest
{
    /**
     * @brief Only merchants can perform this action
     */
    public function authorize()
    {
        return auth()->user()->isMerchant();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'          => ['required', 'alphanum', 'min:3', 'max:50', 'unique:coupons'],
            'discount'      => ['required', 'numeric', 'between:1,100'],
            'usage_limit'   => ['required', 'numeric', 'between:1,100000'],
            'expires_at'    => ['required', 'date', 'after_or_equal:now'],
        ];
    }
}
