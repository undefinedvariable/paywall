<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMerchant extends FormRequest
{
    /**
     * @brief Only staff members can perform this request
     */
    public function authorize()
    {
        return auth()->user()->isStaff() || auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['required', 'numeric', 'exists:users'],
        ];
    }
}
