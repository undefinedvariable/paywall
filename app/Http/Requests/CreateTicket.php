<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTicket extends FormRequest
{
    /**
     * @brief Only logged-in users can perform this action
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_category_id'    => ['required', 'numeric', 'exists:ticket_categories,id'],
            'title'                 => ['required', 'string', 'min:3', 'max:190'],
            'message'               => ['required', 'string', 'min:10', 'max:5000'],
        ];
    }
}
