<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplyCoupon extends FormRequest
{
    /**
     * @brief Only logged-in users can perform this action
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'coupon'                => ['required', 'alpha', 'max:50', 'exists:coupons,code'],
            'payment_request_id'    => ['required', 'uuid', 'exists:payment_requests,id'],
        ];
    }
}
