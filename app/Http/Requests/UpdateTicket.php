<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTicket extends FormRequest
{
    /**
     * @brief Only staff members can perform this request
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_id'     => ['required', 'numeric', 'exists:tickets,id'],
            'comment'       => ['required', 'string', 'min:10', 'max:5000'],
        ];
    }
}
