<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPswd extends FormRequest
{
    /**
     * @brief Everyone can perform this action
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token'         => ['required'],
            'password'      => 'required|min:8|max:50|confirmed',
        ];
    }
}
