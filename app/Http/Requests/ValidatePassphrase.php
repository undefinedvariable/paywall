<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidatePassphrase extends FormRequest
{
    /**
     * @brief Everyone can perform this action
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'      => ['required', 'max:50', 'string', 'exists:users'],
            'passphrase'    => ['required', 'string', 'max:200'],
        ];
    }
}
