<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMultisigWallet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'multisig_info'     => ['required_without:multisig_address', 'alphanum', 'min:6', 'max:250', 'unique:multisig_wallets,prepare_multisig_data_customer', 'unique:multisig_wallets,prepare_multisig_data_vendor', 'unique:multisig_wallets,make_multisig_data_customer', 'unique:multisig_wallets,make_multisig_data_vendor'],
            'multisig_address'  => ['required_without:multisig_info', 'alphanum', 'min:90', 'max:110'],
        ];
    }
}
