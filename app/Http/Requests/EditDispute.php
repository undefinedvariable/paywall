<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditDispute extends FormRequest
{
    /**
     * @brief Only merchants can perform this action
     */
    public function authorize()
    {
        return auth()->user()->isMerchant();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'merchant_notes' => ['required', 'string', 'min:10', 'max:5000'],
        ];
    }
}
