<?php

namespace App\Http\Requests;

use App\Enums\CommissionType;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMarketplace extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isMarketplace();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'redirect'          => ['nullable', 'url', 'max:190'],
            'ipn_enable'        => ['nullable', 'boolean'],
            'ipn_url'           => ['nullable', 'required_if:ipn_enable,1', 'url', 'max:190'],
            'commission'        => ['required', 'numeric', 'between:0,99'],
            'commission_type'   => ['required', new EnumValue(CommissionType::class, false)],
        ];
    }
}
