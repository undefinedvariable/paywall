<?php

namespace App\Http\Requests;

use App\Rules\ValidPGPRule;
use Illuminate\Foundation\Http\FormRequest;

class AddPgp extends FormRequest
{
    /**
     * @brief Only logged-in users can perform this action
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pgp' => ['required', 'string', 'min:256', 'max:262144', new ValidPGPRule()],
        ];
    }
}
