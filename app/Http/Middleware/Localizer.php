<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Support\Facades\Auth;

class Localizer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // If user is logged in
        if (Auth::check()) {
            // Get the user specific language
            $lang = Auth::user()->language->key;

            // Set the language
            App::setLocale($lang);
        }

        return $next($request);
    }
}
