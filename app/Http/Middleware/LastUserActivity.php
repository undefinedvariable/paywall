<?php

namespace App\Http\Middleware;

use Cache;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class LastUserActivity
{
    public function handle($request, Closure $next)
    {
        if (Auth::check() && ! Cache::has(Auth::user()->id.'_online')) {
            $expiresAt = Carbon::now()->addMinutes(5); // keep online for 5 min
            Cache::put(Auth::user()->id.'_online', true, $expiresAt);
        }

        return $next($request);
    }
}
