<?php

namespace App\Http\Middleware;

use Illuminate\Routing\Middleware\ThrottleRequests;
use RuntimeException;

class Throttler extends ThrottleRequests
{
    protected function resolveRequestSignature($request)
    {
        if ($user = $request->user()) {
            return sha1($user->getAuthIdentifier());
        }

        if ($route = $request->route()) {
            return sha1(uniqid()); // sha1($route->getDomain().'|'.$request->session()->getId());
        }

        throw new RuntimeException('Unable to generate fingerprint. Route unavailable.');
    }
}
