<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class CheckoutSteps
{
    private $exludePaths = [
        'checkout/cancel',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! in_array($request->path(), $this->exludePaths)) {
            $payment = $request->route()->parameter('payment');
            if ($payment != null) {
                $payment->update(['form_step' => Route::currentRouteName()]);

                // redirect to cancel page if payment has been cancelled
                if ($payment->isCancelled()) {
                    return redirect()->route('checkout.cancel', ['payment' => $payment]);
                }

                // if the user is not set or is a new one, redirect to the review page
                /*
                if ($payment->user_id == null || $payment->user_id != Auth::user()->id || $payment->form_step == null) {
                    return redirect($payment->url);
                }
                */
            }
        }

        return $next($request);
    }
}
