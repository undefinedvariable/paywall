<?php

namespace App\Http\Middleware;

use Illuminate\Routing\Middleware\ThrottleRequests;
use RuntimeException;

class ApiThrottler extends ThrottleRequests
{
    protected function resolveRequestSignature($request)
    {
        if ($route = $request->route()) {
            return sha1($route->getDomain().'|'.$request->header('Authorization'));
        }

        if ($user = $request->user()) {
            return sha1($user->getAuthIdentifier());
        }

        throw new RuntimeException('Unable to generate fingerprint. Route unavailable.');
    }
}
