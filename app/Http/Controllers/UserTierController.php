<?php

namespace App\Http\Controllers;

use App\Enums\Tier;
use App\Exceptions\WalletError;
use App\Http\Requests\ChangeTier;
use App\Jobs\CheckTierPayment;
use App\Models\Merchant;
use App\Services\MoneroRPC\Monero;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserTierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @brief Creates a subaddress where the merchant can send funds to pay for tier upgrade
     */
    public function generateDepositAddress(ChangeTier $request)
    {
        $user = Auth::user();
        $tier = Tier::coerce((int) $request->tier);
        $key = strtolower($tier->key);
        $amount = config('tiers.'.$key.'_threshold');

        if ($tier->is(Tier::FREE)) {
            $user->user_tier->update([
                'credits' => config('api.free_credits'),
                'tier' => Tier::FREE,
                'tier_expiration' => Carbon::now()->addMonths(config('api.tier_expiration')),
            ]);

            return redirect()->route('users.show', ['user' => $user->id])->with('message', __('Tier updated successfully.'));
        }

        try {
            $new_address = Monero::create_subaddress(config('monero.admin_wallet_filename'), $user->id.'_deposit_'.uniqid());
        } catch (WalletError $e) {
            Log::error('[EscrowController] exception: '.$e->getMessage());

            return redirect()->route('users.show', ['user' => $user->id])->withErrors('API error, retry');
        }

        CheckTierPayment::dispatch($user, $new_address['address_index']);

        return redirect()->route('users.show', ['user' => $user->id])->with('message', __('Deposit :amount XMR to address :address', ['amount' => $amount, 'address' => $new_address['address']]));
    }
}
