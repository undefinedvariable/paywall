<?php

namespace App\Http\Controllers;

use App\Enums\Language;
use App\Models\PaymentRequest;
use App\Models\States\Dispute\InReview;
use App\Models\States\Dispute\Open;
use App\Models\States\PaymentRequest\CustomerPayout;
use App\Models\States\PaymentRequest\Funded;
use App\Models\States\PaymentRequest\MerchantPayout;
use App\Models\States\PaymentRequest\Reported;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
        $this->middleware('auth');
    }

    /**
     * @brief Shows the specified user
     */
    public function show(User $user)
    {
        $languages = Language::asSelectArray();

        if ($user->pgp != null) {
            $publickey = Storage::disk('local')->get('keys/'.$user->username.'_pgp.key');
        } else {
            $publickey = '';
        }

        return view('users.show', compact('publickey', 'languages'));
    }

    /**
     * @brief Removes the specified user and all their PGP keys from storage
     */
    public function destroy(User $user)
    {
        if (session()->has('acting_as_user')) {
            session()->forget('acting_as_user');
        }

        if ($user->isMarketplace()) {
            if ($user->marketplace->merchants != null) {
                $merchants = $user->marketplace->merchants;

                foreach ($merchants as $merchant) {
                    if ($merchant->paymentRequests()->whereState('payment_requests.status', [Funded::class, Reported::class, MerchantPayout::class, CustomerPayout::class])->count() != 0) {
                        return redirect()->route('users.show', ['user' => $user])->withErrors(__('Cannot delete the user, there are still payments being processed.'));
                    }
                }
            }
        } elseif ($user->isMerchant()) {
            if ($user->merchant->disputes != null) {
                $open_disputes = $user->merchant->disputes()->whereState('disputes.status', Open::class)->count();
                $inreview_disputes = $user->merchant->disputes()->whereState('disputes.status', InReview::class)->count();

                if (($open_disputes + $inreview_disputes) > 0) {
                    return redirect()->route('users.show', ['user' => $user])->withErrors(__('Cannot delete the user, there are still open disputes.'));
                }
            }

            if ($user->merchant->paymentRequests != null) {
                $total_payment_requests = PaymentRequest::where('merchant_id', $user->merchant->id)->count();
                $closed_payment_requests = PaymentRequest::where('merchant_id', $user->merchant->id)->closed()->count();
                $refunded_payment_requests = PaymentRequest::where('merchant_id', $user->merchant->id)->refunded()->count();

                if (($total_payment_requests - $closed_payment_requests - $refunded_payment_requests) > 0) {
                    return redirect()->route('users.show', ['user' => $user])->withErrors(__('Cannot delete the user, there are still payments being processed.'));
                }
            }
        } else {
            // user
            if ($user->disputes != null) {
                $open_disputes = $user->disputes()->open()->count();
                /** @todo fix N+1 error */
                $inreview_disputes = $user->disputes()->inReview()->count();

                if ($open_disputes + $inreview_disputes > 0) {
                    return redirect()->route('users.show', ['user' => $user])->withErrors(__('Cannot delete the user, there are still open disputes.'));
                }
            }

            if ($user->paymentRequests != null) {
                $total_payment_requests = PaymentRequest::where('user_id', $user->id)->count();
                $closed_payment_requests = PaymentRequest::where('user_id', $user->id)->closed()->count();
                $refunded_payment_requests = PaymentRequest::where('user_id', $user->id)->refunded()->count();

                if (($total_payment_requests - $closed_payment_requests - $refunded_payment_requests) > 0) {
                    return redirect()->route('users.show', ['user' => $user])->withErrors(__('Cannot delete the user, there are still payments being processed.'));
                }
            }
        }

        // delete PGP key
        Storage::disk('local')->delete('keys/'.$user->username.'_pgp.key');

        $user->delete();

        // check if it is a staff member replying
        if ($user->id != Auth::user()->id) {
            return redirect()->route('admin.users.index')->with('message', __('The user’s profile was deleted.'));
        }

        return redirect('login')->with('message', __('The user’s profile was deleted.'));
    }
}
