<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDispute;
use App\Http\Requests\EditDispute;
use App\Models\Dispute;
use App\Models\PaymentRequest;
use App\Models\States\Dispute\InReview;
use App\Models\States\PaymentRequest\Funded;
use App\Models\States\PaymentRequest\Reported;
use App\Notifications\DisputeOpened;
use App\Notifications\DisputeUpdated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DisputeController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Dispute::class, 'dispute');
        $this->middleware('auth');
    }

    /**
     * @brief Shows all disputes, latest 3 refunds
     *        and latest 3 open disputes
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->isMerchant()) {
            $disputes = $user->merchant->disputes()->with('paymentRequest')->latest()->get();
            $latest_open = []; /* $user->merchant->disputes()->InReview()->latest()->limit(3)->get(); */
            $refunds = $user->merchant->paymentRequests()->with('dispute')->refunded()->latest()->limit(3)->get();
        } else {
            $disputes = $user->disputes()->with('paymentRequest')->get();
            $latest_open = []; /* $user->disputes()->InReview()->with('paymentRequest')->latest()->limit(3)->get(); */
            $refunds = PaymentRequest::refunded()->where('user_id', $user->id)->with('dispute')->latest()->limit(3)->get();
        }

        return view('disputes.index', compact('disputes', 'latest_open', 'refunds'));
    }

    /**
     * @brief Creates a dispute for a given payment request
     *        only if there is no dispute already registered
     */
    public function create()
    {
        $payment = PaymentRequest::findOrFail(request()->payment_id);

        if ($payment->dispute) {
            return redirect()->route('disputes.index')->withError(__('A dispute for this payment is already opened.'));
        }

        return view('disputes.create', compact('payment'));
    }

    /**
     * @brief Shows merchant reply form
     */
    public function edit(Dispute $dispute)
    {
        $payment = $dispute->paymentRequest;

        return view('disputes.edit', compact('dispute', 'payment'));
    }

    /**
     * @brief Stores a newly created dispute and sends a notification
     *        to the merchant
     */
    public function store(CreateDispute $request)
    {
        $user = Auth::user();
        $payment = PaymentRequest::findOrFail($request->payment_request_id);
        if ($payment->isFE()) {
            return redirect()->route('disputes.index')->withError(__('The finalize early payment method is not moderated.'));
        }

        if ($user->isMerchantInPayment($payment)) {
            return redirect()->route('disputes.index')->withError(__('Only customers can open a dispute'));
        }

        $payment->transitionTo(Reported::class);

        $dispute = Dispute::firstOrCreate([
            'payment_request_id'    => $request->payment_request_id,
            'user_notes'            => $request->user_notes,
        ]);

        $payment->merchant->user->notify(new DisputeOpened($dispute));

        activity()
        ->performedOn($dispute)
        ->causedBy($user)
        ->log('new dispute');

        return redirect()->route('disputes.index')->with('message', __('A new dispute has been opened.'));
    }

    /**
     * @brief Shows a dispute
     */
    public function show(Dispute $dispute)
    {
        return view('disputes.show', compact('dispute'));
    }

    /**
     * @brief Saves merchant reply
     */
    public function update(EditDispute $request, Dispute $dispute)
    {
        if (! $dispute->isOpen()) {
            return redirect()->route('disputes.index')->withError(__('Disputes under review cannot be updated.'));
        }

        $dispute->update(['merchant_notes' => $request->merchant_notes]);
        $dispute->transitionTo(InReview::class);

        $dispute->paymentRequest->user->notify(new DisputeUpdated($dispute));

        activity()
        ->performedOn($dispute)
        ->causedBy(Auth::user())
        ->log('dispute updated');

        return redirect()->route('disputes.index')->with('message', __('Merchant reply was saved.'));
    }

    /**
     * @brief Deletes a dispute
     */
    public function destroy(Dispute $dispute)
    {
        if (! $dispute->isOpen()) {
            return redirect()->route('disputes.index')->withError(__('This dispute cannot be deleted yet.'));
        }

        $dispute->paymentRequest->transitionTo(Funded::class);

        activity()
        ->performedOn($dispute)
        ->causedBy(Auth::user())
        ->log('dispute deleted');

        $dispute->delete();

        return redirect()->route('disputes.index')->with('message', __('The dispute has been deleted.'));
    }
}
