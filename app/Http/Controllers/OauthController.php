<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Passport\Http\Controllers\ApproveAuthorizationController;
use Nyholm\Psr7\Response as Psr7Response;

class OauthController extends ApproveAuthorizationController
{
    public function approve(Request $request)
    {
        $this->assertValidAuthToken($request);

        $authRequest = $this->getAuthRequestFromSession($request);

        $merchant_user = User::findOrFail($authRequest->getUser()->getIdentifier());
        $marketplace_user = User::where('username', $authRequest->getClient()->getName())->firstOrFail();

        if (! $merchant_user->merchant->marketplaces->contains($marketplace_user->marketplace->id)) {
            $merchant_user->merchant->marketplaces()->attach($marketplace_user->marketplace->id);
        }

        return $this->convertResponse(
            $this->server->completeAuthorizationRequest($authRequest, new Psr7Response)
        );
    }
}
