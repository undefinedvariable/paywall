<?php

namespace App\Http\Controllers;

use App\Enums\Language;
use App\Http\Requests\AddPgp;
use App\Http\Requests\ChangeLanguage;
use App\Http\Requests\UpdateRefundWallet;
use App\Models\News;
use App\Models\User;
use App\Services\PGP\PGP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PrivateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @brief Shows the user dashboard with
     *        latest news and notifications list
     */
    public function home()
    {
        $news = News::latest()->get();
        $user = Auth::user();

        return view('private.home', compact('news'));
    }

    /**
     * @brief If enabled, merchants and marketplaces can act as users
     */
    public function actingAsUser(Request $request)
    {
        if ($request->has('act_as') && $request->act_as == 'true') {
            session(['acting_as_user' => true]);
        } else {
            session()->forget('acting_as_user');
        }

        return redirect()->back();
    }

    /**
     * @brief Sets the user desired language
     */
    public function setLanguage(ChangeLanguage $request)
    {
        $user = Auth::user();
        $user->update(['language' => (int) $request->language]);

        return redirect()->route('users.show', ['user' => $user]);
    }

    /**
     * @brief Marks a specific notification as read
     */
    public function markAsRead($id)
    {
        $notification = Auth::user()->notifications()->where('id', $id)->first();
        if ($notification) {
            $notification->delete();
        }

        return redirect()->back();
    }

    /**
     * @brief Enable two factor authentication only if user has already uploaded
     *        a valid PGP key
     */
    public function enable2fa()
    {
        $user = Auth::user();

        if ($user->pgp == null) {
            return redirect()->route('users.show', ['user' => $user])->withError(__('Add your PGP key first.'));
        }

        $user->update(['pgp_auth' => true]);

        return redirect()->route('users.show', ['user' => $user])->with('message', __('2FA enabled.'));
    }

    /**
     * @brief Disables two factor authentication without removing the PGP key
     */
    public function disable2fa()
    {
        $user = Auth::user();

        $user->update(['pgp_auth' => false]);

        return redirect()->route('users.show', ['user' => $user])->with('message', __('2FA disabled.'));
    }

    /**
     * @brief Uploads a PGP key, validates it and saves on local storage disk
     *        while it also saves the key hash to the user profile
     */
    public function addPgpKey(AddPgp $request, PGP $pgp)
    {
        $user = Auth::user();

        if ($user->pgp != null) {
            return redirect()->route('users.show', ['user' => $user])->withError(__('This key already exists.'));
        }

        $key = $pgp->loadKey($request->pgp);

        if ($key == null) {
            return redirect()->route('users.show', ['user' => $user])->withError(__('Invalid PGP key.'));
        }

        if (User::where('pgp', $key->packets[0]->fingerprint)->exists()) {
            return redirect()->route('users.show', ['user' => $user])->withError(__('This key is already registered by another user.'));
        }

        if (! $pgp->saveKey($request->pgp, $user->username)) {
            return redirect()->route('users.show', ['user' => $user])->withError(__('A problem occurred when adding the PGP key, please retry.'));
        }

        $user->update(['pgp' => $key->packets[0]->fingerprint]);

        return redirect()->route('users.show', ['user' => $user])->with('message', __('PGP key added.'));
    }

    /**
     * @brief Deletes a user own PGP key from local storage, removes the key hash
     *        from the user profile and automatically disables 2FA
     */
    public function removePgpKey(PGP $pgp)
    {
        $user = Auth::user();
        $user->update([
            'pgp' => '',
            'pgp_auth' => false,
        ]);

        $pgp->removeKey($user->username);

        return redirect()->route('users.show', ['user' => $user])->with('message', __('PGP key removed.'));
    }

    /**
     * @brief Change refund wallet for the client
     */
    public function setRefundWallet(UpdateRefundWallet $request)
    {
        $user = Auth::user();
        $user->update(['refund_address' => $request->refund_address]);

        return redirect()->route('users.show', ['user' => $user])->with('message', __('Wallet info updated.'));
    }
}
