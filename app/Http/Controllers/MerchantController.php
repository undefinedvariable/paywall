<?php

namespace App\Http\Controllers;

use App\Enums\Tier;
use App\Http\Requests\UpdateMerchant;
use App\Http\Requests\UpdatePersonalWallet;
use App\Jobs\CreatePersonalWallet;
use App\Models\Merchant;
use Illuminate\Support\Facades\Auth;

class MerchantController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Merchant::class, 'merchant');
        $this->middleware('merchant');
    }

    public function index()
    {
        //
    }

    /**
     * @brief Show a specific merchant.
     */
    public function show(Merchant $merchant)
    {
        $credits = Auth::user()->user_tier->credits;
        $tiers = Tier::asSelectArray();
        $marketplaces = $merchant->marketplaces;

        return view('merchants.show', compact('tiers', 'marketplaces', 'credits'));
    }

    /**
     * @brief Updates the specified merchant.
     */
    public function update(UpdateMerchant $request, Merchant $merchant)
    {
        $merchant->update($request->validated());

        activity()
        ->performedOn($merchant)
        ->causedBy(Auth::user())
        ->log('edit model');

        return back()->with('message', __('Your merchant profile has been updated.'));
    }

    /**
     * @brief Create merchant personal wallet
     */
    public function setPersonalWallet(UpdatePersonalWallet $request)
    {
        $merchant = Auth::user()->merchant;

        CreatePersonalWallet::dispatch($merchant, $request->personal_wallet_address, $request->personal_view_key);

        return redirect()->route('merchants.show', ['merchant' => $merchant])->with('message', __('Wallet info updated.'));
    }
}
