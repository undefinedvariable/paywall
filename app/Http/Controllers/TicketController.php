<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTicket;
use App\Http\Requests\UpdateTicket;
use App\Models\States\Ticket\Open;
use App\Models\Ticket;
use App\Models\TicketCategory;
use App\Models\TicketComment;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Ticket::class, 'ticket');
        $this->middleware('auth');
    }

    /**
     * @brief Displays a listing of the ticket.
     */
    public function index()
    {
        $tickets = Ticket::where('user_id', Auth::user()->id)->get();
        $open_tickets = Ticket::where('user_id', Auth::user()->id)->open()->latest()->limit(3)->get();
        $closed_tickets = Ticket::where('user_id', Auth::user()->id)->closed()->latest()->limit(3)->get();

        return view('tickets.index', compact('tickets', 'open_tickets', 'closed_tickets'));
    }

    /**
     * @brief Shows the form for opening a new ticket.
     */
    public function create()
    {
        $categories = TicketCategory::all();

        return view('tickets.create', compact('categories'));
    }

    /**
     * @brief Stores a newly created ticket in storage.
     */
    public function store(CreateTicket $request)
    {
        $user = Auth::user();

        $ticket = Ticket::firstOrCreate([
            'user_id' => $user->id,
            'ticket_category_id' => $request->ticket_category_id,
            'title' => $request->title,
            'message' => $request->message,
        ]);

        activity()
        ->performedOn($ticket)
        ->causedBy($user)
        ->log('new ticket created');

        return redirect()->route('tickets.index')->with('message', __('Ticket successfully created.'));
    }

    /**
     * @brief Displays the specified ticket.
     */
    public function show(Ticket $ticket)
    {
        return view('tickets.show', compact('ticket'));
    }

    /**
     * @brief Updates the specified ticket in storage adding a new user comment.
     */
    public function update(UpdateTicket $request, Ticket $ticket)
    {
        if ($ticket->isClosed()) {
            $ticket->transitionTo(Open::class);
        } else {
            $ticket->touch();
        }

        $user = Auth::user();

        TicketComment::firstOrCreate([
            'comment' => $request->comment,
            'ticket_id' => $request->ticket_id,
            'user_id' => $user->id,
        ]);

        activity()
        ->performedOn($ticket)
        ->causedBy($user)
        ->log('added comment to ticket');

        // check if it is a staff member replying
        if ($ticket->user_id != $user->id) {
            return redirect()->route('admin.tickets.show', ['ticket' => $ticket])->with('message', __('Your reply has been saved.'));
        }

        return redirect()->route('tickets.index')->with('message', __('Your reply has been saved.'));
    }

    /**
     * @brief Removes the specified ticket from storage.
     */
    public function destroy(Ticket $ticket)
    {
        $ticket->delete();

        return redirect()->route('tickets.index')->with('message', __('Ticket successfully deleted.'));
    }
}
