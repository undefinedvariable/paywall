<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class TokenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();

        $tokens = $user->tokens;
        $clients = $user->clients;
        $most_recent = $user->tokens()->where('revoked', false)->latest()->limit(3)->get();

        return view('private.tokens', compact('tokens', 'clients', 'most_recent'));
    }

    public function revoke($token)
    {
        Auth::user()->tokens()->where('id', $token)->update([
            'revoked' => true,
        ]);

        return redirect()->route('tokens.index')->with('message', __('The token has been revoked.'));
    }

    public function destroy($token)
    {
        Auth::user()->tokens()->where('id', $token)->delete();

        return redirect()->route('tokens.index')->with('message', __('The token has been deleted.'));
    }
}
