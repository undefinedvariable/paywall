<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePaymentRequest;
use App\Http\Resources\PaymentRequest as PaymentRequestResource;
use App\Models\PaymentRequest;
use App\Models\States\PaymentRequest\Cancelled;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class PaymentApiController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(PaymentRequest::class, 'payment');
    }

    public function index()
    {
        $user = Auth::user();

        if ($user->isMerchant()) {
            $payment_requests = PaymentRequest::where('merchant_id', $user->merchant->id)->orWhere('user_id', $user->id)->latest()->get();
        } else {
            $payment_requests = PaymentRequest::where('user_id', $user->id)->latest()->get();
        }

        return response()->json(['message' => PaymentRequestResource::collection($payment_requests)]);
    }

    public function show(PaymentRequest $payment)
    {
        return response()->json(['message' => new PaymentRequestResource($payment)]);
    }

    public function store(CreatePaymentRequest $request)
    {
        $user = Auth::user();

        if ($user->merchant->personal_wallet_address == null) {
            return response()->json(['message' => 'Personal Monero wallet missing.'], 422);
        }

        $marketplace_id = null;

        // merchant
        if ($user->token()->client->name == 'Honorarium Personal Access Client') {
            if ($user->user_tier->credits > 0) {
                $user->user_tier->decrement('credits');
            } else {
                abort(402, 'Not enough credits.');
            }
            // marketplace
        } else {
            if ($user->token()->client->user->user_tier->credits > 0) {
                $user->token()->client->user->user_tier->decrement('credits');

                $marketplace_id = $user->token()->client->user->marketplace->id;
            } else {
                abort(402, 'Not enough credits.');
            }
        }

        $success_url = empty($request->success_url) ? route('checkout.success') : $request->success_url;
        $cancel_url = empty($request->cancel_url) ? route('checkout.cancel') : $request->cancel_url;
        $payment = PaymentRequest::firstOrCreate([
            'orderid'           => $request->orderid,
            'amount_usd'        => $request->amount_usd,
            'shipping_usd'      => $request->shipping_usd,
            'goods_type'        => $request->goods_type,
            'payment_type'      => $request->payment_type,
            'item_list'         => $request->item_list,
            'success_url'       => $success_url,
            'cancel_url'        => $cancel_url,
            'merchant_id'       => $user->merchant->id,
            'marketplace_id'    => $marketplace_id,
        ]);

        $url = URL::temporarySignedRoute(
            'checkout.review', now()->addDays(config('expiration.payment.pending')), ['payment' => $payment]
        );
        $payment->update(['url' => $url]);

        activity()
        ->performedOn($payment)
        ->causedBy($user)
        ->log('new model');

        return response()->json(['message' => new PaymentRequestResource($payment)]);
    }
}
