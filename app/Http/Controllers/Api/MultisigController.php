<?php

namespace App\Http\Controllers\Api;

use App\Events\MultisigDataUpdated;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateMultisigWallet;
use App\Http\Resources\MultisigWallet as MultisigWalletResource;
use App\Jobs\Multisig\FinalizeMultisigWallet;
use App\Jobs\Multisig\MakeMultisigWallet;
use App\Jobs\Multisig\VerifyMultisigWallet;
use App\Models\PaymentRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class MultisigController extends Controller
{
    public function __construct()
    {
    }

    public function show(PaymentRequest $payment)
    {
        Log::debug('[MultisigController] someone polling a single wallet');

        if ($payment->multisigWallet == null) {
            return response()->json(['error' => 'Wallet not created yet.'], 422);
        }

        return response()->json(['message' => new MultisigWalletResource($payment->multisigWallet)]);
    }

    public function update(UpdateMultisigWallet $request, PaymentRequest $payment)
    {
        if ($payment->multisigWallet == null) {
            return response()->json(['error' => 'Wallet not created yet.'], 422);
        }

        $user = Auth::user();

        // ------------------- Client -------------------
        if ($payment->user_id == $user->id) {
            Log::debug('[MultisigController] new update request from client');

            // Wallet is MADE
            if ($payment->multisigWallet->isMade() && $payment->multisigWallet->finalize_multisig_data_customer == null) {
                Log::debug('[MultisigController] client is sending finalize_multisig_data_customer');

                if ($request->multisig_address == null) {
                    return response()->json(['error' => ['multisig_address' => 'The multisig address field is required.']], 422);
                }

                $payment->multisigWallet->update(['finalize_multisig_data_customer' => $request->multisig_address]);
                //broadcast(new MultisigDataUpdated($payment->multisigWallet, $user))->toOthers();
            // Wallet is PREPARED
            } elseif ($payment->multisigWallet->isPrepared() && $payment->multisigWallet->make_multisig_data_customer == null) {
                Log::debug('[MultisigController] client is sending make_multisig_data_customer');

                if ($request->multisig_info == null) {
                    return response()->json(['error' => ['multisig_info' => 'The multisig info field is required.']], 422);
                }

                $payment->multisigWallet->update(['make_multisig_data_customer' => $request->multisig_info]);
                //broadcast(new MultisigDataUpdated($payment->multisigWallet, $user))->toOthers();
            // Wallet is CREATED
            } elseif ($payment->multisigWallet->isCreated() && $payment->multisigWallet->prepare_multisig_data_customer == null) {
                Log::debug('[MultisigController] client is sending prepare_multisig_data_customer');

                if ($request->multisig_info == null) {
                    return response()->json(['error' => ['multisig_info' => 'The multisig info field is required.']], 422);
                }

                $payment->multisigWallet->update(['prepare_multisig_data_customer' => $request->multisig_info]);
                //broadcast(new MultisigDataUpdated($payment->multisigWallet, $user))->toOthers();
            }
            // ------------------- Merchant -------------------
        } elseif ($payment->merchant->user->id == $user->id) {
            Log::debug('[MultisigController] new update request from merchant');

            // Wallet is MADE
            if ($payment->multisigWallet->isMade() && $payment->multisigWallet->finalize_multisig_data_vendor == null) {
                Log::debug('[MultisigController] merchant is sending finalize_multisig_data_vendor');

                if ($request->multisig_address == null) {
                    return response()->json(['error' => ['multisig_address' => 'The multisig address field is required.']], 422);
                }

                $payment->multisigWallet->update(['finalize_multisig_data_vendor' => $request->multisig_address]);
                //broadcast(new MultisigDataUpdated($payment->multisigWallet, $user))->toOthers();
            // Wallet is PREPARED
            } elseif ($payment->multisigWallet->isPrepared() && $payment->multisigWallet->make_multisig_data_vendor == null) {
                Log::debug('[MultisigController] merchant is sending make_multisig_data_vendor');

                if ($request->multisig_info == null) {
                    return response()->json(['error' => ['multisig_info' => 'The multisig info field is required.']], 422);
                }

                $payment->multisigWallet->update(['make_multisig_data_vendor' => $request->multisig_info]);
                //broadcast(new MultisigDataUpdated($payment->multisigWallet, $user))->toOthers();
            // Wallet is CREATED
            } elseif ($payment->multisigWallet->isCreated() && $payment->multisigWallet->prepare_multisig_data_vendor == null) {
                Log::debug('[MultisigController] merchant is sending prepare_multisig_data_vendor');

                if ($request->multisig_info == null) {
                    return response()->json(['error' => ['multisig_info' => 'The multisig info field is required.']], 422);
                }

                $payment->multisigWallet->update(['prepare_multisig_data_vendor' => $request->multisig_info]);
                //broadcast(new MultisigDataUpdated($payment->multisigWallet, $user))->toOthers();
            }
            // ------------------- Unknown -------------------
        } else {
            Log::error('[MultisigController] unauthorized update request from user#'.$user->id);

            return response()->json(['error' => 'unauthorized'], 403);
        }

        // Wallet is CREATED and ALL data is filled
        if ($payment->multisigWallet->hasBeenPrepared()) {
            Log::debug('[MultisigController] Launching MakeMultisigWallet job');

            MakeMultisigWallet::dispatch($payment->multisigWallet);
        }
        // Wallet is PREPARED and ALL data is filled
        elseif ($payment->multisigWallet->hasBeenMade()) {
            Log::debug('[MultisigController] Launching FinalizeMultisigWallet job');

            FinalizeMultisigWallet::dispatch($payment->multisigWallet);
        }
        // Wallet is MADE and ALL data is filled
        elseif ($payment->multisigWallet->hasBeenFinalized()) {
            Log::debug('[MultisigController] Launching VerifyMultisigWallet job');

            VerifyMultisigWallet::dispatch($payment->multisigWallet);
        }

        Log::debug('[MultisigController] No jobs to launch');

        return response()->json(['message' => 'ok']);
    }
}
