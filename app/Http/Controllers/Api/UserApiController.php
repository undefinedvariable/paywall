<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UserApiController extends Controller
{
    public function __construct()
    {
    }

    public function login()
    {
        if (Auth::attempt(['username' => request('username'), 'password' => request('password')])) {
            $user = Auth::user();

            // delete just personal tokens
            foreach ($user->tokens as $t) {
                if ($t->client->name == 'Honorarium Personal Access Client') {
                    $t->delete();
                }
            }
            $token = $user->createToken(Str::random(10).'_personal_access_token', ['read', 'write', 'delete']);

            return response()->json(['message' => ['token' => $token->accessToken, 'user' => new UserResource($user)]]);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    public function whoami()
    {
        $user = Auth::user();

        return response()->json(['message' => ['user' => new UserResource($user)]]);
    }
}
