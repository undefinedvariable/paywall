<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ApiController extends Controller
{
    public function __construct()
    {
    }

    public function version()
    {
        return response()->json([
            'success' => true,
            'message' => [
                'version' => 'v1.0.0',
            ],
        ]);
    }
}
