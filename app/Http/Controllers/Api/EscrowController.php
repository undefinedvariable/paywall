<?php

namespace App\Http\Controllers\Api;

use App\Enums\TransferAction;
use App\Events\MultisigDataUpdated;
use App\Exceptions\WalletError;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTransfer;
use App\Http\Requests\SyncMultisigWallet;
use App\Http\Resources\Transfer as TransferResource;
use App\Models\PaymentRequest;
use App\Models\States\PaymentRequest\Closed;
use App\Models\States\PaymentRequest\Refunded;
use App\Notifications\PaymentClosed;
use App\Notifications\PaymentRefunded;
use App\Services\MoneroRPC\Monero;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class EscrowController extends Controller
{
    public function __construct()
    {
    }

    // ------- SYNC -------
    public function getSyncData(PaymentRequest $payment)
    {
        if ($payment->multisigWallet == null) {
            return response()->json(['error' => 'Wallet not created yet.'], 500);
        }

        try {
            Log::debug('[EscrowController] Exporting multisig info...');
            $res = Monero::export_multisig_info($payment->multisigWallet->filename);
            Log::debug('[EscrowController] ...ok');
        } catch (WalletError $e) {
            Log::error('[EscrowController] exception: '.$e->getMessage());

            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['message' => ['info' => $res['info']]]);
    }

    public function updateSyncData(SyncMultisigWallet $request, PaymentRequest $payment)
    {
        if ($payment->multisigWallet == null) {
            return response()->json(['error' => 'Wallet not created yet.'], 500);
        }

        try {
            Log::debug('[EscrowController] Importing multisig info...');
            Monero::import_multisig_info($payment->multisigWallet->filename, $request->info);
            Log::debug('[EscrowController] ...ok');
        } catch (WalletError $e) {
            Log::error('[EscrowController] exception: '.$e->getMessage());

            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['message' => 'ok']);
    }

    // ------- TRANSFER -------
    public function getPaymentAddress(PaymentRequest $payment)
    {
        if ($payment->multisigWallet == null) {
            return response()->json(['error' => 'Wallet not created yet.'], 422);
        }

        /* @todo use subaddress */
        return response()->json(['message' => new TransferResource($payment)]);
    }

    public function transfer(CreateTransfer $request, PaymentRequest $payment)
    {
        if ($payment->multisigWallet == null) {
            return response()->json(['error' => 'Wallet not created yet.'], 500);
        } elseif ($payment->isPending() || $payment->isConfirming() || $payment->isCancelled() || $payment->isExpired()) {
            return response()->json(['error' => 'Cannot be processed.'], 500);
        } elseif ($payment->isClosed() || $payment->isRefunded()) {
            return response()->json(['error' => 'Closed or refunded.'], 500);
        } elseif ($payment->isReported()) {
            return response()->json(['error' => 'Under moderation.'], 500);
        }

        $user = Auth::user();

        if ($request->action == TransferAction::CLOSE) {
            Log::debug('[EscrowController] Closing');

            // merchant can unlock funds after two weeks
            if ($user->isMerchantInPayment($payment) && ! $payment->isMerchantPayout()) {
                Log::debug('[EscrowController] It’s the merchant');

                if ($payment->updated_at->addWeeks(2) > Carbon::now()) {
                    return response()->json(['error' => 'waiting time not elapsed'], 401);
                }
            }

            Log::debug('[EscrowController] Running sign & submit...');
            if (! $this->signAndSubmit($payment, $request->txinfo, $payment->merchant->personal_wallet_address, $request->action)) {
                Log::debug('[EscrowController] ...err');

                return response()->json(['error' => 'invalid txinfo'], 422);
            } else {
                Log::debug('[EscrowController] ...ok');

                Log::debug('[EscrowController] Transitioning to closed...');
                $payment->transitionTo(Closed::class);
                Log::debug('[EscrowController] ...ok');

                $payment->merchant->user->notify(new PaymentClosed($payment));
                $payment->user->notify(new PaymentClosed($payment));

                return response()->json(['message' => 'ok']);
            }
        } elseif ($request->action == TransferAction::REFUND) {
            Log::debug('[EscrowController] Refunding');

            // return error if it's not the merchant (except accepted dispute)
            if (! $user->isMerchantInPayment($payment) && ! $payment->isCustomerPayout()) {
                Log::debug('[EscrowController] It’s the client');

                return response()->json(['error' => 'Unauthorized.'], 403);
            }

            Log::debug('[EscrowController] Running sign & submit...');
            if (! $this->signAndSubmit($payment, $request->txinfo, $payment->user->refund_address, $request->action)) {
                Log::debug('[EscrowController] ...err');

                return response()->json(['error' => 'invalid txinfo'], 422);
            } else {
                Log::debug('[EscrowController] ...ok');

                Log::debug('[EscrowController] Transitioning to refunded...');
                $payment->transitionTo(Refunded::class);
                Log::debug('[EscrowController] ...ok');

                $payment->merchant->user->notify(new PaymentRefunded($payment));
                $payment->user->notify(new PaymentRefunded($payment));

                return response()->json(['message' => 'ok']);
            }
        }

        Log::debug('[EscrowController] Invalid request');

        return response()->json(['error' => 'invalid request'], 404);
    }

    // check the destination of the transaction then sign and submit to the Monero network
    private function signAndSubmit(PaymentRequest $payment, $txinfo, $destination_address, $action): bool
    {
        try {
            $res = Monero::describe_transfer($payment->multisigWallet->filename, $txinfo);
            Log::debug('[EscrowController] describe_transfer ok');
        } catch (WalletError $e) {
            Log::error('[EscrowController] exception: '.$e->getMessage());

            return false;
        }

        if ($payment->isMarketplaceInvolved() && $action == TransferAction::CLOSE) {
            $verifications = 0;

            foreach ($res['desc'][0]['recipients'] as $recipient) {
                if (($recipient['address'] == $destination_address) && (monero_balance_formatter($recipient['amount']) >= $payment->amount_xmr)) {
                    $verifications++;
                } elseif (($recipient['address'] == $payment->getCommissionAddress()) && (monero_balance_formatter($recipient['amount']) >= $payment->getCommissionInXMR())) {
                    $verifications++;
                }
            }

            if ($verifications === 2) {
                try {
                    $data = Monero::sign_multisig($payment->multisigWallet->filename, $txinfo);

                    Monero::submit_multisig($payment->multisigWallet->filename, $data['tx_data_hex']);

                    //broadcast(new MultisigDataUpdated($payment->multisigWallet, Auth::user()));

                    return true;
                } catch (WalletError $e) {
                    Log::error('[EscrowController] exception: '.$e->getMessage());

                    return false;
                }
            }
        } else {
            foreach ($res['desc'][0]['recipients'] as $recipient) {
                try {
                    if (($recipient['address'] == $destination_address) && (monero_balance_formatter($recipient['amount']) >= $payment->amount_xmr)) {
                        $data = Monero::sign_multisig($payment->multisigWallet->filename, $txinfo);

                        Monero::submit_multisig($payment->multisigWallet->filename, $data['tx_data_hex']);

                        //broadcast(new MultisigDataUpdated($payment->multisigWallet, Auth::user()));

                        return true;
                    }
                } catch (WalletError $e) {
                    Log::error('[EscrowController] exception: '.$e->getMessage());

                    return false;
                }
            }
        }

        return false;
    }
}
