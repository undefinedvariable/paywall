<?php

namespace App\Http\Controllers;

use App\Enums\CommissionType;
use App\Enums\Tier;
use App\Http\Requests\UpdateMarketplace;
use App\Http\Requests\UpdateWallet;
use App\Models\Marketplace;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\ClientRepository;

class MarketplaceController extends Controller
{
    private $clients;

    public function __construct(ClientRepository $clients)
    {
        $this->authorizeResource(Marketplace::class, 'marketplace');
        $this->middleware('marketplace');

        $this->clients = $clients;
    }

    public function index()
    {
        //
    }

    /**
     * @brief Show a specific marketplace.
     */
    public function show(Marketplace $marketplace)
    {
        $client = Auth::user()->clients->first();
        $tiers = Tier::asSelectArray();
        $credits = Auth::user()->user_tier->credits;
        $commission_types = CommissionType::asSelectArray();

        return view('marketplaces.show', compact('client', 'tiers', 'credits', 'commission_types'));
    }

    /**
     * @brief Updates the specified marketplace.
     */
    public function update(UpdateMarketplace $request, Marketplace $marketplace)
    {
        $marketplace->update($request->validated());
        $user = Auth::user();

        activity()
        ->performedOn($marketplace)
        ->causedBy($user)
        ->log('edit model');

        if ($request->has('redirect')) {
            $this->clients->update($user->clients->first(), $user->username, $request->redirect);
        }

        return back()->with('message', __('Your marketplace profile has been updated.'));
    }

    /**
     * @brief Set marketplace wallet
     */
    public function setWallet(UpdateWallet $request)
    {
        $user = Auth::user();
        $user->marketplace->update(['wallet_address' => $request->wallet_address]);

        return redirect()->route('marketplaces.show', ['marketplace' => $user->marketplace])->with('message', __('Wallet info updated.'));
    }
}
