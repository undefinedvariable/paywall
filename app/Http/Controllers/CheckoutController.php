<?php

namespace App\Http\Controllers;

use App\DataTransferObjects\Estimate;
use App\DataTransferObjects\ItemCollection;
use App\Exceptions\APIError;
use App\Exceptions\WalletError;
use App\Http\Requests\ApplyCoupon;
use App\Jobs\Multisig\CreateMultisigWallet;
use App\Jobs\SendIPN;
use App\Jobs\VerifyEscrowTransaction;
use App\Jobs\VerifyFETransaction;
use App\Models\Coupon;
use App\Models\Cryptocurrency;
use App\Models\PaymentRequest;
use App\Models\States\PaymentRequest\Cancelled;
use App\Models\States\PaymentRequest\Confirming;
use App\Notifications\PaymentCancelled;
use App\Services\CryptoExchange\CryptoExchangeContract;
use App\Services\CryptoRates\CryptoRatesContract;
use App\Services\MoneroRPC\Monero;
use Cache;
use Cookie;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Spatie\DataTransferObject\DataTransferObjectError;

/**
 * @todo avoid skipping steps
 */
class CheckoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('signed')->only('review');
        $this->middleware('checkout');
        $this->middleware('auth');
    }

    /**
     * @brief Shows payment request info to the user accessing
     *        via a signed url. The user can decide to cancel
     *        or continue
     */
    public function review(PaymentRequest $payment)
    {
        if (! $payment->isPending()) {
            abort(404);
        }

        if (! $payment->isValid()) {
            return redirect()->route('checkout.expired');
        }

        Cookie::queue('verify', 'verified_user', 30);

        // decode items list
        $arr = "[ $payment->item_list ]";
        $arr = json_decode($arr, true);

        if (! empty($arr)) {
            try {
                $items = ItemCollection::create($arr);
            } catch (DataTransferObjectError $e) {
                return back()->withError($e->getMessage());
            }
        } else {
            $items = [];
        }

        // get PGP key
        if ($payment->merchant->user->pgp != null) {
            $publickey = Storage::disk('local')->get('keys/'.$payment->merchant->user->username.'_pgp.key');
        } else {
            $publickey = '';
        }

        $payment->update(['form_step' => Route::currentRouteName()]);

        return view('checkout.review', compact('payment', 'items', 'publickey'));
    }

    /**
     * @brief Multisig escrow wallet generation service page
     */
    public function wallet(PaymentRequest $payment)
    {
        if ($payment->isFE()) {
            // if payment not escrow go to next step
            return redirect()->route('checkout.currency', ['payment' => $payment]);
        } elseif ($payment->multisigWallet == null) {
            // payment is escrow and we need to create a wallet
            CreateMultisigWallet::dispatch($payment);
        }

        return view('checkout.wallet', compact('payment'));
    }

    /**
     * @brief Lets the user select the cryptocurrency showing conversion
     *        fees for currencies other than Monero. The user can decide
     *        to cancel or continue
     */
    public function currency(PaymentRequest $payment, CryptoRatesContract $cryptorates, CryptoExchangeContract $cryptoexchange)
    {
        try {
            // redirect back if wallet not created
            if ($payment->isEscrow() && ($payment->multisigWallet == null || ! $payment->multisigWallet->isFinalized())) {
                return redirect()->route('checkout.wallet', ['payment' => $payment])->withErrors(__('Escrow wallet not created yet.'));
            }

            $usd_to_xmr_rateo = Cache::remember('usd_to_xmr', 60 * 5, function () use ($cryptorates) {
                return $cryptorates->usd_to_xmr();
            });

            $estimates = [];

            $cryptocurrencies = Cryptocurrency::all();
            foreach ($cryptocurrencies as $crypto) {
                if ($crypto->isMonero()) {
                    $conversion_rate = 1;
                    $fee = 0;
                    $min = 0.001;
                } else {
                    $res = $cryptoexchange->get_rates('XMR', $crypto->abbreviation);
                    $conversion_rate = $res['rate'];
                    $fee = $res['fee'];
                    $min = $res['min'];
                }

                $price = make_price($payment->getFinalPrice(), $usd_to_xmr_rateo, $conversion_rate);

                $estimate = new Estimate([
                    'model_id'          => $crypto->id,
                    'name'              => $crypto->name,
                    'abbreviation'      => $crypto->abbreviation,
                    'price'             => $price,
                    'fee'               => $fee,
                    'min'               => $min,
                ]);
                array_push($estimates, $estimate);
            }
        } catch (APIError $e) {
            return redirect()->route('checkout.currency', ['payment' => $payment])->withErrors($e->getMessage());
        } catch (ConnectionException $e) {
            return redirect()->route('checkout.currency', ['payment' => $payment])->withErrors($e->getMessage());
        } catch (RequestException $e) {
            return redirect()->route('checkout.currency', ['payment' => $payment])->withErrors($e->getMessage());
        }

        return view('checkout.currency', compact('estimates', 'payment'));
    }

    /**
     * @brief Sends a request to cryptoexchange in order to conver user
     *        favourite currency to Monero and put as return address
     *        the escrow wallet. Returns to the user the cryptoexchange
     *        deposit address and the total amount to be paid
     */
    public function pay(PaymentRequest $payment, Cryptocurrency $cryptocurrency, CryptoExchangeContract $cryptoexchange, CryptoRatesContract $cryptorates)
    {
        try {
            if ($payment->isEscrow()) {
                // Handle escrow payment
                $addr = $payment->multisigWallet->address;
            } else {
                // Handle FE payment
                $wallet_name = merchant_wallet_name($payment->merchant->user->id);

                try {
                    $res = Monero::make_integrated_address($wallet_name);
                } catch (WalletError $e) {
                    Log::error('[EscrowController] exception: '.$e->getMessage());

                    return redirect()->route('checkout.currency', ['payment' => $payment])->withErrors(__('Monero API error, please wait a minute and retry.'));
                }

                $payment->update(['payment_id' => $res['payment_id']]);

                $addr = $res['integrated_address'];
            }

            // real price, including cryptoexchange fees
            $usd_to_xmr_rateo = Cache::remember('usd_to_xmr', 60 * 5, function () use ($cryptorates) {
                return $cryptorates->usd_to_xmr();
            });

            if ($cryptocurrency->isMonero()) {
                $address = $addr;

                $price = make_price($payment->getFinalPrice(), $usd_to_xmr_rateo, 1);
            } else {
                $result = $cryptoexchange->make_swap($cryptocurrency->abbreviation, 'XMR', $addr, $payment->merchant->personal_wallet_address);

                $address = $result->address;
                $payment->update(['cryptoexchange_id' => $result->id]);

                $rate = $cryptoexchange->get_rates('XMR', $cryptocurrency->abbreviation);
                $price = make_price($payment->getFinalPrice(), $usd_to_xmr_rateo, $rate) + $cryptocurrency->fee;
            }
        } catch (APIError $e) {
            return redirect()->route('checkout.currency', ['payment' => $payment])->withErrors($e->getMessage());
        } catch (ConnectionException $e) {
            return redirect()->route('checkout.currency', ['payment' => $payment])->withErrors($e->getMessage());
        } catch (RequestException $e) {
            return redirect()->route('checkout.currency', ['payment' => $payment])->withErrors($e->getMessage());
        }

        $payment->update(['cryptocurrency_id' => $cryptocurrency->id, 'amount_xmr' => make_price($payment->getFinalPrice(), $usd_to_xmr_rateo, 1)]);

        return view('checkout.pay', compact('payment', 'address', 'price', 'cryptocurrency'));
    }

    /**
     * @brief Cycles until the transaction becomes confirmed
     */
    public function verify(PaymentRequest $payment)
    {
        if ($payment->isEscrow()) {
            VerifyEscrowTransaction::dispatch($payment);
        } else {
            VerifyFETransaction::dispatch($payment);
        }

        if ($payment->isPending()) {
            $payment->transitionTo(Confirming::class);
        } elseif ($payment->isFunded() || $payment->isClosed()) {
            Auth::user()->increment('reputation');

            return redirect()->away($payment->success_url);
        } elseif ($payment->isCancelled() || $payment->isExpired() || $payment->isRefunded()) {
            return redirect()->away($payment->cancel_url);
        }

        return response()->view('checkout.verify', compact('payment'))->header('Refresh', '15');
    }

    /**
     * @brief Cancels the payment request and sends a notification to
     *        the merchant
     */
    public function cancel(PaymentRequest $payment)
    {
        $payment->transitionTo(Cancelled::class);
        $payment->merchant->user->notify(new PaymentCancelled($payment));

        SendIPN::dispatch($payment);

        return redirect()->away($payment->cancel_url);
    }

    /**
     * @brief Applies a coupon to the current payment request
     */
    public function coupon(ApplyCoupon $request)
    {
        $user = Auth::user();

        $coupon = Coupon::where('code', $request->coupon)->first();
        $payment = PaymentRequest::findOrFail($request->payment_request_id);

        if ($coupon->was_used($user->id)) {
            return back()->withErrors(__('This coupon has already been used.'));
        }

        if ($coupon->users->count() >= $coupon->usage_limit) {
            return back()->withErrors(__('This coupon has expired.'));
        }

        $user->coupons()->attach($coupon->id);
        $payment->update(['coupon_id' => $coupon->id]);

        return back()->with('message', __('Coupon successfully applied.'));
    }

    // Standard success and cancel pages

    public function cancelPage()
    {
        return view('checkout.cancel');
    }

    public function successPage()
    {
        return view('checkout.success');
    }

    public function expiredPage()
    {
        return view('checkout.expired');
    }
}
