<?php

namespace App\Http\Controllers;

use App\Enums\GoodsType;
use App\Enums\PaymentType;
use App\Http\Requests\CreatePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Models\Coupon;
use App\Models\PaymentRequest;
use App\Models\States\PaymentRequest\Cancelled;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class PaymentRequestController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(PaymentRequest::class, 'payment');
        $this->middleware('auth');
    }

    /**
     * Get the map of resource methods to ability names.
     *
     * @return array
     */
    protected function resourceAbilityMap()
    {
        return [
            'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'delete',
            'cancel' =>'cancel',
        ];
    }

    /**
     * Get the list of resource methods which do not have model parameters.
     *
     * @return array
     */
    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'create', 'store'];
    }

    public function index()
    {
        $user = Auth::user();

        if ($user->isMerchant()) {
            $coupons = Coupon::where('merchant_id', $user->merchant->id)->get();
            $payment_requests = PaymentRequest::where('merchant_id', $user->merchant->id)->with('user')->with('merchant')->with('dispute')->latest()->get();

            return view('payments.index', compact('payment_requests', 'coupons'));
        } else {
            $payment_requests = PaymentRequest::where('user_id', $user->id)->with('user')->with('merchant')->with('dispute')->latest()->get();

            return view('payments.index', compact('payment_requests'));
        }
    }

    public function create()
    {
        $types = GoodsType::asSelectArray();
        $payment_types = PaymentType::asSelectArray();

        return view('payments.create', compact('types', 'payment_types'));
    }

    public function store(CreatePaymentRequest $request)
    {
        $user = Auth::user();

        if ($user->merchant->personal_wallet_address == null) {
            return redirect()->route('merchants.show', ['merchant' => $user->merchant])->withError(__('Add your personal Monero wallet first.'));
        }

        if ($user->user_tier->credits > 0) {
            $user->user_tier->decrement('credits');
        } else {
            return redirect()->route('payments.index')->withError(__('Not enough API credits.'));
        }

        $success_url = empty($request->success_url) ? route('checkout.success') : $request->success_url;
        $cancel_url = empty($request->cancel_url) ? route('checkout.cancel') : $request->cancel_url;
        $payment = PaymentRequest::firstOrCreate([
            'orderid'           => $request->orderid,
            'amount_usd'        => $request->amount_usd,
            'shipping_usd'      => $request->shipping_usd,
            'goods_type'        => $request->goods_type,
            'payment_type'      => $request->payment_type,
            'description'       => $request->description,
            'success_url'       => $success_url,
            'cancel_url'        => $cancel_url,
            'merchant_id'       => $user->merchant->id,
        ]);

        $url = URL::temporarySignedRoute(
            'checkout.review', now()->addDays(config('expiration.payment.pending')), ['payment' => $payment]
        );
        $payment->update(['url' => $url]);

        activity()
        ->performedOn($payment)
        ->causedBy($user)
        ->log('new payment');

        return redirect()->route('payments.index')->with('message', __('A new payment has been initiated, the redirect url is :url', ['url' => $url]));
    }

    public function show(PaymentRequest $payment)
    {
        return view('payments.show', compact('payment'));
    }

    public function update(UpdatePaymentRequest $request, PaymentRequest $payment)
    {
        $user = Auth::user();

        $payment->update([
            'notes'     =>  $request->notes,
            'user_id'   =>  $user->id,
        ]);

        activity()
        ->performedOn($payment)
        ->causedBy($user)
        ->log('edit payment');

        if ($payment->isEscrow()) {
            return redirect()->route('checkout.wallet', ['payment' => $payment]);
        } else {
            return redirect()->route('checkout.currency', ['payment' => $payment]);
        }
    }

    public function cancel(PaymentRequest $payment)
    {
        $payment->transitionTo(Cancelled::class);

        activity()
        ->performedOn($payment)
        ->causedBy(Auth::user())
        ->log('payment cancelled');

        return redirect()->route('payments.show', $payment)->with('message', __('The payment request has been cancelled.'));
    }

    public function destroy(PaymentRequest $payment)
    {
        activity()
        ->performedOn($payment)
        ->causedBy(Auth::user())
        ->log('payment deleted');

        $payment->delete();

        return redirect()->route('payments.index')->with('message', __('The payment request has been deleted.'));
    }
}
