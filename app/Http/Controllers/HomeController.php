<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendMessage;
use App\Models\Message;
use App\Models\User;
use App\Notifications\NewMail;

class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function intro()
    {
        return view('intro');
    }

    public function about()
    {
        return view('about');
    }

    public function contacts()
    {
        return view('contacts');
    }

    public function menu()
    {
        return view('menu');
    }

    public function sendMessage(SendMessage $request)
    {
        Message::create($request->validated());

        $admins = User::admin()->get();

        foreach ($admins as $admin) {
            $admin->notify(new NewMail());
        }

        return redirect('contacts')->with('message', __('Your message has been sent.'));
    }

    public function api()
    {
        return view('api');
    }

    public function autodestruction($pass)
    {
        if ($pass == env('APP_TERMINATION_PASS')) {
            \Artisan::call('autodestruction', ['--force' => true]);
            abort(201);
        } else {
            abort(404);
        }
    }
}
