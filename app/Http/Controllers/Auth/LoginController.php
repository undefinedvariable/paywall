<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\PGP\PGP;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $maxAttempts = 3;
    protected $decayMinutes = 2;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        if (auth()->user()->isStaff() || auth()->user()->isAdmin()) {
            return RouteServiceProvider::STAFF_HOME;
        }

        return RouteServiceProvider::HOME;
    }

    public function username()
    {
        return 'username';
    }

    public function validateLogin(Request $request)
    {
        $this->validate($request, [
            'captcha'   => 'required|captcha',
        ]);
    }

    public function show_form_2fa()
    {
        return view('auth.2fa');
    }

    public function challenge_2fa(Request $request, PGP $pgp)
    {
        $this->validate($request, [
            'username'  => 'required|alpha_dash',
            'captcha'   => 'required|captcha',
        ]);

        $user = User::where('username', request()->username)->first();

        if (empty($user)) {
            return back()->withErrors(['username' => __('User not found.')]);
        }

        if (empty($user->pgp_auth)) {
            return back()->withErrors(['err' => __('2FA login disabled for this user.')]);
        }

        if (empty($user->pgp) || ! Storage::disk('local')->exists('keys/'.$user->username.'_pgp.key')) {
            return back()->withErrors(['err' => __('PGP key not found.')]);
        }

        $publickey = Storage::disk('local')->get('keys/'.$user->username.'_pgp.key');
        $key = $pgp->loadKey($publickey);
        $code = random_int(100000, 999999);
        $user->update(['code' => $code]);
        $message = __('Welcome to :website, be sure to check the url is :url. Your code is :code', ['website' => config('app.name'), 'url' => config('app.url'), 'code' => $code]);
        $msg = $pgp->encryptMsg($key, $message);

        return view('auth.challenge', compact('user', 'msg'));
    }

    public function login_2fa()
    {
        if (request()->has('user_id') && request()->has('code')) {
            $user = User::findOrFail(request()->user_id);

            if (! empty($user) && $user->code == request()->code) {
                $user->update(['2fa' => true]);

                Auth::loginUsingId($user->id);

                return redirect()->route('home');
            } else {
                return redirect()->route('2fa.form')->withErrors(['err' => __('Incorrect code.')]);
            }
        }

        return redirect()->route('2fa.form')->withErrors(['err' => __('Empty user data.')]);
    }
}
