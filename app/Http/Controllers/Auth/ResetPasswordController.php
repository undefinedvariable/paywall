<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPswd;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\DB;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected function redirectTo()
    {
        session()->flash('message', __('Recovering password.'));

        return '/login';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(ResetPswd $request)
    {
        $tokenRecord = DB::table('password_resets')->where('token', $request->token)->first();
        if (Carbon::parse($tokenRecord->created_at)->addSeconds(3600)->isPast()) {
            return redirect()->route('password.recover')->withError(__('The token has expired, please retry.'));
        }

        $user = User::where('username', $tokenRecord->username)->first();
        if (empty($user)) {
            abort(404);
        }

        $user->update([
            'password' => bcrypt($request->password),
        ]);

        DB::table('password_resets')->where('token', $request->token)->delete();

        return redirect('login')->with('message', __('Password successfully changed.'));
    }
}
