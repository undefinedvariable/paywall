<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePswd;
use App\Http\Requests\ValidatePassphrase;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class PasswordController extends Controller
{
    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['changePswd', 'showPasswordChangeForm']);
    }

    /**
     * Shows a user password recover form to check user data.
     *
     * @return \Illuminate\View\View
     */
    public function showPasswordRecoverForm()
    {
        return view('auth.passwords.recover');
    }

    /**
     * Creates a password reset token.
     *
     * @param  \App\Http\Requests\ValidatePassphrase  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkUserCredentials(ValidatePassphrase $request)
    {
        $username = $request->username;
        $passphrase = $request->passphrase;

        $user = User::where(['username' => $username])->first();

        if (Hash::check($passphrase, $user->passphrase)) {
            $token = hash_hmac('sha256', Str::random(40), env('APP_KEY'));
            DB::table('password_resets')->insert([
                'username'      => $user->username,
                'token'         => $token,
                'created_at'    => Carbon::now(),
            ]);

            return redirect('password/reset/'.$token)->with('token', $token);
        } else {
            return back()->withErrors(['passphrase' => 'wrong_passphrase']);
        }
    }

    /**
     * Shows a password reset form to change pswd.
     *
     * @return \Illuminate\View\View
     */
    public function showPasswordResetForm($token)
    {
        return view('auth.passwords.reset', compact('token'));
    }

    /**
     * Show the form to change the password.
     *
     * @return \Illuminate\View\View
     */
    public function showPasswordChangeForm()
    {
        return view('auth.passwords.change');
    }

    /**
     * Change the given user's password.
     *
     * @param  \App\Http\Requests\ChangePswd  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePswd(ChangePswd $request)
    {
        if (! Hash::check($request->old_password, Auth::user()->password)) {
            return back()->withErrors(['old_password' => __('Incorrect password.')]);
        }

        Auth::user()->update([
            'password' => bcrypt($request->password),
        ]);

        return redirect('home')->with('message', __('Password updated.'));
    }
}
