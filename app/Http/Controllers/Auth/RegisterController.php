<?php

namespace App\Http\Controllers\Auth;

use App\Enums\AccountType;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register-optional';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username'  => 'bail|required|alpha_dash|allowed_username|min:3|max:50|unique:users',
            'password'  => 'bail|required|min:8|max:50|confirmed',
            'captcha'   => 'required|captcha',
        ]);
    }

    protected function create(array $data)
    {
        $passphrase = $this->passphrase();
        session()->flash('passphrase', $passphrase);

        $user = User::create([
            'username'      => $data['username'],
            'password'      => Hash::make($data['password']),
            'passphrase'    => bcrypt($passphrase),
            'account'       => AccountType::USER,
        ]);

        return $user;
    }

    protected function passphrase()
    {
        $str = '';

        $f_contents = file(storage_path().'/app/words.txt');
        $length = count($f_contents) - 1;

        for ($i = 0; $i < 8; $i++) {
            $str .= $f_contents[random_int(0, $length)].' ';
        }

        return trim(preg_replace('/\s\s+/', ' ', $str));
    }
}
