<?php

namespace App\Http\Controllers\Auth;

use App\Enums\Flower;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUser;
use App\Services\PGP\PGP;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class RegisterOptionalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        $flowers = Flower::asSelectArray();

        return view('auth.register_optional', compact('flowers'));
    }

    public function postForm(RegisterUser $request, PGP $pgpservice)
    {
        $user = Auth::user();

        $pgp = '';
        if ($request->pgp != null) {
            $key = $pgpservice->loadKey($request->pgp);

            if ($key == null) {
                return back()->withErrors(['pgp' => __('Invalid PGP key.')]);
            }

            $pgp = Storage::disk('local')->put('keys/'.$user->username.'_pgp.key', $request->pgp);

            auth()->user()->update([
                'pgp' => $key->packets[0]->fingerprint,
            ]);
        }

        auth()->user()->update([
            'flower' => (int) $request->flower,
            'refund_address' => $request->refund_address,
        ]);

        return redirect()->route('home');
    }
}
