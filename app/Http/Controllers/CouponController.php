<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCoupon;
use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CouponController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Coupon::class, 'coupon');
        $this->middleware('merchant');
    }

    /**
     * @brief Shows coupon create page
     */
    public function create()
    {
        return view('coupons.create');
    }

    /**
     * @brief Stores a new coupon
     */
    public function store(CreateCoupon $request)
    {
        Coupon::firstOrCreate([
            'code' => $request->code,
            'discount' => $request->discount,
            'user_limit' => $request->user_limit,
            'usage_limit' => $request->usage_limit,
            'expires_at' => Carbon::parse($request->expires_at),
            'merchant_id' => Auth::user()->merchant->id,
        ]);

        return redirect()->route('payments.index')->with('message', __('A new coupon has been created.'));
    }

    /**
     * @brief Deletes a coupon
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();

        return redirect()->route('payments.index')->with('message', __('The coupon has been deleted.'));
    }
}
