<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\WalletError;
use App\Http\Controllers\Controller;
use App\Models\Dispute;
use App\Models\Message;
use App\Models\Ticket;
use App\Models\User;
use App\Services\MoneroRPC\Monero;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Spatie\Activitylog\Models\Activity;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff');
    }

    public function dashboard()
    {
        // Check if in maintenance mode
        $maintenance = app()->isDownForMaintenance();

        // Wallet balance
        $balance = Cache::remember('main_wallet_balance', 60 * 2, function () {
            try {
                $get_balance = Monero::get_balance(config('monero.admin_wallet_filename'));

                return monero_balance_formatter($get_balance);
            } catch (WalletError $e) {
                Log::error('[EscrowController] exception: '.$e->getMessage());

                return -1;
            }
        });

        // Users counting
        $new_users = User::where('created_at', '>', Carbon::now()->subWeek())->count();
        $new_disputes = Dispute::where('created_at', '>', Carbon::now()->subWeek())->count();
        $new_tickets = Ticket::where('created_at', '>', Carbon::now()->subWeek())->count();

        // Disputes counting
        $open_disputes = Dispute::open()->count();
        $accepted_disputes = Dispute::accepted()->count();
        $rejected_disputes = Dispute::rejected()->count();
        $in_review_disputes = Dispute::inReview()->count();

        $users_stats_week = [];
        for ($n = 10; $n > 0; $n--) {
            $temp = User::whereBetween('created_at', [
                today()->subWeeks($n)->startOfWeek()->toDateTimeString(),
                today()->subWeeks($n)->endOfWeek()->toDateTimeString(),
            ])->count();

            array_push($users_stats_week, $temp);
        }

        return view('admin.dashboard', compact(
            'maintenance',
            'balance',
            'new_users',
            'new_disputes',
            'new_tickets',
            'open_disputes',
            'accepted_disputes',
            'rejected_disputes',
            'in_review_disputes',
            'users_stats_week'
        ));
    }

    public function interactions()
    {
        $activities = Activity::all();

        return view('admin.interactions', compact('activities'));
    }

    public function news()
    {
        return view('admin.news');
    }

    public function online()
    {
        \Artisan::call('up');

        return redirect()->route('admin.dashboard')->with('message', 'website is online now');
    }

    public function offline()
    {
        \Artisan::call('down');

        return redirect()->route('admin.dashboard')->with('message', 'website is offline now');
    }

    public function messages()
    {
        $messages = Message::all();

        return view('admin.messages', compact('messages'));
    }
}
