<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class LogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $filename = realpath(storage_path().'/logs/laravel.log');
        $logs = [];
        $labels = [
          'NOTICE' => 'default',
          'DEBUG' => 'default',
          'INFO'  => 'info',
          'ALERT' => 'warning',
          'WARNING' => 'warning',
          'ERROR' => 'danger',
          'CRITICAL' => 'danger',
          'EMERGENCY' => 'danger',
        ];

        if (file_exists($filename)) {
            $lines = file($filename);
            foreach ($lines as $line) {
                $line = trim($line);
                if (preg_match('/^\[(.+)\]\s+local\.([^\s]+):\s+(.+)$/i', $line, $m)) {
                    $text = $m[3];
                    if (mb_strlen($text) > 100) {
                        $text = substr($text, 0, 100).' ...';
                    }

                    $logs[] = [
                'date'  => $m[1],
                'type'  => $m[2],
                'label' => $labels[$m[2]],
                'msg'   => $text,
                'full'  => $m[3],
              ];
                }
            }
        }

        return view('admin.logs', ['logs' => array_reverse($logs)]);
    }

    public function clear()
    {
        $filename = realpath(storage_path().'/logs/laravel.log');
        @unlink($filename);

        return redirect()->route('admin.logs');
    }
}
