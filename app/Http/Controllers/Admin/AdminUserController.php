<?php

namespace App\Http\Controllers\Admin;

use App\Enums\AccountType;
use App\Http\Controllers\Controller;
use App\Models\Marketplace;
use App\Models\Merchant;
use App\Models\States\User\Active;
use App\Models\States\User\Banned;
use App\Models\User;
use App\Notifications\PromotedToMarketplace;
use App\Notifications\PromotedToMerchant;
use Carbon\Carbon;
use Laravel\Passport\ClientRepository;

class AdminUserController extends Controller
{
    private $clients;

    public function __construct(ClientRepository $clients)
    {
        $this->authorizeResource(User::class, 'user');
        $this->middleware('admin');

        $this->clients = $clients;
    }

    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    public function show(User $user)
    {
        $paid = $earned = 0;
        foreach ($user->paymentRequests as $payment) {
            $paid += $payment->getFinalPrice();
        }

        if ($user->isMerchant()) {
            foreach ($user->merchant->paymentRequests as $payment) {
                $earned += $payment->getFinalPrice();
            }
        }

        return view('admin.users.show', compact('user', 'earned', 'paid'));
    }

    /**
     * @brief Shows only users who deleted their accounts
     */
    public function removed()
    {
        $users = User::onlyTrashed()->get();

        return view('admin.users.removed', compact('users'));
    }

    /**
     * @brief Restores a deleted user
     */
    public function restore($id)
    {
        $user = User::withTrashed()->findOrFail($id);
        $user->restore();

        return redirect()->route('admin.users.index')->with('message', 'user #'.$user->id.' restored');
    }

    /**
     * @brief Sets the user status to banned and sets the bantime to two weeks
     */
    public function ban(User $user)
    {
        if ($user->isAdmin()) {
            return redirect()->route('home');
        }

        if ($user->isBanned()) {
            // already banned, long term ban
            $user->update([
                'banned_until' => Carbon::now()->addYear(),
            ]);
        } else {
            // short term ban
            $user->update([
                'status' => Banned::class,
                'banned_until' => Carbon::now()->addDays(15),
            ]);
        }

        return redirect()->route('admin.users.index')->with('message', 'user banned');
    }

    /**
     * @brief Unbans a user
     */
    public function unban(User $user)
    {
        $user->update([
            'status' => Active::class,
            'banned_until' => Carbon::now(),
        ]);

        return redirect()->route('admin.users.index')->with('message', 'user unbanned');
    }

    /**
     * @brief Promotes a user to merchant
     */
    public function makeMerchant(User $user)
    {
        if ($user->merchant != null && $user->marketplace != null) {
            return redirect()->route('admin.users.show', ['user' => $user])->with('message', 'user is already a merchant or a marketplace');
        }

        $user->update([
            'account' => AccountType::MERCHANT,
        ]);

        Merchant::create(['user_id' => $user->id]);

        $user->notify(new PromotedToMerchant($user));

        return redirect()->route('admin.users.show', ['user' => $user])->with('message', 'user is now a merchant');
    }

    /**
     * @brief Promotes a user to marketplace
     */
    public function makeMarketplace(User $user)
    {
        if ($user->merchant != null && $user->marketplace != null) {
            return redirect()->route('admin.users.show', ['user' => $user])->with('message', 'user is already a merchant or a marketplace');
        }

        $user->update([
            'account' => AccountType::MARKETPLACE,
        ]);

        Marketplace::create([
            'user_id' => $user->id,
            'wallet_address' => $user->refund_address,
        ]);

        $this->clients->create($user->id, $user->username, 'http://test.com');

        $user->notify(new PromotedToMarketplace($user));

        return redirect()->route('admin.users.show', ['user' => $user])->with('message', 'user is now a marketplace');
    }
}
