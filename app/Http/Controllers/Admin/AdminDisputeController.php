<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dispute;
use App\Models\States\Dispute\Accepted;
use App\Models\States\Dispute\Rejected;
use App\Models\States\PaymentRequest\CustomerPayout;
use App\Models\States\PaymentRequest\MerchantPayout;
use App\Notifications\DisputeAccepted;
use App\Notifications\DisputeRejected;

class AdminDisputeController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Dispute::class, 'dispute');
        $this->middleware('staff');
    }

    /**
     * @brief View all disputes
     */
    public function index()
    {
        $disputes = Dispute::with('paymentRequest')->get();

        return view('admin.disputes.index', compact('disputes'));
    }

    /**
     * @brief View all disputes in review
     */
    public function review()
    {
        $disputes = Dispute::with('paymentRequest')->inReview()->get();

        return view('admin.disputes.review', compact('disputes'));
    }

    /**
     * @brief Show a specific dispute
     */
    public function show(Dispute $dispute)
    {
        return view('admin.disputes.show', compact('dispute'));
    }

    /**
     * @brief Accepts the dispute and sends a notification to
     *        the user and the merchant
     */
    public function accept(Dispute $dispute)
    {
        if (! $dispute->isInReview()) {
            return redirect()->route('admin.disputes.index')->with('error', 'dispute cannot be accepted or rejected now');
        }

        $dispute->transitionTo(Accepted::class);
        $dispute->paymentRequest->transitionTo(CustomerPayout::class);

        $dispute->paymentRequest->user->notify(new DisputeAccepted($dispute));
        $dispute->paymentRequest->merchant->user->notify(new DisputeAccepted($dispute));

        return redirect()->route('admin.disputes.index')->with('message', 'dispute accepted');
    }

    /**
     * @brief Rejects the dispute and sends a notification to
     *        the user and the merchant
     */
    public function reject(Dispute $dispute)
    {
        if (! $dispute->isInReview()) {
            return redirect()->route('admin.disputes.index')->with('error', 'dispute cannot be accepted or rejected now');
        }

        $dispute->transitionTo(Rejected::class);
        $dispute->paymentRequest->transitionTo(MerchantPayout::class);

        $dispute->paymentRequest->user->notify(new DisputeRejected($dispute));
        $dispute->paymentRequest->merchant->user->notify(new DisputeRejected($dispute));

        return redirect()->route('admin.disputes.index')->with('message', 'dispute rejected');
    }
}
