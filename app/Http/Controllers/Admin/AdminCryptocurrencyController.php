<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCryptocurrency;
use App\Http\Requests\UpdateCryptocurrency;
use App\Models\Cryptocurrency;

class AdminCryptocurrencyController extends Controller
{
    public function __construct()
    {
        //$this->authorizeResource(Cryptocurrency::class, 'cryptocurrency');
        $this->middleware('admin');
    }

    /**
     * @brief Displays all supported cryptocurrencies
     */
    public function index()
    {
        $cryptocurrencies = Cryptocurrency::all();

        return view('admin.cryptocurrencies.index', compact('cryptocurrencies'));
    }

    /**
     * @brief Shows the form for creating a new cryptocurrency
     */
    public function create()
    {
        return view('admin.cryptocurrencies.create');
    }

    /**
     * @brief Shows the form for editing a cryptocurrency
     */
    public function edit(Cryptocurrency $cryptocurrency)
    {
        return view('admin.cryptocurrencies.edit', compact('cryptocurrency'));
    }

    /**
     * @brief Stores a newly created cryptocurrency in storage
     */
    public function store(CreateCryptocurrency $request, Cryptocurrency $cryptocurrency)
    {
        Cryptocurrency::firstOrCreate($request->validated());

        return redirect()->route('admin.cryptocurrencies.index')->with('message', 'cryptocurrency created');
    }

    /**
     * @brief Updates an existing cryptocurrency in storage
     */
    public function update(UpdateCryptocurrency $request, Cryptocurrency $cryptocurrency)
    {
        $cryptocurrency->update([
            'fee' => $request->fee,
        ]);

        return redirect()->route('admin.cryptocurrencies.index')->with('message', 'cryptocurrency updated');
    }

    /**
     * @brief Removes the specified cryptocurrency
     */
    public function destroy(Cryptocurrency $cryptocurrency)
    {
        $cryptocurrency->delete();

        return redirect()->route('admin.cryptocurrencies.index')->with('message', 'cryptocurrency deleted');
    }
}
