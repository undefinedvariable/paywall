<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateNews;
use App\Models\News;

class AdminNewsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(News::class, 'news');
        $this->middleware('admin');
    }

    /**
     * @brief Displays all news
     */
    public function index()
    {
        $news = News::all();

        return view('admin.news.index', compact('news'));
    }

    /**
     * @brief Shows the form for creating a new news
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * @brief Stores a newly created news in storage
     */
    public function store(CreateNews $request)
    {
        News::create($request->validated());

        return redirect()->route('admin.news.index')->with('message', 'news successfully created');
    }

    /**
     * @brief Removes the specified news
     */
    public function destroy(News $news)
    {
        $news->delete();

        return redirect()->route('admin.news.index')->with('message', 'news successfully deleted');
    }
}
