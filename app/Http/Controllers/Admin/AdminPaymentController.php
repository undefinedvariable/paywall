<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\WalletError;
use App\Http\Controllers\Controller;
use App\Models\PaymentRequest;
use App\Services\MoneroRPC\Monero;
use Cache;
use Illuminate\Support\Facades\Log;

class AdminPaymentController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(PaymentRequest::class, 'payment_request');
        $this->middleware('staff');
    }

    public function index()
    {
        $payments = PaymentRequest::with(['user', 'merchant'])->get();

        return view('admin.payments.index', compact('payments'));
    }

    public function reported()
    {
        $payments = PaymentRequest::with(['user', 'merchant'])->reported()->get();

        return view('admin.payments.reported', compact('payments'));
    }

    public function show(PaymentRequest $payment)
    {
        if ($payment->isFE() || $payment->multisigWallet == null) {
            $balance = -1;
            $transactions = [];
        } else {
            // Cache for 2 minutes
            $balance = Cache::remember($payment->id.'_wallet_balance', 60 * 2, function () use ($payment) {
                try {
                    $get_balance = Monero::get_balance($payment->multisigWallet->filename);

                    return $get_balance / 1000000000000;
                } catch (WalletError $e) {
                    Log::error('[EscrowController] exception: '.$e->getMessage());

                    return -1;
                }
            });

            $transfers = Cache::remember($payment->id.'_wallet_transactions', 60 * 2, function () use ($payment) {
                $transfers = Monero::check_transactions($payment->multisigWallet->filename);

                return $transfers;
            });

            $transactions = [];

            if ($transfers != null) {
                if (array_key_exists('in', $transfers)) {
                    foreach ($transfers['in'] as $t) {
                        $t += ['in' => true];
                        array_push($transactions, $t);
                    }
                }

                if (array_key_exists('out', $transfers)) {
                    foreach ($transfers['out'] as $t) {
                        $t += ['in' => false];
                        array_push($transactions, $t);
                    }
                }
            }
        }

        return view('admin.payments.show', compact('payment', 'balance', 'transactions'));
    }
}
