<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Priority;
use App\Http\Controllers\Controller;
use App\Models\States\Ticket\Closed;
use App\Models\States\Ticket\Open;
use App\Models\Ticket;
use App\Models\TicketComment;

class AdminTicketController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Ticket::class, 'ticket');
        $this->middleware('staff');
    }

    public function index()
    {
        $tickets = Ticket::with('user')->get();

        return view('admin.tickets.index', compact('tickets'));
    }

    public function open()
    {
        $tickets = Ticket::with('user')->open()->get();

        return view('admin.tickets.open', compact('tickets'));
    }

    public function show(Ticket $ticket)
    {
        $user_comments = TicketComment::where('ticket_id', $ticket->id)->where('user_id', $ticket->user_id)->get();
        $staff_comments = TicketComment::where('ticket_id', $ticket->id)->where('user_id', '!=', $ticket->user_id)->get();

        return view('admin.tickets.show', compact('ticket', 'user_comments', 'staff_comments'));
    }

    public function changeStatus(Ticket $ticket, $status)
    {
        switch($status)
        {
            case 'open':
                $ticket->transitionTo(Open::class);
                break;

            case 'closed':
                $ticket->transitionTo(Closed::class);
                break;

            default:
                return redirect()->route('admin.tickets.show', ['ticket' => $ticket])->withErrors('undefined destination status');
        }

        return redirect()->route('admin.tickets.show', ['ticket' => $ticket])->with('message', 'status changed');
    }

    public function changePriority(Ticket $ticket, $priority)
    {
        switch($priority)
        {
            case 'low':
                $ticket->update(['priority' => Priority::LOW]);
                break;

            case 'medium':
                $ticket->update(['priority' => Priority::MEDIUM]);
                break;

            case 'high':
                $ticket->update(['priority' => Priority::HIGH]);
                break;

            default:
                return redirect()->route('admin.tickets.show', ['ticket' => $ticket])->withErrors('undefined destination priority');
        }

        return redirect()->route('admin.tickets.show', ['ticket' => $ticket])->with('message', 'ticket priority changed');
    }
}
