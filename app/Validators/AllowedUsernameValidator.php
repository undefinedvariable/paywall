<?php

namespace App\Validators;

use Illuminate\Config\Repository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Routing\Router;

class AllowedUsernameValidator
{
    private $router;
    private $files;
    private $config;

    public function __construct(Router $router, Filesystem $files, Repository $config)
    {
        $this->config = $config;
        $this->router = $router;
        $this->files = $files;
    }

    public function validate($attribute, $username)
    {
        $username = trim(strtolower($username));

        if ($this->isReservedUsername($username)) {
            return false;
        }

        if ($this->matchesRoute($username)) {
            return false;
        }

        if ($this->matchesPublicFileOrDirectory($username)) {
            return false;
        }

        return true;
    }

    private function isReservedUsername($username)
    {
        return in_array(strtolower($username), $this->config->get('reserved_words.usernames'));
    }

    private function matchesRoute($username)
    {
        foreach ($this->router->getRoutes() as $route) {
            if (strtolower($route->uri) === $username) {
                return true;
            }
        }

        return false;
    }

    private function matchesPublicFileOrDirectory($username)
    {
        foreach ($this->files->glob(public_path().'/*') as $path) {
            if (strtolower(basename($path)) === $username) {
                return true;
            }
        }

        return false;
    }
}
