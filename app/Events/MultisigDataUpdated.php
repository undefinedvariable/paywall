<?php

namespace App\Events;

use App\Models\MultisigWallet;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class MultisigDataUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $user_id;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(MultisigWallet $ms_wallet, User $user)
    {
        $this->user_id = $user->id;

        if ($user->isMerchantInPayment($ms_wallet->paymentRequest)) {
            $whoami = 'MERCHANT';
        } else {
            $whoami = 'CUSTOMER';
        }

        if ($ms_wallet->isCreated()) {
            $multisig_info = $user->isMerchantInPayment($ms_wallet->paymentRequest) ? $ms_wallet->prepare_multisig_data_vendor : $ms_wallet->prepare_multisig_data_customer;

            $this->data = [
                'multisig_info'                 => $multisig_info,
                'status'                        => $ms_wallet->getStatusString(),
                'whoami'                        => $whoami,
                'payment_request_id'            => $ms_wallet->payment_request_id,
            ];
        } elseif ($ms_wallet->isPrepared()) {
            $multisig_info = $user->isMerchantInPayment($ms_wallet->paymentRequest) ? $ms_wallet->make_multisig_data_vendor : $ms_wallet->make_multisig_data_customer;

            $multisig_info_peers = [$ms_wallet->prepare_multisig_data_system];
            if ($user->isMerchantInPayment($ms_wallet->paymentRequest)) {
                array_push($multisig_info_peers, $ms_wallet->prepare_multisig_data_customer);
            } else {
                array_push($multisig_info_peers, $ms_wallet->prepare_multisig_data_vendor);
            }

            $this->data = [
                'multisig_info'                 => $multisig_info,
                'multisig_info_peers'           => Arr::remove_null($multisig_info_peers),
                'status'                        => $ms_wallet->getStatusString(),
                'whoami'                        => $whoami,
                'payment_request_id'            => $ms_wallet->payment_request_id,
            ];
        } elseif ($ms_wallet->isMade()) {
            $multisig_info = $user->isMerchantInPayment($ms_wallet->paymentRequest) ? $ms_wallet->finalize_multisig_data_vendor : $ms_wallet->finalize_multisig_data_customer;

            $multisig_info_peers = [$ms_wallet->make_multisig_data_system];
            if ($user->isMerchantInPayment($ms_wallet->paymentRequest)) {
                array_push($multisig_info_peers, $ms_wallet->make_multisig_data_customer);
            } else {
                array_push($multisig_info_peers, $ms_wallet->make_multisig_data_vendor);
            }

            $this->data = [
                'multisig_address'              => $multisig_info,
                'multisig_info_peers'           => Arr::remove_null($multisig_info_peers),
                'status'                        => $ms_wallet->getStatusString(),
                'whoami'                        => $whoami,
                'payment_request_id'            => $ms_wallet->payment_request_id,
            ];
        } else {
            $this->data = [
                'address'                       => $ms_wallet->address,
                'status'                        => $ms_wallet->getStatusString(),
                'whoami'                        => $whoami,
                'payment_request_id'            => $ms_wallet->payment_request_id,
            ];
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('users.'.$this->user_id);
    }
}
