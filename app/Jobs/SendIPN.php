<?php

namespace App\Jobs;

use App\Exceptions\JobRetryException;
use App\Http\Resources\PaymentRequest as PaymentRequestResource;
use App\Models\PaymentRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\RateLimited;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class SendIPN implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 10;
    public $backoff = 120;
    public $deleteWhenMissingModels = true;

    protected $payment_request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PaymentRequest $payment_request)
    {
        $this->payment_request = $payment_request;
    }

    /*
    public function middleware()
    {
        return [new RateLimited('ipn')];
    }
    */

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = Http::timeout(30)->retry(3, 100)->withHeaders([
            'Accept' => 'application/json',
            'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; rv:52.0) Gecko/20100101 Firefox/52.0',
        ])->post($this->payment_request->merchant->ipn_url, [
            'payment' => new PaymentRequestResource($this->payment_request),
        ]);

        if ($response->status() != 200) {
            throw new JobRetryException('IPN failed');
        }
    }
}
