<?php

namespace App\Jobs\General;

use App\Models\PaymentRequest;
use App\Models\States\PaymentRequest\Expired;
use App\Notifications\PaymentExpired;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PaymentRequestsExpirer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 2;

    public function __construct()
    {
    }

    public function handle()
    {
        $pending = PaymentRequest::pending()->where('updated_at', '<', Carbon::now()->subDays(config('expiration.payment.pending')))->get();
        foreach ($pending as $payment) {
            $payment->merchant->user->notify(new PaymentExpired($payment));

            $payment->transitionTo(Expired::class);
        }

        $pending = PaymentRequest::confirming()->where('updated_at', '<', Carbon::now()->subDays(config('expiration.payment.confirming')))->get();
        foreach ($pending as $payment) {
            $payment->merchant->user->notify(new PaymentExpired($payment));

            $payment->transitionTo(Expired::class);
        }
    }
}
