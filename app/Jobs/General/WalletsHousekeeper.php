<?php

namespace App\Jobs\General;

use App\Models\MultisigWallet;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WalletsHousekeeper implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $wallets = MultisigWallet::where(['created_at', '<', Carbon::now()->subWeek()])->get();
        foreach ($wallets as $wallet) {
            if ($wallet->paymentRequest->isCancelled() || $wallet->paymentRequest->isExpired() || $wallet->paymentRequest->isClosed() || $wallet->paymentRequest->isRefunded()) {
                $wallet->delete();
            }
        }
    }
}
