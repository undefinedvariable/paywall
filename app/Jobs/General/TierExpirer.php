<?php

namespace App\Jobs\General;

use App\Enums\Tier;
use App\Models\UserTier;
use App\Notifications\TierExpired;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TierExpirer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user_tiers = UserTier::where('tier_expiration', '<', Carbon::now())->get();

        foreach ($user_tiers as $user_tier) {
            $user_tier->update([
                'credits' => config('api.free_credits'),
                'tier' => Tier::FREE,
                'tier_expiration' => Carbon::now()->addMonths(config('api.tier_expiration')),
            ]);

            $user_tier->user->notify(new TierExpired($user_tier->user));
        }
    }
}
