<?php

namespace App\Jobs;

use App\Exceptions\WalletError;
use App\Models\Merchant;
use App\Services\MoneroRPC\Monero;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\Middleware\RateLimited;

/**
 * @todo RPC blocks after view-only wallet creation
 */
class CreatePersonalWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $backoff = 60;
    public $deleteWhenMissingModels = true;

    private $merchant;
    private $address;
    private $view_key;

    public function __construct(Merchant $merchant, $address, $view_key)
    {
        $this->merchant = $merchant;
        $this->address = $address;
        $this->view_key = $view_key;
    }

    /*
    public function middleware()
    {
        return [new RateLimited('create-wallet')];
    }
    */

    public function handle()
    {
        Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] Start');

        $filename = merchant_wallet_name($this->merchant->user->id);
        if (Monero::wallet_exists($filename)) {
            Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] Wallet already exists');

            if ($this->merchant->paymentRequests()->funded()->count() > 0 || $this->merchant->paymentRequests()->reported()->count() > 0) {
                Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] Wallet currently in use');

                return;
            }

            $this->merchant->update(['personal_wallet_address' => $this->address]);

            Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] Deleting wallet...');
            Monero::delete_wallet($filename);
            Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] ...OK');
        }

        Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] Generating wallet...');
        if (Monero::generate_from_keys($filename, $this->address, $this->view_key)) {
            Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] ...OK');

            Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] Adding address to the wallet...');
            $this->merchant->update(['personal_wallet_address' => $this->address]);
            Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] ...OK');
        } else {
            throw new WalletError('connection problem');
        }

        Log::debug('[CreatePersonalWallet #'.$this->job->getJobId().'] End');
    }
}
