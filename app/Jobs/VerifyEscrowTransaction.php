<?php

namespace App\Jobs;

use App\Exceptions\JobRetryException;
use App\Models\PaymentRequest;
use App\Models\States\PaymentRequest\Funded;
use App\Notifications\PaymentReceived;
use App\Notifications\PaymentSent;
use App\Services\CryptoExchange\CryptoExchangeContract;
use App\Services\MoneroRPC\Monero;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class VerifyEscrowTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 10;
    public $backoff = 20;
    public $deleteWhenMissingModels = true;

    private $payment_request;

    public function __construct(PaymentRequest $payment_request)
    {
        $this->payment_request = $payment_request;
    }

    /*
    public function middleware()
    {
        return [
            new WithoutOverlapping($this->payment_request->id)
        ];
    }
    */

    public function handle(CryptoExchangeContract $api)
    {
        // wrong job invoked
        if ($this->payment_request->isFE() || $this->payment_request->multisigWallet == null) {
            Log::error('[VerifyEscrowTransaction #'.$this->job->getJobId().'] Wong job invoked for payment: '.$this->payment_request->id);

            return;
        }

        Log::debug('[VerifyEscrowTransaction #'.$this->job->getJobId().'] payment status:'.$this->payment_request->status);

        // Payment already processed
        if ($this->payment_request->isFunded()) {
            return;
        }

        Log::debug('[VerifyEscrowTransaction #'.$this->job->getJobId().'] 1');
        // payment is confirming
        if ($this->payment_request->isConfirming()) {
            Log::debug('[VerifyEscrowTransaction #'.$this->job->getJobId().'] 2');

            // payment was made in Monero
            if (Monero::get_balance($this->payment_request->multisigWallet->filename) >= $this->payment_request->amount_xmr) {
                $this->payment_request->transitionTo(Funded::class);

                $this->payment_request->merchant->user->notify(new PaymentReceived($this->payment_request));
                $this->payment_request->user->notify(new PaymentSent($this->payment_request));
                Log::debug('[VerifyEscrowTransaction #'.$this->job->getJobId().'] 3');
            } else {
                Log::debug('[VerifyEscrowTransaction #'.$this->job->getJobId().'] Payment not detected');

                throw new JobRetryException('payment not detected');
            }
            Log::debug('[VerifyEscrowTransaction #'.$this->job->getJobId().'] 4');
        } else {
            Log::debug('[VerifyEscrowTransaction #'.$this->job->getJobId().'] 5');
        }
    }
}
