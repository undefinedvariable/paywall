<?php

namespace App\Jobs\Multisig;

use App\Events\MultisigDataUpdated;
use App\Exceptions\WalletError;
use App\Models\MultisigWallet;
use App\Services\MoneroRPC\Monero;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class PrepareMultisigWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $backoff = 5;
    public $deleteWhenMissingModels = true;

    public $multisigWallet;

    public function __construct(MultisigWallet $multisigWallet)
    {
        $this->multisigWallet = $multisigWallet;
    }

    /*
    public function middleware()
    {
        return [new WithoutOverlapping($this->payment_request->id)];
    }
    */

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('[PrepareMultisigWallet #'.$this->job->getJobId().'] Job start');

        if ($this->multisigWallet == null) {
            Log::error('[PrepareMultisigWallet #'.$this->job->getJobId().'] Multisig wallet not found');

            return;
        }

        $this->multisigWallet->fresh();

        if ($this->multisigWallet->prepare_multisig_data_system == null) {
            $multisig_info = Monero::prepare_multisig($this->multisigWallet->filename);

            if (empty($multisig_info)) {
                Log::error('[PrepareMultisigWallet #'.$this->job->getJobId().'] Monero API error');

                throw new WalletError('monero API error');
            } else {
                $this->multisigWallet->update([
                    'prepare_multisig_data_system' => $multisig_info,
                ]);

                //broadcast(new MultisigDataUpdated($this->multisigWallet, $this->multisigWallet->paymentRequest->user));
                //broadcast(new MultisigDataUpdated($this->multisigWallet, $this->multisigWallet->paymentRequest->merchant->user));
            }
        }

        Log::debug('[PrepareMultisigWallet #'.$this->job->getJobId().'] Job end');
    }
}
