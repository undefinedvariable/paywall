<?php

namespace App\Jobs\Multisig;

use App\Exceptions\WalletError;
use App\Models\MultisigWallet;
use App\Models\PaymentRequest;
use App\Services\MoneroRPC\Monero;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CreateMultisigWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $backoff = 5;
    public $deleteWhenMissingModels = true;

    public $payment_request;

    public function __construct(PaymentRequest $payment_request)
    {
        $this->payment_request = $payment_request;
    }

    /*
    public function middleware()
    {
        return [new WithoutOverlapping($this->payment_request->id)];
    }
    */

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('[CreateMultisigWallet #'.$this->job->getJobId().'] Job start');

        // wrong job invoked
        if ($this->payment_request->isFE()) {
            Log::error('[CreateMultisigWallet #'.$this->job->getJobId().'] Wrong job invoked for payment: '.$this->payment_request->id);

            return;
        }

        if ($this->payment_request->multisigWallet == null || ! Monero::wallet_exists($this->payment_request->multisigWallet->filename)) {
            $filename = escrow_wallet_name($this->payment_request->id);
            if (! Monero::create_wallet($filename)) {
                Log::error('[CreateMultisigWallet #'.$this->job->getJobId().'] Monero API error');

                throw new WalletError('connection problem');
            }
            $wallet = MultisigWallet::create([
                'filename'  =>  $filename,
                'payment_request_id' => $this->payment_request->id,
            ]);

            if (empty($wallet)) {
                Log::error('[CreateMultisigWallet #'.$this->job->getJobId().'] Empty wallet');

                throw new WalletError('monero API error');
            } else {
                PrepareMultisigWallet::dispatch($wallet);
            }
        }

        Log::debug('[CreateMultisigWallet #'.$this->job->getJobId().'] Job end');
    }
}
