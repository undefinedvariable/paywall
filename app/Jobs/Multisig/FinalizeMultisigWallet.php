<?php

namespace App\Jobs\Multisig;

use App\Events\MultisigDataUpdated;
use App\Exceptions\WalletError;
use App\Models\MultisigWallet;
use App\Models\States\MultisigWallet\Made;
use App\Services\MoneroRPC\Monero;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class FinalizeMultisigWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $backoff = 5;
    public $deleteWhenMissingModels = true;

    public $multisigWallet;

    public function __construct(MultisigWallet $multisigWallet)
    {
        $this->multisigWallet = $multisigWallet;
    }

    /*
    public function middleware()
    {
        return [new WithoutOverlapping($this->payment_request->id)];
    }
    */

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->multisigWallet == null) {
            Log::error('[FinalizeMultisigWallet #'.$this->job->getJobId().'] Multisig wallet not found');

            return;
        }

        $this->multisigWallet->fresh();

        Log::debug('[FinalizeMultisigWallet] 1');

        if ($this->multisigWallet->address == null) {
            $multisig_info = Monero::finalize_multisig(
                $this->multisigWallet->filename,
                $this->multisigWallet->make_multisig_data_customer,
                $this->multisigWallet->make_multisig_data_vendor
            );

            Log::debug('[FinalizeMultisigWallet] 2');

            if (empty($multisig_info)) {
                Log::error('[FinalizeMultisigWallet #'.$this->job->getJobId().'] Monero API error');

                throw new WalletError('monero API error');
            } else {
                $this->multisigWallet->update([
                    'prepare_multisig_data_customer' => '',
                    'prepare_multisig_data_vendor' => '',
                    'prepare_multisig_data_system' => '',
                    'address' => $multisig_info,
                ]);

                Log::debug('[FinalizeMultisigWallet] updating wallet status to made...');
                $this->multisigWallet->transitionTo(Made::class);
                Log::debug('[FinalizeMultisigWallet] ...ok');

                //broadcast(new MultisigDataUpdated($this->multisigWallet, $this->multisigWallet->paymentRequest->user));
                //broadcast(new MultisigDataUpdated($this->multisigWallet, $this->multisigWallet->paymentRequest->merchant->user));
            }
        } else {
            Log::debug('[FinalizeMultisigWallet] 3');
        }
    }
}
