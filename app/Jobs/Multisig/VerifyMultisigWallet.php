<?php

namespace App\Jobs\Multisig;

use App\Events\MultisigDataUpdated;
use App\Exceptions\WalletError;
use App\Models\MultisigWallet;
use App\Models\States\MultisigWallet\Finalized;
use App\Services\MoneroRPC\Monero;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class VerifyMultisigWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;
    public $backoff = 5;
    public $deleteWhenMissingModels = true;

    public $multisigWallet;

    public function __construct(MultisigWallet $multisigWallet)
    {
        $this->multisigWallet = $multisigWallet;
    }

    /*
    public function middleware()
    {
        return [new WithoutOverlapping($this->payment_request->id)];
    }
    */

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->multisigWallet == null) {
            Log::error('[VerifyMultisigWallet #'.$this->job->getJobId().'] Multisig wallet not found');

            return;
        }

        $this->multisigWallet->fresh();

        if (
            $this->multisigWallet->finalize_multisig_data_customer ===
            $this->multisigWallet->finalize_multisig_data_vendor &&
            $this->multisigWallet->finalize_multisig_data_vendor ==
            $this->multisigWallet->address
        ) {
            if (Monero::is_multisig($this->multisigWallet->filename)) {
                $this->multisigWallet->update([
                    'make_multisig_data_customer' => '',
                    'make_multisig_data_vendor' => '',
                    'make_multisig_data_system' => '',
                    'finalize_multisig_data_customer' => '',
                    'finalize_multisig_data_vendor' => '',
                ]);

                Log::debug('[VerifyMultisigWallet] updating wallet status to finalized...');
                $this->multisigWallet->transitionTo(Finalized::class);
                Log::debug('[VerifyMultisigWallet] ...ok');

                //broadcast(new MultisigDataUpdated($this->multisigWallet, $this->multisigWallet->paymentRequest->user));
                //broadcast(new MultisigDataUpdated($this->multisigWallet, $this->multisigWallet->paymentRequest->merchant->user));
            } else {
                Log::error('[VerifyMultisigWallet #'.$this->job->getJobId().'] Monero API error');

                throw new WalletError('monero API error');
            }
        } else {
            Log::error('[VerifyMultisigWallet #'.$this->job->getJobId().'] Wallet not verified');

            throw new WalletError('wallet not verified');
        }
    }
}
