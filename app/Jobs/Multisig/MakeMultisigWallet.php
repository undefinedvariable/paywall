<?php

namespace App\Jobs\Multisig;

use App\Events\MultisigDataUpdated;
use App\Exceptions\WalletError;
use App\Models\MultisigWallet;
use App\Models\States\MultisigWallet\Prepared;
use App\Services\MoneroRPC\Monero;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class MakeMultisigWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $backoff = 5;
    public $deleteWhenMissingModels = true;

    public $multisigWallet;

    public function __construct(MultisigWallet $multisigWallet)
    {
        $this->multisigWallet = $multisigWallet;
    }

    /*
    public function middleware()
    {
        return [new WithoutOverlapping($this->payment_request->id)];
    }
    */

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->multisigWallet == null) {
            Log::error('[MakeMultisigWallet #'.$this->job->getJobId().'] Multisig wallet not found');

            return;
        }

        $this->multisigWallet->fresh();

        if ($this->multisigWallet->make_multisig_data_system == null) {
            $multisig_info = Monero::make_multisig(
                $this->multisigWallet->filename,
                $this->multisigWallet->prepare_multisig_data_customer,
                $this->multisigWallet->prepare_multisig_data_vendor
            );

            if (empty($multisig_info)) {
                Log::error('[MakeMultisigWallet #'.$this->job->getJobId().'] Monero API error');

                throw new WalletError('monero API error');
            } else {
                $this->multisigWallet->update([
                    'make_multisig_data_system' => $multisig_info,
                ]);

                Log::debug('[MakeMultisigWallet] updating wallet status to prepared...');
                $this->multisigWallet->transitionTo(Prepared::class);
                Log::debug('[MakeMultisigWallet] ...ok');

                //broadcast(new MultisigDataUpdated($this->multisigWallet, $this->multisigWallet->paymentRequest->user));
                //broadcast(new MultisigDataUpdated($this->multisigWallet, $this->multisigWallet->paymentRequest->merchant->user));
            }
        }
    }
}
