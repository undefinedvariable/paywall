<?php

namespace App\Jobs;

use App\Exceptions\JobRetryException;
use App\Models\PaymentRequest;
use App\Models\States\PaymentRequest\Closed;
use App\Notifications\PaymentReceived;
use App\Notifications\PaymentSent;
use App\Services\MoneroRPC\Monero;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class VerifyFETransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 10;
    public $backoff = 20;
    public $deleteWhenMissingModels = true;

    private $payment_request;

    public function __construct(PaymentRequest $payment_request)
    {
        $this->payment_request = $payment_request;
    }

    /*
    public function middleware()
    {
        return [
            new WithoutOverlapping($this->payment_request->id)
        ];
    }
    */

    public function handle()
    {
        Log::debug('[VerifyFETransaction #'.$this->job->getJobId().'] Start');

        // wrong job invoked
        if ($this->payment_request->isEscrow()) {
            Log::error('wrong job invoked for payment: '.$this->payment_request->id);

            return;
        }

        // Payment already processed
        if ($this->payment_request->isClosed()) {
            return;
        }

        $wallet_name = merchant_wallet_name($this->payment_request->merchant->user->id);
        $payments = Monero::get_payments($wallet_name, $this->payment_request->payment_id);

        foreach ($payments as $payment) {
            if (monero_balance_formatter($payment[0]['amount']) >= $this->payment_request->amount_xmr) {
                Log::debug('[VerifyFETransaction #'.$this->job->getJobId().'] Payment verified');

                Log::debug('[VerifyFETransaction #'.$this->job->getJobId().'] Transition');
                $this->payment_request->transitionTo(Closed::class);
                Log::debug('[VerifyFETransaction #'.$this->job->getJobId().'] send notification');

                $this->payment_request->merchant->user->notify(new PaymentReceived($this->payment_request));
                $this->payment_request->user->notify(new PaymentSent($this->payment_request));

                Log::debug('[VerifyFETransaction #'.$this->job->getJobId().'] End');

                return;
            }
        }

        throw new JobRetryException('payment not detected');
    }
}
