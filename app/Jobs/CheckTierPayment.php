<?php

namespace App\Jobs;

use App\Enums\Tier;
use App\Exceptions\JobRetryException;
use App\Models\User;
use App\Notifications\TierChanged;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use MoneroIntegrations\MoneroPhp\walletRPC;

class CheckTierPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 10;
    public $backoff = 20;
    public $deleteWhenMissingModels = true;

    private $user;
    private $subaddr_index;

    public function __construct(User $user, $subaddr_index)
    {
        $this->user = $user;
        $this->subaddr_index = $subaddr_index;
    }

    public function handle(walletRPC $walletRPC)
    {
        $tier = Tier::FREE;
        $credits = config('api.free_credits');

        /* @todo simplify */
        $walletRPC->open_wallet(config('monero.admin_wallet_filename'), config('monero.wallet_pass'));
        $transfers = $walletRPC->incoming_transfers('available', 0, $this->subaddr_index);

        if (empty($transfers)) {
            throw new JobRetryException('payment not detected');
        }
        $amount = monero_balance_formatter($transfers['transfers'][0]['amount']);

        if ($amount >= config('tiers.gold_threshold')) {
            $tier = Tier::GOLD;
            $credits = config('api.gold_credits');
        } elseif ($amount >= config('tiers.silver_threshold')) {
            $tier = Tier::SILVER;
            $credits = config('api.silver_credits');
        } elseif ($amount >= config('tiers.bronze_threshold')) {
            $tier = Tier::BRONZE;
            $credits = config('api.bronze_credits');
        }

        if ($tier != Tier::FREE) {
            $this->user->user_tier->update([
                'credits' => $credits,
                'tier' => $tier,
                'tier_expiration' => Carbon::now()->addMonths(config('api.tier_expiration')),
            ]);
            $this->user->touch();
            $this->user->notify(new TierChanged($this->user, Tier::fromValue($tier)));
        } else {
            throw new JobRetryException('Verification failed');
        }
    }
}
