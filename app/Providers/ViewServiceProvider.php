<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(\App\Http\Views\Composers\UserComposer::class);
    }

    public function boot()
    {
        // Using class based composers...
        View::composer(
            '*',
            'App\Http\Views\Composers\UserComposer'
        );
    }
}
