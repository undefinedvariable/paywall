<?php

namespace App\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @brief Registers Telescope Service only in local (debug) environment
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        /*
        RateLimiter::for('ipn', function ($job) {
            Limit::perHour(6)->by($job->payment_request->id);
        });
        */

        Arr::macro('remove_null', function ($arr) {
            if (! is_array($arr)) {
                return $arr;
            }

            foreach ($arr as &$value) {
                if (is_array($value)) {
                    $value = Arr::remove_null($value);
                }
            }

            return array_filter($arr, function ($var) {
                return ! is_null($var);
            });
        });

        Validator::extend(
            'allowed_username',
            'App\Validators\AllowedUsernameValidator@validate',
        );

        RateLimiter::for('create-wallet', function ($job) {
            return Limit::perHour(1)->by($job->user->id);
        });

        RateLimiter::for('ipn', function ($job) {
            return Limit::perMinute(1)->by($job->payment_request->id);
        });
    }
}
