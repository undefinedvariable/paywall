<?php

namespace App\Providers;

use App\Models\Coupon;
use App\Models\Cryptocurrency;
use App\Models\Dispute;
use App\Models\Marketplace;
use App\Models\Merchant;
use App\Models\News;
use App\Models\PaymentRequest;
use App\Models\Ticket;
use App\Models\User;
use App\Policies\CouponPolicy;
use App\Policies\CryptocurrencyPolicy;
use App\Policies\DisputePolicy;
use App\Policies\MarketplacePolicy;
use App\Policies\MerchantPolicy;
use App\Policies\NewsPolicy;
use App\Policies\PaymentRequestPolicy;
use App\Policies\TicketPolicy;
use App\Policies\UserPolicy;
use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * @brief The policy mappings for the application.
     */
    protected $policies = [
        Coupon::class => CouponPolicy::class,
        Cryptocurrency::class => CryptocurrencyPolicy::class,
        Dispute::class => DisputePolicy::class,
        News::class => NewsPolicy::class,
        Merchant::class => MerchantPolicy::class,
        Marketplace::class => MarketplacePolicy::class,
        PaymentRequest::class => PaymentRequestPolicy::class,
        Ticket::class => TicketPolicy::class,
        User::class => UserPolicy::class,
    ];

    /**
     * @brief Register any authentication / authorization services.
     *        Register passport api authorization system
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::tokensExpireIn(Carbon::now()->addDays(30));
        //Passport::refreshTokensExpireIn(Carbon::now()->addDays(10));
    }
}
