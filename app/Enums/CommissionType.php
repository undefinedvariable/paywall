<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class CommissionType extends Enum
{
    const FIXED = 100;
    const PERCENTAGE = 101;
    const NO_COMMISSION = 102;
}
