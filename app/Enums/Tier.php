<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @brief Merchant tiers from free to premium
 */
final class Tier extends Enum
{
    const FREE = 80;
    const BRONZE = 81;
    const SILVER = 82;
    const GOLD = 83;
}
