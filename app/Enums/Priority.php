<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class Priority extends Enum
{
    const LOW = 70;
    const MEDIUM = 71;
    const HIGH = 72;
}
