<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TransferAction extends Enum
{
    const CLOSE = 90;
    const REFUND = 91;
}
