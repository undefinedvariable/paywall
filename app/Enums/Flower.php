<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @brief Flower used as an antiphishing
 */
final class Flower extends Enum
{
    const WHITE = 20;
    const PINK = 21;
    const BLUE = 22;
    const GREEN = 23;
    const GREY = 24;
    const YELLOW = 25;
    const BLACK = 26;
    const RED = 27;
}
