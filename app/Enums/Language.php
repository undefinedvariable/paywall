<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @brief Languages supported
 */
final class Language extends Enum
{
    const EN = 40;
    const ES = 41;
    const FR = 42;
    const IT = 44;
    const RU = 45;
}
