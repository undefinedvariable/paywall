<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @brief Type of payments supported
 *        Finalize Early: directly paying the merchant
 *        Escrow: long term payment where the platform
 *                holds funds until released or refunded
 */
final class PaymentType extends Enum
{
    const ESCROW = 60;
    const FE = 61;
}
