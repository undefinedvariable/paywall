<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @brief Type of goods of the payment request
 */
final class GoodsType extends Enum
{
    const DIGITAL = 30;
    const PHYSICAL = 31;
    const SERVICES = 32;
}
