<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @brief The type of accounts available on the platform
 */
final class AccountType extends Enum
{
    const USER = 10;
    const MERCHANT = 11;
    const MARKETPLACE = 12;
    const STAFF = 13;
    const ADMIN = 14;
}
