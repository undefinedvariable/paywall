<?php

namespace App\Services\PGP;

use Illuminate\Support\Facades\Storage;

class PGP
{
    public function __construct()
    {
    }

    public function loadKey($publickey)
    {
        $key = \OpenPGP_Message::parse(
            \OpenPGP::unarmor($publickey, 'PGP PUBLIC KEY BLOCK')
        );

        return $key;
    }

    public function encryptMsg($key, $message)
    {
        $data = new \OpenPGP_LiteralDataPacket($message);
        $encrypted = \OpenPGP_Crypt_Symmetric::encrypt(
                $key,
                new \OpenPGP_Message([$data])
        );
        $msg = \OpenPGP::enarmor($encrypted->to_bytes(), 'PGP MESSAGE');

        return $msg;
    }

    public function saveKey($key, $username): bool
    {
        Storage::disk('local')->put('keys/'.$username.'_pgp.key', $key);
        if (! Storage::disk('local')->exists('keys/'.$username.'_pgp.key')) {
            return false;
        }

        return true;
    }

    public function removeKey($username)
    {
        Storage::disk('local')->delete('keys/'.$username.'_pgp.key');
    }
}
