<?php

namespace App\Services\PGP;

use Illuminate\Support\ServiceProvider;

class PGPServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(PGP::class, function () {
            return new PGP();
        });
    }

    public function provides()
    {
        return [PGP::class];
    }
}
