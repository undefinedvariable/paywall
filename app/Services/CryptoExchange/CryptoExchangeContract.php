<?php

namespace App\Services\CryptoExchange;

interface CryptoExchangeContract
{
    public function make_swap($input_coin, $output_coin, $address, $refund = '');

    public function get_swap($id);

    public function get_rates($from, $to);
}
