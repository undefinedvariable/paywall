<?php

namespace App\Services\CryptoExchange;

use App\Exceptions\APIError;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class XChangeMeAPI implements CryptoExchangeContract
{
    public function make_swap($input_coin, $output_coin, $address, $refund = '')
    {
        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->post('https://xchange.me/api/v1/exchange', [
            'from_currency' => $input_coin,
            'to_currency' => $output_coin,
            'receive_address' => $address,
            'refund_address' => $refund,
            'from_currency_alt' => 0,
        ]);
        $res = $this->response_handler($response);

        return [
                'id' => $res['uuid'],
                'address' => $res['receive_address'], 
        ];
    }

    public function get_swap($id)
    {
        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->get('https://xchange.me/api/v1/exchange/'.$id);
        $res = $this->response_handler($response);

        return [
            'amount_receiced' => $res['receive_amount'],
            'status' => $res['stage'], 
        ];
    }

    public function get_rates($from, $to)
    {
        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->get('https://xchange.me/api/v1/exchange/estimate?from_currency='.strtolower($from).'&to_currency='.strtolower($to));
        $res = $this->response_handler($response);

        return [
            'rate'  => floatval($res['rate']),
            'fee'   => floatval($res['fee_from']),
            'min'   => floatval(config('cryptoexchange.xchangeme.'.$from.'.min')),
        ];
    }

    private function response_handler($response)
    {
        if ($response->status() == 200) {
            $res = json_decode($response->body(), true);

            return $res;
        } elseif($response->status() == 422) {
            $res = json_decode($response->body(), true);

            throw new APIError($res->message);
        } else {
            Log::error('XChangeMe API currently unavailable');

            throw new APIError('XChangeMe API currently unavailable');
        }
    }
}
