<?php

namespace App\Services\CryptoExchange;

use App\Exceptions\APIError;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class MorphTokenAPI implements CryptoExchangeContract
{
    public function make_swap($input_coin, $output_coin, $address, $refund = '')
    {
        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->post('https://api.morphtoken.com/morph', ['json' => [
            'input' => [
                'asset' => $input_coin,
                'refund' => $refund,
            ],
            'output' => [[
                'asset' => $output_coin,
                'weight' => 10000,
                'address' =>  $address,
            ]],
        ]]);
        $res = $this->response_handler($response);

        return [
            'id' => $res['id'],
            'address' => $res['input']['deposit_address'], 
        ];
    }

    public function get_swap($id)
    {
        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->get('https://api.morphtoken.com/morph/'.$id);
        $res = $this->response_handler($response);

        return [
            'amount_receiced' => $res['input']['received'],
            'status' => $res['state'], 
        ];
    }

    public function get_rates($from, $to)
    {
        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->get('https://api.morphtoken.com/rates');
        $res = $this->response_handler($response);

        return [
            'rate'  => floatval($res['data'][$from][$to]),
            'fee'   => floatval(config('cryptoexchange.xchangeme.'.$from.'.fee')),
            'min'   => floatval(config('cryptoexchange.xchangeme.'.$from.'.min')),
        ];
    }

    private function response_handler($response)
    {
        if ($response->status() == 200) {
            $res = json_decode($response->body(), true);

            return $res;
        } elseif($response->status() == 422) {
            $res = json_decode($response->body(), true);

            throw new APIError($res->message);
        } else {
            Log::error('MorphToken API currently unavailable');

            throw new APIError('MorphToken API currently unavailable');
        }
    }
}
