<?php

namespace App\Services\CryptoExchange;

class FallbackClient implements CryptoExchangeContract {
    private $primaryClient;
    private $fallbackClient;

    public function __construct(
        CryptoExchangeContract $primaryClient,
        CryptoExchangeContract $fallbackClient
    ) {
        $this->primaryClient = $primaryClient;
        $this->fallbackClient = $fallbackClient;
    }

    public function make_swap($input_coin, $output_coin, $address, $refund = '')
    {
        return rescue(
            fn () => $this->primaryClient->make_swap($input_coin, $output_coin, $address, $refund = ''),
            fn () => $this->fallbackClient->make_swap($input_coin, $output_coin, $address, $refund = ''),
        );
    }

    public function get_swap($id)
    {
        return rescue(
            fn () => $this->primaryClient->get_swap($id),
            fn () => $this->fallbackClient->get_swap($id),
        );
    }

    public function get_rates($from, $to)
    {
        return rescue(
            fn () => $this->primaryClient->get_rates($from, $to),
            fn () => $this->fallbackClient->get_rates($from, $to),
        );
    }
}