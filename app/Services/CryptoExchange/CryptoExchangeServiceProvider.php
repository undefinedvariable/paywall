<?php

namespace App\Services\CryptoExchange;

use Illuminate\Support\ServiceProvider;

class CryptoExchangeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(CryptoExchangeContract::class, function () {
            return new FallbackClient(
                $this->app->make(MorphTokenAPI::class),
                $this->app->make(XChangeMeAPI::class),
            );
        });
    }
}
