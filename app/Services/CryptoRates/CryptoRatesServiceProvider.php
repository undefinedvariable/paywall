<?php

namespace App\Services\CryptoRates;

use Illuminate\Support\ServiceProvider;

class CryptoRatesServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(CryptoRatesContract::class, function () {
            return new FallbackClient(
                $this->app->make(CryptoCompareAPI::class),
                $this->app->make(CoinPaprikaAPI::class),
            );
        });
    }
}
