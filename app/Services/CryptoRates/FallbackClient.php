<?php

namespace App\Services\CryptoRates;

class FallbackClient implements CryptoRatesContract {
    private $primaryClient;
    private $fallbackClient;

    public function __construct(
        CryptoRatesContract $primaryClient,
        CryptoRatesContract $fallbackClient
    ) {
        $this->primaryClient = $primaryClient;
        $this->fallbackClient = $fallbackClient;
    }

    public function usd_to_xmr()
    {
        return rescue(
            fn () => $this->primaryClient->usd_to_xmr(),
            fn () => $this->fallbackClient->usd_to_xmr(),
        );
    }

    public function xmr_to_usd()
    {
        return rescue(
            fn () => $this->primaryClient->xmr_to_usd(),
            fn () => $this->fallbackClient->xmr_to_usd(),
        );
    }
}