<?php

namespace App\Services\CryptoRates;

use App\Exceptions\APIError;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CoinPaprikaAPI implements CryptoRatesContract
{
    public function usd_to_xmr(): float
    {
        if (Cache::has('usd_to_xmr')) {
            return Cache::get('usd_to_xmr');
        }

        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->get('https://api.coinpaprika.com/v1/coins/xmr-monero/ohlcv/latest');
        if ($response->status() == 200) {
            $res = json_decode($response->body());
            if (! property_exists($res, 'close') || $res->close <= 0) {
                Log::error('CoinPaprika API error');

                throw new APIError('CoinPaprika API error');
            } else {
                Cache::put('usd_to_xmr', 1 / $res->close, now()->addMinutes(10));

                return 1 / $res->close;
            }
        } else {
            Log::error('CoinPaprika API currently unavailable');

            throw new APIError('CoinPaprika API currently unavailable');
        }
    }

    public function xmr_to_usd(): float
    {
        if (Cache::has('xmr_to_usd')) {
            return Cache::get('xmr_to_usd');
        }

        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->get('https://api.coinpaprika.com/v1/coins/xmr-monero/ohlcv/latest');
        if ($response->status() == 200) {
            $res = json_decode($response->body());
            if (! property_exists($res, 'close') || $res->close <= 0) {
                Log::error('CoinPaprika API error');

                throw new APIError('CoinPaprika API error');
            } else {
                Cache::put('xmr_to_usd', $res->close, now()->addMinutes(10));

                return $res->close;
            }
        } else {
            Log::error('CoinPaprika API currently unavailable');

            throw new APIError('CoinPaprika API currently unavailable');
        }
    }
}
