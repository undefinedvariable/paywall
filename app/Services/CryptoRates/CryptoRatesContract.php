<?php

namespace App\Services\CryptoRates;

interface CryptoRatesContract
{
    public function usd_to_xmr();

    public function xmr_to_usd();
}
