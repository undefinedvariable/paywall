<?php

namespace App\Services\CryptoRates;

use App\Exceptions\APIError;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CryptoCompareAPI implements CryptoRatesContract
{
    public function usd_to_xmr(): float
    {
        if (Cache::has('usd_to_xmr')) {
            return Cache::get('usd_to_xmr');
        }

        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->get('https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=XMR');
        if ($response->status() == 200) {
            $res = json_decode($response->body());
            if (! property_exists($res, 'XMR') || $res->XMR <= 0) {
                Log::error('CryptoCompare API error');

                throw new APIError('CryptoCompare API error');
            } else {
                Cache::put('usd_to_xmr', $res->XMR, now()->addMinutes(10));

                return $res->XMR;
            }
        } else {
            Log::error('CryptoCompare API currently unavailable');

            throw new APIError('CryptoCompare API currently unavailable');
        }
    }

    public function xmr_to_usd(): float
    {
        if (Cache::has('xmr_to_usd')) {
            return Cache::get('xmr_to_usd');
        }

        $response = Http::withHeaders(['Accept' => 'application/json'])->timeout(30)->retry(3, 100)->get('https://min-api.cryptocompare.com/data/price?fsym=XMR&tsyms=USD');
        if ($response->status() == 200) {
            $res = json_decode($response->body());
            if (! property_exists($res, 'USD') || $res->USD <= 0) {
                Log::error('CryptoCompare API error');

                throw new APIError('CryptoCompare API error');
            } else {
                Cache::put('xmr_to_usd', $res->USD, now()->addMinutes(10));

                return $res->USD;
            }
        } else {
            Log::error('CryptoCompare API currently unavailable');

            throw new APIError('CryptoCompare API currently unavailable');
        }
    }
}
