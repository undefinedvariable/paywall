<?php

namespace App\Services\MoneroRPC;

use App\Exceptions\WalletError;
use Exception;
use Illuminate\Support\Facades\Log;
use MoneroIntegrations\MoneroPhp\walletRPC;

class MoneroRPC
{
    public $wallet;

    public function __construct(walletRPC $walletRPC)
    {
        $this->wallet = $walletRPC;
    }

    public function wallet_exists($wallet_filename): bool
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function check_transactions($wallet_filename): array
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $transfers = $this->wallet->get_transfers();

            //$this->wallet->close_wallet();

            return $transfers;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function checkOutgoingTransaction($wallet_filename, $out_addr, $amount): bool
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $transfers = $this->wallet->get_transfers();

            if ($transfers != null) {
                if (array_key_exists('out', $transfers)) {
                    foreach ($transfers['out'] as $t) {
                        if ($t['address'] == $out_addr && $t['amount'] >= $amount) { // tx fee
                            return true;
                        }
                    }
                }
            }

            //$this->wallet->close_wallet();

            return false;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function transfer($wallet_filename, $out_addr, $amount): array
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $transfer_info = $this->wallet->transfer([
                'address' => $out_addr,
                'amount' => $amount,
                'priority' => 1,
            ]);

            //$this->wallet->close_wallet();

            return $transfer_info;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function transfer_split($wallet_filename, $params): array
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $transfer_info = $this->wallet->transfer_split($params);

            //$this->wallet->close_wallet();

            return $transfer_info;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function sweep_all($wallet_filename, $out_addr): array
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $transfer_info = $this->wallet->sweep_all($out_addr);

            //$this->wallet->close_wallet();

            return $transfer_info;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function get_address($wallet_filename): string
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $address = $this->wallet->get_address()['address'];
            //$this->wallet->close_wallet();

            return $address;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function get_balance($wallet_filename): int
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $balance = $this->wallet->get_balance()['balance'];
            //$this->wallet->close_wallet();

            return $balance;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            return -1;
        }
    }

    public function validate_address($address): array
    {
        try {
            return $this->wallet->validate_address($address);
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function delete_wallet($wallet_filename): bool
    {
        try {
            /* @todo implement it */
            return true;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function create_wallet($wallet_filename): bool
    {
        try {
            $this->wallet->create_wallet($wallet_filename, config('monero.wallet_pass'), 'English');

            return true;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function create_subaddress($wallet_filename, $label, $index = 0): array
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $address = $this->wallet->create_address($index, $label);
            //$this->wallet->close_wallet();

            return $address;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function generate_from_keys($wallet_filename, $address, $view_key): bool
    {
        try {
            $res = $this->wallet->generate_from_keys($wallet_filename, config('monero.wallet_pass'), $address, $view_key);

            return $res['address'] == $address;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Integrated Address Helper Functions
    |--------------------------------------------------------------------------
    */

    public function make_integrated_address($wallet_filename, $address = '', $payment_id = ''): array
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $integrated_address = $this->wallet->make_integrated_address($address, $payment_id);
            //$this->wallet->close_wallet();

            return $integrated_address;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function get_payments($wallet_filename, $payment_id): array
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $payments = $this->wallet->get_payments($payment_id);
            //$this->wallet->close_wallet();

            return $payments;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] monero exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Multisig Helper Functions
    |--------------------------------------------------------------------------
    */

    public function prepare_multisig($wallet_filename)
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $ms = $this->wallet->prepare_multisig()['multisig_info'];
            //$this->wallet->close_wallet();

            return $ms;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] prepare_multisig exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function make_multisig($wallet_filename, $prepare_multisig_data_1, $prepare_multisig_data_2)
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $ms = $this->wallet->make_multisig([
                $prepare_multisig_data_1,
                $prepare_multisig_data_2,
            ], 2, config('monero.wallet_pass'))['multisig_info'];
            //$this->wallet->close_wallet();

            return $ms;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] make_multisig exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function finalize_multisig($wallet_filename, $make_multisig_data_1, $make_multisig_data_2)
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $ms = $this->wallet->finalize_multisig([
                $make_multisig_data_1,
                $make_multisig_data_2,
            ], config('monero.wallet_pass'))['address'];
            //$this->wallet->close_wallet();

            return $ms;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] finalize_multisig exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function is_multisig($wallet_filename): bool
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            return $this->wallet->is_multisig()['multisig'];
        } catch (Exception $e) {
            Log::error('finalize_multisig exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Multisig Sync Functions
    |--------------------------------------------------------------------------
    */
    public function export_multisig_info($wallet_filename)
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $data = $this->wallet->export_multisig_info();

            return $data;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] export_multisig_info exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function import_multisig_info($wallet_filename, $multisig_info)
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $data = $this->wallet->import_multisig_info([$multisig_info]);

            return $data;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] import_multisig_info exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function sign_multisig($wallet_filename, $tx_data_hex)
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $data = $this->wallet->sign_multisig($tx_data_hex);

            return $data;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] finalize_multisig exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function submit_multisig($wallet_filename, $tx_data_hex)
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $data = $this->wallet->submit_multisig($tx_data_hex);

            return $data;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] finalize_multisig exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }

    public function describe_transfer($wallet_filename, $txinfo)
    {
        try {
            $this->wallet->open_wallet($wallet_filename, config('monero.wallet_pass'));
            $this->wallet->refresh();

            $data = $this->wallet->describe_transfer($txinfo);

            return $data;
        } catch (Exception $e) {
            Log::error('[MoneroRPC] finalize_multisig exception: '.$e->getMessage());

            session()->flash('error', $e->getMessage());

            throw new WalletError($e->getMessage());
        }
    }
}
