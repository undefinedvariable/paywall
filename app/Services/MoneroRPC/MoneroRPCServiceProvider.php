<?php

namespace App\Services\MoneroRPC;

use Illuminate\Support\ServiceProvider;
use MoneroIntegrations\MoneroPhp\walletRPC;

class MoneroRPCServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $config = config('monero');
        $walletRPC = new walletRPC($config['rpc_host'], $config['rpc_port']);

        $this->app->singleton('Monero', function () use ($walletRPC) {
            return new MoneroRPC($walletRPC);
        });
    }
}
