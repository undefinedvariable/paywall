<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class APIError extends Exception
{
    public function render($request, Exception $exception)
    {
        Log::error($exception->getMessage());

        $message = Str::before(Str::after($exception->getMessage(), '"description": "'), '",');

        return back()->with('error', $message);
    }
}
