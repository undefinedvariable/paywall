<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class WalletError extends Exception
{
    public function render($request, Exception $exception)
    {
        Log::error($exception->getMessage());

        /*
        if (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_WRONG_ADDRESS')) {
            return back()->with('error', 'Wrong Monero address');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_ZERO_DESTINATION')) {
            return back()->with('error', 'No destinations for this transfer');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_TX_NOT_POSSIBLE')) {
            return back()->with('error', 'No transaction created');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_BAD_HEX')) {
            return back()->with('error', 'Failed to parse hex');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_BAD_UNSIGNED_TX_DATA')) {
            return back()->with('error', 'Failed to parse unsigned transfers');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_BAD_MULTISIG_TX_DATA')) {
            return back()->with('error', 'Cannot load multisig_txset');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_BAD_TX_METADATA')) {
            return back()->with('error', 'Failed to parse transaction metadata');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_GENERIC_TRANSFER_ERROR')) {
            return back()->with('error', 'Failed to commit transaction');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_WRONG_PAYMENT_ID')) {
            return back()->with('error', 'Payment ID shouldn\'t be left unspecified');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_NOT_MULTISIG')) {
            return back()->with('error', 'This wallet is multisig, but not yet finalized');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_WATCH_ONLY')) {
            return back()->with('error', 'The wallet is watch-only. Cannot retrieve seed');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_NON_DETERMINISTIC')) {
            return back()->with('error', 'The wallet is non-deterministic. Cannot display seed');
        } elseif (Str::contains($exception->getMessage(), 'WALLET_RPC_ERROR_CODE_WRONG_KEY')) {
            return back()->with('error', 'Tx key has invalid format');
        } elseif (Str::contains($exception->getMessage(), 'not enough money')) {
            return back()->with('error', 'Not enough funds');
        } elseif (Str::contains($exception->getMessage(), 'not enough unlocked money')) {
            return back()->with('error', 'Wait for funds to settle');
        } else {
            return back()->with('error', 'General wallet API error, retry later');
        }
        */
    }
}
