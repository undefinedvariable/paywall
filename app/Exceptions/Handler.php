<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Routing\Exceptions\InvalidSignatureException;
use Laravel\Passport\Exceptions\MissingScopeException;
use Spatie\ModelStates\Exceptions\TransitionNotFound;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        JobRetryException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
        'passphrase',
        'code',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        //parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof WalletError ||
            $exception instanceof APIError) {
            return $exception->render($request, $exception);
        }

        if ($exception instanceof InvalidSignatureException) {
            return redirect()->route('home')->withError($exception->getMessage());
        }

        if ($exception instanceof TransitionNotFound) {
            if ($request->expectsJson()) {
                return response()->json(['success' => false, 'error' => 'Status transition not permitted.']);
            } else {
                return back()->withError('Status transition not permitted.');
            }
        }

        // Exceptions to JSON

        if ($request->expectsJson()) {
            if ($exception instanceof NotFoundHttpException) {
                return response()->json(['success' => false, 'error' => 'Not found.'], 404);
            }

            if ($exception instanceof MissingScopeException) {
                return response()->json(['success' => false, 'error' => $exception->getMessage()], 403);
            }

            if ($exception instanceof HttpException) {
                return response()->json(['success' => false, 'error' => $exception->getMessage()], $exception->getStatusCode());
            }
        }

        return parent::render($request, $exception);
    }
}
