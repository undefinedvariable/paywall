<?php

namespace App\Models\States\User;

class Active extends UserState
{
    public static $name = 'active';

    public function text(): string
    {
        return 'active';
    }
}
