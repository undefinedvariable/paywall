<?php

namespace App\Models\States\User;

class Inactive extends UserState
{
    public static $name = 'inactive';

    public function text(): string
    {
        return 'inactive';
    }
}
