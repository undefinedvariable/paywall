<?php

namespace App\Models\States\User;

use Spatie\ModelStates\State;

abstract class UserState extends State
{
    abstract public function text(): string;
}
