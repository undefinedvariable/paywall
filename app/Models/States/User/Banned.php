<?php

namespace App\Models\States\User;

class Banned extends UserState
{
    public static $name = 'banned';

    public function text(): string
    {
        return 'banned';
    }
}
