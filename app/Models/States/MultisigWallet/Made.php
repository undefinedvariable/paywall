<?php

namespace App\Models\States\MultisigWallet;

class Made extends MultisigWalletState
{
    public static $name = 'made';

    public function text(): string
    {
        return 'made';
    }
}
