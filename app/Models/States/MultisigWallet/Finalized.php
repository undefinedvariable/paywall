<?php

namespace App\Models\States\MultisigWallet;

class Finalized extends MultisigWalletState
{
    public static $name = 'finalized';

    public function text(): string
    {
        return 'finalized';
    }
}
