<?php

namespace App\Models\States\MultisigWallet;

class Prepared extends MultisigWalletState
{
    public static $name = 'prepared';

    public function text(): string
    {
        return 'prepared';
    }
}
