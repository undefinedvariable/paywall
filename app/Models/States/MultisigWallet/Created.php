<?php

namespace App\Models\States\MultisigWallet;

class Created extends MultisigWalletState
{
    public static $name = 'created';

    public function text(): string
    {
        return 'created';
    }
}
