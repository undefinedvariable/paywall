<?php

namespace App\Models\States\MultisigWallet;

use Spatie\ModelStates\State;

abstract class MultisigWalletState extends State
{
    abstract public function text(): string;
}
