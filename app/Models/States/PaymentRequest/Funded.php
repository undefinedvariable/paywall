<?php

namespace App\Models\States\PaymentRequest;

class Funded extends PaymentRequestState
{
    public static $name = 'funded';

    public function text(): string
    {
        return 'funded';
    }
}
