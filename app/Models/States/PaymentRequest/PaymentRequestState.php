<?php

namespace App\Models\States\PaymentRequest;

use Spatie\ModelStates\State;

abstract class PaymentRequestState extends State
{
    abstract public function text(): string;
}
