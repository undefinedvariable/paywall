<?php

namespace App\Models\States\PaymentRequest;

class Refunded extends PaymentRequestState
{
    public static $name = 'refunded';

    public function text(): string
    {
        return 'refunded';
    }
}
