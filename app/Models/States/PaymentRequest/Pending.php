<?php

namespace App\Models\States\PaymentRequest;

class Pending extends PaymentRequestState
{
    public static $name = 'pending';

    public function text(): string
    {
        return 'pending';
    }
}
