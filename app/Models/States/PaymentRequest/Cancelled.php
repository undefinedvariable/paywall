<?php

namespace App\Models\States\PaymentRequest;

class Cancelled extends PaymentRequestState
{
    public static $name = 'cancelled';

    public function text(): string
    {
        return 'cancelled';
    }
}
