<?php

namespace App\Models\States\PaymentRequest;

class Reported extends PaymentRequestState
{
    public static $name = 'reported';

    public function text(): string
    {
        return 'reported';
    }
}
