<?php

namespace App\Models\States\PaymentRequest;

class CustomerPayout extends PaymentRequestState
{
    public static $name = 'customer_payout';

    public function text(): string
    {
        return 'customer_payout';
    }
}
