<?php

namespace App\Models\States\PaymentRequest;

class Expired extends PaymentRequestState
{
    public static $name = 'expired';

    public function text(): string
    {
        return 'expired';
    }
}
