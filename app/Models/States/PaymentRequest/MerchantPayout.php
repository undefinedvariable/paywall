<?php

namespace App\Models\States\PaymentRequest;

class MerchantPayout extends PaymentRequestState
{
    public static $name = 'merchant_payout';

    public function text(): string
    {
        return 'merchant_payout';
    }
}
