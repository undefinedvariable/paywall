<?php

namespace App\Models\States\PaymentRequest;

class Closed extends PaymentRequestState
{
    public static $name = 'closed';

    public function text(): string
    {
        return 'closed';
    }
}
