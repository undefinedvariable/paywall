<?php

namespace App\Models\States\PaymentRequest;

class Confirming extends PaymentRequestState
{
    public static $name = 'confirming';

    public function text(): string
    {
        return 'confirming';
    }
}
