<?php

namespace App\Models\States\Dispute;

class Accepted extends DisputeState
{
    public static $name = 'accepted';

    public function text(): string
    {
        return 'accepted';
    }
}
