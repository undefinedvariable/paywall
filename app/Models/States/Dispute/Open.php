<?php

namespace App\Models\States\Dispute;

class Open extends DisputeState
{
    public static $name = 'open';

    public function text(): string
    {
        return 'open';
    }
}
