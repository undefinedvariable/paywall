<?php

namespace App\Models\States\Dispute;

class Rejected extends DisputeState
{
    public static $name = 'rejected';

    public function text(): string
    {
        return 'rejected';
    }
}
