<?php

namespace App\Models\States\Dispute;

use Spatie\ModelStates\State;

abstract class DisputeState extends State
{
    abstract public function text(): string;
}
