<?php

namespace App\Models\States\Dispute;

class InReview extends DisputeState
{
    public static $name = 'in_review';

    public function text(): string
    {
        return 'in_review';
    }
}
