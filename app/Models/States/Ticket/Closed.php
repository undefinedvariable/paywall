<?php

namespace App\Models\States\Ticket;

class Closed extends TicketState
{
    public static $name = 'closed';

    public function text(): string
    {
        return 'closed';
    }
}
