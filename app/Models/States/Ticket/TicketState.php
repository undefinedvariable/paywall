<?php

namespace App\Models\States\Ticket;

use Spatie\ModelStates\State;

abstract class TicketState extends State
{
    abstract public function text(): string;
}
