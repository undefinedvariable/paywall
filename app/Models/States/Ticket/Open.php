<?php

namespace App\Models\States\Ticket;

class Open extends TicketState
{
    public static $name = 'open';

    public function text(): string
    {
        return 'open';
    }
}
