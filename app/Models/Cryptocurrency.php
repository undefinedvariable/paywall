<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Cryptocurrency extends Model
{
    use Cachable, HasFactory;

    protected $fillable = [
        'name',
        'abbreviation',
        'fee',
    ];

    protected $casts = [
        'fee'         => 'decimal:8',
        'created_at'  => 'datetime',
        'updated_at'  => 'datetime',
    ];

    /**
     * Eloquent Relationships.
     */
    public function paymentRequests(): HasMany
    {
        return $this->hasMany(PaymentRequest::class);
    }

    public function isBitcoin(): bool
    {
        return $this->name == 'Bitcoin';
    }

    public function isBitcoinCash(): bool
    {
        return $this->name == 'Bitcoin Cash';
    }

    public function isLitecoin(): bool
    {
        return $this->name == 'Litecoin';
    }

    public function isMonero(): bool
    {
        return $this->name == 'Monero';
    }

    public function isDash(): bool
    {
        return $this->name == 'Dash';
    }

    public function isEthereum(): bool
    {
        return $this->name == 'Ethereum';
    }
}
