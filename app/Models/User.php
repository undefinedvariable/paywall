<?php

namespace App\Models;

use App\Enums\AccountType;
use App\Enums\Flower;
use App\Enums\Language;
use App\Models\States\User\Active;
use App\Models\States\User\Banned;
use App\Models\States\User\Inactive;
use App\Models\States\User\UserState;
use App\Traits\RedirectsUsers;
use App\Traits\RetrievesApiTokens;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\ModelStates\HasStates;

class User extends Authenticatable implements HasLocalePreference
{
    use HasApiTokens, CastsEnums, Notifiable, SoftDeletes, HasStates, RetrievesApiTokens, RedirectsUsers, HasFactory;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'pgp',
        'password',
        'passphrase',
        'refund_address',
        'language',
        'flower',
        'account',
        'status',
        'reputation',
        'pgp_auth',
        'code',
        'banned_until',
        'last_login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'passphrase',
        'code',
        'banned_until',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'language'      => Language::class,
        'flower'        => Flower::class,
        'account'       => AccountType::class,
        'reputation'    => 'integer',
        'last_login'    => 'datetime',
        'banned_until'  => 'datetime',
        'created_at'    => 'datetime',
        'updated_at'    => 'datetime',
    ];

    /**
     * Scopes.
     */
    public function scopeActive($query)
    {
        return $query->whereState('status', Active::class);
    }

    public function scopeInactive($query)
    {
        return $query->whereState('status', Inactive::class);
    }

    public function scopeBanned($query)
    {
        return $query->whereState('status', Banned::class);
    }

    public function scopeUser($query)
    {
        return $query->where('account', AccountType::USER);
    }

    public function scopeMerchant($query)
    {
        return $query->where('account', AccountType::MERCHANT);
    }

    public function scopeStaff($query)
    {
        return $query->where('account', AccountType::STAFF);
    }

    public function scopeAdmin($query)
    {
        return $query->where('account', AccountType::ADMIN);
    }

    /**
     * Identities.
     */
    public function isAdmin()
    {
        $account = AccountType::getInstance($this->account);

        return $account->is(AccountType::ADMIN);
    }

    public function isStaff()
    {
        $account = AccountType::getInstance($this->account);

        return $account->is(AccountType::STAFF);
    }

    public function isMerchant()
    {
        if (session()->has('acting_as_user')) {
            return false;
        }

        $account = AccountType::getInstance($this->account);

        return $account->is(AccountType::MERCHANT);
    }

    public function isMarketplace()
    {
        if (session()->has('acting_as_user')) {
            return false;
        }

        $account = AccountType::getInstance($this->account);

        return $account->is(AccountType::MARKETPLACE);
    }

    public function isActive()
    {
        return $this->status == 'active';
    }

    public function isInactive()
    {
        return $this->status == 'inactive';
    }

    public function isBanned()
    {
        return $this->status == 'banned';
    }

    /**
     * States.
     */
    protected function registerStates(): void
    {
        $this
            ->addState('status', UserState::class)
            ->default(Active::class)
            ->allowTransition(Active::class, Banned::class)
            ->allowTransition(Active::class, Inactive::class)
            ->allowTransition(Inactive::class, Active::class)
            ->allowTransition(Banned::class, Active::class);
    }

    /**
     * Eloquent relationships.
     */
    public function merchant(): HasOne
    {
        return $this->hasOne(Merchant::class);
    }

    public function user_tier(): HasOne
    {
        return $this->hasOne(UserTier::class);
    }

    public function marketplace(): HasOne
    {
        return $this->hasOne(Marketplace::class);
    }

    public function paymentRequests(): HasMany
    {
        return $this->hasMany(PaymentRequest::class);
    }

    public function user_notification_channels(): HasMany
    {
        return $this->hasMany(UserNotificationChannel::class);
    }

    public function multisig_wallets(): HasManyThrough
    {
        return $this->hasManyThrough(MultisigWallet::class, PaymentRequest::class);
    }

    public function disputes(): HasManyThrough
    {
        return $this->hasManyThrough(Dispute::class, PaymentRequest::class);
    }

    public function coupons(): BelongsToMany
    {
        return $this->belongsToMany(Coupon::class);
    }

    public function ticket_comments(): HasMany
    {
        return $this->hasMany(TicketComment::class);
    }

    public function tickets(): HasMany
    {
        return $this->hasMany(Ticket::class);
    }

    /**
     * Custom primary key.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = hexdec(uniqid());
        });
    }

    /**
     * Find the user instance for the given username.
     */
    public function findForPassport($username): User
    {
        return $this->where('username', $username)->first();
    }

    /**
     * Get the user's preferred locale.
     *
     * @return string
     */
    public function preferredLocale()
    {
        return $this->language->key;
    }

    /**
     * Get the user's registered notification channels.
     */
    public function notifyViaTelegram(): bool
    {
        return $this->user_notification_channels()->telegram()->count() > 0;
    }

    public function notifyViaJabber(): bool
    {
        return $this->user_notification_channels()->jabber()->count() > 0;
    }

    public function isClientInPayment(PaymentRequest $paymentRequest): bool
    {
        if ($paymentRequest->user == null) {
            return false;
        }

        return $this->id === $paymentRequest->user->id;
    }

    public function isMerchantInPayment(PaymentRequest $paymentRequest): bool
    {
        if(!$this->isMerchant()) return false;

        return $this->merchant->id === $paymentRequest->merchant->id;
    }

    public static function findByUsername(string $username): self
    {
        return static::where('username', $username)->firstOrFail();
    }

    public function deleteClients()
    {
        foreach ($this->clients as $client) {
            $client->delete();
        }
    }

    public function deleteTokens()
    {
        foreach ($this->tokens as $token) {
            $token->delete();
        }
    }

    public function deleteTickets()
    {
        foreach ($this->tickets as $ticket) {
            $ticket->delete();
        }
    }

    public function delete()
    {
        $this->deleteClients();
        $this->deleteTokens();
        $this->deleteTickets();

        parent::delete();
    }
}
