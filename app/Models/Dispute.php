<?php

namespace App\Models;

use App\Models\States\Dispute\Accepted;
use App\Models\States\Dispute\DisputeState;
use App\Models\States\Dispute\InReview;
use App\Models\States\Dispute\Open;
use App\Models\States\Dispute\Rejected;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Spatie\ModelStates\HasStates;

class Dispute extends Model
{
    use HasStates, HasFactory, Cachable;

    public $incrementing = false;

    protected $fillable = [
        'payment_request_id',
        'user_notes',
        'merchant_notes',
        'status',
    ];

    protected $casts = [
        'user_notes'            => Encrypted::class,
        'merchant_notes'        => Encrypted::class,
        'created_at'            => 'datetime',
        'updated_at'            => 'datetime',
    ];

    /**
     * Scopes.
     */
    public function scopeOpen($query)
    {
        return $query->whereState('status', Open::class);
    }

    public function scopeAccepted($query)
    {
        return $query->whereState('status', Accepted::class);
    }

    public function scopeRejected($query)
    {
        return $query->whereState('status', Rejected::class);
    }

    public function scopeInReview($query)
    {
        return $query->whereState('status', InReview::class);
    }

    /**
     * Identities.
     */
    public function isOpen(): bool
    {
        return $this->status == 'open';
    }

    public function isAccepted(): bool
    {
        return $this->status == 'accepted';
    }

    public function isRejected(): bool
    {
        return $this->status == 'rejected';
    }

    public function isInReview(): bool
    {
        return $this->status == 'in_review';
    }

    /**
     * States.
     */
    protected function registerStates(): void
    {
        $this
            ->addState('status', DisputeState::class)
            ->default(Open::class)
            ->allowTransition(Open::class, InReview::class)
            ->allowTransition(InReview::class, Accepted::class)
            ->allowTransition(InReview::class, Rejected::class);
    }

    /**
     * Eloquent Relationships.
     */
    public function paymentRequest(): BelongsTo
    {
        return $this->belongsTo(PaymentRequest::class);
    }

    /**
     * Custom primary key.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = hexdec(uniqid());
        });
    }
}
