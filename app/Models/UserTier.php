<?php

namespace App\Models;

use App\Enums\Tier;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserTier extends Model
{
    use CastsEnums, HasFactory;

    protected $fillable = [
        'user_id',
        'tier',
        'tier_expiration',
        'credits',
    ];

    protected $casts = [
        'user_id'           => 'integer',
        'credits'           => 'integer',
        'tier_expiration'   => 'datetime',
        'created_at'        => 'datetime',
        'updated_at'        => 'datetime',
        'tier'              => Tier::class,
    ];

    public function scopeFreeTier($query)
    {
        return $query->where('tier', Tier::FREE);
    }

    public function scopeBronzeTier($query)
    {
        return $query->where('tier', Tier::BRONZE);
    }

    public function scopeSilverTier($query)
    {
        return $query->where('tier', Tier::SILVER);
    }

    public function scopeGoldTier($query)
    {
        return $query->where('tier', Tier::GOLD);
    }

    /**
     * Identities.
     */
    public function isFreeTier()
    {
        $tier = Tier::getInstance($this->tier);

        return $tier->is(Tier::FREE);
    }

    public function isBronzeTier()
    {
        $tier = Tier::getInstance($this->tier);

        return $tier->is(Tier::BRONZE);
    }

    public function isSilverTier()
    {
        $tier = Tier::getInstance($this->tier);

        return $tier->is(Tier::SILVER);
    }

    public function isGoldTier()
    {
        $tier = Tier::getInstance($this->tier);

        return $tier->is(Tier::GOLD);
    }

    /**
     * Eloquent.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
