<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use RichardStyles\EloquentEncryption\Casts\Encrypted;

class Merchant extends Model
{
    use Cachable, HasFactory;

    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'personal_wallet_address',
        'website',
        'ipn_enable',
        'ipn_url',
    ];

    protected $casts = [
        'user_id'                   => 'integer',
        'ipn_enable'                => 'boolean',
        'created_at'                => 'datetime',
        'updated_at'                => 'datetime',
        'personal_wallet_address'   => Encrypted::class,
    ];

    /**
     * Eloquent Relationships.
     */
    public function coupons(): HasMany
    {
        return $this->hasMany(Coupon::class);
    }

    public function paymentRequests(): HasMany
    {
        return $this->hasMany(PaymentRequest::class);
    }

    public function multisigWallets(): HasManyThrough
    {
        return $this->hasManyThrough(MultisigWallet::class, PaymentRequest::class);
    }

    public function disputes(): HasManyThrough
    {
        return $this->hasManyThrough(Dispute::class, PaymentRequest::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function marketplaces(): BelongsToMany
    {
        return $this->belongsToMany(Marketplace::class);
    }

    /**
     * Custom primary key.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = hexdec(uniqid());
        });
    }

    public function deleteCoupons()
    {
        foreach ($this->coupons as $coupon) {
            $coupon->delete();
        }
    }

    public function deletePaymentRequests()
    {
        foreach ($this->paymentRequests as $paymentRequest) {
            $paymentRequest->delete();
        }
    }

    public function delete()
    {
        $this->deleteCoupons();
        $this->deletePaymentRequests();

        parent::delete();
    }
}
