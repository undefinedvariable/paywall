<?php

namespace App\Models;

use App\Models\States\MultisigWallet\Created;
use App\Models\States\MultisigWallet\Finalized;
use App\Models\States\MultisigWallet\Made;
use App\Models\States\MultisigWallet\MultisigWalletState;
use App\Models\States\MultisigWallet\Prepared;
use App\Services\MoneroRPC\Monero;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\ModelCleanup\CleanupConfig;
use Spatie\ModelCleanup\GetsCleanedUp;
use Spatie\ModelStates\HasStates;

class MultisigWallet extends Model implements GetsCleanedUp
{
    use HasStates, Cachable, HasFactory;

    protected $fillable = [
        'filename',
        'address',
        'payment_request_id',
        'prepare_multisig_data_customer',
        'prepare_multisig_data_vendor',
        'prepare_multisig_data_system',
        'make_multisig_data_customer',
        'make_multisig_data_vendor',
        'make_multisig_data_system',
        'finalize_multisig_data_customer',
        'finalize_multisig_data_vendor',
        'status',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Scopes.
     */
    public function scopeCreated($query)
    {
        return $query->whereState('status', Created::class);
    }

    public function scopePrepared($query)
    {
        return $query->whereState('status', Prepared::class);
    }

    public function scopeMade($query)
    {
        return $query->whereState('status', Made::class);
    }

    public function scopeFinalized($query)
    {
        return $query->whereState('status', Finalized::class);
    }

    /**
     * Identities.
     */
    public function isCreated(): bool
    {
        return $this->status == 'created';
    }

    public function isPrepared(): bool
    {
        return $this->status == 'prepared';
    }

    public function isMade(): bool
    {
        return $this->status == 'made';
    }

    public function isFinalized(): bool
    {
        return $this->status == 'finalized';
    }

    /**
     * States.
     */
    protected function registerStates(): void
    {
        $this
            ->addState('status', MultisigWalletState::class)
            ->default(Created::class)
            ->allowTransition(Created::class, Prepared::class)
            ->allowTransition(Prepared::class, Made::class)
            ->allowTransition(Made::class, Finalized::class);
    }

    /**
     * Clean-up.
     */
    public function cleanUp(CleanupConfig $config): void
    {
        $config->useDateAttribute('updated_at')->olderThanDays(30);
    }

    /**
     * Identities.
     */
    public function paymentRequest(): BelongsTo
    {
        return $this->belongsTo(PaymentRequest::class, 'payment_request_id');
    }

    public function hasBeenPrepared(): bool
    {
        return $this->isCreated() &&
                $this->prepare_multisig_data_customer != null &&
                $this->prepare_multisig_data_vendor != null &&
                $this->prepare_multisig_data_system != null;
    }

    public function hasBeenMade(): bool
    {
        return $this->isPrepared() &&
                $this->make_multisig_data_customer != null &&
                $this->make_multisig_data_vendor != null &&
                $this->make_multisig_data_system != null;
    }

    public function hasBeenFinalized(): bool
    {
        return $this->isMade() &&
                $this->finalize_multisig_data_customer != null &&
                $this->finalize_multisig_data_vendor != null &&
                $this->address != null;
    }

    public function getStatusString()
    {
        switch ($this->status) {
            case 'created':
                return 'created';

            case 'prepared':
                return 'prepared';

            case 'made':
                return 'made';

            case 'finalized':
                return 'finalized';
        }
    }

    protected static function boot()
    {
        parent::boot();
        MultisigWallet::deleting(function ($model) {
            Monero::delete_wallet($model->filename);
        });
    }
}
