<?php

namespace App\Models;

use App\Enums\NotificationType;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use RichardStyles\EloquentEncryption\Casts\Encrypted;

class UserNotificationChannel extends Model
{
    use CastsEnums, HasFactory;

    protected $fillable = [
        'user_id',
        'type',
        'account',
    ];

    protected $casts = [
        'user_id'               => 'integer',
        'type'                  => NotificationType::class,
        'account'               => Encrypted::class,
        'created_at'            => 'datetime',
        'updated_at'            => 'datetime',
    ];

    /**
     * Scopes.
     */
    public function scopeTelegram($query)
    {
        return $query->where('type', NotificationType::TELEGRAM);
    }

    public function scopeJabber($query)
    {
        return $query->where('type', NotificationType::JABBER);
    }

    /**
     * Eloquent relationships.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
