<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TicketCategory extends Model
{
    use Cachable, HasFactory;

    const UPDATED_AT = null;

    protected $fillable = [
        'name',
    ];

    protected $casts = [
        'created_at'  => 'datetime',
    ];

    /**
     * Eloquent Relationships.
     */
    public function tickets(): HasMany
    {
        return $this->hasMany(Ticket::class);
    }
}
