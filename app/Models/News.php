<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\ModelCleanup\CleanupConfig;
use Spatie\ModelCleanup\GetsCleanedUp;

class News extends Model implements GetsCleanedUp
{
    use Cachable, HasFactory;

    const UPDATED_AT = null;

    protected $fillable = [
        'body',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    /**
     * Clean-up.
     */
    public function cleanUp(CleanupConfig $config): void
    {
        $config->olderThanDays(30);
    }
}
