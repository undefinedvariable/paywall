<?php

namespace App\Models;

use App\Enums\Priority;
use App\Models\States\Ticket\Closed;
use App\Models\States\Ticket\Open;
use App\Models\States\Ticket\TicketState;
use BenSampo\Enum\Traits\CastsEnums;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Spatie\ModelCleanup\CleanupConfig;
use Spatie\ModelCleanup\GetsCleanedUp;
use Spatie\ModelStates\HasStates;

class Ticket extends Model implements GetsCleanedUp
{
    use HasStates, CastsEnums, Cachable, HasFactory;

    protected $fillable = [
        'user_id',
        'ticket_category_id',
        'title',
        'priority',
        'message',
        'status',
    ];

    protected $casts = [
        'user_id'               => 'integer',
        'ticket_category_id'    => 'integer',
        'created_at'            => 'datetime',
        'updated_at'            => 'datetime',
        'priority'              => Priority::class,
        'message'               => Encrypted::class,
    ];

    /**
     * Scopes.
     */
    public function scopeOpen($query)
    {
        return $query->whereState('status', Open::class);
    }

    public function scopeClosed($query)
    {
        return $query->whereState('status', Closed::class);
    }

    public function scopeLowPriority($query)
    {
        return $query->where('priority', Priority::LOW);
    }

    public function scopeMediumPriority($query)
    {
        return $query->where('priority', Priority::MEDIUM);
    }

    public function scopeHighPriority($query)
    {
        return $query->where('priority', Priority::HIGH);
    }

    /**
     * Identities.
     */
    public function isOpen(): bool
    {
        return $this->status == 'open';
    }

    public function isClosed(): bool
    {
        return $this->status == 'closed';
    }

    public function isLowPriority()
    {
        $priority = Priority::getInstance($this->priority);

        return $priority->is(Priority::LOW);
    }

    public function isMediumPriority()
    {
        $priority = Priority::getInstance($this->priority);

        return $priority->is(Priority::MEDIUM);
    }

    public function isHighPriority()
    {
        $priority = Priority::getInstance($this->priority);

        return $priority->is(Priority::HIGH);
    }

    /**
     * States.
     */
    protected function registerStates(): void
    {
        $this
            ->addState('status', TicketState::class)
            ->default(Open::class)
            ->allowTransition(Open::class, Closed::class);
    }

    /**
     * Clean-up.
     */
    public function cleanUp(CleanupConfig $config): void
    {
        $config->useDateAttribute('updated_at')->olderThanDays(30);
    }

    /**
     * Eloquent Relationships.
     */
    public function ticketComments(): HasMany
    {
        return $this->hasMany(TicketComment::class);
    }

    public function ticketCategory(): BelongsTo
    {
        return $this->belongsTo(TicketCategory::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
