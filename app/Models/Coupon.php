<?php

namespace App\Models;

use Carbon\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\ModelCleanup\CleanupConfig;
use Spatie\ModelCleanup\GetsCleanedUp;

class Coupon extends Model implements GetsCleanedUp
{
    use Cachable;
    use HasFactory;

    protected $fillable = [
        'merchant_id',
        'code',
        'discount',
        'usage_limit',
        'expires_at',
    ];

    protected $casts = [
        'discount'      => 'integer',
        'usage_limit'   => 'integer',
        'expires_at'    => 'datetime',
        'created_at'    => 'datetime',
        'updated_at'    => 'datetime',
    ];

    /**
     * Scopes.
     */
    public function scopeExpired($query)
    {
        return $query->where('expires_at', '<', Carbon::now());
    }

    /**
     * Eloquent Relationships.
     */
    public function paymentRequests(): HasMany
    {
        return $this->hasMany(PaymentRequest::class);
    }

    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function was_used($id): bool
    {
        if ($this->belongsToMany(User::class)->where('id', $id)->count() == 0) {
            return false;
        }

        return true;
    }

    /**
     * Clean-up.
     */
    public function cleanUp(CleanupConfig $config): void
    {
        $config
           ->olderThanDays(30)
           ->useDateAttribute('expires_at');
    }
}
