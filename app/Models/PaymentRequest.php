<?php

namespace App\Models;

use App\Enums\GoodsType;
use App\Enums\PaymentType;
use App\Models\States\PaymentRequest\Cancelled;
use App\Models\States\PaymentRequest\CustomerPayout;
use App\Models\States\PaymentRequest\Closed;
use App\Models\States\PaymentRequest\Confirming;
use App\Models\States\PaymentRequest\Expired;
use App\Models\States\PaymentRequest\Funded;
use App\Models\States\PaymentRequest\MerchantPayout;
use App\Models\States\PaymentRequest\PaymentRequestState;
use App\Models\States\PaymentRequest\Pending;
use App\Models\States\PaymentRequest\Refunded;
use App\Models\States\PaymentRequest\Reported;
use BenSampo\Enum\Traits\CastsEnums;
use Carbon\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Spatie\ModelStates\HasStates;

class PaymentRequest extends Model
{
    use HasStates, CastsEnums, Cachable, HasFactory;

    public $incrementing = false;

    protected $fillable = [
        'merchant_id',
        'marketplace_id',
        'user_id',
        'orderid',
        'amount_usd',
        'shipping_usd',
        'amount_xmr',
        'cryptocurrency_id',
        'goods_type',
        'payment_type',
        'coupon_id',
        'payment_id',
        'description',
        'item_list',
        'notes',
        'form_step',
        'url',
        'success_url',
        'cancel_url',
        'cryptoexchange_id',
        'status',
    ];

    protected $casts = [
        'merchant_id'           => 'integer',
        'user_id'               => 'integer',
        'cryptocurrency_id'     => 'integer',
        'coupon_id'             => 'integer',
        'notes'                 => Encrypted::class,
        'amount_xmr'            => 'decimal:18',
        'created_at'            => 'datetime',
        'updated_at'            => 'datetime',
        'goods_type'            => GoodsType::class,
        'payment_type'          => PaymentType::class,
    ];

    /**
     * Scopes.
     */

    // Status scopes

    public function scopePending($query)
    {
        return $query->whereState('status', Pending::class);
    }

    public function scopeConfirming($query)
    {
        return $query->whereState('status', Confirming::class);
    }

    public function scopeExpired($query)
    {
        return $query->whereState('status', Expired::class);
    }

    public function scopeCancelled($query)
    {
        return $query->whereState('status', Cancelled::class);
    }

    public function scopeFunded($query)
    {
        return $query->whereState('status', Funded::class);
    }

    public function scopeRefunded($query)
    {
        return $query->whereState('status', Refunded::class);
    }

    public function scopeReported($query)
    {
        return $query->whereState('status', Reported::class);
    }

    public function scopeClosed($query)
    {
        return $query->whereState('status', Closed::class);
    }

    public function scopeCustomerPayout($query)
    {
        return $query->whereState('status', CustomerPayout::class);
    }

    public function scopeMerchantPayout($query)
    {
        return $query->whereState('status', MerchantPayout::class);
    }

    // Type scopes

    public function scopeDigital($query)
    {
        return $query->where('goods_type', GoodsType::DIGITAL);
    }

    public function scopePhysical($query)
    {
        return $query->where('goods_type', GoodsType::PHYSICAL);
    }

    public function scopeServices($query)
    {
        return $query->where('goods_type', GoodsType::SERVICES);
    }

    public function scopeFE($query)
    {
        return $query->where('payment_type', PaymentType::FE);
    }

    public function scopeEscrow($query)
    {
        return $query->where('payment_type', PaymentType::ESCROW);
    }

    // Other scopes

    public function scopeBroadcastable($query)
    {
        return $query->where('payment_type', PaymentType::ESCROW)
        ->whereNotState('status', [Expired::class, Cancelled::class, Closed::class])
        ->whereNotNull('user_id')->whereHas('multisigWallet');
    }

    /**
     * Identities.
     */
    public function isPending(): bool
    {
        return $this->status == 'pending';
    }

    public function isConfirming(): bool
    {
        return $this->status == 'confirming';
    }

    public function isExpired(): bool
    {
        return $this->status == 'expired';
    }

    public function isCancelled(): bool
    {
        return $this->status == 'cancelled';
    }

    public function isFunded(): bool
    {
        return $this->status == 'funded';
    }

    public function isRefunded(): bool
    {
        return $this->status == 'refunded';
    }

    public function isReported(): bool
    {
        return $this->status == 'reported';
    }

    public function isClosed(): bool
    {
        return $this->status == 'closed';
    }

    public function isCustomerPayout(): bool
    {
        return $this->status == 'customer_payout';
    }

    public function isMerchantPayout(): bool
    {
        return $this->status == 'merchant_payout';
    }

    // Type scopes

    public function isFE()
    {
        $payment_type = PaymentType::getInstance($this->payment_type);

        return $payment_type->is(PaymentType::FE);
    }

    public function isEscrow()
    {
        $payment_type = PaymentType::getInstance($this->payment_type);

        return $payment_type->is(PaymentType::ESCROW);
    }

    /**
     * States.
     */
    protected function registerStates(): void
    {
        $this
            ->addState('status', PaymentRequestState::class)
            ->default(Pending::class)
            // ESCROW
            ->allowTransition(Pending::class, Confirming::class)
            ->allowTransition(Pending::class, Cancelled::class)
            ->allowTransition(Pending::class, Expired::class)
            ->allowTransition(Confirming::class, Funded::class)
            ->allowTransition(Confirming::class, Expired::class)
            ->allowTransition(Funded::class, Refunded::class)
            ->allowTransition(Funded::class, Closed::class)
            ->allowTransition(Funded::class, Reported::class)
            ->allowTransition(Reported::class, Funded::class)
            ->allowTransition(Reported::class, CustomerPayout::class)
            ->allowTransition(Reported::class, MerchantPayout::class)
            ->allowTransition(CustomerPayout::class, Refunded::class)
            ->allowTransition(MerchantPayout::class, Closed::class)
            // FE
            ->allowTransition(Confirming::class, Closed::class);
    }

    /**
     * Eloquent Relationships.
     */
    public function dispute(): HasOne
    {
        return $this->hasOne(Dispute::class);
    }

    public function multisigWallet(): HasOne
    {
        return $this->hasOne(MultisigWallet::class);
    }

    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class);
    }

    public function marketplace(): BelongsTo
    {
        return $this->belongsTo(Marketplace::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function coupon(): BelongsTo
    {
        return $this->belongsTo(Coupon::class);
    }

    public function cryptocurrency(): BelongsTo
    {
        return $this->belongsTo(Cryptocurrency::class);
    }

    /**
     * Custom primary key.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Str::orderedUuid();
        });
    }

    /**
     * Helper functions.
     */
    public function isMarketplaceInvolved(): bool
    {
        return $this->marketplace != null;
    }

    public function isValid(): bool
    {
        if ($this->isPending()) {
            return $this->created_at->addDays(config('expiration.payment.pending')) >= Carbon::now();
        } elseif ($this->isConfirming()) {
            return $this->created_at->addDays(config('expiration.payment.confirming')) >= Carbon::now();
        } else {
            return true;
        }
    }

    public function getFinalPrice(): float
    {
        if ($this->coupon != null) {
            return $this->amount_usd + $this->shipping_usd - ($this->amount_usd * $this->coupon->discount * 0.01);
        } else {
            return $this->amount_usd + $this->shipping_usd;
        }
    }

    public function canBeCancelled(): bool
    {
        return $this->isPending();
    }

    public function canBeDeleted(): bool
    {
        return ($this->updated_at->addDays(5) > Carbon::now()) && ($this->isExpired() || $this->isCancelled() || $this->isClosed());
    }

    public function getMerchantPayoutInXMR(): float
    {
        return $this->amount_xmr - $this->getCommissionInXMR();
    }

    public function getCommissionInXMR(): float
    {
        if ($this->marketplace == null || $this->marketplace->hasNoCommission()) {
            return 0;
        }

        if ($this->marketplace->hasFixedCommission()) {
            if ($this->marketplace->commission >= $this->amount_xmr) {
                return 0;
            }

            return $this->marketplace->commission;
        }

        if ($this->marketplace->hasPercentageCommission()) {
            return $this->marketplace->commission * 0.01 * $this->amount_xmr;
        }

        return 0;
    }

    public function getCommissionAddress()
    {
        if ($this->marketplace == null) {
            throw new \Exception('no marketplace');
        }

        return $this->marketplace->wallet_address;
    }
}
