<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use RichardStyles\EloquentEncryption\Casts\Encrypted;
use Spatie\ModelCleanup\CleanupConfig;
use Spatie\ModelCleanup\GetsCleanedUp;

class Message extends Model implements GetsCleanedUp
{
    use Cachable, HasFactory;

    const UPDATED_AT = null;

    protected $fillable = [
        'contact',
        'contact_type',
        'body',
    ];

    protected $casts = [
        'body'          => Encrypted::class,
        'created_at'    => 'datetime',
    ];

    /**
     * Clean-up.
     */
    public function cleanUp(CleanupConfig $config): void
    {
        $config->olderThanDays(30);
    }
}
