<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use RichardStyles\EloquentEncryption\Casts\Encrypted;

class TicketComment extends Model
{
    use Cachable, HasFactory;

    const UPDATED_AT = null;

    protected $fillable = [
        'user_id',
        'ticket_id',
        'comment',
    ];

    protected $casts = [
        'user_id'       => 'integer',
        'ticket_id'     => 'integer',
        'comment'       => Encrypted::class,
        'created_at'    => 'datetime',
    ];

    /**
     * Eloquent Relationships.
     */
    public function ticket(): BelongsTo
    {
        return $this->belongsTo(Ticket::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
