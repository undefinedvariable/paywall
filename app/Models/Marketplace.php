<?php

namespace App\Models;

use App\Enums\CommissionType;
use BenSampo\Enum\Traits\CastsEnums;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use RichardStyles\EloquentEncryption\Casts\Encrypted;

class Marketplace extends Model
{
    use CastsEnums, Cachable, HasFactory;

    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'wallet_address',
        'commission',
        'commission_type',
        'ipn_enable',
        'ipn_url',
    ];

    protected $casts = [
        'user_id'           => 'integer',
        'ipn_enable'        => 'boolean',
        'tier_expiration'   => 'datetime',
        'created_at'        => 'datetime',
        'updated_at'        => 'datetime',
        'commission_type'   => CommissionType::class,
        'wallet_address'    => Encrypted::class,
    ];

    /**
     * Scopes.
     */

    // Type scopes

    public function scopeFixed($query)
    {
        return $query->where('commission_type', CommissionType::FIXED);
    }

    public function scopePercentage($query)
    {
        return $query->where('commission_type', CommissionType::PERCENTAGE);
    }

    // Type scopes

    public function hasFixedCommission()
    {
        $commission_type = CommissionType::getInstance($this->commission_type);

        return $commission_type->is(CommissionType::FIXED);
    }

    public function hasPercentageCommission()
    {
        $commission_type = CommissionType::getInstance($this->commission_type);

        return $commission_type->is(CommissionType::PERCENTAGE);
    }

    public function hasNoCommission()
    {
        $commission_type = CommissionType::getInstance($this->commission_type);

        return $commission_type->is(CommissionType::NO_COMMISSION);
    }

    /**
     * Eloquent Relationships.
     */
    public function paymentRequests(): HasMany
    {
        return $this->hasMany(PaymentRequest::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function merchants(): BelongsToMany
    {
        return $this->belongsToMany(Merchant::class);
    }

    /**
     * Custom primary key.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = hexdec(uniqid());
        });
    }
}
