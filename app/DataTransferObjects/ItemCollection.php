<?php

namespace App\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObjectCollection;

class ItemCollection extends DataTransferObjectCollection
{
    public function current(): Item
    {
        return parent::current();
    }

    public static function create(array $data): ItemCollection
    {
        $collection = [];

        foreach ($data as $item) {
            $collection[] = new Item($item);
        }

        return new self($collection);
    }
}
