<?php

namespace App\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class Estimate extends DataTransferObject
{
    /** @var int */
    public $model_id;

    /** @var string */
    public $name;

    /** @var string */
    public $abbreviation;

    /** @var int|float */
    public $price;

    /** @var int|float */
    public $fee;

    /** @var int|float */
    public $min;
}
