<?php

namespace App\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObjectCollection;

class EstimateCollection extends DataTransferObjectCollection
{
    public function current(): Estimate
    {
        return parent::current();
    }

    public static function create(array $data): EstimateCollection
    {
        $collection = [];

        foreach ($data as $estimate) {
            $collection[] = new Estimate($estimate);
        }

        return new self($collection);
    }
}
