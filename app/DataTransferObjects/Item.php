<?php

namespace App\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class Item extends DataTransferObject
{
    /** @var string */
    public $name;

    /** @var string|null */
    public $description;

    /** @var int|float */
    public $price;

    /** @var int|float|null */
    public $quantity;
}
