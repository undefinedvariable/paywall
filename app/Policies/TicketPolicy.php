<?php

namespace App\Policies;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TicketPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * @brief Only staff can see all tickets
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * @brief The staff can see any specific ticket while only the
     *        user who opened the ticket can see it
     */
    public function view(User $user, Ticket $ticket)
    {
        return $user->isStaff() || $user->id == $ticket->user_id;
    }

    /**
     * @brief Anyone can open a ticket
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * @brief Only the staff can update (reply to) a ticket
     */
    public function update(User $user, Ticket $ticket)
    {
        return $user->isStaff() || $user->id == $ticket->user_id;
    }

    /**
     * @brief The owner or the staff can delete a ticket
     */
    public function delete(User $user, Ticket $ticket)
    {
        return $user->isStaff() || $user->id == $ticket->user_id;
    }
}
