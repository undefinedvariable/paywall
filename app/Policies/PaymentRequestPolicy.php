<?php

namespace App\Policies;

use App\Models\PaymentRequest;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PaymentRequestPolicy
{
    use HandlesAuthorization;

    /**
     * @todo does not work
     */
    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * @brief Only staff can see all tickets
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * @brief Anyone can see a payment request in 'pending' status (via signed url for instance)
     *        Only owners can see their payment requests
     */
    public function view(User $user, PaymentRequest $paymentRequest)
    {
        if ($paymentRequest->isPending()) {
            return true;
        }

        return $user->isMerchantInPayment($paymentRequest) || $user->isClientInPayment($paymentRequest);
    }

    /**
     * @brief Only merchants can create payment requests
     */
    public function create(User $user)
    {
        return $user->isMerchant();
    }

    /**
     * @brief Merchants can update payment requests in status 'pending'
     */
    public function update(User $user, PaymentRequest $paymentRequest)
    {
        if ($paymentRequest->isPending()) {
            if ($user->isMerchantInPayment($paymentRequest)) {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * @brief Merchants can delete payment requests
     */
    public function delete(User $user, PaymentRequest $paymentRequest)
    {
        if (! $user->isMerchantInPayment($paymentRequest)) {
            return false;
        }

        if ($paymentRequest->canBeDeleted()) {
            return true;
        } else {
            Response::deny('Payment request cannot be deleted. Wait a few more days.');
        }
    }

    /**
     * @brief Merchants can cancel a payment request in status pending, expired, cancelled, closed or refunded after a day
     */
    public function cancel(User $user, PaymentRequest $paymentRequest)
    {
        if (! $user->isMerchantInPayment($paymentRequest)) {
            return false;
        }

        if ($paymentRequest->canBeCancelled()) {
            return true;
        } else {
            Response::deny('Payment request cannot be cancelled. If the user paid already, issue a refund instead.');
        }
    }
}
