<?php

namespace App\Policies;

use App\Models\Marketplace;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MarketplacePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function viewAny(User $user)
    {
        return $user->isStaff();
    }

    public function view(User $user, Marketplace $model)
    {
        return $user->isMarketplace() && $user->marketplace->id == $model->id;
    }

    public function update(User $user, Marketplace $model)
    {
        return $user->isMarketplace() && $user->marketplace->id == $model->id;
    }

    public function delete(User $user, Marketplace $model)
    {
        return $user->isMarketplace() && $user->marketplace->id == $model->id;
    }
}
