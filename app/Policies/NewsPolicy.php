<?php

namespace App\Policies;

use App\Models\News;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * @brief All users can see the news
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * @brief All users can see a specific news
     */
    public function view(User $user, News $news)
    {
        return true;
    }

    /**
     * @brief Only the staff can create a news
     */
    public function create(User $user)
    {
        return $user->isStaff();
    }

    /**
     * @brief Only the staff can delete a news
     */
    public function delete(User $user, News $news)
    {
        return $user->isStaff();
    }
}
