<?php

namespace App\Policies;

use App\Models\Merchant;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MerchantPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function viewAny(User $user)
    {
        return $user->isStaff();
    }

    public function view(User $user, Merchant $model)
    {
        return $user->isMerchant() && $user->merchant->id == $model->id;
    }

    public function update(User $user, Merchant $model)
    {
        return $user->isMerchant() && $user->merchant->id == $model->id;
    }

    public function delete(User $user, Merchant $model)
    {
        return $user->isMerchant() && $user->merchant->id == $model->id;
    }
}
