<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * @brief Only the staff can see all users
     */
    public function viewAny(User $user)
    {
        return $user->isStaff();
    }

    /**
     * @brief The staff or the owner can see a user account
     */
    public function view(User $user, User $model)
    {
        return $user->isStaff() || $user->id == $model->id;
    }

    /**
     * @brief Only the staff can directly create a user
     */
    public function create(User $user)
    {
        return $user->isStaff();
    }

    /**
     * @brief Only the owner can update their account
     */
    public function update(User $user, User $model)
    {
        return $user->id == $model->id;
    }

    /**
     * @brief Only the owner can delete their account
     */
    public function delete(User $user, User $model)
    {
        if ($user->isAdmin()) {
            return false;
        }

        return $user->id == $model->id;
    }

    /**
     * @brief The staff only can restore an account
     */
    public function restore(User $user, User $model)
    {
        return false;
    }

    /**
     * @brief The staff only can permanently delete an account
     */
    public function forceDelete(User $user, User $model)
    {
        if ($user->isAdmin()) {
            return false;
        }

        return $user->id == $model->id && $user->disputes->count() == 0;
    }
}
