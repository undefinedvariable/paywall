<?php

namespace App\Policies;

use App\Models\Cryptocurrency;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CryptocurrencyPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isStaff();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cryptocurrency  $cryptocurrency
     * @return mixed
     */
    public function view(User $user, Cryptocurrency $cryptocurrency)
    {
        return $user->isStaff();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cryptocurrency  $cryptocurrency
     * @return mixed
     */
    public function update(User $user, Cryptocurrency $cryptocurrency)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cryptocurrency  $cryptocurrency
     * @return mixed
     */
    public function delete(User $user, Cryptocurrency $cryptocurrency)
    {
        if (! $user->isAdmin()) {
            return false;
        }
        if ($cryptocurrency->paymentRequests->count() === 0) {
            return true;
        }

        return (
            $cryptocurrency->paymentRequests()->pending()->count() +
            $cryptocurrency->paymentRequests()->confirming()->count() +
            $cryptocurrency->paymentRequests()->funded()->count() +
            $cryptocurrency->paymentRequests()->reported()->count()
            ) === 0;
    }
}
