<?php

namespace App\Policies;

use App\Models\Dispute;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DisputePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * @brief Users can see all disputes
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * @brief User/Merchant can see only their own disputes
     */
    public function view(User $user, Dispute $dispute)
    {
        if ($user->isMerchantInPayment($dispute->paymentRequest)) {
            return $dispute->paymentRequest->merchant->id == $user->merchant->id;
        }

        return $dispute->paymentRequest->user->id == $user->id;
    }

    /**
     * @brief Any customer can create a dispute
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * @brief Only merchants can update (reply to) disputes
     */
    public function update(User $user, Dispute $dispute)
    {
        return $user->isMerchantInPayment($dispute->paymentRequest);
    }

    /**
     * @brief Only the user who opened a dispute can delete it
     */
    public function delete(User $user, Dispute $dispute)
    {
        return $dispute->paymentRequest->user->id == $user->id;
    }
}
