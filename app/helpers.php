<?php

if (! function_exists('array_remove_null')) {
    function array_remove_null($arr)
    {
        if (! is_array($arr)) {
            return $arr;
        }

        return collect($arr)
            ->reject(function ($arr) {
                return is_null($arr);
            })
            ->flatMap(function ($arr, $key) {
                return is_numeric($key)
                    ? [array_remove_null($arr)]
                    : [$key => array_remove_null($arr)];
            })
            ->toArray();
    }
}

if (! function_exists('make_price')) {
    function make_price($price_usd, $usd_xmr, $xmr_other): float
    {
        return $price_usd * $usd_xmr * (float) $xmr_other;
    }
}

if (! function_exists('monero_balance_formatter')) {
    function monero_balance_formatter($val): float
    {
        return $val / 1000000000000;
    }
}

if (! function_exists('escrow_wallet_name')) {
    function escrow_wallet_name($payment_id)
    {
        return $payment_id.'_escrow_wallet';
    }
}

if (! function_exists('merchant_wallet_name')) {
    function merchant_wallet_name($merchant_id)
    {
        return $merchant_id.'_personal_wallet';
    }
}
