<?php

namespace App\Notifications;

use App\Models\PaymentRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class EscrowWalletCreated extends Notification
{
    use Queueable;

    private $payment;

    public function __construct(PaymentRequest $payment)
    {
        $this->payment = $payment;
    }

    public function via($notifiable)
    {
        $channels = ['database'];

        if ($notifiable->notifyViaTelegram()) {
            array_push($channels, TelegramChannel::class);
        }

        return $channels;
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
        ->to($notifiable->telegram_chat_id)
        ->content('The escrow wallet for payment #'.$this->payment->id.' was created, now run the script and complete the wallet validation process');
    }

    public function toArray($notifiable)
    {
        return [
            'message' => 'The escrow wallet for payment #'.$this->payment->id.' was created, now run the script and complete the wallet validation process',
            'when' => $this->payment->updated_at,
        ];
    }
}
