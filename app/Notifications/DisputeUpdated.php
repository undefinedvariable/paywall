<?php

namespace App\Notifications;

use App\Models\Dispute;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class DisputeUpdated extends Notification
{
    use Queueable;

    private $dispute;

    public function __construct(Dispute $dispute)
    {
        $this->dispute = $dispute;
    }

    public function via($notifiable)
    {
        $channels = ['database'];

        if ($notifiable->notifyViaTelegram()) {
            array_push($channels, TelegramChannel::class);
        }

        return $channels;
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
        ->to($notifiable->telegram_chat_id)
        ->content('Merchant '.$this->dispute->paymentRequest->merchant->user->username.' added a reply to the dispute #'.$this->dispute->id);
    }

    public function toArray($notifiable)
    {
        return [
            'message' => 'Merchant '.$this->dispute->paymentRequest->merchant->user->username.' added a reply to the dispute #'.$this->dispute->id,
            'when' => $this->dispute->updated_at,
        ];
    }
}
