<?php

namespace App\Notifications;

use App\Models\Dispute;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class DisputeOpened extends Notification
{
    use Queueable;

    private $dispute;

    public function __construct(Dispute $dispute)
    {
        $this->dispute = $dispute;
    }

    public function via($notifiable)
    {
        $channels = ['database'];

        if ($notifiable->notifyViaTelegram()) {
            array_push($channels, TelegramChannel::class);
        }

        return $channels;
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
        ->to($notifiable->telegram_chat_id)
        ->content('User '.$this->dispute->paymentRequest->user->username.' opened a dispute for payment #'.$this->dispute->paymentRequest->id);
    }

    public function toArray($notifiable)
    {
        return [
            'message' => 'User '.$this->dispute->paymentRequest->user->username.' opened a dispute for payment #'.$this->dispute->paymentRequest->id,
            'when' => $this->dispute->updated_at,
        ];
    }
}
