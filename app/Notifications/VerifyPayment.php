<?php

namespace App\Notifications;

use App\Models\PaymentRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class VerifyPayment extends Notification
{
    use Queueable;

    private $paymentRequest;

    public function __construct(PaymentRequest $paymentRequest)
    {
        $this->paymentRequest = $paymentRequest;
    }

    public function via($notifiable)
    {
        $channels = ['database'];

        if ($notifiable->notifyViaTelegram()) {
            array_push($channels, TelegramChannel::class);
        }

        return $channels;
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
        ->to($notifiable->telegram_chat_id)
        ->content('Payment #'.$this->paymentRequest->id.' needs verification, use the script or verify it yourself, monero_tx_info = '.$this->paymentRequest->multisigWallet->txid);
    }

    public function toArray($notifiable)
    {
        return [
            'message' => 'Payment #'.$this->paymentRequest->id.' needs verification, accept the transfer in the desktop client',
            'when' => $this->paymentRequest->updated_at,
        ];
    }
}
