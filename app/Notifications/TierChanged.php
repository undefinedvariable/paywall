<?php

namespace App\Notifications;

use App\Enums\Tier;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class TierChanged extends Notification
{
    use Queueable;

    private $user;
    private $tier;

    public function __construct(User $user, Tier $tier)
    {
        $this->user = $user;
        $this->tier = $tier;
    }

    public function via($notifiable)
    {
        $channels = ['database'];

        if ($notifiable->notifyViaTelegram()) {
            array_push($channels, TelegramChannel::class);
        }

        return $channels;
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
        ->to($notifiable->telegram_chat_id)
        ->content('Tier upgraded to '.$this->tier->key);
    }

    public function toArray($notifiable)
    {
        return [
            'message' => 'Tier upgraded to '.$this->tier->key,
            'when' => $this->user->updated_at,
        ];
    }
}
