<?php

namespace Database\Seeders;

use App\Models\Cryptocurrency;
use Illuminate\Database\Seeder;

class CryptocurrencyTableSeeder extends Seeder
{
    public function run()
    {
        $items = [
            [
                'name' => 'Bitcoin',
                'abbreviation' => 'BTC',
                'fee' => 0,
            ],
            [
                'name' => 'Litecoin',
                'abbreviation' => 'LTC',
                'fee' => 0,
            ],
            [
                'name' => 'Ethereum',
                'abbreviation' => 'ETH',
                'fee' => 0,
            ],
            [
                'name' => 'Dash',
                'abbreviation' => 'DASH',
                'fee' => 0,
            ],
            [
                'name' => 'Monero',
                'abbreviation' => 'XMR',
                'fee' => 0,
            ],
            [
                'name' => 'Bitcoin Cash',
                'abbreviation' => 'BCH',
                'fee' => 0,
            ],
        ];

        foreach ($items as $item) {
            Cryptocurrency::updateOrCreate(['name' => $item['name']], $item);
        }
    }
}
