<?php

namespace Database\Seeders;

use App\Models\TicketCategory;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TicketCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Payments',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Accounts',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Disputes',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'API',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'General',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Bugs',
                'created_at' => Carbon::now(),
            ],
        ];

        foreach ($items as $item) {
            TicketCategory::updateOrCreate(['name' => $item['name']], $item);
        }
    }
}
