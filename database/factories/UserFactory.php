<?php

namespace Database\Factories;

use App\Enums\AccountType;
use App\Enums\Flower;
use App\Enums\Language;
use App\Models\States\User\Active;
use App\Models\States\User\Banned;
use App\Models\States\User\Inactive;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'username' => $this->faker->unique()->word,
            'password' => bcrypt($this->faker->password),
            'passphrase' => bcrypt($this->faker->word),
            'refund_address' => $this->faker->unique()->sha256,
            'language' => Language::EN,
            'flower' => $this->faker->randomElement([Flower::PINK, Flower::YELLOW, Flower::BLACK, Flower::RED, Flower::WHITE, Flower::GREEN, Flower::BLUE, Flower::GREY]),
            'account' => AccountType::USER,
            'status' => Active::class,
            'reputation' => $this->faker->numberBetween(-10, 100),
            'pgp_auth' => false,
            'last_login' => $this->faker->dateTime(),
            'remember_token' => Str::random(10),
        ];
    }
}
