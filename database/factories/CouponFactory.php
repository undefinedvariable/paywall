<?php

namespace Database\Factories;

use App\Models\Coupon;
use App\Models\Merchant;
use Illuminate\Database\Eloquent\Factories\Factory;

class CouponFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = Coupon::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'merchant_id' => Merchant::factory()->create()->id,
            'code' => $this->faker->unique()->word,
            'discount' => $this->faker->numberBetween(1, 99),
            'usage_limit' => $this->faker->numberBetween(1, 1000),
            'expires_at' => $this->faker->dateTime(),
        ];
    }
}
