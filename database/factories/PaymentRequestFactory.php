<?php

namespace Database\Factories;

use App\Enums\GoodsType;
use App\Enums\PaymentType;
use App\Models\Coupon;
use App\Models\Cryptocurrency;
use App\Models\Marketplace;
use App\Models\Merchant;
use App\Models\PaymentRequest;
use App\Models\States\PaymentRequest\Cancelled;
use App\Models\States\PaymentRequest\CustomerPayout;
use App\Models\States\PaymentRequest\Closed;
use App\Models\States\PaymentRequest\Confirming;
use App\Models\States\PaymentRequest\Expired;
use App\Models\States\PaymentRequest\Funded;
use App\Models\States\PaymentRequest\MerchantPayout;
use App\Models\States\PaymentRequest\Pending;
use App\Models\States\PaymentRequest\Refunded;
use App\Models\States\PaymentRequest\Reported;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\URL;

class PaymentRequestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = PaymentRequest::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'merchant_id' => Merchant::factory()->create()->id,
            'marketplace_id' => Marketplace::factory()->create()->id,
            'user_id' => User::factory()->create()->id,
            'orderid' => $this->faker->word,
            'amount_usd' => $this->faker->randomFloat(),
            'shipping_usd' => $this->faker->randomFloat(),
            'amount_xmr' => $this->faker->randomFloat(),
            'cryptocurrency_id' => Cryptocurrency::factory()->create()->id,
            'goods_type' => $this->faker->randomElement([GoodsType::DIGITAL, GoodsType::PHYSICAL, GoodsType::SERVICES]),
            'payment_type' => $this->faker->randomElement([PaymentType::FE, PaymentType::ESCROW]),
            'coupon_id' => Coupon::factory()->create()->id,
            'payment_id' => $this->faker->word,
            'description' => $this->faker->realText,
            'success_url' => route('checkout.success'),
            'cancel_url' => route('checkout.cancel'),
            'cryptoexchange_id' => $this->faker->word,
            'status' => $this->faker->randomElement([Cancelled::class, CustomerPayout::class, Closed::class, Confirming::class, Expired::class, Funded::class, MerchantPayout::class, Pending::class, Refunded::class, Reported::class]),
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (PaymentRequest $payment) {
            $url = URL::temporarySignedRoute('checkout.review', now()->addDays(config('expiration.payment.pending')), ['payment' => $payment]);
            $payment->update(['url' => $url]);
        });
    }
}
