<?php

namespace Database\Factories;

use App\Models\Merchant;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class MerchantFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = Merchant::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'personal_wallet_address' => $this->faker->unique()->sha256,
            'website' => $this->faker->url,
            'ipn_enable' => $this->faker->boolean,
            'ipn_url' => $this->faker->url,
        ];
    }
}
