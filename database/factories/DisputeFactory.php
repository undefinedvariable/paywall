<?php

namespace Database\Factories;

use App\Models\Dispute;
use App\Models\PaymentRequest;
use App\Models\States\Dispute\Accepted;
use App\Models\States\Dispute\InReview;
use App\Models\States\Dispute\Open;
use App\Models\States\Dispute\Rejected;
use Illuminate\Database\Eloquent\Factories\Factory;

class DisputeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = Dispute::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'payment_request_id' => PaymentRequest::factory()->create()->id,
            'status' => Open::class,
            'user_notes' => $this->faker->realText,
            'merchant_notes' => $this->faker->realText,
        ];
    }
}
