<?php

namespace Database\Factories;

use App\Enums\CommissionType;
use App\Models\Marketplace;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class MarketplaceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = Marketplace::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'wallet_address' => $this->faker->unique()->sha256,
            'commission' => $this->faker->numberBetween(1, 50),
            'commission_type' => $this->faker->randomElement([CommissionType::FIXED, CommissionType::PERCENTAGE]),
            'ipn_enable' => $this->faker->boolean,
            'ipn_url' => $this->faker->url,
        ];
    }
}
