<?php

namespace Database\Factories;

use App\Models\MultisigWallet;
use App\Models\PaymentRequest;
use App\Models\States\MultisigWallet\Created;
use App\Models\States\MultisigWallet\Finalized;
use App\Models\States\MultisigWallet\Made;
use App\Models\States\MultisigWallet\Prepared;
use Illuminate\Database\Eloquent\Factories\Factory;

class MultisigWalletFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = MultisigWallet::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'filename' => $this->faker->unique()->numberBetween(10000, 90000).'_wallet',
            'address' => $this->faker->unique()->word,
            'payment_request_id' => PaymentRequest::factory()->create()->id,
            'prepare_multisig_data_customer' => $this->faker->word,
            'prepare_multisig_data_vendor' => $this->faker->word,
            'prepare_multisig_data_system' => $this->faker->word,
            'make_multisig_data_customer' => $this->faker->word,
            'make_multisig_data_vendor' => $this->faker->word,
            'make_multisig_data_system' => $this->faker->word,
            'finalize_multisig_data_customer' => $this->faker->word,
            'finalize_multisig_data_vendor' => $this->faker->word,
            'status' => Created::class,
        ];
    }
}
