<?php

namespace Database\Factories;

use App\Enums\Tier;
use App\Models\User;
use App\Models\UserTier;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserTierFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = UserTier::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'tier' => $this->faker->randomElement([Tier::FREE, Tier::BRONZE, Tier::SILVER, Tier::GOLD]),
            'tier_expiration' => $this->faker->dateTime(),
            'credits' => $this->faker->randomNumber(),
        ];
    }
}
