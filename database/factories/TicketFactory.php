<?php

namespace Database\Factories;

use App\Enums\Priority;
use App\Models\States\Ticket\Closed;
use App\Models\States\Ticket\Open;
use App\Models\Ticket;
use App\Models\TicketCategory;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = Ticket::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'ticket_category_id' => TicketCategory::factory()->create()->id,
            'title' => $this->faker->unique()->sentence,
            'priority' => $this->faker->randomElement([Priority::LOW, Priority::MEDIUM, Priority::HIGH]),
            'message' => $this->faker->realText,
            'status' => Open::class,
        ];
    }
}
