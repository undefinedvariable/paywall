<?php

namespace Database\Factories;

use App\Enums\NotificationType;
use App\Models\User;
use App\Models\UserNotificationChannel;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserNotificationChannelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var  string
     */
    protected $model = UserNotificationChannel::class;

    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'type' => $this->faker->randomElement(NotificationType::TELEGRAM, NotificationType::JABBER),
            'account' => $this->faker->unique()->word,
        ];
    }
}
