<?php

use App\Enums\Tier;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateUserTiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tiers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->unique();
            $table->unsignedTinyInteger('tier')->default(Tier::FREE);
            $table->dateTime('tier_expiration')->default(Carbon::now()->addMonths(12));
            $table->integer('credits');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') !== 'testing') {
            Schema::table('user_tiers', function (Blueprint $table) {
                $table->dropForeign('user_tiers_user_id_foreign');
            });
        }
        Schema::dropIfExists('user_tiers');
    }
}
