<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketplaceMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_merchant', function (Blueprint $table) {
            $table->unsignedBigInteger('merchant_id')->index();
            $table->unsignedBigInteger('marketplace_id')->index();
            $table->timestamp('created_at')->useCurrent();

            $table->primary(['merchant_id', 'marketplace_id']);
            $table->foreign('merchant_id')->references('id')->on('merchants')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('marketplace_id')->references('id')->on('marketplaces')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') !== 'testing') {
            Schema::table('marketplace_merchant', function (Blueprint $table) {
                $table->dropForeign('marketplace_merchant_merchant_id_foreign');
                $table->dropForeign('marketplace_merchant_marketplace_id_foreign');
            });
        }
        Schema::dropIfExists('marketplace_merchant');
    }
}
