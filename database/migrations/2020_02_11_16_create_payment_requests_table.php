<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_requests', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->unsignedBigInteger('merchant_id')->index();
            $table->unsignedBigInteger('marketplace_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('orderid')->nullable();
            $table->decimal('amount_usd', 10, 2);
            $table->decimal('shipping_usd', 10, 2);
            $table->decimal('amount_xmr', 18, 12)->nullable();
            $table->unsignedInteger('cryptocurrency_id')->nullable();
            $table->unsignedTinyInteger('goods_type');
            $table->unsignedTinyInteger('payment_type');
            $table->unsignedBigInteger('coupon_id')->nullable();
            $table->string('payment_id')->nullable();
            $table->text('description')->nullable();
            $table->json('item_list')->nullable();
            $table->encrypted('notes')->nullable();
            $table->string('form_step')->nullable();
            $table->text('url')->nullable();
            $table->text('success_url');
            $table->text('cancel_url');
            $table->string('cryptoexchange_id')->nullable();
            $table->string('status');
            $table->timestamps();

            $table->foreign('merchant_id')->references('id')->on('merchants')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('marketplace_id')->references('id')->on('marketplaces')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('coupon_id')->references('id')->on('coupons')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('cryptocurrency_id')->references('id')->on('cryptocurrencies')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') !== 'testing') {
            Schema::table('payment_requests', function (Blueprint $table) {
                $table->dropForeign('payment_requests_merchant_id_foreign');
                $table->dropForeign('payment_requests_marketplace_id_foreign');
                $table->dropForeign('payment_requests_user_id_foreign');
                $table->dropForeign('payment_requests_coupon_id_foreign');
                $table->dropForeign('payment_requests_cryptocurrency_id_foreign');
            });
        }
        Schema::dropIfExists('payment_requests');
    }
}
