<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('merchant_id')->index();
            $table->string('code')->unique();
            $table->integer('discount');
            $table->unsignedInteger('usage_limit');
            $table->dateTime('expires_at');
            $table->timestamps();

            $table->foreign('merchant_id')->references('id')->on('merchants')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') !== 'testing') {
            Schema::table('coupons', function (Blueprint $table) {
                $table->dropForeign('coupons_merchant_id_foreign');
            });
        }
        Schema::dropIfExists('coupons');
    }
}
