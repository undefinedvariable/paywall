<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_user', function (Blueprint $table) {
            $table->unsignedBigInteger('coupon_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->timestamp('created_at')->useCurrent();

            $table->primary(['coupon_id', 'user_id']);
            $table->foreign('coupon_id')->references('id')->on('coupons')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') !== 'testing') {
            Schema::table('coupon_user', function (Blueprint $table) {
                $table->dropForeign('coupon_user_user_id_foreign');
                $table->dropForeign('coupon_user_coupon_id_foreign');
            });
        }
        Schema::dropIfExists('coupon_user');
    }
}
