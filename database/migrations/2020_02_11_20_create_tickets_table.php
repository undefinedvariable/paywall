<?php

use App\Enums\Priority;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedInteger('ticket_category_id')->index();
            $table->string('title');
            $table->unsignedTinyInteger('priority')->default(Priority::LOW);
            $table->encrypted('message');
            $table->string('status');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ticket_category_id')->references('id')->on('ticket_categories')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') !== 'testing') {
            Schema::table('tickets', function (Blueprint $table) {
                $table->dropForeign('tickets_user_id_foreign');
                $table->dropForeign('tickets_ticket_category_id_foreign');
            });
        }
        Schema::dropIfExists('tickets');
    }
}
