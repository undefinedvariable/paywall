<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultisigWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multisig_wallets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('filename')->unique();
            $table->string('address', 250)->nullable();
            $table->uuid('payment_request_id');
            $table->string('prepare_multisig_data_customer', 250)->nullable();
            $table->string('prepare_multisig_data_vendor', 250)->nullable();
            $table->string('prepare_multisig_data_system', 250)->nullable();
            $table->string('make_multisig_data_customer', 250)->nullable();
            $table->string('make_multisig_data_vendor', 250)->nullable();
            $table->string('make_multisig_data_system', 250)->nullable();
            $table->string('finalize_multisig_data_customer', 250)->nullable();
            $table->string('finalize_multisig_data_vendor', 250)->nullable();
            $table->string('status');
            $table->timestamps();

            $table->foreign('payment_request_id')->references('id')->on('payment_requests')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') !== 'testing') {
            Schema::table('multisig_wallets', function (Blueprint $table) {
                $table->dropForeign('multisig_wallets_payment_request_id_foreign');
            });
        }
        Schema::dropIfExists('multisig_wallets');
    }
}
