<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisputesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disputes', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->unique();
            $table->uuid('payment_request_id')->index();
            $table->encrypted('user_notes');
            $table->encrypted('merchant_notes')->nullable();
            $table->string('status');
            $table->timestamps();

            $table->foreign('payment_request_id')->references('id')->on('payment_requests')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') !== 'testing') {
            Schema::table('disputes', function (Blueprint $table) {
                $table->dropForeign('disputes_payment_request_id_foreign');
            });
        }
        Schema::dropIfExists('disputes');
    }
}
