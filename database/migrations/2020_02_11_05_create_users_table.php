<?php

use App\Enums\Flower;
use App\Enums\AccountType;
use App\Enums\Language;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->unique();
            $table->string('username')->unique();
            $table->string('pgp')->nullable();
            $table->string('password');
            $table->string('passphrase');
            $table->string('refund_address')->nullable();
            $table->unsignedTinyInteger('language')->default(Language::EN);
            $table->unsignedTinyInteger('flower')->default(Flower::WHITE);
            $table->unsignedTinyInteger('account')->default(AccountType::USER);
            $table->string('status');
            $table->integer('reputation')->default(0);
            $table->boolean('pgp_auth')->default(false);
            $table->string('code')->nullable();
            $table->dateTime('banned_until')->nullable();
            $table->timestamp('last_login')->default(Carbon::now());
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
