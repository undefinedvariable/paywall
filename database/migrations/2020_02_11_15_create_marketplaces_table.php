<?php

use App\Enums\CommissionType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplaces', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->unique();
            $table->unsignedBigInteger('user_id')->index();
            $table->encrypted('wallet_address')->nullable();
            $table->decimal('commission', 4, 2)->default(0);
            $table->unsignedTinyInteger('commission_type')->default(CommissionType::NO_COMMISSION);
            $table->boolean('ipn_enable')->default(false);
            $table->text('ipn_url')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') !== 'testing') {
            Schema::table('marketplaces', function (Blueprint $table) {
                $table->dropForeign('marketplaces_user_id_foreign');
            });
        }
        Schema::dropIfExists('marketplaces');
    }
}
