@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">

        <!-- col -->
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title text-center">{{ __('Create New Coupon') }}</h2>
                <form method="POST" action="{{ route('coupons.store') }}" class="tm-login-form">
                @csrf

                    <!-- Code -->
                    <div class="form-group">
                        <label for="code">{{ __('Code') }}</label>
                        <input id="code" name="code" type="text" class="form-control validate @error('code') is-invalid @enderror" value="{{ old('code') }}" minlength="3" maxlength="50" required autofocus>
                        @error('code')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <!-- Discount -->
                    <div class="form-group">
                        <label for="discount">{{ __('Discount') }}&nbsp;(%)</label>
                        <input id="discount" name="discount" type="number" class="form-control validate @error('discount') is-invalid @enderror" value="{{ old('discount') }}" min="1" max="100" required>
                        @error('discount')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <!-- Usage Limit -->
                    <div class="form-group">
                        <label for="usage_limit">{{ __('N. of coupons available') }}</label>
                        <input id="usage_limit" name="usage_limit" type="number" class="form-control validate @error('usage_limit') is-invalid @enderror" value="{{ old('usage_limit') }}" required>
                        @error('usage_limit')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <!-- Expires At -->
                    <div class="form-group">
                        <label for="expires_at">{{ __('Expiration Date') }}&nbsp;(mm/dd/yyyy)</label>
                        <input id="expires_at" name="expires_at" type="date" class="form-control validate @error('expires_at') is-invalid @enderror" value="{{ old('expires_at') }}" required>
                        @error('expires_at')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">{{ __('Add') }}</button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
        <!-- /.col -->

    </div>
    <!-- /.row -->
@endsection
