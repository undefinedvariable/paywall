@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">News</h1>
<p class="mb-4">Here are all news on the platform</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Summary - All News</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Text</th>
                        <th>Created At</th>
                        <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>Text</th>
                        <th>Created At</th>
                        <th>&nbsp;</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($news as $n)
                    <tr>
                        <td>{{ $n->body }}</td>
                        <td>{{ $n->created_at }}</td>
                        <td>
                            <form action="{{ route('admin.news.destroy', $n) }}" method="POST" role="form">
                            @csrf
                                <input type="hidden" name="_method" value="DELETE" />
                                <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                            @honeypot
                            </form>
                        <td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
