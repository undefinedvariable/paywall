@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">News</h1>
<p class="mb-4">Send an update to all users</p>

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-4">
            <div class="card-header">
                Create a News
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.news.store') }}" class="tm-login-form">
                @csrf
                    <textarea id="body" name="body" class="form-control validate @error('body') is-invalid @enderror"
                     required>{{ old('body') }}</textarea>
                    @error('body')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">Send</button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
