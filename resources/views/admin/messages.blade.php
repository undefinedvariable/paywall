@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Messages</h1>
<p class="mb-4">Here are all messages sent to the admins</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Received Messages</h6>
    </div>
    <div class="card-body">

        @foreach($messages as $msg)
        <div class="card text-center">
            <div class="card-header">{{ $msg->contact_type }}</div>
            <div class="card-body">
                <h5 class="card-title">{{ $msg->contact }}</h5>
                <p class="card-text">{{ $msg->body }}</p>
            </div>
            <div class="card-footer text-muted">{{ \Carbon\Carbon::parse($msg->created_at)->diffForHumans() }}</div>
        </div>
        <br>
        @endforeach

    </div>
</div>
@endsection
