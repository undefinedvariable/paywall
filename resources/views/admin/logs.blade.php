@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    <a href="{{ route('admin.clearlogs') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-trash fa-sm text-white-50"></i>&nbsp;&nbsp;Clear</a>
</div>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Summary - All Logs</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Message</th>
                        <th>Date</th>
                    </tr>
                  </thead>
                  <tfoot>
                  <tr>
                        <th>Type</th>
                        <th>Message</th>
                        <th>Date</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  @foreach( $logs as $i => $l )
                    <tr>
                        <td>{{ $l['type'] }}</td>
                        <td>{{ $l['msg'] }}</td>
                        <td>{{ $l['date'] }}</td>
                    </tr>
                 @endforeach
                 </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
