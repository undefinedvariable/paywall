@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Interactions</h1>
<p class="mb-4">See users' actions on the platform</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Summary - All Interactions</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Object</th>
                        <th>Action</th>
                        <th>Time</th>
                    </tr>
                  </thead>
                  <tfoot>
                  <tr>
                        <th>User</th>
                        <th>Object</th>
                        <th>Action</th>
                        <th>Time</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($activities as $activity)
                    <tr>
                        <td>
                            @if(empty($activity->causer))
                                Deleted
                            @else
                            <a href="{{ route('admin.users.show', $activity->causer) }}" title="see user profile">{{ $activity->causer->username }}</a>
                            @endif
                        </td>
                        <td>
                            @if(empty($activity->subject))
                                Deleted
                            @else
                                {{ \Str::after(get_class($activity->subject), 'Models\\') }} ID #{{ $activity->subject->id }}
                            @endif
                        </td>
                        <td>{{ $activity->description }}</td>
                        <td>{{ $activity->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
