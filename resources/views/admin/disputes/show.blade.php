@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Disputes</h1>
<p class="mb-4">Detail view of dispute <b>#{{ $dispute->id }}</b></p>
<div class="row">
    <div class="col-lg-6">

        <!-- Default Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">User's Notes - {{ $dispute->paymentRequest->user->username }}</h6>
            </div>
            <div class="card-body">
                {{ $dispute->user_notes }}
            </div>
            <div class="card-body">
            <a href="{{ route('admin.disputes.accept', $dispute) }}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Accept</span>
                  </a>
            </div>
        </div>

    </div>
    <div class="col-lg-6">

        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Merchant Notes - {{ $dispute->paymentRequest->merchant->user->username }}</h6>
            </div>
            <div class="card-body">
                {{ $dispute->merchant_notes }}
            </div>
            <div class="card-body">
                <a href="{{ route('admin.disputes.reject', $dispute) }}" class="btn btn-warning btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-exclamation-triangle"></i>
                    </span>
                    <span class="text">Reject</span>
                  </a>
            </div>
        </div>

    </div>
</div>
@endsection