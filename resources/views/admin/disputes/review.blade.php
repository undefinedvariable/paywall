@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Disputes</h1>
<p class="mb-4">Here are all disputes waiting to be handled on the platform</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Summary - Open Disputes</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Client</th>
                        <th>Merchant</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Client</th>
                        <th>Merchant</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($disputes as $dispute)

                    <tr>
                      <td><a href="{{ route('admin.disputes.show', $dispute) }}">{{ $dispute->id }}</a></td>
                      <td>{{ $dispute->paymentRequest->user->username }}</td>
                      <td>{{ $dispute->paymentRequest->merchant->user->username }}</td>
                      <td>{{ $dispute->created_at }}</td>
                      <td>{{ $dispute->updated_at }}</td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
