@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Payments</h1>
<p class="mb-4">Detail view of payment <b>#{{ $payment->id }}</b></p>
<div class="row">
    <div class="col-lg-12">

        <!-- Default Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Info</h6>
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><b>Merchant:</b>&nbsp;{{ $payment->merchant->user->username }}</li>
                    @if($payment->user != null)
                    <li class="list-group-item"><b>Client:</b>&nbsp;
                    {{ $payment->user->username }}
                    @endif
                    </li>
                    <li class="list-group-item"><b>Type:</b>&nbsp;{{ $payment->payment_type->key }}</li>
                    <li class="list-group-item"><b>Amount:</b>&nbsp;${{ $payment->getFinalPrice() }}</li>
                    @if($balance > 0)
                    <li class="list-group-item"><b>In Escrow:</b>&nbsp;{{ $balance }} XMR</li>
                    @endif
                    <li class="list-group-item"><b>Status:</b>&nbsp;{{ $payment->status }}</li>
                    <li class="list-group-item"><b>Created:</b>&nbsp;{{ $payment->created_at->format('h:i:s m/d/Y') }}</li>
                    <li class="list-group-item"><b>Updated:</b>&nbsp;{{ $payment->updated_at->format('h:i:s m/d/Y') }}</li>
                    @if($payment->coupon != null)
                    <li class="list-group-item"><b>Discount:</b>&nbsp;
                    -{{ $payment->coupon->discount }}%
                    @endif
                    </li>
                    @if($payment->item_list != null)
                        @foreach( json_decode($payment->item_list) as $item)
                            @foreach($item as $key => $value)
                            <li class="list-group-item"><b>Item #{{ $loop->parent->iteration }}:</b>&nbsp;
                            <i>{{ ucfirst($key) }}</i> - {{ $value }}
                            </li>
                            @endforeach
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>

    </div>
</div>

@if($payment->isEscrow())
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Wallet Transactions</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Address</th>
                                <th>Amount</th>
                                <th>Fee</th>
                                <th>Confirmations</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Address</th>
                                <th>Amount</th>
                                <th>Fee</th>
                                <th>Confirmations</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($transactions as $transaction)
                            <tr>
                                <td>{{ $transaction['address'] }}</td>
                                <td>
                                    @if($transaction['in'] == true)
                                    <span class="badge badge-success">
                                    @else
                                    <span class="badge badge-danger">
                                    @endif
                                    {{ monero_balance_formatter($transaction['amount']) }}
                                    </span>
                                </td>
                                <td>{{ monero_balance_formatter($transaction['fee']) }}</td>
                                <td>{{ $transaction['confirmations'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@endsection