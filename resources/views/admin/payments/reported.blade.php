@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Payments</h1>
<p class="mb-4">Here are all payments reported on the platform</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Summary - All Reported Payment Requests</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Client</th>
                        <th>Merchant</th>
                        <th>Amount</th>
                        <th>Created At</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Client</th>
                        <th>Merchant</th>
                        <th>Amount</th>
                        <th>Created At</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($payments as $payment)
                    <tr>
                      <td><a href="{{ route('admin.payments.show', $payment) }}">{{ $payment->id }}</a></td>
                      @if($payment->user)
                      <td><a href="{{ route('admin.users.show', $payment->user) }}">{{ $payment->user->username }}</a></td>
                      @else
                      <td>-</td>
                      @endif
                      <td><a href="{{ route('admin.users.show', $payment->merchant->user) }}">{{ $payment->merchant->user->username }}</a></td>
                      <td>${{ $payment->getFinalPrice() }}</td>
                      <td>{{ $payment->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
