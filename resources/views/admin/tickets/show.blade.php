@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tickets</h1>
<p class="mb-4">Detail view about ticket #{{ $ticket->id }}</p>

<div class="row">

    <div class="col-lg-6">

        <!-- Default Card Example -->
        <div class="card mb-4">
            <div class="card-header">
                {{ $ticket->title }}
            </div>
            <div class="card-body">
                {{ $ticket->message }}
            </div>
        </div>

        @foreach($user_comments as $comment)
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardUser{{ $loop->index }}" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardUser{{ $loop->index }}">
                <h6 class="m-0 font-weight-bold text-primary">User on {{ $comment->created_at->format('h:i:s m/d/Y') }}</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardUser{{ $loop->index }}">
                <div class="card-body">
                    {{ $comment->comment }}
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <div class="col-lg-6">

        <!-- Default Card Example -->
        <div class="card mb-4">
            <div class="card-header">
                Info
            </div>
            <div class="card-body">
                <nav class="navbar navbar-expand navbar-light bg-light mb-3">
                    <a class="navbar-brand" href="#">Priority</a>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="priorityDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $ticket->priority->key }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated--grow-in" aria-labelledby="priorityDropdown">
                                <a class="dropdown-item" href="{{ route('admin.tickets.changepriority', $ticket, 'low') }}">LOW</a>
                                <a class="dropdown-item" href="{{ route('admin.tickets.changepriority', $ticket, 'medium') }}">MEDIUM</a>
                                <a class="dropdown-item" href="{{ route('admin.tickets.changepriority', $ticket, 'high') }}">HIGH</a>
                            </div>
                        </li>
                    </ul>
                </nav>
                <nav class="navbar navbar-expand navbar-light bg-light mb-3">
                    <a class="navbar-brand" href="#">Status</a>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="statusDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ strtoupper($ticket->status) }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated--grow-in" aria-labelledby="statusDropdown">
                                <a class="dropdown-item" href="{{ route('admin.tickets.changestatus', $ticket, 'open') }}">OPEN</a>
                                <a class="dropdown-item" href="{{ route('admin.tickets.changestatus', $ticket, 'closed') }}">CLOSED</a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>

        </div>

        @foreach($staff_comments as $comment)
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardStaff{{ $loop->index }}" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardStaff{{ $loop->index }}">
                <h6 class="m-0 font-weight-bold text-primary">Staff member on {{ $comment->created_at->format('h:i:s m/d/Y') }}</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardStaff{{ $loop->index }}">
                <div class="card-body">
                    {{ $comment->comment }}
                </div>
            </div>
        </div>
        @endforeach

    </div>
</div>
<div class="row">

    <div class="col-lg-12">
        <div class="card mb-4">
            <div class="card-header">
                Send Reply
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('tickets.update', $ticket) }}" class="tm-login-form">
                @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <input id="ticket_id" name="ticket_id" value="{{ $ticket->id }}" type="hidden">
                    <textarea id="comment" name="comment" class="form-control validate @error('comment') is-invalid @enderror"
                     required>{{ old('comment') }}</textarea>
                    @error('comment')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="form-group mt-4">
                    @if($ticket->isClosed())
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">Reopen Ticket</button>
                    @else
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">Reply</button>
                    @endif
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
