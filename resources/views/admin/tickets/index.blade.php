@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tickets</h1>
<p class="mb-4">Here are all tickets on the platform</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Summary - All Tickets</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Created At</th>
                        <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Created At</th>
                        <th>&nbsp;</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($tickets as $ticket)
                    <tr>
                      <td><a href="{{ route('admin.tickets.show', $ticket) }}">{{ $ticket->id }}</a></td>
                      <td><a href="{{ route('admin.users.show', $ticket->user) }}">{{ $ticket->user->username }}</a></td>
                      <td>{{ $ticket->title }}</td>
                      <td>
                        @if($ticket->isOpen())
                        <span class="badge badge-primary">
                        @else
                        <span class="badge badge-secondary">
                        @endif
                        {{ $ticket->status }}</span>
                      </td>
                      <td>
                        @if($ticket->isHighPriority())
                        <span class="badge badge-danger">
                        @elseif($ticket->isMediumPriority())
                        <span class="badge badge-warning">
                        @else
                        <span class="badge badge-info">
                        @endif
                        {{ $ticket->priority->key }}</span>
                      </td>
                      <td>{{ $ticket->created_at }}</td>
                      <td>
                        <form action="{{ route('tickets.destroy', $ticket) }}" method="POST">
                          @csrf
                            <input type="hidden" name="_method" value="DELETE" />
                            <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                          @honeypot
                        </form>
                      <td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
