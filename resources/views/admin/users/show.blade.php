@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Users</h1>
<p class="mb-4">Detail view about user #{{ $user->id }}</p>

<!-- Content Row -->
<div class="row">

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Payments</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">${{ $paid }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Earnings</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">${{ $earned }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Disputes</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $user->disputes->count() }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-gavel fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Tickets</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $user->tickets->count() }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-lg-6">

        <!-- Default Card Example -->
        <div class="card mb-4">
            <div class="card-header">
                Info
            </div>
            <div class="card-body">
                ID: <b>{{ $user->id }}</b><br>
                Username: <b>{{ $user->username }}</b><br>
                Created: <b>{{ $user->created_at }}</b><br>
                Updated: <b>{{ $user->updated_at }}</b><br>
                Last login: <b>{{ $user->last_login }}</b><br>
            </div>
        </div>

    </div>

    <div class="col-lg-6">

        <!-- Default Card Example -->
        <div class="card mb-4">
            <div class="card-header">
                Actions
            </div>
            <div class="card-body">
                <nav class="navbar navbar-expand navbar-light bg-light mb-3">
                    <a class="navbar-brand" href="#">Account</a>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="accountDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $user->account->key }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated--grow-in" aria-labelledby="accountDropdown">
                                <a class="dropdown-item" href="">USER</a>
                                <a class="dropdown-item" href="{{ route('admin.users.makemerchant', $user) }}">MERCHANT</a>
                                <a class="dropdown-item" href="{{ route('admin.users.makemarketplace', $user) }}">MARKETPLACE</a>
                                <!--<a class="dropdown-item" href="">STAFF</a>-->
                            </div>
                        </li>
                    </ul>
                </nav>
                <nav class="navbar navbar-expand navbar-light bg-light mb-3">
                    <a class="navbar-brand" href="#">Status</a>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="statusDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if($user->isBanned() && $user->banned_until > \Carbon\Carbon::now()->addMonths(6))
                                BANNED (long term)
                            @else
                                {{ strtoupper($user->status) }}
                            @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated--grow-in" aria-labelledby="statusDropdown">
                                <a class="dropdown-item" href="{{ route('admin.users.unban', $user) }}">ACTIVE</a>
                                @if($user->isBanned())
                                <a class="dropdown-item" href="{{ route('admin.users.ban', $user) }}">BANNED (long term)</a>
                                @else
                                <a class="dropdown-item" href="{{ route('admin.users.ban', $user) }}">BANNED</a>
                                @endif
                                <div class="dropdown-item">
                                    <form action="{{ route('users.destroy', $user) }}" method="POST">
                                    @csrf
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button  class="dropdown-item" type="submit">DELETE</button>
                                    @honeypot
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

    </div>
</div>

@if($user->isMerchant() || $user->isMarketplace())
<div class="row">

    <div class="col-lg-12">

        <div class="card mb-4">
            <div class="card-header">
                Special User Info
            </div>

            <div class="card-body">

            @if($user->isMerchant())
                Merchant ID: <b>{{ $user->merchant->id }}</b><br>
                Website: <b>{{ $user->merchant->website }}</b><br>
                IPN URL: <b>{{ $user->merchant->ipn_url }}</b><br>
                IPN Enabled: <b>{{ $user->merchant->ipn_enable ? "YES" : "NO"  }}</b><br>
            @else
                Marketplace ID: <b>{{ $user->marketplace->id }}</b><br>
                Redirect Uri: <b>{{ $user->clients->first()->redirect }}</b><br>
                IPN URL: <b>{{ $user->marketplace->ipn_url }}</b><br>
                IPN Enabled: <b>{{ $user->marketplace->ipn_enable ? "YES" : "NO" }}</b><br>
            @endif
                Tier: <b>{{ $user->user_tier->tier->key }}</b><br>
                Tier Expiration: <b>{{ $user->user_tier->tier_expiration->diffForHumans() }}</b><br>
            </div>
        </div>

    </div>
</div>
@endif

@endsection