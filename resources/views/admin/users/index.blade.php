@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Users</h1>
<p class="mb-4">Here are all registered users on the platform</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Summary - All Users</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Account</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Last Login</th>
                        <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Account</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Last Login</th>
                        <th>&nbsp;</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($users as $user)

                        @if(!$currentUser->isAdmin() && $user->isStaff())
                            @continue
                        @endif

                    <tr>
                      <td><a href="{{ route('admin.users.show', $user) }}">{{ $user->id }}</a></td>
                      <td>{{ $user->username }}</td>
                      <td>{{ $user->account->key }}</td>
                      <td>
                        @if(\Cache::has($user->id.'_online'))
                          &#9654;
                        @endif

                        @if($user->isBanned())
                        <span class="badge badge-danger">
                        @else
                        <span class="badge badge-primary">
                        @endif
                        {{ $user->status }}</span>
                      </td>
                      <td>{{ $user->created_at }}</td>
                      <td>
                          @if($user->last_login == null)
                            Never logged in
                          @else
                            {{ $user->last_login->format('d M Y') }}
                          @endif
                       </td>
                       <td>
                        @if($user->isActive())
                        <form action="{{ route('admin.users.ban', $user->id) }}" method="GET" role="form">
                        @csrf
                            <button class="btn btn-danger" type="submit"><i class="fa fa-user-slash"></i></button>
                        @honeypot
                        </form>
                        @else
                        <form action="{{ route('admin.users.unban', $user) }}" method="GET" role="form">
                        @csrf
                            <button class="btn btn-primary" type="submit"><i class="fa fa-user-slash"></i></button>
                        @honeypot
                        </form>
                        @endif
                       </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
