@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Users</h1>
<p class="mb-4">Here are all deleted users on the platform</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Summary - Deleted Users</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Account</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Deleted At</th>
                        <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Account</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Deleted At</th>
                        <th>&nbsp;</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($users as $user)

                        @if(!$currentUser->isAdmin() && $user->isStaff())
                            @continue
                        @endif

                    <tr>
                      <td><a href="{{ route('admin.users.show', $user) }}">{{ $user->id }}</a></td>
                      <td>{{ $user->username }}</td>
                      <td>{{ $user->account->key }}</td>
                      <td>{{ $user->status }}</td>
                      <td>{{ $user->created_at }}</td>
                      <td>{{ $user->deleted_at }}</td>
                      <td>
                        <form action="{{ route('admin.users.restore', $user) }}" method="GET" role="form">
                            @csrf
                            <button class="btn btn-info" type="submit"><i class="fa fa-trash-restore"></i></button>
                            @honeypot
                        </form>
                       </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
