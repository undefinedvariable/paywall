@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Cryptocurrencies</h1>
<p class="mb-4">Add a new supported cryptocurrency</p>

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-4">
            <div class="card-header">
                Add a Cryptocurrency
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.cryptocurrencies.store') }}" class="tm-login-form">
                @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" class="form-control validate @error('name') is-invalid @enderror" value="{{ old('name') }}" maxlength="20" required>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="abbreviation">Abbreviation</label>
                        <input type="text" id="abbreviation" name="abbreviation" class="form-control validate @error('abbreviation') is-invalid @enderror" value="{{ old('abbreviation') }}" maxlength="10" required>
                        @error('abbreviation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="min_val">Minimum Value for Transacting</label>
                        <input type="text" id="min_val" name="min_val" class="form-control validate @error('min_val') is-invalid @enderror" value="{{ old('min_val') }}" required>
                        @error('min_val')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="fee">Conversion Fee</label>
                        <input type="text" id="fee" name="fee" class="form-control validate @error('fee') is-invalid @enderror" value="{{ old('fee') }}" required>
                        @error('fee')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">Send</button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
