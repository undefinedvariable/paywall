@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Cryptocurrencies</h1>
<p class="mb-4">Edit a supported cryptocurrency</p>

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-4">
            <div class="card-header">
                Edit a Cryptocurrency
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.cryptocurrencies.update', $cryptocurrency) }}" class="tm-login-form">
                @csrf
                    <div class="form-group">
                        <label for="fee">Fee</label>
                        <input type="text" id="fee" name="fee" class="form-control validate @error('fee') is-invalid @enderror" value="{{ $cryptocurrency->fee }}" required>
                        @error('fee')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">Send</button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
