@extends('layouts.admin')

@section('css')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Cryptocurrencies</h1>
<p class="mb-4">Here are all the supported cryptocurrencies</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Summary - All Cryptocurrencies</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Min Val</th>
                        <th>Fee</th>
                        <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Abbreviation</th>
                        <th>Fee</th>
                        <th>&nbsp;</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($cryptocurrencies as $crypto)
                    <tr>
                        <td><a href="{{ route('admin.cryptocurrencies.edit', $crypto->id) }}" title="edit">{{ $crypto->name }}</a></td>
                        <td>{{ $crypto->abbreviation }}</td>
                        <td>{{ $crypto->fee }}</td>
                        <td>
                            <form action="{{ route('admin.cryptocurrencies.destroy', $crypto) }}" method="POST" role="form">
                            @csrf
                                <input type="hidden" name="_method" value="DELETE" />
                                <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                            @honeypot
                            </form>
                        <td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
