@extends('layouts.checkout')

@section('content')

        <div class="card-deck mb-3 text-center">

            @foreach($estimates as $estimate)
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">{{ $estimate->name }}</h4>
                </div>
                <div class="card-body">
                    <h4 class="card-title pricing-card-title">{{ number_format($estimate->price, 8, '.', ',') }} <small class="text-muted">{{ $estimate->abbreviation }}</small></h4>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>+{{ $estimate->fee }}&nbsp;{{ __('conversion fee') }}</li>
                    </ul>
                    @if($estimate->price < $estimate->min)
                    <button class="btn btn-lg btn-block btn-primary" disabled>{{ __('Not Available') }}</button>
                    @else
                    <a class="btn btn-lg btn-block 
                    @if($estimate->fee == 0)
                    btn-outline-primary
                    @else
                    btn-primary
                    @endif
                    " href="{{ route('checkout.pay', [$payment, $estimate->model_id]) }}">{{ __('Pay in :abbreviation', ['abbreviation' => $estimate->abbreviation]) }}</a>
                    @endif
                </div>
            </div>

            @if($loop->index+1 % 3 == 0)
            </div><div class="card-deck mb-3 text-center">
            @endif

            @endforeach
        </div>

@endsection