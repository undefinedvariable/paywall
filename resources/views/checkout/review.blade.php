@extends('layouts.checkout')

@section('content')

<div class="row">

    <!-- Cart -->
    <div class="col-md-4 order-md-2 mb-4">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">{{ __('Cart') }}</span>
            @unless(empty($items))
            <span class="badge badge-secondary badge-pill">{{ $items->count() }}</span>
            @endunless
        </h4>
        <ul class="list-group mb-3">
            @foreach($items as $item)
            <!-- Items -->
            <li class="list-group-item d-flex justify-content-between lh-condensed">
                <div>
                    <h6 class="my-0">{{ $item->name }}</h6>
                    <small class="text-muted">{{ $item->description }}</small>
                </div>
                <span class="text-muted">${{ $item->price }}</span>
            </li>
            @endforeach

            @if(count($items) == 0)
            <li class="list-group-item d-flex justify-content-between bg-light">
                <div class="text">
                    <h6 class="my-0">{{ __('Price') }}</h6>
                </div>
                <span class="text-primary">${{ $payment->amount_usd }}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between bg-light">
                <div class="text">
                    <h6 class="my-0">{{ __('Shipping') }}</h6>
                </div>
                <span class="text-primary">${{ $payment->shipping_usd }}</span>
            </li>
            @endif

            @if($payment->coupon_id != null)
            <!-- Coupon -->
            <li class="list-group-item d-flex justify-content-between bg-light">
                <div class="text-success">
                    <h6 class="my-0">{{ $payment->coupon->code }}</h6>
                </div>
                <span class="text-success">-{{ $payment->coupon->discount }}%</span>
            </li>
            @endif

            <li class="list-group-item d-flex justify-content-between">
                <span>{{ __('Total') }}</span>
                <strong>${{ $payment->getFinalPrice() }}</strong>
            </li>
        </ul>

        <form action="{{ route('checkout.coupon') }}" class="card p-2 needs-validation" method="POST">
        @csrf
            <input name="payment_request_id" type="hidden" value="{{ $payment->id }}">
            <div class="input-group">
                <input id="coupon" name="coupon" type="text" class="form-control" placeholder="{{ __('Coupon') }}" maxlength="50" required>
                <div class="input-group-append">
                    <button type="submit" class="btn btn-secondary">{{ __('Redeem') }}</button>
                </div>
            </div>
        @honeypot
        </form>
    </div>

    <div class="col-md-8 order-md-1">
        <h4 class="mb-3">{{ __('Order Info') }}</h4>

        <form action="{{ route('payments.update', $payment) }}" class="needs-validation" method="POST">
        @csrf

            <div class="mb-3">
                <label for="username">{{ __('Merchant Username') }}</label>
                <div class="input-group">
                    <a class="input-group-prepend" href="#">
                        <span class="input-group-text">@</span>
                    </a>
                    <p class="form-control">{{ $payment->merchant->user->username }}</p>
                </div>
            </div>

            <div class="mb-3">
                <label for="pgp">{{ __('Merchant PGP key') }}</label>
                <textarea id="pgp" class="form-control" disabled>{{ $publickey }}</textarea>
            </div>

            <div class="mb-3">
                <label for="pgp">{{ __('Payment Type') }}</label>
                <p><i>{{ $payment->payment_type->key }}</i> (<a href="#" target="_blank" title="{{ __('See the blog') }}">?</a>)</p>
            </div>

            <div class="mb-3">
                <label for="notes">{{ __('Add notes') }}&nbsp;<span class="text-muted">({{ __('Optional') }})</span></label>
                <textarea class="form-control" id="notes" name="notes" placeholder="{{ __('Add your notes here, if possible, encrypted with the merchant’s PGP key') }}">{{ old('notes') }}</textarea>

                @if($errors->has('notes'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('notes') }}</strong>
                </div>
                @endif
            </div>

            <input type="hidden" name="_method" value="PUT" />

            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">{{ __('Go To Checkout') }}</button>
            @honeypot
        </form>

    </div>
</div>
<br> 

@endsection