@extends('layouts.checkout')

@section('content')
<div class='container'>

    <br><br>

    <div class='row mx-2'>
        <div class='col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center'>
            <div class='card box-shadow'>
                <div class='card-header'>
                    <h4 class='font-weight-normal'>{{ __('Create multisig deposit wallet') }}</h4>
                </div>
                <div class='card-body'>
                    <p class="card-text">{{ __('Create multisig deposit wallet instructions') }}</p>
                    <p class="card-text">{{ __('Payment expires on :expiration', ['expiration' => $payment->created_at->addDays(config('expiration.payment.pending'))->format('m/d/Y h:m:s')]) }}</p>
                    <h4 class='card-title'>
                    <a class='acrypto_wallet_address' style='line-height:1.5;' href='https://github.com/Honorarium/client'>{{ __('Download script') }}</a>
                    </h4>
                    <br>

                    <ol class="breadcrumb">{{ __('Payment') }}&nbsp;ID:&nbsp;&nbsp;<b>{{ $payment->id }}</b></ol>

                    <br>
                    <a href="{{ route('checkout.currency', $payment) }}" class='btn btn-lg btn-block btn-outline-primary' style='white-space:normal'>{{ __('Continue') }}</a>
                </div>
            </div>
        </div>
    </div>
    
    <br><br><br>

</div>
@endsection
