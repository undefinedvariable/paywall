@extends('layouts.checkout')

@section('css')
<style>
.loader8{
    position: relative;
    width: 80px;
    height: 80px;

    top: 28%;
    top: -webkit-calc(50% - 43px);
    top: calc(50% - 43px);
    left: 35%;
    left: -webkit-calc(50% - 43px);
    left: calc(50% - 43px);

    border-radius: 50px;
    background-color: rgba(255, 255, 255, .2);
    border-width: 40px;
    border-style: double;
    border-color:transparent  black;

    -webkit-box-sizing:border-box;
    -moz-box-sizing:border-box;
        box-sizing:border-box;

    -webkit-transform-origin:  50% 50%;
        transform-origin:  50% 50% ;
    -webkit-animation: loader8 2s linear infinite;
        animation: loader8 2s linear infinite;
}

@-webkit-keyframes loader8{
    0%{-webkit-transform:rotate(0deg);}
    100%{-webkit-transform:rotate(360deg);}
}

@keyframes loader8{
    0%{transform:rotate(0deg);}
    100%{transform:rotate(360deg);}
}
</style>
@endsection

@section('content')
<div class='container'>

    <br><br>

    <div class='row mx-2'>
        <div class='col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center'>
            @if($payment->isFunded())
            <div class="jumbotron">
                <h1 class="display-4">{{ __('Payment verified') }}!</h1>
                <hr class="my-4">
                <p>{{ __('Your transfer has been received.') }}</p>
                <p class="lead">
                    <a class="btn btn-primary btn-lg" href="{{ $payment->success_url }}" role="button">{{ __('Continue') }}</a>
                </p>
            </div>
            @else
            <div class="box">
                <div class="loader8"></div>
                <h1>{{ __('Waiting for payment to show up') }}...</h1>
            </div>
            @endif
        </div>
    </div>

    <br><br><br>

</div>
@endsection
