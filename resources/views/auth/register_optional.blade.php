@extends('layouts.private')

@section('css')
<style>
/* HIDE RADIO */
[type=radio] { 
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
}

/* IMAGE STYLES */
[type=radio] + img {
  cursor: pointer;
}

/* CHECKED STYLES */
[type=radio]:checked + img {
  outline: 2px solid #f00;
}
</style>
@endsection

@section('content')
<div class="container tm-mt-big tm-mb-big">
    @if(session()->has('passphrase'))
    <br>
    <div class="alert alert-info" role="alert">
        <p>This is your account recovery passphrase, store it safely. It is needed to recover your account if you lose your password.
        <hr>
        <p class="mb-0"><strong>{{ session()->get('passphrase') }}</strong></p>
    </div>
    <br>
    @endif

    <div class="row mt-2">
        <div class="col-12 mx-auto tm-login-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <div class="row">
                    <div class="col-12 text-center">
                    <h2 class="tm-block-title mb-4">Sign up to <a style="color: #f5a623" href="{{ url('/') }}" target="_self" title="home">{{ config('app.name') }}</a> - Part 2</h2>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        <form method="POST" action="{{ route('register.finalize') }}" class="tm-login-form">
                        @csrf

                            <div class="form-group">
                                <label for="refund_address">Monero Refund Address</label>
                                <input type="text" id="refund_address" name="refund_address" class="form-control validate @error('refund_address') is-invalid @enderror" minlength="90" maxlength="110" value="{{ old('refund_address') }}" required>

                                @error('refund_address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="pgp">PGP (optional)</label>
                                <textarea id="pgp" name="pgp" class="form-control validate @error('pgp') is-invalid @enderror" minlength="100" maxlength="210000">{{ old('pgp') }}</textarea>

                                @error('pgp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mt-3">
                                <label for="flower">Pick a flower, it will be your antiphishing canary</label>
                                @foreach($flowers as $key => $value)
                                <label>
                                    <input type="radio" id="flower" name="flower" class="form-control validate @error('flower') is-invalid @enderror" value="{{ $key }}" required>
                                    <img alt="flower" class="icons" src="{{ url('/images/flowers').'/'.strtolower($value).'.png' }}">
                                </label>
                                @endforeach

                                @error('flower')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-block text-uppercase">Complete Registration</button>
                            </div>
                        @honeypot
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
