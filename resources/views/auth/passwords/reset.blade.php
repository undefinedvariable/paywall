@extends('layouts.private')

@section('content')
<div class="container tm-mt-big tm-mb-big">
    <div class="row">
        <div class="col-12 mx-auto tm-login-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="tm-block-title mb-4">Reset Password</h2>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-12">
                        <form method="POST" action="{{ url('password/reset') }}" class="tm-login-form">
                        @csrf

                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group">
                                <label for="password">New Password</label>
                                <input id="password" name="password" type="password" class="form-control validate @error('password') is-invalid @enderror" required autofocus>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mt-3">
                                <label for="password_confirmation">Confirm New Password</label>
                                <input id="password_confirmation" type="password" class="form-control validate @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required>

                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-block text-uppercase">Send</button>
                            </div>
                            <a href="{{ route('login') }}"><button type="button" class="mt-5 btn btn-default btn-block text-uppercase">Back to Login</button></a>
                        @honeypot
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
