@extends('layouts.private')

@section('content')
<div class="container tm-mt-big tm-mb-big">
    <div class="row">
        <div class="col-12 mx-auto tm-login-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="tm-block-title mb-4">Recover Password</h2>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-12">
                        <form method="POST" action="{{ url('password/recover') }}" class="tm-login-form">
                        @csrf

                            <div class="form-group">
                                <label for="username">Username</label>
                                <input id="username" name="username" type="text" class="form-control validate @error('username') is-invalid @enderror" value="{{ old('username') }}" required autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mt-3">
                                <label for="passphrase">Passphrase</label>
                                <input id="passphrase" type="text" class="form-control validate @error('passphrase') is-invalid @enderror" name="passphrase" required>

                                @error('passphrase')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-block text-uppercase">Send</button>
                            </div>
                            <a href="{{ route('login') }}"><button type="button" class="mt-5 btn btn-default btn-block text-uppercase">Back to Login</button></a>
                        @honeypot
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
