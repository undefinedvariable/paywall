@extends('layouts.private')

@section('content')
<div class="container tm-mt-big tm-mb-big">
    <div class="row">
        <div class="col-12 mx-auto tm-login-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="tm-block-title mb-4">Sign in to <a style="color: #f5a623" href="{{ url('/') }}" target="_self" title="home">{{ config('app.name') }}</a> user area</h2>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        <form method="POST" action="{{ route('2fa.login') }}" class="tm-login-form">
                        @csrf

                        <input name="user_id" type="hidden" value="{{ $user->id }}">

                        <div class="form-group">
                                <label for="msg">Message</label>
                                <textarea id="msg" name="msg" class="form-control" disabled>{{ $msg }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="code">Code</label>
                                <input id="code" name="code" type="text" class="form-control validate @error('code') is-invalid @enderror" value="{{ old('code') }}" required autofocus>
                                @error('code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-block text-uppercase">Login</button>
                            </div>
                        @honeypot
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
