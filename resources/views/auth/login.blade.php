@extends('layouts.private')

@section('content')
<div class="container tm-mt-big tm-mb-big">
    <div class="row">
        <div class="col-12 mx-auto tm-login-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="tm-block-title mb-4">Sign in to <a style="color: #f5a623" href="{{ url('/') }}" target="_self" title="home">{{ config('app.name') }}</a> user area</h2>
                        <h2><a href="{{ route('2fa.form') }}"><span class="badge badge-secondary">Switch to 2FA</span></a></h2>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        <form method="POST" action="{{ route('login') }}" class="tm-login-form">
                        @csrf

                            <div class="form-group">
                                <label for="username">Username</label>
                                <input id="username" name="username" type="text" class="form-control validate @error('username') is-invalid @enderror" value="{{ old('username') }}" required autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mt-3">
                                <label for="password">Password</label>
                                <input id="password" type="password" class="form-control validate @error('password') is-invalid @enderror" name="password" required>
                                <a href="{{ route('password.recover') }}">Forgot your password?</a>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group mt-3">
                                <label for="captcha">Captcha</label><br>

                                @captcha

                                <br>
                                <input id="captcha" type="text" class="form-control validate @error('captcha') is-invalid @enderror" name="captcha" autocomplete="off" required>

                                @error('captcha')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-block text-uppercase">Login</button>
                            </div>
                            <hr>
                            <a href="{{ route('register') }}"><button type="button" class="mt-5 btn btn-default btn-block text-uppercase">Register</button></a>
                        @honeypot
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
