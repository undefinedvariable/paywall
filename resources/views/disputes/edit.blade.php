@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title">{{ __('Open new dispute') }}</h2>
                    <table class="table tm-table-small tm-product-table">
                        <tbody>
                            <tr>
                                <td class="tm-product-name">{{ __('Payment') }}&nbsp;ID</td>
                                <td>{{ $payment->id }}</td>
                            </tr>
                            <tr>
                                <td class="tm-product-name">{{ __('Merchant') }}</td>
                                <td>{{ $payment->merchant->user->username }}</td>
                            </tr>
                            <tr>
                                <td class="tm-product-name">{{ __('Price') }}</td>
                                <td>${{ $payment->getFinalPrice() }}</td>
                            </tr>
                            <tr>
                                <td class="tm-product-name">{{ __('Payment Status') }}</td>
                                <td>{{ strtoupper($payment->status) }}</td>
                            </tr>
                            @if($payment->coupon != null)
                            <tr>
                                <td class="tm-product-name">{{ __('Discount Applied') }}</td>
                                <td>{{ $payment->coupon->discount }}%</td>
                            </tr>
                            @endif
                            @if($payment->item_list != null)
                                @foreach( json_decode($payment->item_list) as $item)
                                    @foreach($item as $key => $value)
                                    <tr>
                                        <td class="tm-product-name">{{ __('Item') }}&nbsp;#{{ $loop->parent->iteration }}</td>
                                        <td>{{ ucfirst($key) }}: {{ $value }}</td>
                                    </tr>
                                    @endforeach
                                @endforeach
                            @endif
                        </tbody>
                    </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-scroll">
                <h2 class="tm-block-title">{{ __('User’s Notes') }}</h2>
                <textarea class="form-control" disabled>{{ $dispute->user_notes }}</textarea>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-scroll">
                <h2 class="tm-block-title">{{ __('Merchant Notes') }}</h2>
                <form method="POST" action="{{ route('disputes.update', $dispute) }}" class="tm-login-form">
                @csrf

                @method('PUT')
                    <textarea id="merchant_notes" name="merchant_notes" class="form-control validate @error('merchant_notes') is-invalid @enderror" placeholder="{{ __('Describe in detail what went wrong') }}" minlength="10" maxlength="5000" required autofocus>{{ old('merchant_notes') }}</textarea>
                    @error('merchant_notes')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">{{ __('Reply') }}</button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection
