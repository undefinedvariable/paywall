@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block">
                <h2 class="tm-block-title">{{ __('Latest Opened') }}</h2>
                <table class="table tm-table-small tm-product-table">
                    <tbody>
                        @foreach($latest_open as $dispute)
                        <tr>
                            <td class="tm-product-name">{{ __('Merchant') }}: {{ $dispute->paymentRequest->merchant->user->username }}</td>
                            <td class="text-center">
                            <a href="{{ route('disputes.show', $dispute->id) }}" class="tm-product-delete-link">
                                <i class="icss-arrow-right tm-product-delete-icon"></i>
                            </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block">
                <h2 class="tm-block-title">{{ __('Refunds') }}</h2>
                <table class="table tm-table-small tm-product-table">
                    <tbody>
                        @foreach($refunds as $refund)
                        <tr>
                            <td class="tm-product-name">{{ __(':username paid back :price', ['username' => $refund->merchant->user->username, 'price' => $refund->getFinalPrice()]) }}</td>
                            <td class="text-center">
                            <a href="{{ route('payments.show', $refund->id) }}" class="tm-product-delete-link">
                                <i class="icss-arrow-right tm-product-delete-icon"></i>
                            </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title">{{ __('Disputes List') }}</h2>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('PAYMENT') }}&nbsp;ID</th>
                            @if($currentUser->isMerchant())
                            <th scope="col">{{ __('MERCHANT') }}</th>
                            @else
                            <th scope="col">{{ __('CLIENT') }}</th>
                            @endif
                            <th scope="col">{{ __('STATUS') }}</th>
                            <th scope="col">{{ __('DATE') }}</th>
                            <th scope="col">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($disputes as $dispute)
                        <tr>
                            <td scope="row"><b><a style="color: white" href="{{ route('payments.show', $dispute->paymentRequest->id) }}">{{ $dispute->paymentRequest->id }}</a></b></td>
                            @if($currentUser->isMerchantInPayment($dispute->paymentRequest))
                            <td class="tm-product-name">{{ $dispute->paymentRequest->merchant->user->username }}</td>
                            @else
                            <td class="tm-product-name">{{ $dispute->paymentRequest->user->username }}</td>
                            @endif
                            <td>{{ strtoupper($dispute->status) }}</td>
                            <td>{{ $dispute->created_at->format('m/d/Y') }}</td>
                            <td>
                            @if($currentUser->isMerchantInPayment($dispute->paymentRequest) && $dispute->isOpen() && empty($dispute->merchant_notes))
                                <a href="{{ route('disputes.edit', $dispute) }}" class="tm-product-delete-link">
                                    <i class="icss-edit tm-product-delete-icon"></i>
                                </a>
                            @else
                                <a href="{{ route('disputes.show', $dispute) }}" class="tm-product-delete-link">
                                    <i class="icss-arrow-right tm-product-delete-icon"></i>
                                </a>
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection