@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-scroll">
                <h2 class="tm-block-title">{{ __('User’s Notes') }}</h2>
                <div class="table tm-table-small tm-product-table">
                {{ $dispute->user_notes }}
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-scroll">
                <h2 class="tm-block-title">{{ __('Merchant Notes') }}</h2>
                <div class="table tm-table-small tm-product-table">
                {{ $dispute->merchant_notes }}
                </div>
            </div>
        </div>
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-scroll">
                <h2 class="tm-block-title">{{ __('See Payment Info') }}</h2>
                <table class="table tm-table-small tm-product-table">
                    <tbody>
                        <tr>
                            <td class="tm-product-name">{{ __('Payment') }}&nbsp;ID</td>
                            <td>{{ $dispute->paymentRequest->id }}</td>
                        </tr>
                        @if($currentUser->isMerchantInPayment($dispute->paymentRequest))
                        <tr>
                            <td class="tm-product-name">{{ __('Client') }}</td>
                            <td>{{ $dispute->paymentRequest->user->username }}</td>
                        </tr>
                        @else
                        <tr>
                            <td class="tm-product-name">{{ __('Merchant') }}</td>
                            <td>{{ $dispute->paymentRequest->merchant->user->username }}</td>
                        </tr>
                        @endif
                        @if($dispute->paymentRequest->item_list != null)
                            @foreach( json_decode($dispute->paymentRequest->item_list) as $item)
                                @foreach($item as $key => $value)
                                <tr>
                                    <td class="tm-product-name">{{ __('Item') }}&nbsp;#{{ $loop->parent->iteration }}</td>
                                    <td>{{ ucfirst($key) }}: {{ $value }}</td>
                                </tr>
                                @endforeach
                            @endforeach
                        @endif
                        <tr>
                            <td class="tm-product-name">{{ __('Price') }}</td>
                            <td>${{ $dispute->paymentRequest->getFinalPrice() }}</td>
                        </tr>
                        <tr>
                            <td class="tm-product-name">{{ __('Status') }}</td>
                            <td>{{ strtoupper($dispute->status) }}</td>
                        </tr>
                    </tbody>
                </table>
                @if(!$currentUser->isMerchantInPayment($dispute->paymentRequest) && $dispute->isOpen())
                <hr>
                <form action="{{ route('disputes.destroy', $dispute) }}" method="POST" role="form">
                @csrf
                    <input type="hidden" name="_method" value="DELETE" />
                    <button class="btn btn-block btn-danger" type="submit">{{ __('Delete Dispute') }}</button>
                @honeypot
                </form>
                @endif
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection
