@extends('layouts.private')

@section('content')
<div class="container tm-mt-big tm-mb-big">
    <div class="row">
        <div class="col-12 mx-auto tm-login-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="tm-block-title mb-4"><u>{{ $client->name }}</u> is requesting permission to access your account</h2>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        @if (count($scopes) > 0)
                        <div class="alert alert-info" role="alert">
                            <p><strong>This application will be able to:</strong></p>
                            <ul>
                                @foreach ($scopes as $scope)
                                    <li>{{ $scope->description }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        <form method="POST" action="{{ route('passport.authorizations.approve') }}" class="tm-login-form">
                        @csrf

                            <input type="hidden" name="state" value="{{ $request->state }}">
                            <input type="hidden" name="client_id" value="{{ $client->id }}">
                            <input type="hidden" name="auth_token" value="{{ $authToken }}">

                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-block text-uppercase">Approve</button>
                            </div>
                        @honeypot
                        </form>
                        <hr>
                        <form method="POST" action="{{ route('passport.authorizations.deny') }}" class="tm-login-form">
                        @csrf
                        
                            @method('DELETE')
                            <input type="hidden" name="state" value="{{ $request->state }}">
                            <input type="hidden" name="client_id" value="{{ $client->id }}">
                            <input type="hidden" name="auth_token" value="{{ $authToken }}">

                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-danger btn-block text-uppercase">Deny</button>
                            </div>
                        @honeypot
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
