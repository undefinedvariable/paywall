<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

            <title>{{ config('app.name') }}</title>

        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">

        <link href="{{ asset('css/all.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/private.css') }}" rel="stylesheet">
        <link href="{{ asset('css/floating.css') }}" rel="stylesheet">

        <style>
        .menuButton {
        padding:20px 0;
        display:block;
        z-index: 12;
        }

        .btn {
        display:block;
        height:7px;
        width:50px;
        z-index:12;
        margin-left:10px;
        background:#fff;
        position:relative;
        -moz-transition-duration: .2s;
        -o-transition-duration: .2s;
        -webkit-transition-duration: .2s;
        transition-duration: .2s;
        }

        .btn::before {
            content:'';
            position:absolute;
            top:-15px;
            left:0;
            width:50px;
            height:7px;
            background:#fff;
            -moz-transition-duration: .2s;
            -o-transition-duration: .2s;
            -webkit-transition-duration: .2s;
            transition-duration: .2s;
        }
        
        .btn::after {
            content:'';
            position:absolute;
            top:15px;
            left:0;
            width:50px;
            height:7px;
            background:#fff;
            -moz-transition-duration: .2s;
            -o-transition-duration: .2s;
            -webkit-transition-duration: .2s;
            transition-duration: .2s;
        }


        .activeBtn {
        background:transparent;
        }

        .activeBtn::before {
            -webkit-transform:rotate(45deg);
            top:0;
        }
        
        .activeBtn::after {
            -webkit-transform:rotate(-45deg);
            top:0;
        }

        #overmenu{
            position: fixed;
            top:0;
            left:-1%;
            background:#EC6B56;
            width:101%;
            margin-left:+=101%;
            height:100%;
            z-index: 10;
            opacity:1;
        }

        #overmenu span{
            padding:5px 7px;
        }

        #overmenu ul {
            position: relative;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }

        #overmenu a {
            color:#fff;
            font-size: 50px;
            font-weight: bold;
            text-decoration:none;
        }

        #overmenu span:hover {
            background:#fff;
            color: #4e657a;
        }

        #overmenu li {
            list-style: none;
            padding: 10px;
            margin-bottom: 5%;
            text-align: center;
        }
        </style>
    </head>
    
    <body id="reportsPage">

        <a class="menuButton" href="{{ url(session()->pull('_previous')['url']) }}">
            <div class="btn activeBtn"></div>
        </a>
        <div id="overmenu">
        <ul>
            <li><a href="{{ route('home') }}"><span>Dashboard</span></a></li>
            <li><a href="{{ route('payments.index') }}"><span>Payments</span></a></li>
            <li><a href="{{ route('disputes.index') }}"><span>Disputes</span></a></li>
            @if($currentUser->isMerchant())
            <li><a href="{{ route('merchants.show', $currentUser->merchant) }}"><span>Merchant</span></a></li>
            @elseif($currentUser->isStaff() || $currentUser->isAdmin())
            <li><a href="{{ route('admin.dashboard') }}"><span>Manage</span></a></li>
            @endif
            <li><a href="{{ route('users.show', $currentUser) }}"><span>My Account</span></a></li>
            <li><a href="{{ route('logout') }}"><span>Logout</span></a></li>
        </ul>
        </div>

    </body>
</html>