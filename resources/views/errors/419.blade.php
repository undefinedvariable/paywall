@extends('layouts.error')

@section('title')
    <title>Error 419 - Resource Expired</title>
@endsection

@section('content')
    <h1>419</h1>
    <h2>Oops, the session has expired.</h2>
    <p>You may want to reload the page.<br>
    If you think something is broken, report a problem.</br></p>
@endsection