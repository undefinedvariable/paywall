@extends('layouts.error')

@section('title')
    <title>Error 401 - Unauthorized</title>
@endsection

@section('content')
    <h1>401</h1>
    <h2>Oops, 
    you can't access the page you're looking for.</h2>
    <p>You may want to head back to the homepage.<br>
    If you think something is broken, report a problem.</br></p>
@endsection