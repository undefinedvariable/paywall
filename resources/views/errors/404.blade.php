@extends('layouts.error')

@section('title')
    <title>Error 404 - Page Not Found</title>
@endsection

@section('content')
    <h1>404</h1>
    <h2>Oops, the page you're looking for does not exist.</h2>
    <p>You may want to head back to the homepage.<br>
    If you think something is broken, report a problem.</br></p>
@endsection