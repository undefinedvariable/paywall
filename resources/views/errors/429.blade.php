@extends('layouts.error')

@section('title')
    <title>Error 429 - Too Many Requests</title>
@endsection

@section('content')
    <h1>429</h1>
    <h2>Oops, you have sent too many requests.</h2>
    <p>You may want to wait a bit before retrying.<br>
    If you think something is broken, report a problem.</br></p>
@endsection