@extends('layouts.error')

@section('title')
    <title>Error 500 - Server Error</title>
@endsection

@section('content')
    <h1>500</h1>
    <h2>Oops, there has been a server error.</h2>
    <p>You may want to head back to the homepage.<br>
    If you think something is broken, report a problem.</br></p>
@endsection