@extends('layouts.private')

@section('content')
<div class="container mt-5">
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <form action="{{ route('changetier') }}" method="POST">
                @csrf
                    <h2 class="tm-block-title">{{ __('Change Tier') }}
                    </h2>
                    <select class="custom-select validate @error('tier') is-invalid @enderror" id="tier" name="tier" required>
                        @foreach($tiers as $key => $tier)
                        <option value="{{ $key }}"
                        @if($currentUser->user_tier->tier->is($key))
                        selected
                        @endif
                        >{{ strtoupper($tier) }}</option>
                        @endforeach
                    </select>
                    @error('tier')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <p>&nbsp;</p>
                    <input type="submit" class="btn btn-info btn-block text-uppercase" value="{{ __('Set') }}">
                @honeypot
                </form>
                <br>
                <h5 class="tm-block-body">{{ __('Remaining :credits credits, expiring on :expiration', ['credits' => $credits, 'expiration' => $currentUser->user_tier->tier_expiration->format('m/d/Y')]) }}</h5>
            </div>
        </div>
    </div>

    <!-- row -->
    <div class="row tm-content-row">
        <div class="tm-block-col tm-col-avatar">
            <div class="tm-bg-primary-dark tm-block tm-block-avatar">
                <h2 class="tm-block-title">{{ __('Marketplaces') }}</h2>
                @if(empty($marketplaces))
                    <h5 class=tm-block-body>---</h5>
                @else
                    @foreach($marketplaces as $marketplace)
                    <h5 class=tm-block-body>{{ $loop->index+1 }}.&nbsp;&nbsp;{{ $marketplace->user->username }}</h5>
                    @endforeach
                @endif
                <br>
                <a class="btn btn-primary btn-block text-uppercase" href="{{ route('tokens.index') }}">{{ __('Manage') }}</a>
            </div>
        </div>

        <div class="tm-block-col tm-col-account-settings">
            <div class="tm-bg-primary-dark tm-block tm-block-settings">
                <h2 class="tm-block-title">{{ __('Merchant Settings') }}</h2>
                <form action="{{ route('merchants.update', $currentUser->merchant) }}" class="tm-signup-form row" method="POST">
                @csrf

                @method('PUT')
                    <div class="form-group col-lg-6">
                        <label for="ipn_url">IPN URL</label>
                        <input id="ipn_url" name="ipn_url" type="text" class="form-control validate" value="{{ $currentUser->merchant->ipn_url }}" maxlength="190" />
                        @error('ipn_url')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="ipn_enable">IPN Callback</label>
                        <select id="ipn_enable" name="ipn_enable" class="custom-select validate @error('ipn_enable') is-invalid @enderror">
                            <option value="1"
                            @if($currentUser->merchant->ipn_enable)
                                selected
                            @endif
                            >enabled
                            </option>
                            <option value="0"
                            @if(!$currentUser->merchant->ipn_enable)
                                selected
                            @endif
                            >disabled
                            </option>
                        </select>
                        @error('ipn_enable')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="website">{{ __('Website') }}</label>
                        <input id="website" name="website" type="text" class="form-control validate" value="{{ $currentUser->merchant->website }}" maxlength="190" />
                        @error('website')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="tm-hide-sm">&nbsp;</label>
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">
                        Update
                        </button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>

    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <form action="{{ route('merchant.setwallet') }}" method="POST">
                @csrf
                    <h2 class="tm-block-title">Monero Personal Wallet</h2>
                    <div class="form-group">
                        <label for="personal_wallet_address">Wallet Address</label>
                        <input id="personal_wallet_address" name="personal_wallet_address" type="text" class="form-control validate" minlength="90" maxlength="110" placeholder="{{ $currentUser->merchant->personal_wallet_address }}" required />
                        @error('personal_wallet_address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                    <label for="personal_view_key">Secret View Key</label>
                    <input id="personal_view_key" name="personal_view_key" type="text" class="form-control validate" minlength="60" maxlength="100" required />
                    @error('personal_view_key')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                    <p>&nbsp;</p>
                    <input type="submit" class="btn btn-primary btn-block text-uppercase" value="update data">
                @honeypot
                </form>
            </div>
        </div>
    </div>
</div>
@endsection