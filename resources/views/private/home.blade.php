@extends('layouts.private')

@section('content')
<div class="container">

    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller">
                <h2 class="tm-block-title">{{ __('Notifications List') }}</h2>
                <div class="tm-notification-items">
                    @foreach ($currentUser->unreadNotifications as $notification)
                    <div class="media tm-notification-item">
                        <div class="media-body">
                            <a class="mb-2" href="{{ route('markasread', $notification) }}" style="color: white"><b>{{ $notification->data['message'] }}</b></a>
                            <span class="tm-small tm-text-color-secondary">{{ \Carbon\Carbon::parse($notification->data['when'])->diffForHumans() }}</span>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title">{{ __('News') }}</h2>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('INFO') }}</th>
                            <th scope="col">{{ __('DATE') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($news as $n)
                        <tr>
                            <td scope="row">{{ $n->body }}</td>
                            <td>{{ $n->created_at->format('m/d/Y') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection