@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block">
                <h2 class="tm-block-title">{{ __('OAuth Clients') }}</h2>
                <table class="table tm-table-small tm-product-table">
                    <tbody>
                        @foreach($clients as $client)
                        <tr>
                            <td class="tm-product-name">{{ $client->name }}</td>
                            <td class="text-center">{{ $client->redirect }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block">
                <h2 class="tm-block-title">{{ __('Recently Generated Tokens') }}</h2>
                <table class="table tm-table-small tm-product-table">
                    <tbody>
                        @foreach($most_recent as $token)
                        <tr>
                            <td class="tm-product-name">{{ $token->name }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title">{{ __('All Tokens List') }}</h2>
                <table class="table tm-table-small tm-product-table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('CODE') }}</th>
                            <th scope="col">{{ __('CLIENT ID') }}</th>
                            <th scope="col">{{ __('STATUS') }}</th>
                            <th scope="col">{{ __('CREATED') }}</th>
                            <th scope="col">{{ __('EXPIRES') }}</th>
                            <th scope="col">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tokens as $token)
                        <tr>
                            <td scope="row">{{ $token->id }}</td>
                            <td>{{ $token->client->name }}</td>
                            <td>
                            @if($token->revoked)
                                {{ __('Revoked') }}
                            @else
                                {{ __('Valid') }}
                            @endif
                            </td>
                            <td>{{ \Carbon\Carbon::parse($token->created_at)->format('m/d/Y') }}</td>
                            <td>{{ \Carbon\Carbon::parse($token->expires_at)->format('m/d/Y') }}</td>
                            <td>
                                @if($token->revoked)
                                    <form action="{{ route('tokens.delete', $token) }}" method="POST">
                                    @csrf
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button class="tm-product-delete-link" type="submit">
                                            <i class="icss-bin tm-product-delete-icon"></i>
                                        </button>
                                    @honeypot
                                    </form>
                                @else
                                    <form action="{{ route('tokens.revoke', $token) }}" method="GET">
                                    @csrf
                                        <button class="tm-product-delete-link" type="submit">
                                            <i class="icss-ban tm-product-delete-icon"></i>
                                        </button>
                                    @honeypot
                                    </form>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection