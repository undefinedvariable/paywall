@extends('layouts.app')

@section('content')

<!-- Main -->
<div id="main">

    <a href="{{ route('index') }}" target="_self" title="home"><i class="icss-x x2"></i></a>

    <!-- Intro -->
    <article id="intro">
        <h2 class="major">Intro</h2>
        <section>
            <p>{{ config('app.name') }} was born with a precise vision: to make private and secure payments possible online with crypto while lowering the barrier for non tech-savy users.
            A unique system made for everyone, simple and handy to use, powerful, production-ready that can easily be integrated on any website using a well-documented set of API.
            By using shreddable intermediate wallets that are disposed of once the transaction is settled, we are able to completely detach clients and vendors offering increased privacy while
            taking care of the whole process. Anonimized data about transactions, disputes and payments is then stored encrypted in a distributed blockchain so that it is impossible to tamper or to
            delete. We support Bitcoin, Litecoin, Monero, Ethereum, ZCash and Dash. Monero is our favourite and has been chosen as intermediate step to break the chain and avoid backlinks
            in the public blockchain.
            </p>
            <p>No more fear of new markets and vendor shops, pay with your favourite crypto and let us protect you from frauds.</p>
        </section>
        <section>
            <h3 class="major">Benefits for Users</h3>
            <ul>
                <li>Multicurrency support.</li>
                <li>Fraud protection.</li>
                <li>Escrow and Multisig.</li>
                <li>Dispute management.</li>
            </ul>
        </section>
        <section>
            <h3 class="major">Benefits for Merchants</h3>
            <ul>
                <li>API integration.</li>
                <li>Multicurrency payments.</li>
                <li>Advanced Security.</li>
                <li>Gold cashout.</li>
            </ul>
        </section>
        <section>
            <h3 class="major">Pricing</h3><div class="table-wrapper">
            <table class="alt">
                <thead>
                    <tr>
                        <th>Tier</th>
                        <th>Description</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Developer</td>
                        <td>One cryptocurrency supported, 10 api calls/day, no business data</td>
                        <td>free</td>
                    </tr>
                    <tr>
                        <td>Bronze</td>
                        <td>One cryptocurrency supported, 50 api calls/day, limited business data</td>
                        <td>$9.99/mo</td>
                    </tr>
                    <tr>
                        <td>Silver</td>
                        <td>Two cryptocurrencies supported, 100 api calls/day, extended business data</td>
                        <td>$15.99/mo</td>
                    </tr><tr>
                        <td>Gold</td>
                        <td>Custom cryptocurrency supported, unlimited api calls/day, custom business data</td>
                        <td><a href="{{ route('contacts') }}" title="contact us">ask</a></td>
                    </tr>
                </tbody>
            </table>
        </section>
    </article>
</div>

@endsection