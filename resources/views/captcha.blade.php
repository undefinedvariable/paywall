@extends('layouts.app')

@section('content')
<!-- Main -->
<div id="main">

    <a href="{{ route('index') }}" target="_self" title="home"><i class="icss-x x2"></i></a>

    <!-- Contact -->
    <article id="contact">
        <section>
            <h2 class="major">Entrance Gate</h2>
            <h4>This is a humans only club, no robots allowed!</h4>
            <p>Solve this simple math question, <b>ignore the last operation</b></p>
            <h2>{{ $question }} - {{ rand(1,10) }} =</h2>
            <form action="{{ route('riddle') }}" method="POST">
            @csrf
            {!! $input !!}
            <br>
            <input type="submit" value="Submit" class="primary">
            @honeypot
            </form>
        </section>
    </article>

</div>
@endsection
