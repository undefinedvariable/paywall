@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">

        <!-- col -->
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title text-center">{{ __('Create New Payment Request') }}</h2>
                <form method="POST" action="{{ route('payments.store') }}" class="tm-login-form">
                @csrf

                    <!-- Description -->
                    <div class="form-group">
                        <label for="description">{{ __('Description') }}</label>
                        <textarea id="description" name="description" class="form-control validate @error('description') is-invalid @enderror h-25" rows="5" required>{{ old('description') }}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <!-- Amount -->
                    <div class="form-group">
                        <label for="amount_usd">{{ __('Amount') }}&nbsp;(USD)</label>
                        <input id="amount_usd" name="amount_usd" type="number" class="form-control validate @error('amount_usd') is-invalid @enderror" value="{{ old('amount_usd') }}" min="0" max="99999999" required>
                        @error('amount_usd')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <!-- Shipping -->
                    <div class="form-group">
                        <label for="shipping_usd">{{ __('Shipping') }} (USD)</label>
                        <input id="shipping_usd" name="shipping_usd" type="number" class="form-control validate @error('shipping_usd') is-invalid @enderror" value="{{ old('shipping_usd') }}" min="0" max="99999999" required>
                        @error('shipping_usd')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <!-- Goods Type -->
                    <div class="form-group">
                        <label for="goods_type">{{ __('Type of Goods') }}</label>
                        <select id="goods_type" name="goods_type" class="custom-select validate @error('goods_type') is-invalid @enderror" required>
                            <option>{{ __('Choose') }}...</option>
                            @foreach($types as $key => $type)
                            <option value="{{ $key }}"
                            @if(old('goods_type') == $key)
                            selected
                            @endif
                            >{{ strtoupper($type) }}</option>
                            @endforeach
                        </select>
                        @error('goods_type')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <!-- Payment Type -->
                    <div class="form-group">
                        <label for="payment_type">{{ __('Type of Payment') }}</label>
                        <select id="payment_type" name="payment_type" class="custom-select validate @error('payment_type') is-invalid @enderror" required>
                            <option>{{ __('Choose') }}...</option>
                            @foreach($payment_types as $key => $payment_type)
                            <option value="{{ $key }}"
                            @if(old('payment_type') == $key)
                            selected
                            @endif
                            >{{ strtoupper($payment_type) }}</option>
                            @endforeach
                        </select>
                        @error('payment_type')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">{{ __('Add') }}</button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
        <!-- /.col -->

    </div>
    <!-- /.row -->
@endsection
