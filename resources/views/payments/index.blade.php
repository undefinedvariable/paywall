@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title">Payments registered</h2>
                    <table class="table table-hover tm-table-small tm-product-table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                @if($currentUser->isMerchant())
                                <th scope="col">{{ __('CLIENT ID') }}</th>
                                @else
                                <th scope="col">{{ __('MERCHANT') }}</th>
                                @endif
                                <th scope="col">{{ __('AMOUNT') }}&nbsp;(USD)</th>
                                <th scope="col">{{ __('STATUS') }}</th>
                                <th scope="col">{{ __('TYPE') }}</th>
                                <th scope="col">{{ __('DATE') }}</th>
                                <th scope="col">{{ __('DISPUTE') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payment_requests as $payment)
                            <tr>
                                <td scope="row"><b><a style="color: white" href="{{ route('payments.show', $payment->id) }}">{{ $payment->id }}</a></b></td>
                                @if($currentUser->isMerchantInPayment($payment))
                                    @if($payment->user != null)
                                    <td>{{ $payment->user->username }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                @else
                                <td>{{ $payment->merchant->user->username }}</td>
                                @endif
                                <td>${{ $payment->getFinalPrice() }}</td>
                                <td>{{ strtoupper($payment->status) }}</td>
                                <td>{{ $payment->payment_type->key }}</td>
                                <td>{{ $payment->created_at->format('m/d/Y') }}</td>
                                <td>
                                    @if( isset($payment->dispute) )
                                        <a class="tm-product-delete-link" href="{{ route('disputes.show', $payment->dispute->id) }}">
                                            <i class="icss-gavel tm-product-delete-icon"></i>
                                        </a>
                                    @elseif(!$currentUser->isMerchantInPayment($payment) && $payment->isEscrow() && $payment->isFunded())
                                        <form action="{{ route('disputes.create') }}" method="GET">
                                        @csrf
                                            <input name="payment_id" id="payment_id" type="hidden" value="{{ $payment->id }}">

                                            <button class="tm-product-delete-link" type="submit">
                                                <i class="icss-flag tm-product-delete-icon"></i>
                                            </button>
                                        @honeypot
                                        </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @if($currentUser->isMerchant())
                <hr>
                <!-- table container -->
                <a href="{{ route('payments.create') }}" class="btn btn-primary btn-block text-uppercase mb-3" title="{{ __('Create New Payment Request') }}">{{ __('Create New Payment Request') }}</a>
                <!--<button class="btn btn-primary btn-block text-uppercase">Delete selected</button>-->
                @endif
            </div>
        </div>

        @if($currentUser->isMerchant())
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title">{{ __('Coupons') }}</h2>
                    <table class="table tm-table-small tm-product-table">
                        <thead>
                            <tr>
                                <th scope="col">{{ __('CODE') }}</th>
                                <th scope="col">{{ __('DISCOUNT') }}</th>
                                <th scope="col">{{ __('USED') }}</th>
                                <th scope="col">{{ __('EXPIRATION') }}</th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($coupons as $coupon)
                            <tr>
                                <td class="tm-product-name">{{ $coupon->code }}</td>
                                <td>{{ $coupon->discount }}%</td>
                                <td>{{ $coupon->users()->count() }}/{{ $coupon->usage_limit }}</td>
                                <td>{{ $coupon->expires_at->format('m/d/Y') }}</td>
                                <td class="text-center">
                                    <form action="{{ route('coupons.destroy', $coupon->id) }}" method="POST">
                                    @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="tm-product-delete-link" type="submit">
                                            <i class="icss-bin tm-product-delete-icon"></i>
                                        </button>
                                    @honeypot
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                <hr>
                <!-- table container -->
                <a href="{{ route('coupons.create') }}" class="btn btn-primary btn-block text-uppercase mb-3">
                    {{ __('Add') }}
                </a>
            </div>
        </div>
        @endif

    </div>
@endsection
