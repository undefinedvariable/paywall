@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-scroll">
                <h2 class="tm-block-title text-center">{{ __('See Payment Info') }}</h2>
                <table class="table tm-table-small tm-product-table">
                    <tbody>
                        <tr>
                            <td class="tm-product-name">ID</td>
                            <td>{{ $payment->id }}</td>
                        </tr>
                        @if($payment->item_list != null)
                            @foreach( json_decode($payment->item_list) as $item)
                                @foreach($item as $key => $value)
                                <tr>
                                    <td class="tm-product-name">{{ __('Item') }}&nbsp;#{{ $loop->parent->iteration }}</td>
                                    <td>{{ ucfirst($key) }}: {{ $value }}</td>
                                </tr>
                                @endforeach
                            @endforeach
                        @else
                        <tr>
                            <td class="tm-product-name">{{ __('Description') }}</td>
                            <td>{{ $payment->description }}</td>
                        </tr>
                        @endif
                        @if( $currentUser->isMerchantInPayment($payment) )
                            @if($payment->user != null)
                            <tr>
                                <td class="tm-product-name">{{ __('Personal Clients') }}</td>
                                <td>{{ $payment->user->username }}</td>
                            </tr>
                            @endif
                            @if($payment->isMarketplaceInvolved())
                            <tr>
                                <td class="tm-product-name">{{ __('Marketplace') }}</td>
                                <td>{{ $payment->marketplace->user->username }}</td>
                            </tr>
                            @endif
                        @endif

                        @if( $currentUser->isClientInPayment($payment) )
                        <tr>
                            <td class="tm-product-name">{{ __('Merchant') }}</td>
                            <td>{{ $payment->merchant->user->username }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td class="tm-product-name">{{ __('Amount') }}</td>
                            <td>${{ $payment->amount_usd }} + (shipping ${{ $payment->shipping_usd }})</td>
                        </tr>
                        @if($payment->coupon != null)
                        <tr>
                            <td class="tm-product-name">{{ __('Applied Discount') }}</td>
                            <td>-{{ $payment->coupon->discount }}%</td>
                        </tr>
                        @endif
                        <tr>
                            <td class="tm-product-name">{{ __('Status') }}</td>
                            <td>{{ strtoupper($payment->status) }}</td>
                        </tr>
                        @if( $payment->isPending() )
                        <tr>
                            <td class="tm-product-name">URL</td>
                            <td><a href="{{ $payment->url }}" target="_blank" title="payment url" style="color: white">{{ $payment->url }}</a></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                @if( $currentUser->isMerchantInPayment($payment) && $payment->notes != '' )
                <br><hr>
                <div>
                    <h2 class="tm-block-title">{{ __('Notes') }}</h2>
                    <div class="table tm-table-small tm-product-table">
                    {{ $payment->notes }}
                    </div>
                </div>
                @endif
                @if($currentUser->isMerchantInPayment($payment) && $payment->merchant_id == $currentUser->merchant->id)
                    @if($payment->isCancelled() || $payment->isExpired())
                        <form action="{{ route('payments.destroy', $payment->id) }}" method="POST" class="form-group mt-4">
                        @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger btn-block text-uppercase">{{ __('Delete') }}</button>
                        @honeypot
                        </form>
                    @elseif($payment->isPending())
                    <form action="{{ route('payments.cancel', $payment->id) }}" method="POST" class="form-group mt-4">
                        @csrf
                            <button type="submit" class="btn btn-danger btn-block text-uppercase">{{ __('Cancel') }}</button>
                    @honeypot
                    </form>
                    @endif
                @endif
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection
