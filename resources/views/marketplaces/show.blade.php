@extends('layouts.private')

@section('content')
<div class="container mt-5">
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <form action="{{ route('changetier') }}" method="POST">
                @csrf
                    <h2 class="tm-block-title">{{ __('Change Tier') }} 
                    @if(!$currentUser->user_tier->isFreeTier())
                    - {{ __('expiring on :expiration', ['expiration' => $currentUser->user_tier->tier_expiration->format('m/d/Y')]) }}
                    @endif
                    </h2>
                    <select class="custom-select validate @error('tier') is-invalid @enderror" id="tier" name="tier" required>
                        @foreach($tiers as $key => $tier)
                        <option value="{{ $key }}"
                        @if($currentUser->user_tier->tier->is($key))
                        selected
                        @endif
                        >{{ strtoupper($tier) }}</option>
                        @endforeach
                    </select>
                    @error('tier')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <p>&nbsp;</p>
                    <input type="submit" class="btn btn-info btn-block text-uppercase" value="{{ __('Set') }}">
                @honeypot
                </form>
                <br>
                <h5 class="tm-block-body">{{ __('Remaining :credits credits, expiring on :expiration', ['credits' => $credits, 'expiration' => $currentUser->user_tier->tier_expiration->format('m/d/Y')]) }}</h5>
            </div>
        </div>
    </div>

    <!-- row -->
    <div class="row tm-content-row">
        <div class="tm-block-col tm-col-avatar">
            <div class="tm-bg-primary-dark tm-block tm-block-avatar">
                <h2 class="tm-block-title">{{ __('OAuth Data') }}</h2>
                <div class="form-group">
                    <label for="client_id">{{ __('Client ID') }}</label>
                    <input id="client_id" class="form-control validate" type="text" value="{{ $client->id }}" disabled>
                </div>
                <div class="form-group">
                    <label for="client_secret">{{ __('Client Secret') }}</label>
                    <input id="client_secret" class="form-control validate" type="text" value="{{ $client->secret }}" disabled>
                </div>
                <br>
                <a class="btn btn-primary btn-block text-uppercase" href="{{ route('tokens.index') }}">{{ __('Manage') }}</a>
            </div>
        </div>

        <div class="tm-block-col tm-col-account-settings">
            <div class="tm-bg-primary-dark tm-block tm-block-settings">
                <h2 class="tm-block-title">{{ __('Marketplace Settings') }}</h2>
                <form action="{{ route('marketplaces.update', $currentUser->marketplace) }}" class="tm-signup-form row" method="POST">
                @csrf

                @method('PUT')
                    <div class="form-group col-lg-6">
                        <label for="ipn_url">IPN URL</label>
                        <input id="ipn_url" name="ipn_url" type="text" class="form-control validate" value="{{ $currentUser->marketplace->ipn_url }}" maxlength="190" />
                        @error('ipn_url')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="ipn_enable">IPN Callback</label>
                        <select id="ipn_enable" name="ipn_enable" class="custom-select validate @error('ipn_enable') is-invalid @enderror">
                            <option value="1"
                            @if($currentUser->marketplace->ipn_enable)
                                selected
                            @endif
                            >enable
                            </option>
                            <option value="0"
                            @if(!$currentUser->marketplace->ipn_enable)
                                selected
                            @endif
                            >disable
                            </option>
                        </select>
                        @error('ipn_enable')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="commission_type">{{ __('Commission Type') }}</label>
                        <select id="commission_type" name="commission_type" class="custom-select validate @error('commission_type') is-invalid @enderror">
                            <option value="">Choose...</option>
                            @foreach($commission_types as $key => $value)
                            <option value="{{ $key }}"
                            @if($currentUser->marketplace->commission_type->value == $key)
                            selected
                            @endif
                            >{{ $value }}</option>
                            @endforeach
                            </option>
                        </select>
                        @error('commission_type')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="commission">{{ __('Commission Value') }}</label>
                        <input id="commission" name="commission" type="text" class="form-control validate" value="{{ $currentUser->marketplace->commission }}" />
                        @error('commission')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="redirect">{{ __('Redirect URL') }}</label>
                        <input id="redirect" name="redirect" type="url" class="form-control validate" value="{{ $client->redirect }}" maxlength="190" />
                        @error('redirect')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="tm-hide-sm">&nbsp;</label>
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">
                        Update
                        </button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>

    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <form action="{{ route('marketplace.setwallet') }}" method="POST">
                @csrf
                    <h2 class="tm-block-title">{{ __('Monero Wallet') }}</h2>
                    <div class="form-group">
                        <label for="wallet_address">{{ __('Wallet Address') }}</label>
                        <input id="wallet_address" name="wallet_address" type="text" class="form-control validate" minlength="90" maxlength="110" placeholder="{{ $currentUser->marketplace->wallet_address }}" required />
                        @error('wallet_address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <p>&nbsp;</p>
                    <input type="submit" class="btn btn-primary btn-block text-uppercase" value="{{ __('Update') }}">
                @honeypot
                </form>
            </div>
        </div>
    </div>
</div>
@endsection