@extends('layouts.private')

@section('content')
<div class="container mt-5">
    <!-- row -->
    @if($currentUser->isMerchant() || $currentUser->isMarketplace() || session()->has('acting_as_user'))
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <form action="{{ route('set_actas') }}" method="POST">
                @csrf
                    <h2 class="tm-block-title">{{ __('Switch to buyer account') }}</h2>
                    <select class="custom-select validate @error('act_as') is-invalid @enderror" id="act_as" name="act_as" required>
                        <option value="true"
                        @if(session()->has('acting_as_user'))
                        selected
                        @endif
                        >YES</option>

                        <option value="false"
                        @if(!session()->has('acting_as_user'))
                        selected
                        @endif
                        >NO</option>
                    </select>
                    @error('act_as')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <p>&nbsp;</p>
                    <input type="submit" class="btn btn-primary btn-block text-uppercase" value="{{ __('SET') }}">
                @honeypot
                </form>
            </div>
        </div>
    </div>
    @endif

    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <form action="{{ route('setlanguage') }}" method="POST">
                @csrf
                    <h2 class="tm-block-title">{{ __('Change Language') }}</h2>
                    <select class="custom-select validate @error('language') is-invalid @enderror" id="language" name="language" required>
                        @foreach($languages as $key => $lang)
                        <option value="{{ $key }}"
                        @if($currentUser->language->is($key))
                        selected
                        @endif
                        >{{ strtoupper($lang) }}</option>
                        @endforeach
                    </select>
                    @error('language')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <p>&nbsp;</p>
                    <input type="submit" class="btn btn-info btn-block text-uppercase" value="{{ __('Set Language') }}">
                @honeypot
                </form>
            </div>
        </div>
    </div>
    <!-- row -->
    <div class="row tm-content-row">
        <div class="tm-block-col tm-col-avatar">
            <div class="tm-bg-primary-dark tm-block tm-block-avatar">
                <h2 class="tm-block-title">{{ __('Security') }}</h2>
                <form action="{{ route('addpgp') }}" method="POST">
                @csrf
                    <div class="tm-avatar-container">
                        @if($currentUser->pgp == null)
                        <textarea class="tm-avatar img-fluid mb-4" id="pgp" name="pgp" minlength="100" maxlength="210000" placeholder="{{ __('Paste your PGP key here') }}" required></textarea>
                        <button class="tm-avatar-delete-link">
                            <i class="icss-plus tm-product-delete-icon"></i>
                        </button>
                        @else
                        <textarea class="tm-avatar img-fluid mb-4" id="pgp" name="pgp" disabled>{{ $publickey }}</textarea>
                        <a class="tm-avatar-delete-link" href="{{ route('removepgp') }}" title="{{ __('Delete PGP key') }}">
                            <i class="icss-trash-drop tm-product-delete-icon"></i>
                        </a>
                        @endif
                    </div>
                @honeypot
                </form>

                @if($currentUser->pgp_auth == true)
                <a class="btn btn-primary btn-block text-uppercase" href="{{ route('disable_2fa') }}" title="{{ __('Disable 2FA') }}">
                {{ __('Disable 2FA') }}
                </a>
                @else
                <a class="btn btn-primary btn-block text-uppercase" href="{{ route('enable_2fa') }}" title="{{ __('Enable 2FA') }}">
                {{ __('Enable 2FA') }}
                </a>
                @endif

            </div>
        </div>
        <div class="tm-block-col tm-col-account-settings">
            <div class="tm-bg-primary-dark tm-block tm-block-settings">
                <h2 class="tm-block-title">{{ __('Account Settings') }}</h2>
                <form action="{{ url('password/change') }}" class="tm-signup-form row" method="POST">
                @csrf
                    <div class="form-group col-lg-6">
                        <label for="username">{{ __('Username') }}</label>
                        <input id="username" name="username" type="text" class="form-control validate" value="{{ $currentUser->username }}" disabled />
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="account">{{ __('Account Type') }}</label>
                        <input id="account" name="account" type="text" class="form-control validate" value="{{ $currentUser->account->key }}" disabled />
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="password">{{ __('New Password') }}</label>
                        <input id="password" name="password" type="password" class="form-control validate @error('password') is-invalid @enderror" minlength="8" maxlength="50" required />
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="password_confirmation">{{ __('Re-enter New Password') }}</label>
                        <input id="password_confirmation" name="password_confirmation" type="password" class="form-control validate @error('password_confirmation') is-invalid @enderror" required/>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="old_password">{{ __('Old Password') }}</label>
                        <input id="old_password" name="old_password" type="password" class="form-control validate @error('old_password') is-invalid @enderror" required />
                        @error('old_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="tm-hide-sm">&nbsp;</label>
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">
                        {{ __('Update Password') }}
                        </button>
                    </div>
                @honeypot
                </form>
                <hr>
                <form action="{{ route('users.destroy', $currentUser->id) }}" class="tm-signup-form row" method="POST">
                @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <div class="col-12">
                        <button type="submit" class="btn btn-danger btn-block text-uppercase">
                        {{ __('Delete My Account') }}
                        </button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>

        <!-- row -->
        <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <form action="{{ route('setwallet') }}" method="POST">
                @csrf
                    <h2 class="tm-block-title">{{ __('Monero Refund Address') }}</h2>
                    <div class="form-group">
                        <label for="refund_address">{{ __('Wallet Address') }}</label>
                        <input id="refund_address" name="refund_address" type="text" class="form-control validate" minlength="90" maxlength="110" placeholder="{{ $currentUser->refund_address }}" required />
                        @error('refund_address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <p>&nbsp;</p>
                    <input type="submit" class="btn btn-primary btn-block text-uppercase" value="{{ __('Update') }}">
                @honeypot
                </form>
            </div>
        </div>
    </div>
</div>
@endsection