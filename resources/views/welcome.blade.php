@extends('layouts.app')

@section('content')

        <!-- Header -->
        <header id="header">
            <div class="logo">
                <i class="icss-guy-fawkes x3"></i>
            </div>
            <div class="content">
                <div class="inner">
                    <h1>{{ config('app.name') }}</h1>
                    <p>A payment gateway made for the crypto world allowing customers, merchants<br />
                    and marketplaces to send and receive funds in complete anonymity and security.</p>
                </div>
            </div>
            <nav>
                <ul>
                    <li><a href="{{ route('intro') }}">Intro</a></li>
                    <li><a href="{{ route('about') }}">About</a></li>
                    <li><a href="{{ route('login') }}">Enter</a></li>
                    <li><a href="{{ route('contacts') }}">Contacts</a></li>
                    <li><a href="{{ route('api') }}">API</a></li>
                </ul>
            </nav>
        </header>

@endsection