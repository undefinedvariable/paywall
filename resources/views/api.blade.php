@extends('layouts.app')

@section('content')

<!-- Main -->
<div id="main">

    <a href="{{ route('index') }}" target="_self" title="home"><i class="icss-x x2"></i></a>

    <!-- Intro -->
    <article id="intro">
        <h2 class="major">API Documentation</h2>
        <section>
            <p>Here is a summary of all avaliable APIs.</p>
            <p>Endpoint: <b>{{ config('app.url').'/api/v1/' }}</b></p>
            <p>All requests must have the <code>Accept: application/json</code> header as well as use the <code>Authorization: Bearer '...'</code>
               token.
            </p>
            <p>{{ config('app.name') }} runs a fully-fledged Oauth2 server, merchants have their own API tokens
                while they can allow marketplaces to access and manage payment requests via cross authorization.<br>
            </p>
        </section>

        <hr>

        <!-- endpoint GET /payments -->
        <section>
            <h3 class="major">[GET] /payments</h3>
            <p>Get data about all payments related to the user</p>
            <h4>Example</h4>
            <pre><code>
$ curl {{ config('app.url') }}/api/v1/payments \
  -X POST \
  -H "Accept: application/json" \
  -H "Authorization: Bearer TOKEN"
            </code></pre>
            <h4>Sample Response</h4>
            <pre><code>{
    "message": [
        {
            "id": "92efc018-a398-12p9-la1b-ce6e3b7788d4",
            "orderid": null,
            "whoami": "MERCHANT",
            "merchant": "someuser",
            "amount_usd": "12.00",
            "shipping_usd": "2.00",
            ... 
        }
    ]
}</code></pre>
            <h4>Response Data</h4>
            <p>see below for info</p>
        </section>

        <hr>

        <!-- endpoint GET /payments/:id -->
        <section>
            <h3 class="major">[GET] /payments/:id</h3>
            <p>Get data about a specific payment</p>
            <h4>URL Parameters</h4>
            <table class="alt">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>id</td>
                        <td>Unique key of the payment (UUID)</td>
                    </tr>
                </tbody>
            </table>
            <h4>Example</h4>
            <pre><code>
$ curl {{ config('app.url') }}/api/v1/payments/923cf45b-8da1-4a57-98fa-36687adec41b \
  -X POST \
  -H "Accept: application/json" \
  -H "Authorization: Bearer TOKEN"
            </code></pre>
            <h4>Sample Response</h4>
            <pre><code>
{
    "id": "92cfc047-a398-46e6-ba1b-da6e3b7785d4",
    "orderid": "Al902p",
    "whoami": "MERCHANT",
    "merchant": "testmerchant",
    "amount_usd": "12.00",
    "shipping_usd": "12.00",
    "goods_type": "DIGITAL",
    "payment_type": "ESCROW",
    "url": "{{ config('app.url') }}/checkout/review/92lop047-a398-46e6-ca1s-da6f3b1930d4?expires=1601814618&signature=c5c361e9311d409095bfef140da9143a2f9fb39f0cd50147108396c72a42c11b",
    "status": "PENDING",
    "expires_at": "2021-02-01T00:00:00.000000Z",
    "created_at": "2021-01-01T00:00:00.000000Z",
    "updated_at": "2021-01-24T00:00:00.000000Z",
    "description": "a test payment",
    "marketplace": "testmarketplace",
    "customer": "testcustomer",
    "notes": null
}</code></pre>
            <h4>Response Data</h4>
            <p>Fields marked with * may be null or not present</p>
            <table class="alt">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>id</td>
                        <td>Unique key of the payment (UUID)</td>
                    </tr>
                    <tr>
                        <td>orderid*</td>
                        <td>A custom ID for the payment</td>
                    </tr>
                    <tr>
                        <td>whoami</td>
                        <td>Whether the user sending the request is a customer or a merchant</td>
                    </tr>
                    <tr>
                        <td>customer*</td>
                        <td>The customer linked to the payment</td>
                    </tr>
                    <tr>
                        <td>merchant</td>
                        <td>The merchant requesting the payment</td>
                    </tr>
                    <tr>
                        <td>marketplace*</td>
                        <td>The marketplace requesting the payment on behalf of the merchant</td>
                    </tr>
                    <tr>
                        <td>amount_usd</td>
                        <td>The items price in USD</td>
                    </tr>
                    <tr>
                        <td>shipping_usd</td>
                        <td>The shipping price in USD</td>
                    </tr>
                    <tr>
                        <td>goods_type</td>
                        <td>The type of goods (PHYSICAL => 30, DIGITAL => 31 or SERVICES => 32)</td>
                    </tr>
                    <tr>
                        <td>payment_type</td>
                        <td>The type of payment (ESCROW => 60 or FE (finalize early) => 61)</td>
                    </tr>
                    <tr>
                        <td>url</td>
                        <td>The checkout URL to redirect the customer to</td>
                    </tr>
                    <tr>
                        <td>status</td>
                        <td>The payment status</td>
                    </tr>
                    <tr>
                        <td>description</td>
                        <td>The payment description in text format</td>
                    </tr>
                    <tr>
                        <td>coupon*</td>
                        <td>Coupon applied</td>
                    </tr>
                    <tr>
                        <td>discount*</td>
                        <td>Discount applied</td>
                    </tr>
                    <tr>
                        <td>notes*</td>
                        <td>Customer notes</td>
                    </tr>
                    <tr>
                        <td>expires_at</td>
                        <td>The payment expiration date</td>
                    </tr>
                    <tr>
                        <td>created_at</td>
                        <td>The payment creation timestamp</td>
                    </tr>
                    <tr>
                        <td>updated_at</td>
                        <td>The payment last update timestamp</td>
                    </tr>
                </tbody>
            </table>
            <h4>States</h4>
            <p>Possible states for a ESCROW payment object are:</p>
            <ul>
                <li>"PENDING" - the payment is still unconfirmed and/or unlinked to a specific customer</li>
                <li>"CONFIRMING" - funds have been sent by the user but the transaction is still unconfirmed</li>
                <li>"FUNDED" - funds have been received, the payment is now confirmed</li>
                <li>"CANCELLED" - payment has been cancelled by the merchant</li>
                <li>"EXPIRED" - funds haven't been sent and the payment expired</li>
                <li>"CLOSED" - the payment has been closed and cannot be further modified</li>
                <li>"REFUNDED" - a refund was issued by the merchant, funds have been sent back to the customer</li>
                <li>"REPORTED" - a dispute has been opened and a moderator will review it</li>
                <li>"CUSTOMER_PAYOUT" - the moderator decided in favour of the customer</li>
                <li>"MERCHANT_PAYOUT" - the moderator decided in favour of the merchant</li>
            </ul>
            <p>Possible states for a FE payment object are:</p>
            <ul>
                <li>"PENDING" - the payment is still unconfirmed and/or unlinked to a specific customer</li>
                <li>"CONFIRMING" - funds have been sent by the user but the transaction is still unconfirmed</li>
                <li>"CLOSED" - the payment has been closed and cannot be further modified</li>
            </ul>
            <p>FE payments are direct and no moderation is possible</p>
        </section>

        <hr>

        <!-- endpoint POST /payments -->
        <section>
            <h3 class="major">[POST] /payments</h3>
            <p>Create a new payment request</p>
            <h4>Parameters</h4>
            <p>Those with * are optional</p>
            <table class="alt">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>orderid*</td>
                        <td>A custom ID for the payment</td>
                    </tr>
                    <tr>
                        <td>amount_usd</td>
                        <td>The items price in USD</td>
                    </tr>
                    <tr>
                        <td>shipping_usd</td>
                        <td>The shipping price in USD</td>
                    </tr>
                    <tr>
                        <td>goods_type</td>
                        <td>The type of goods (PHYSICAL => 30, DIGITAL => 31 or SERVICES => 32)</td>
                    </tr>
                    <tr>
                        <td>payment_type</td>
                        <td>The type of payment (ESCROW => 60 or FE (finalize early) => 61)</td>
                    </tr>
                    <tr>
                        <td>description</td>
                        <td>The payment description in text format</td>
                    </tr>
                </tbody>
            </table>
            <h4>Example</h4>
            <pre><code>
$ curl {{ config('app.url') }}/api/v1/payments \
  -X POST \
  -H "Accept: application/json" \
  -H "Authorization: Bearer TOKEN" \
  -d '{
        "orderid": "ABy32k",
        "amount_usd": 12.11,
        "shipping_usd" => 1.99,
        "goods_type" => 30,
        "payment_type" => 60,
        "description" => "test payment",
    }'
</code></pre>
            <h4>Response</h4>
            <p>A 200 status and the JSON-encoded printout of the newly-created payment request</p>
        </section>

        <hr>

        <!-- endpoint POST /tokens -->
        <section>
            <h3 class="major">[POST] /tokens</h3>
            <p>Create a new OAuth token</p>
            <h4>Parameters</h4>
            <table class="alt">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>client_id</td>
                        <td>A custom ID for the client</td>
                    </tr>
                    <tr>
                        <td>client_secret</td>
                        <td>The shared secret between the client and the server</td>
                    </tr>
                    <tr>
                        <td>grant_type</td>
                        <td>Should be set to 'authorization_code'</td>
                    </tr>
                    <tr>
                        <td>redirect_uri</td>
                        <td>The callback url to return the token to</td>
                    </tr>
                    <tr>
                        <td>code</td>
                        <td>The code obtained by the /authorize API call</td>
                    </tr>
                </tbody>
            </table>
            <h4>Example</h4>
            <pre><code>
        $ curl {{ config('app.url') }}/api/v1/tokens \
        -X POST \
        -H "Accept: application/json" \
        -d '{
        "code": "dab...",
        "redirect_uri": 'http://callback.com',
        "grant_type" => 'authorization_code',
        "client_id" => 30,
        "client_secret" => '...',
        }'
        </code></pre>
            <h4>Response</h4>
            <p>A 200 status and the JSON-encoded printout of the newly-created payment request</p>
        </section>

    </article>
</div>

@endsection