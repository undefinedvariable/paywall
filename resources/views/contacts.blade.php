@extends('layouts.app')

@section('content')
<!-- Main -->
<div id="main">

    <a href="{{ route('index') }}" target="_self" title="home"><i class="icss-x x2"></i></a>

    <!-- Contact -->
    <article id="contact">
        <section>
            <h2 class="major">Contacts</h2>
            <h4>PGP Key</h4>
            <pre><code>
pgp
            </code></pre>
        </section>
        <section>
            @if(count($errors) > 0)
            <h3 class="major">Errors</h3>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @elseif(session()->has('message'))
            <h4>{{ session()->get('message') }}</h4>
            @endif
            <form method="POST" action="{{ route('send_msg') }}">
            @csrf
                <div class="fields">
                    <div class="field half">
                        <label for="contact">Contact</label>
                        <input type="text" name="contact" id="contact" maxlength="100" placeholder="someone@said.hello" required>
                    </div>
                    <div class="field half">
                        <label for="contact_type">Contact Channel</label>
                        <input type="text" name="contact_type" id="contact_type" maxlength="200" placeholder="specify jabber/email/telegram/..." required>
                    </div>
                    <div class="field">
                        <label for="body">Message</label>
                        <textarea name="body" id="body" rows="4" minlength="10" maxlength="1000" placeholder="Everything starts with one word..." required></textarea>
                    </div>
                    <div class="field">
                        <label for="demo-category">Are you a human?</label>
                        <select name="robot" id="robot" required>
                            <option value="">-</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                            <option value="3">Maybe</option>
                        </select>
                    </div>
                </div>
                <ul class="actions">
                    <li><input type="submit" value="Send Message" class="primary"></li>
                    <li><input type="reset" value="Reset"></li>
                </ul>
            @honeypot
            </form>
        </section>
    </article>

</div>
@endsection