<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="robots" content="noindex,nofollow">

        <title>{{ config('app.name') }}</title>

        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">

        <link href="{{ asset('css/all.css') }}" rel="stylesheet">
        <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

        @yield('css')

    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-gamepad"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">Staff Area</div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Nav Item - Single -->
                <li class="nav-item {{ request()->is('admin/dashboard') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('admin.dashboard') }}">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Controls
                </div>

                @if($currentUser->isAdmin())
                <!-- Nav Item - Collapse Menu -->
                <li class="nav-item {{ (request()->is('admin/users') || request()->is('admin/users/*')) ? 'active' : '' }}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers" aria-expanded="true" aria-controls="collapseUsers">
                        <i class="fas fa-fw fa-users"></i>
                        <span>Users</span>
                    </a>
                    <div id="collapseUsers" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Filter:</h6>
                            <a class="collapse-item" href="{{ route('admin.users.index') }}">All</a>
                            <a class="collapse-item" href="{{ route('admin.users.removed') }}">Removed</a>
                        </div>
                    </div>
                </li>
                @endif

                <!-- Nav Item - Collapse Menu -->
                <li class="nav-item {{ (request()->is('admin/disputes') || request()->is('admin/disputes/*')) ? 'active' : '' }}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDisputes" aria-expanded="true" aria-controls="collapseDisputes">
                        <i class="fas fa-fw fa-balance-scale-right"></i>
                        <span>Disputes</span>
                    </a>
                    <div id="collapseDisputes" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Filter:</h6>
                            <a class="collapse-item" href="{{ route('admin.disputes.index') }}">All</a>
                            <a class="collapse-item" href="{{ route('admin.disputes.review') }}">In Review</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Collapse Menu -->
                <li class="nav-item {{ (request()->is('admin/payments') || request()->is('admin/payments/*')) ? 'active' : '' }}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePayments" aria-expanded="true" aria-controls="collapsePayments">
                        <i class="fas fa-fw fa-exchange-alt"></i>
                        <span>Payments</span>
                    </a>
                    <div id="collapsePayments" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Filter:</h6>
                            <a class="collapse-item" href="{{ route('admin.payments.index') }}">All</a>
                            <a class="collapse-item" href="{{ route('admin.payments.reported') }}">Reported</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Collapse Menu -->
                <li class="nav-item {{ (request()->is('admin/tickets') || request()->is('admin/tickets/*')) ? 'active' : '' }}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTickets" aria-expanded="true" aria-controls="collapseTickets">
                        <i class="fas fa-fw fa-life-ring"></i>
                        <span>Tickets</span>
                    </a>
                    <div id="collapseTickets" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Filter:</h6>
                            <a class="collapse-item" href="{{ route('admin.tickets.index') }}">All</a>
                            <a class="collapse-item" href="{{ route('admin.tickets.open') }}">Open</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Collapse Menu -->
                <li class="nav-item {{ (request()->is('admin/news') || request()->is('admin/news/*')) ? 'active' : '' }}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseNews" aria-expanded="true" aria-controls="collapseNews">
                        <i class="fas fa-fw fa-newspaper"></i>
                        <span>News</span>
                    </a>
                    <div id="collapseNews" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Filter:</h6>
                            <a class="collapse-item" href="{{ route('admin.news.index') }}">All</a>
                            <a class="collapse-item" href="{{ route('admin.news.create') }}">Create</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Collapse Menu -->
                <li class="nav-item {{ (request()->is('admin/cryptocurrencies') || request()->is('admin/cryptocurrencies/*')) ? 'active' : '' }}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCryptocurrencies" aria-expanded="true" aria-controls="collapseCryptocurrencies">
                        <i class="fas fa-fw fa-coins"></i>
                        <span>Cryptocurrencies</span>
                    </a>
                    <div id="collapseCryptocurrencies" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Filter:</h6>
                            <a class="collapse-item" href="{{ route('admin.cryptocurrencies.index') }}">All</a>
                            <a class="collapse-item" href="{{ route('admin.cryptocurrencies.create') }}">Create</a>
                        </div>
                    </div>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Tools
                </div>

                <!-- Nav Item - Single -->
                <li class="nav-item {{ request()->is('admin/messages') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('admin.messages') }}">
                        <i class="fas fa-fw fa-envelope-open-text"></i>
                        <span>Messages</span>
                    </a>
                </li>
                <li class="nav-item {{ request()->is('admin/interactions') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('admin.interactions') }}">
                        <i class="fas fa-fw fa-mouse-pointer"></i>
                        <span>Interactions</span>
                    </a>
                </li>
                <li class="nav-item {{ request()->is('admin/logs') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('admin.logs') }}">
                        <i class="fas fa-fw fa-pen"></i>
                        <span>Logs</span>
                    </a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">

                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>

            </ul>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->
                        <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                            <div class="input-group">
                                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fas fa-search fa-sm"></i>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <!-- Topbar Navbar -->
                        <ul class="navbar-nav ml-auto">

                            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                            <li class="nav-item dropdown no-arrow d-sm-none">
                                <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-search fa-fw"></i>
                                </a>
                                <!-- Dropdown - Messages -->
                                <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="form-inline mr-auto w-100 navbar-search">
                                        <div class="input-group">
                                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button">
                                                    <i class="fas fa-search fa-sm"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>

                            <!-- Nav Item - Alerts -->
                            <li class="nav-item dropdown no-arrow mx-1">
                                <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bell fa-fw"></i>
                                    <!-- Counter - Alerts -->
                                    <span class="badge badge-danger badge-counter">{{ $currentUser->unreadNotifications->count() }}</span>
                                </a>
                                <!-- Dropdown - Alerts -->
                                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                                    <h6 class="dropdown-header">
                                        Alerts Center
                                    </h6>
                                    @foreach ($currentUser->unreadNotifications as $notification)
                                    <a class="dropdown-item d-flex align-items-center" href="{{ route('markasread', $notification) }}">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-primary">
                                                <i class="fas fa-comment text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">{{ \Carbon\Carbon::parse($notification->data['when'])->format('h:i:s m/d/Y') }}</div>
                                            <span class="font-weight-bold">{{ $notification->data['message'] }}</span>
                                        </div>
                                    </a>
                                    @endforeach
                                    <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                                </div>
                            </li>

                            <div class="topbar-divider d-none d-sm-block"></div>

                            <!-- Nav Item - User Information -->
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ $currentUser->username }}</span>
                                    <img class="img-profile rounded-circle" src="{{ url('/images/flowers/'.strtolower(Auth::user()->flower->key).'.png') }}">
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="{{ route('users.show', $currentUser) }}">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i> Profile
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i> Logout
                                    </a>
                                </div>
                            </li>

                        </ul>

                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                    @if(session()->has('message'))
                    <br>
                    <div class="alert alert-success" role="alert">
                        <strong>{{ session()->get('message') }}</strong>
                    </div>
                    <br>
                    @endif

                    @if(session()->has('error'))
                    <br>
                    <div class="alert alert-danger" role="alert">
                        <strong>{{ session()->get('error') }}</strong>
                    </div>
                    <br>
                    @endif

                        @yield('content')

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            Made with <span style="color: #e25555;">&#9829;</span> in the onion land 
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="{{ route('logout') }}">Logout</a>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

        @yield('js')

        <!-- Page level plugins -->
        <script src="vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="js/demo/chart-area-demo.js"></script>
        <script src="js/demo/chart-pie-demo.js"></script>

    </body>

</html>