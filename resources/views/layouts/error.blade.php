<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="robots" content="noindex,nofollow">

        @yield('title')

        <link rel="icon" href="{{ asset('images/favicon.ico') }}">
        <link href="{{ asset('css/errors.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class='container'>
            <div class='row content'>
                <div class='col-lg-12'></div>
                <div class='col-lg-12'>

                @yield('content')

                    <a class='btn' href="{{ route('index') }}" style="color:white">RETURN HOME</a>
                    <a class='btn' href="{{ route('contacts') }}" style="color:white">REPORT PROBLEM</a>
                </div>
            </div>
        </div>
        <div class='bg-img'></div>
    </body>
</html>
