<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="robots" content="noindex,nofollow">

            <title>{{ config('app.name') }} - {{ __('Private Area') }}</title>

        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">

        <link href="{{ asset('css/iconicss.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/private.css') }}" rel="stylesheet">
        <link href="{{ asset('css/floating.css') }}" rel="stylesheet">

        @yield('css')

    </head>

    <body id="reportsPage">

        @auth
        <div id="home">
            <nav class="navbar navbar-expand-xl">
                <div class="container h-100">
                    <a class="navbar-brand" href="{{ route('index') }}">
                        <h1 class="tm-site-title mb-0">{{ config('app.name') }}</h1>
                    </a>
                    
                    <button class="navbar-toggler ml-auto mr-0" type="button">
                        <a href="{{ route('menu') }}"><i class="icss-bars tm-nav-icon"></i></a>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto h-100">
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('home') ? 'active' : '' }}" href="{{ route('home') }}" title="dashboard">
                                    <i class="icss-speedometer x3"></i>
                                    {{ __('Dashboard') }}
                                    @if(request()->is('home'))
                                    <span class="sr-only">(current)</span>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (request()->is('payments') || request()->is('payments/*')) ? 'active' : '' }}" href="{{ route('payments.index') }}">
                                    <i class="icss-money-wallet x3"></i>
                                    {{ __('Payments') }}
                                    @if(request()->is('payments') || request()->is('payments/*'))
                                    <span class="sr-only">(current)</span>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (request()->is('disputes') || request()->is('disputes/*')) ? 'active' : '' }}" href="{{ route('disputes.index') }}">
                                    <i class="icss-justice x3"></i>
                                    {{ __('Disputes') }}
                                    @if(request()->is('disputes') || request()->is('disputes/*'))
                                    <span class="sr-only">(current)</span>
                                    @endif
                                </a>
                            </li>
                            @if($currentUser->isMerchant())
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('merchants/*') ? 'active' : '' }}" href="{{ route('merchants.show', $currentUser->merchant) }}">
                                    <i class="icss-shop x3"></i>
                                    {{ __('Merchant') }}
                                    @if(request()->is('merchants/*'))
                                    <span class="sr-only">(current)</span>
                                    @endif
                                </a>
                            </li>
                            @elseif($currentUser->isMarketplace())
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('marketplaces/*') ? 'active' : '' }}" href="{{ route('marketplaces.show', $currentUser->marketplace) }}">
                                    <i class="icss-shop x3"></i>
                                    {{ __('Marketplace') }}
                                    @if(request()->is('marketplaces/*'))
                                    <span class="sr-only">(current)</span>
                                    @endif
                                </a>
                            @elseif($currentUser->isStaff() || $currentUser->isAdmin())
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('admin/dashboard') ? 'active' : '' }}" href="{{ route('admin.dashboard') }}">
                                    <i class="icss-gears x3"></i>
                                    {{ __('Manage') }}
                                    @if(request()->is('admin/dashboard'))
                                    <span class="sr-only">(current)</span>
                                    @endif
                                </a>
                            </li>
                            @endif
                            <li class="nav-item">
                            <a class="nav-link {{ request()->is('users/*') ? 'active' : '' }}" href="{{ route('users.show', $currentUser) }}">
                                    <i class="icss-user x3"></i>
                                    {{ __('My Account') }}
                                    @if(request()->is('users/*'))
                                    <span class="sr-only">(current)</span>
                                    @endif
                                </a>
                            </li>
                        </ul>

                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <img alt="flower" style="height: 70px; width: 70px;" src="{{ url('/images/flowers/'.strtolower(Auth::user()->flower->key).'.png') }}" />
                            </li>
                            <li class="nav-item">
                                <a class="nav-link d-block" href="{{ route('logout') }}" title="logout">
                                    {{ $currentUser->username }}, <b>{{ __('Logout') }}</b>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        @endif

        <div class="container">

        <br>
        @include('components.alerts')
        <br>

        @yield('content')

        <br>

        </div>

        @auth
        <a href="{{ route('tickets.index') }}" target="_self" title="Help"><img class="ggv" src="{{ asset('images/help.png') }}"></a>

        <footer class="tm-footer row tm-mt-small">
            <div class="col-12 font-weight-light">
                <p class="text-center text-white mb-0 px-4 small">
                Made with <span style="color: #e25555;">&#9829;</span> in the onion land 
                </p>
            </div>
        </footer>
        @endif

        @include('components.jswarn')
    </body>
</html>
