<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="robots" content="noindex,nofollow">

            <title>{{ config('app.name') }} - {{ __('Checkout') }}</title>

        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">

        <link href="{{ asset('css/all.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <style>
            html {
            font-size: 14px;
            }

            @media (min-width: 768px) {
            html {
                font-size: 16px;
            }
            }

            .container {
            max-width: 960px;
            }

            .lh-condensed { line-height: 1.25; }
        
            .pricing-header {
            max-width: 700px;
            }

            .card-deck .card {
            min-width: 220px;
            }

            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }

            .circleborder {
            border: 2px solid gray;
            border-radius: 100%;
            display: inline-block;
            }
        </style>

        @yield('css')

    </head>

    <body class="bg-light">
        <div class="container">
            <div class="py-5 text-center">
                @guest
                <img class="d-block mx-auto mb-4" src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}" width="85" height="85">
                @endguest

                @auth
                <img class="d-block mx-auto mb-4 circleborder" src="{{ url('/images/flowers/'.strtolower(Auth::user()->flower->key).'.png') }}" alt="Antiphishing" width="100" height="100">
                @endauth
                <h2>{{ __('Checkout form') }}</h2>
                <p class="lead">{{ __('Follow the steps to complete the payment. To resume the process, use the url provided by the seller. Always check that the canary displayed is the same you picked during registration.') }}</p>
            </div>

            @if(session()->has('message'))
            <div class="alert alert-info" role="alert">
                <strong>{{ session()->get('message') }}</strong>
            </div>
            @endif

            @if(session()->has('errors'))
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                <strong>{{ $error }}</strong>
            </div>
            @endforeach
            @endif

            @yield('content')

            @include('components.jswarn')
        </div>
    </body>

</html>