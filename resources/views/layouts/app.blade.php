<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

            <title>{{ config('app.name') }}</title>

        <meta name="description" content="A secure anonymous decentralized payment processor aimed at simplifying trades in cryptocurrency while offering full privacy.">

        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">

        <link href="{{ asset('css/iconicss.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <style>
            body.is-preload #bg:before {
                background-color: transparent;
            }

            body.is-preload #header {
                -moz-filter: none;
                -webkit-filter: none;
                -ms-filter: none;
                filter: none;
            }

                body.is-preload #header > * {
                    opacity: 1;
                }

                body.is-preload #header .content .inner {
                    max-height: none;
                    padding: 3rem 2rem;
                    opacity: 1;
                }

            #main article {
                opacity: 1;
                margin: 4rem 0 0 0;
            }
        </style>
    </head>
    <body class="is-preload">
        
    <!-- Wrapper -->
    <div id="wrapper">

        @yield('content')

    </div>

    <!-- BG -->
    <div id="bg"></div>

    @include('components.jswarn')
    </body>
</html>
