@extends('layouts.app')

@section('content')

<!-- Main -->
<div id="main">

    <a href="{{ route('index') }}" target="_self" title="home"><i class="icss-x x2"></i></a>

    <!-- Intro -->
    <article id="intro">
        <h2 class="major">About</h2>
        <section>
            <p>Beyond this project there is an active group of enthusiasts users who, tired of the several exit scams, 
                continuous and tiresome need to migrate from market to market and loss of funds, decided to create a platform
                to facilitate commercial exchanges online.</p>
            <p>We are a group of developers, security advisors and hackers, all anonymous but active on the majour platforms like Dread or Torch,
                committed to guarantee a professional service at extremely low prices, barely enough to cover maintenance costs and infrastructure.</p>
            <p>There is no limit to what you can trade, we don't have a code of conduct or terms of service except the basic rule of safeguarding a 
                productive and helpful space for both clients and merchants where scams are not allowed and persecuted.
            </p>
        </section>
        <section>
            <h3 class="major">Timeline</h3>
            <ol>
                <li><i>February 2020</i> - {{ config('app.name') }} was launched</li>
            </ol>
        </section>
    </article>
</div>

@endsection