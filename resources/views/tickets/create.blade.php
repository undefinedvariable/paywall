@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-scroll">
                <h2 class="tm-block-title">{{ __('Open a Ticket') }}</h2>
                <form method="POST" action="{{ route('tickets.store') }}" class="tm-login-form">
                @csrf
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input id="title" name="title" type="text" class="form-control validate @error('title') is-invalid @enderror" placeholder="{{ __('Sum up your problem in a few words') }}" value="{{ old('title') }}"  minlength="3" maxlength="190" required autofocus>
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="ticket_category_id">{{ __('Category') }}</label>
                        <select id="ticket_category_id" name="ticket_category_id" class="custom-select validate @error('ticket_category_id') is-invalid @enderror" required>
                            <option>{{ __('Choose') }}...</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}"
                            @if(old('ticket_category_id') == $category->id)
                            selected
                            @endif
                            >{{ $category->name }}</option>
                            @endforeach
                        </select>
                        @error('ticket_category_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="message">{{ __('Message') }}</label>
                        <textarea id="message" name="message" class="form-control validate @error('message') is-invalid @enderror" placeholder="{{ __('Describe in detail what went wrong') }}" minlength="10" maxlength="5000" required>{{ old('message') }}</textarea>
                        @error('message')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">{{ __('Create') }}</button>
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection