@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block">
                <h2 class="tm-block-title">{{ __('Open') }}</h2>
                <table class="table tm-table-small tm-product-table">
                    <tbody>
                        @foreach($open_tickets as $ticket)
                        <tr>
                            <td class="tm-product-name">{{ $ticket->title }}</td>
                            <td class="text-center">
                            <a href="{{ route('tickets.show', $ticket) }}" class="tm-product-delete-link">
                                <i class="icss-arrow-right tm-product-delete-icon"></i>
                            </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block">
                <h2 class="tm-block-title">{{ __('Closed') }}</h2>
                <table class="table tm-table-small tm-product-table">
                    <tbody>
                        @foreach($closed_tickets as $ticket)
                        <tr>
                            <td class="tm-product-name">{{ $ticket->title }}</td>
                            <td class="text-center">
                            <a href="{{ route('tickets.show', $ticket) }}" class="tm-product-delete-link">
                                <i class="icss-arrow-right tm-product-delete-icon"></i>
                            </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title">{{ __('Tickets List') }}</h2>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('TITLE') }}</th>
                            <th scope="col">{{ __('CATEGORY') }}</th>
                            <th scope="col">{{ __('STATUS') }}</th>
                            <th scope="col">{{ __('DATE') }}</th>
                            <th scope="col">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tickets as $ticket)
                        <tr>
                            <td scope="row"><b><a style="color: white" href="{{ route('tickets.show', $ticket->id) }}">{{ $ticket->title }}</a></b></td>
                            <td>{{ $ticket->ticketCategory->name }}</td>
                            <td>
                                @if($ticket->isOpen())
                                <div class="tm-status-circle moving">
                                </div>{{ __('Open') }}
                                @else
                                <div class="tm-status-circle cancelled">
                                </div>{{ __('Closed') }}
                                @endif
                            </td>
                            <td>{{ $ticket->created_at->format('m/d/Y') }}</td>
                            <td>
                                <form action="{{ route('tickets.destroy', $ticket) }}" method="POST">
                                @csrf
                                    <input type="hidden" name="_method" value="DELETE" />
                                    <button class="tm-product-delete-link" type="submit">
                                        <i class="icss-bin tm-product-delete-icon"></i>
                                    </button>
                                @honeypot
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr>
                <a href="{{ route('tickets.create') }}" class="btn btn-primary btn-block text-uppercase mb-3" title="{{ __('Open a ticket') }}">{{ __('Open a ticket') }}</a>
            </div>
        </div>
    </div>
@endsection