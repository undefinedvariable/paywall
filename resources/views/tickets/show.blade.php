@extends('layouts.private')

@section('content')
    <!-- row -->
    <div class="row tm-content-row">
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                <h2 class="tm-block-title">{{ __('View a Ticket') }}</h2>
                    <table class="table tm-table-small tm-product-table">
                        <tbody>
                            <tr>
                                <td class="tm-product-name">{{ __('Ticket ID') }}</td>
                                <td>{{ $ticket->id }}</td>
                            </tr>
                            <tr>
                                <td class="tm-product-name">{{ __('Title') }}</td>
                                <td>{{ $ticket->title }}</td>
                            </tr>
                            <tr>
                                <td class="tm-product-name">{{ __('Category') }}</td>
                                <td>{{ $ticket->ticketCategory->name }}</td>
                            </tr>
                            <tr>
                                <td class="tm-product-name">{{ __('Message') }}</td>
                                <td>{{ $ticket->message }}</td>
                            </tr>
                            <tr>
                                <td class="tm-product-name">{{ __('Priority') }}</td>
                                <td>{{ $ticket->priority->key }}</td>
                            </tr>
                            <tr>
                                <td class="tm-product-name">{{ __('Status') }}</td>
                                <td>{{ strtoupper($ticket->status) }}</td>
                            </tr>
                            @foreach($ticket->ticketComments as $comment)
                            <tr>
                                <td class="tm-product-name">{{ $comment->user->username }} {{ __('commented') }}</td>
                                <td>{{ $comment->comment }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-scroll">
                <h2 class="tm-block-title">{{ __('Add Comment') }}</h2>
                <form method="POST" action="{{ route('tickets.update', $ticket) }}" class="tm-login-form">
                @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <input id="ticket_id" name="ticket_id" value="{{ $ticket->id }}" type="hidden">
                    <textarea id="comment" name="comment" class="form-control validate @error('comment') is-invalid @enderror"
                     required>{{ old('comment') }}</textarea>
                    @error('comment')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="form-group mt-4">
                    @if($ticket->isClosed())
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">{{ __('Reopen Ticket') }}</button>
                    @else
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">{{ __('Reply') }}</button>
                    @endif
                    </div>
                @honeypot
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection
