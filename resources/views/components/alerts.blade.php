            @if(session()->has('message'))
            <div class="alert alert-info text-wrap" role="alert">
                <strong>{{ session()->get('message') }}</strong>
            </div>
            @endif

            @if(session()->has('error'))
            <div class="alert alert-danger text-wrap" role="alert">
                <strong>{{ session()->get('error') }}</strong>
            </div>
            @endif

            @if(session()->has('errors'))
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger text-wrap" role="alert">
                    <strong>{{ $error }}</strong>
                </div>
                @endforeach
            @endif