<?php

/** 
 * @todo TBC 
 */

return [
    'net_type' => 'stagenet',
    'rpc_host' => env('MONERO_RPC_HOST'),
    'rpc_port' => env('MONERO_RPC_PORT'),
    'wallet_pass' => env('WALLET_PASS'),
    'admin_wallet_filename' => 'admin_wallet',
    'admin_wallet_address' => '',
];
