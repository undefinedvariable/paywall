<?php

return [
    /*
    |--------------------------------------------------------------------------
    | API Usage Limits
    |--------------------------------------------------------------------------
    */

    // FREE
    'free_credits'      => 10,

    // BRONZE
    'bronze_threshold'  => 0.2,
    'bronze_credits'    => 50,

    // SILVER
    'silver_threshold'  => 0.7,
    'silver_credits'    => 100,

    // GOLD
    'gold_threshold'    => 1.2,
    'gold_credits'      => 500,

    'tier_expiration' => 6, // in months
];
