<?php

/*
    |--------------------------------------------------------------------------
    | Tiers payment thresholds in XMR
    |--------------------------------------------------------------------------
    |
    | These are the prices for each account type expressed in Monero
    |
    */

return [
    'gold_threshold' => 1.1,
    'silver_threshold' => 0.9,
    'bronze_threshold' => 0.5,
];
