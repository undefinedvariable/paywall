<?php

/**
 * @todo TBC
 */
return [

    'morphtoken' => [
        'BTC' => [
            'min' => 0.01,
            'fee' => 0.01,
        ],
        'XMR' => [
            'min' => 0.01,
            'fee' => 0.01,
        ],
    ],

    'xchangeme' => [
        'BTC' => [
            'min' => 0.01,
            'fee' => 0.01,
        ],
        'XMR' => [
            'min' => 0.01,
            'fee' => 0.01,
        ],
    ],
];