<?php

return [
    'usernames' => [
        'admin',
        'administrator',
        'administrators',
        'merchant',
        'market',
        'marketplace',
        'staff',
        'user',
    ],
];
