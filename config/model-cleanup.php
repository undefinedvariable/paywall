<?php

return [

    /*
     * All models in this array that implement `Spatie\ModelCleanup\GetsCleanedUp`
     * will be cleaned.
     */
    'models' => [
        App\Models\Coupon::class,
        App\Models\MultisigWallet::class,
        App\Models\News::class,
        App\Models\Ticket::class,
    ],
];
