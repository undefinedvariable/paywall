<?php

return [
    // fixed transaction fees
    'btc_fee' => 0.0005,
    'bch_fee' => 0.0001,
    'ltc_fee' => 0.001,
    'eth_fee' => 0.008,
    'dash_fee' => 0.0001,

    // min payment value excluding fees
    'min_btc' => 0.001,
    'min_bch' => 0.21,
    'min_ltc' => 0.21,
    'min_eth' => 0.21,
    'min_dash' => 0.21,
];
