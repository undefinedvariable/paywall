## Introduction
Honorarium is the first payment gateway made for the dark web.
It was born with a precise vision: **to make private and secure crypto payments possible  for ALL users**. Who said you need to be an IT guy to use multisignature, view-only wallets and other security features until recently precluded to the less experienced. Honorarium empowers you to upscale your OpSec to the next level.

We designed a unique system made for everyone, simple and handy to use, powerful, production-ready that can easily be integrated on any website using a well-documented set of API. By using shreddable intermediate wallets that are disposed of once the transaction is settled, we are able to completely detach clients and vendors offering increased privacy while taking care of the whole process. Anonymised data about transactions, disputes and payments is then stored encrypted in a distributed blockchain so that it is impossible to tamper or to delete.  

## Background
Beyond this project there is an active group of enthusiasts users who, tired of the several exit scams, continuous and tiresome need to migrate from market to market and loss of funds, decided to create a platform to facilitate commercial exchanges online.

We are committed to guarantee a professional service at extremely low prices, barely enough to cover maintenance costs and infrastructure since we strongly believe and advocate for privacy as a fundamental right.

There is no limit to what you can trade, we don't have a code of conduct or terms of service except the basic rule of safeguarding a productive and helpful space for both clients and merchants where scams are not allowed and persecuted.

## Technical details 
Honorarium natively supports Monero (XMR). A real privacy-preserving decentralised cryptocurrency constantly improving to tackle the utmost threats to anonymity and security.
Customers can pay in any of the supported languages: all incoming transactions, if not in Monero, will be converted to XMR and, for escrow payments only, sent to a multisig wallet. The payment will then be handled by the joint decision of the seller, the buyer and the moderator that may step in to solve disputes or to forward funds to the merchant in case the customer disappears for more than a week.

Thanks to a fully fledged internal Oauth2 server Honorarium can accept third-part marketplaces to create payments on behalf of registered merchants and take a percentage, either fixed or proportional, on the transaction.

## Usage
composer install

php artisan migrate:fresh

php artisan db:seed

php artisan storage:link

php artisan passport:install

php artisan encrypt:generate
