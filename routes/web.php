<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Auth')->middleware(['throttle:20,1'])->group(function () {
    // Banned routes...
    Route::get('ban', 'LoginController@ban')->name('ban');
    // Authentication routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::get('2fa/form', 'LoginController@show_form_2fa')->name('2fa.form');
    Route::post('2fa/challenge', 'LoginController@challenge_2fa')->name('2fa.challenge');
    Route::post('2fa/login', 'LoginController@login_2fa')->name('2fa.login');
    // Registration routes...
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');
    Route::get('register-optional', 'RegisterOptionalController@showForm')->name('register.optional');
    Route::post('register-optional', 'RegisterOptionalController@postForm')->name('register.finalize');
    // Password reset routes...
    Route::get('password/recover', 'PasswordController@showPasswordRecoverForm')->name('password.recover');
    Route::post('password/recover', 'PasswordController@checkUserCredentials');
    Route::get('password/reset/{token}', 'PasswordController@showPasswordResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');
    Route::get('password/change', 'PasswordController@showPasswordChangeForm')->name('password.change');
    Route::post('password/change', 'PasswordController@changePswd');
});

Route::middleware(['throttle:60,1'])->group(function () {
    // public routes
    Route::get('/', 'HomeController@index')->name('index');
    Route::get('intro', 'HomeController@intro')->name('intro');
    Route::get('about', 'HomeController@about')->name('about');
    Route::get('contacts', 'HomeController@contacts')->name('contacts');
    Route::post('contacts', 'HomeController@sendMessage')->name('send_msg');
    Route::get('menu', 'HomeController@menu')->name('menu');
    Route::get('api', 'HomeController@api')->name('api');

    // user private routes
    Route::get('home', 'PrivateController@home')->name('home');
    Route::post('changetier', 'UserTierController@generateDepositAddress')->name('changetier');
    Route::post('wallet', 'PrivateController@setRefundWallet')->name('setwallet');
    Route::post('setlanguage', 'PrivateController@setLanguage')->name('setlanguage');
    Route::post('actas', 'PrivateController@actingAsUser')->name('set_actas');
    Route::get('markasread/{id}', 'PrivateController@markAsRead')->name('markasread');
    Route::post('addpgp', 'PrivateController@addPgpKey')->name('addpgp');
    Route::get('removepgp', 'PrivateController@removePgpKey')->name('removepgp');
    Route::get('enable2fa', 'PrivateController@enable2fa')->name('enable_2fa');
    Route::get('disable2fa', 'PrivateController@disable2fa')->name('disable_2fa');

    // tokens
    Route::get('tokens', 'TokenController@index')->name('tokens.index');
    Route::delete('tokens/{token}', 'TokenController@destroy')->name('tokens.delete');
    Route::get('tokens/revoke/{token}', 'TokenController@revoke')->name('tokens.revoke');

    // cancel payments
    Route::post('/payments/cancel/{payment}', 'PaymentRequestController@cancel')->name('payments.cancel');

    // resource controllers
    Route::resource('users', 'UserController');
    Route::resource('payments', 'PaymentRequestController');
    Route::resource('disputes', 'DisputeController');
    Route::resource('coupons', 'CouponController');
    Route::resource('tickets', 'TicketController');
    Route::resource('merchants', 'MerchantController');
    Route::resource('marketplaces', 'MarketplaceController');

    // merchant wallet
    Route::post('merchants/wallet', 'MerchantController@setPersonalWallet')->name('merchant.setwallet');

    // marketplace wallet
    Route::post('marketplaces/wallet', 'MarketplaceController@setWallet')->name('marketplace.setwallet');

    // checkout process
    Route::prefix('checkout')->group(function () {
        Route::get('/review/{payment}', 'CheckoutController@review')->name('checkout.review');
        Route::get('/wallet/{payment}', 'CheckoutController@wallet')->name('checkout.wallet');
        Route::get('/currency/{payment}', 'CheckoutController@currency')->name('checkout.currency');
        Route::get('/pay/{payment}/{cryptocurrency}', 'CheckoutController@pay')->name('checkout.pay');
        Route::get('/verify/{payment}', 'CheckoutController@verify')->name('checkout.verify');
        Route::post('/coupon', 'CheckoutController@coupon')->name('checkout.coupon');
        Route::get('/success', 'CheckoutController@successPage')->name('checkout.success');
        Route::get('/cancel', 'CheckoutController@cancelPage')->name('checkout.cancel');
        Route::get('/expired', 'CheckoutController@expiredPage')->name('checkout.expired');
    });

    Route::namespace('Admin')->middleware(['staff'])->prefix('admin')->group(function () {
        Route::get('/online', 'AdminController@online')->name('admin.online');
        Route::get('/offline', 'AdminController@offline')->name('admin.offline');
        Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
        Route::get('/interactions', 'AdminController@interactions')->name('admin.interactions');
        Route::get('/messages', 'AdminController@messages')->name('admin.messages');
        Route::get('/logs', 'LogsController@index')->name('admin.logs');
        Route::get('/logs/clear', 'LogsController@clear')->name('admin.clearlogs');

        // news
        Route::get('/news', 'AdminNewsController@index')->name('admin.news.index');
        Route::get('/news/create', 'AdminNewsController@create')->name('admin.news.create');
        Route::post('/news', 'AdminNewsController@store')->name('admin.news.store');
        Route::delete('/news/{news}', 'AdminNewsController@destroy')->name('admin.news.destroy');
        // users
        Route::get('/users', 'AdminUserController@index')->name('admin.users.index');
        Route::get('/users/show/{user}', 'AdminUserController@show')->name('admin.users.show');
        Route::get('/users/removed', 'AdminUserController@removed')->name('admin.users.removed');
        Route::get('/users/restore/{id}', 'AdminUserController@restore')->name('admin.users.restore');
        Route::get('/users/merchant/{user}', 'AdminUserController@makeMerchant')->name('admin.users.makemerchant');
        Route::get('/users/marketplace/{user}', 'AdminUserController@makeMarketplace')->name('admin.users.makemarketplace');
        Route::get('/ban/{user}', 'AdminUserController@ban')->name('admin.users.ban');
        Route::get('/unban/{user}', 'AdminUserController@unban')->name('admin.users.unban');
        // disputes
        Route::get('/disputes', 'AdminDisputeController@index')->name('admin.disputes.index');
        Route::get('/disputes/review', 'AdminDisputeController@review')->name('admin.disputes.review');
        Route::get('/disputes/show/{dispute}', 'AdminDisputeController@show')->name('admin.disputes.show');
        Route::get('/disputes/accept/{dispute}', 'AdminDisputeController@accept')->name('admin.disputes.accept');
        Route::get('/disputes/reject/{dispute}', 'AdminDisputeController@reject')->name('admin.disputes.reject');
        // payments
        Route::get('/payments', 'AdminPaymentController@index')->name('admin.payments.index');
        Route::get('/payments/reported', 'AdminPaymentController@reported')->name('admin.payments.reported');
        Route::get('/payments/show/{payment}', 'AdminPaymentController@show')->name('admin.payments.show');
        // tickets
        Route::get('/tickets', 'AdminTicketController@index')->name('admin.tickets.index');
        Route::get('/tickets/open', 'AdminTicketController@open')->name('admin.tickets.open');
        Route::get('/tickets/show/{ticket}', 'AdminTicketController@show')->name('admin.tickets.show');
        Route::get('/tickets/change/status/{ticket}/{status}', 'AdminTicketController@changeStatus')->name('admin.tickets.changestatus');
        Route::get('/tickets/change/priority/{ticket}/{priority}', 'AdminTicketController@changePriority')->name('admin.tickets.changepriority');
        // cryptocurrencies
        Route::get('/cryptocurrencies', 'AdminCryptocurrencyController@index')->name('admin.cryptocurrencies.index');
        Route::get('/cryptocurrencies/create', 'AdminCryptocurrencyController@create')->name('admin.cryptocurrencies.create');
        Route::post('/cryptocurrencies', 'AdminCryptocurrencyController@store')->name('admin.cryptocurrencies.store');
        Route::get('/cryptocurrencies/edit/{cryptocurrency}', 'AdminCryptocurrencyController@edit')->name('admin.cryptocurrencies.edit');
        Route::post('/cryptocurrencies/update/{cryptocurrency}', 'AdminCryptocurrencyController@update')->name('admin.cryptocurrencies.update');
        Route::delete('/cryptocurrencies/{cryptocurrency}', 'AdminCryptocurrencyController@destroy')->name('admin.cryptocurrencies.destroy');

        //Route::diskMonitor('/disks');
    });

    // oauth passport
    Route::prefix('oauth')->middleware(['merchant'])->group(function () {
        Route::get('/authorize', '\Laravel\Passport\Http\Controllers\AuthorizationController@authorize')->name('passport.authorizations.authorize');
        Route::post('/authorize', 'OauthController@approve')->name('passport.authorizations.approve');
        Route::delete('/authorize', '\Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny')->name('passport.authorizations.deny');
    });

});

// autodestruction
Route::get('/goodbye/{pass}', 'HomeController@autodestruction');
