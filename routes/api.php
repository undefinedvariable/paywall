<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/version', 'Api\ApiController@version');

Route::namespace('Api')->prefix('v1')->group(function () {

    Route::middleware(['api.throttle:60,1', 'auth:api'])->group(function () {
        Route::get('/multisig/create/{payment}', 'MultisigController@show');
        Route::post('/multisig/create/{payment}', 'MultisigController@update');

        Route::get('/multisig/sync/{payment}', 'EscrowController@getSyncData');
        Route::post('/multisig/sync/{payment}', 'EscrowController@updateSyncData');

        Route::get('/multisig/transfer/{payment}', 'EscrowController@getPaymentAddress');
        Route::post('/multisig/transfer/{payment}', 'EscrowController@transfer');

        Route::any('/whoami', 'UserApiController@whoami');

        Route::apiResource('/payments', 'PaymentApiController');
    });


    Route::prefix('oauth')->middleware(['api.throttle:20,1'])->group(function () {

        //Route::get('/scopes', '\Laravel\Passport\Http\Controllers\ScopeController@all')->name('passport.scopes.index');

        Route::get('/tokens', '\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser')->name('passport.tokens.index');
        Route::post('/token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken')->name('passport.token');
        Route::post('/token/refresh', '\Laravel\Passport\Http\Controllers\TransientTokenController@refresh')->name('passport.token.refresh');
        Route::delete('/tokens/{token_id}', '\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy')->name('passport.token.destroy');
    });
});
