<?php

use App\Events\MultisigDataUpdated;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('users.{connectedUser}', function ($user, User $connectedUser) {
    if ($user->id != $connectedUser->id) {
        return false;
    }

    foreach ($user->paymentRequests()->broadcastable()->get() as $payment) {
        \Log::error('payment id:'.$payment->id);
        //broadcast(new MultisigDataUpdated($payment->multisigWallet, $user));
    }

    if ($user->isMerchant()) {
        foreach ($user->merchant->paymentRequests()->broadcastable()->get() as $payment) {
            \Log::error('payment id:'.$payment->id);
            //broadcast(new MultisigDataUpdated($payment->multisigWallet, $user));
        }
    }

    return true;
});

Broadcast::channel('test', function ($user) {
    return $user;
});
